import subprocess
from subprocess import Popen, PIPE
import time
import os
from six.moves import input
from zmq_client import Client
import re
import datetime as dt
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
import threading
parser = configparser.ConfigParser()
parser.read("DAQConfig.ini")
location=parser['DCSServer']['location']
sudoPassword=parser['DCSServer']['password']

master=Client(location,"5550")
daq_server=Client("127.0.0.1","5555")

def isPipeOpen():
    processes=subprocess.check_output(('ps','-a'))
    return 'hsioPipe' in processes

class OpenHsio:
    def __enter__(self):
        OpenPipe()
    def __exit__(self,type, value, traceback):
        if not isPipeOpen():
            return
        ClosePipe()

def OpenPipe():
    #If i upload this to git without removing the password please find me and punch me in the face
    command='./run_pipe.sh'
    os.system('echo %s|sudo -S %s' % (sudoPassword, command)) 
def ClosePipe():
    ps = subprocess.Popen(('ps', '-a'), stdout=subprocess.PIPE)
    job = subprocess.check_output(('grep', 'hsioPipe'), stdin=ps.stdout)
    pid=job.split()[0]
    command='kill -9 '+str(pid)
    os.system('echo %s|sudo -S %s' % (sudoPassword, command))
def KillRoot():
    ps = subprocess.Popen(('ps', '-a'), stdout=subprocess.PIPE)
    job = subprocess.check_output(('grep', 'root.exe'), stdin=ps.stdout)
    pid=job.split()[0]
    command='kill -9 '+str(pid)
    os.system('echo %s|sudo -S %s' % (sudoPassword, command))


def hists():
    p = Popen(['root',"-l","summary.root"],stdin=PIPE)
    p.stdin.write('A6Q2XKH_089->Draw()\n'.encode())
    p.stdin.write('c1->Print("FromPython.pdf")\n'.encode())


def batchMode():
    #subprocess.call(["root", "-l","-q", "-b"," test.C"])
    os.system("root -b -l -q  test.C ")

def strobe_delay():
    p = Popen(['root',"-l","Stavelet.cpp"],stdin=PIPE)
    p.stdin.write("gROOT->ProcessLine(\".L ABC130StrobeDelay.cpp\")\n".encode())
    p.stdin.write("gROOT->ProcessLine(\".L ABC130StrobeDelayPlot.cpp\")\n".encode())
    p.stdin.write("ABC130StrobeDelay(1, 0.57)\n".encode())
    #p.communicate("\n".encode())

def RunTests(list_of_tests,lock):
    with open('log.txt','a') as log:
        log.write("tests done ")
        log.write(str(dt.datetime.now()))
        log.write(":")
        log.write(",".join([str(b) for b in list_of_tests]))
        log.write("\n")

    test_run=[]
    if not (list_of_tests[2] and list_of_tests[3]):
        os.system("source exchange_trims.sh")
    
    with lock as l,OpenHsio() as hs:
        p = Popen(['root',"-l","Stavelet.cpp"],stdin=PIPE)
        p.stdin.write("gROOT->ProcessLine(\".L ABC130StrobeDelay.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L ABC130StrobeDelayPlot.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L ABC130ThreePointGain.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L ABC130NoiseTrimPlot.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L pedestal.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L ABC130TrimRangePlot.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L ABC130TrimRange.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L abc130_mux.cpp\");\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L abc130_test.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L  ABC130ResponseCurve.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L ABC130No.cpp\")\n".encode())
        p.stdin.write("gROOT->ProcessLine(\".L ABC130NoPlot.cpp\")\n".encode())

        if(list_of_tests[0]):
            #p.stdin.write("ABC130StrobeDelay(1, 0.57)\n".encode())
            p.stdin.write("ABC130StrobeDelay(1,0.57,256,256,0);\n".encode())


        if(list_of_tests[1]):
            p.stdin.write("e->CacheMasks(0);\n".encode())
            #p.stdin.write("ABC130ThreePointGain(1.0);\n".encode())
            #second to last arguement is number of triggers
            p.stdin.write("ABC130ThreePointGain(0.0,1.0,1.0,256,512,0);\n".encode())
            p.stdin.write("e->CacheMasks(1);\n".encode())

        if(list_of_tests[2] ):
            p.stdin.write("e->CacheMasks(0)\n".encode())
            p.stdin.write("pedestalScan(6,15,100);".encode())

            p.stdin.write("ABC130NoiseTrimPlot(e->runnum, e->scannum, 6, 15);".encode())
            p.stdin.write("e->CacheMasks(1)\n".encode())

            p.stdin.write("ABC130TrimRange();".encode())

        if(list_of_tests[3]):
            p.stdin.write("e->CacheMasks(0);\n".encode())
            #p.stdin.write("ABC130ThreePointGain(1.0);\n".encode())
            p.stdin.write("ABC130ThreePointGain(0.0,1.0,1.0,256,32,0);\n".encode())
            p.stdin.write("e->CacheMasks(1);\n".encode())

        if(list_of_tests[4]):
            p.stdin.write("e->CacheMasks(0);\n".encode())
            p.stdin.write("ABC130ResponseCurve();\n".encode())
            p.stdin.write("e->CacheMasks(1);\n".encode())

        if(list_of_tests[5]):
            p.stdin.write("e->CacheMasks(0);\n".encode())
            p.stdin.write("ABC130ThreePointGain(0,1,1.0,43,512,0);\n".encode())
            p.stdin.write("e->CacheMasks(1);\n".encode())

        if(list_of_tests[6]):
            p.stdin.write("e->CacheMasks(0);\n".encode())
            p.stdin.write("ABC130No(0,0,64000);\n".encode())
            p.stdin.write("e->CacheMasks(1);\n".encode())


        p.communicate(".q".encode())
            #os.system(r"rm userfile.txt")
    if(list_of_tests[2] and list_of_tests[3]) :
        path=os.getcwd()
        os.system("source "+path+"/move_trims.sh")
    master.SendServerMessage("Finished Running tests")
    print("finished tests")
    #daq_server.SendServerMessage("start_sensors")

def StartAmac():
    with OpenHsio():
        p = Popen(['root',"-l","Stavelet.cpp"],stdin=PIPE)
        # p.stdin.write("e->AmacWriteConfig(-1)\n".encode())
        # p.stdin.write("e->AmacReadAnalogue(-1)\n".encode())
        # p.stdin.write("e->LVOn()\n".encode())
        # p.stdin.write("e->HVOn()\n".encode())
        p.stdin.write(".L CaptureWhateverABC130.cpp\n".encode())
        p.stdin.write("CaptureABC130_HCC_Pattern()\n".encode())
        p.stdin.write("CaptureABC130Chips(2048+50)\n".encode())
        p.communicate(".q".encode())
def StopAmac():
    with OpenHsio():
        p = Popen(['root',"-l"],stdin=PIPE)
        p.stdin.write(".x Stavelet.cpp\n".encode())
        p.stdin.write("e->AmacWriteConfig(-1)\n".encode())
        p.stdin.write("e->AmacReadAnalogue(-1)\n".encode())
        p.stdin.write("e->HVOff()\n".encode())
        p.stdin.write("e->LVOff()\n".encode())
        p.communicate(".q".encode())

def read_hybrid_ntc():
    with OpenHsio():
        p = Popen(['root',"-l","Stavelet.cpp"],stdin=PIPE,stdout=PIPE)
        #flags = fcntl(p.stdout, F_GETFL) # get current p.stdout flags
        #fcntl(p.stdout, F_SETFL, flags | O_NONBLOCK)
        #p.stdin.write(".x Stavelet.cpp\n".encode())
        data=p.communicate("burstDisp->HandleMenu(115)\n".encode())
        its_data=data[0].split('\n')
        ntc_data=[]
        for line in its_data:
            if('tntc' in line.lower()):
                if 'AM5' in line:
                    matches=re.findall(r"-*\d+\.*\d*",line)
                    ntc_data.append(matches[-1])
    return ntc_data
                
def read_pb_ntc():
    with OpenHsio():
        p = Popen(['root',"-l","Stavelet.cpp"],stdin=PIPE,stdout=PIPE)
        data=p.communicate("e->AmacReadAnalogue(-1)\n".encode())
        amac_data=data[0].split('\n')
        for i in amac_data:
            if('ntc' in i.lower()):
                matches=re.findall(r"-*\d+\.*\d*",i)
                return matches[-1]


def start_itsdaq_session():
    #OpenPipe()
    working_dir=os.getcwd()
    os.system("source "+str(working_dir)+"/RUNITSDAQ.sh")
    
if __name__=="__main__":
    #StartAmac()
    #print(read_hybrid_ntc())
    #read_pb_ntc()
    lock=threading.Lock()
    tests=[True,True,False,False,False,False,True]
    RunTests(tests,lock)
    #OpenPipe()
    #ClosePipe()
    #strobe_delay()
    #start_itsdaq_session()
    
    pass
