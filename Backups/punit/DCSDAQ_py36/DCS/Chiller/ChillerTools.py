import math
def PadBinary(binary):
    size=len(binary)
    while(size <20):
        binary="0"+binary
        size+=1
    return binary

def PadResults(binary):
    size=len(binary)
    while(size <24):
        binary="0"+binary
        size+=1
    return binary
#choose set point 1, 2
def SetPoint(temperature, point):
    command="*W0"+str(point) #for now, only set point 1

    SetPoint1="" #binary to convert to hex
    if(temperature<0):
        Sign="1"
    else:
        Sign="0"

    temperature=abs(temperature)
    if(temperature/1000 >= 1):
        digits=4
        pass
    elif(temperature/100 >= 1):
        digits=3
        temperature=temperature*10
        temperature=math.floor(temperature)
    elif(temperature/10 >=1):
        digits=2
        temperature=temperature*100
        temperature=math.floor(temperature)
    elif(temperature>=1):
        digits=1
        temperature=temperature*1000
        temperature=math.floor(temperature)
    else: #think about this case a little, here the binary is less <1000
        digits=1  
        temperature=temperature*1000
        temperature=math.floor(temperature)

    switch={1:"100",
            2:"011",
            3:"010",
            4:"001"}
    temp=int(temperature)
    SetPoint1=str(bin(temp))[2:]
    SetPoint1=PadBinary(SetPoint1)
    SetPoint1=Sign+switch[digits]+SetPoint1
    SetPoint1=hex(int(SetPoint1, 2))
    SetPoint1=SetPoint1[2:]
    command+=SetPoint1.upper()
    command+="\r"
    return command

def GetPoint(hexvalue):

    divider={"100" :1000,
             "011" :100,
             "010" :10,
             "001" :1}
    
    binary=bin(int(hexvalue,16))
    binary=binary[2:]
    binary=PadResults(binary)
    Sign=binary[0]
    decimal=binary[1:4]
    temperature=binary[4:]
    temperature=int(temperature,2)
    temperature=temperature/divider[decimal]
    return temperature
