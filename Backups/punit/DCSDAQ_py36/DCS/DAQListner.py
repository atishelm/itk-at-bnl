# -*- python-indent-offset: 4; -*-
import threading
import re
import zmq_client as zq
import zmq
import sys

import time

last_hearbeat=time.time()

        
class ServerThread(threading.Thread):   
    def __init__(self,threadID):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.lock = threading.Lock()
        self.StartTest=False
        
    def run(self):
        print("Starting " + str(self.threadID))
        self.server()

    def server(self):
        port = "5550"
        context = zmq.Context()
        socket=context.socket(zmq.REP)
        socket.bind("tcp://*:%s" %port)
        last_heartbeat=time.time()
        while True:
            message = socket.recv()
            message=str(message)
            print("recieved message: ",message)
            if "break" in message.lower():
                socket.send("Goodbye!".encode())
                print("from listner: goodbye")
                break
            elif "finished" in message.lower():
                print("changeing temperature")
                ColdJig.Thermal_Cycle("change_temp")
                last_heartbeat=time.time()
                socket.send("We are finishing the test".encode())
            elif "daqheartbeat" in message.lower():
                print("received heartbeat")
                last_heartbeat=time.time()
                socket.send("beating heart")
            else:
                socket.send("Good to go".encode())
                

def start():
    listner=ServerThread("Sensor Listner")
    listner.start()
                    

