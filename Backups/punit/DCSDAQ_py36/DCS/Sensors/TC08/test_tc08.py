import tc08

# Create 2 TC08 instances
unit1 = tc08.TC08("A0043/462")


# Open each unit
unit1.open_unit()


# Set mains frequency to reject
unit1.mains_frequency = 50


# Just print out last error
unit1.get_last_error()


# Set units
print( "*****************")
print( "* SETTING UNITS *")
unit1.units = "centigrade"


print( "* RECALL UNITS *")
print( unit1.units)

print( "*****************")

print( "MODEL NUMBER TEST")
print( unit1.model_number)
#unit1.model_number = "FISH!"
#print unit1.model_number



# Print out unit properties
print( unit1.serial_number)
print( unit1.handle)
print( unit1.driver_version)
print( unit1.kernel_driver_version)
print( unit1.hardware_version)
print( unit1.model_number)
print( unit1.calibration_date)
print( unit1.mains_frequency)

# Set channels to unit1
unit1.set_channel(1,"K")
unit1.set_channel(2,"K")
unit1.set_channel(3,"k")
unit1.set_channel(4,"K")
unit1.set_channel(5,"K")
unit1.set_channel(6,"K")
unit1.set_channel(7,"K")
unit1.set_channel(8,"T")

print( unit1.minimum_interval)


# Read out all channels
unit1.read_channels()


# Print out temperatures of all channels
# -- Unit 1 temperatures
print( unit1.temp_1, unit1.temp_2, unit1.temp_3,unit1.temp_4,)
print( unit1.temp_5,unit1.temp_6,unit1.temp_7,unit1.temp_8,unit1.temp_cj)
# -- Unit 1 overflows
print( unit1.ovf_1,unit1.ovf_2, unit1.ovf_3, unit1.ovf_4,)
print( unit1.ovf_5,unit1.ovf_6,unit1.ovf_7, unit1.ovf_8,unit1.ovf_cj)



# Explictly close unit1
unit1.close_unit()

# Close all units
tc08.TC08.close_units()

