#vacuum pressure sensor I2C 

import smbus
from Sensors.Sensor import Sensor
class hscsip(Sensor):
    def __init__(self,bus=1,address=0x28):
        print(bus)
        self.bus=smbus.SMBus(int(bus))
        self.address=address

    def get_data(self):
        lvP = self.bus.read_i2c_block_data(self.address, 0x0, 4)
        datavP = float(((lvP[0] & 63) << 8) | lvP[1]) * 15.0 / 16384.0
        return {"vacuum":datavP}

if __name__=="__main__":
    sensor=hscsip(bus=0)
    print(sensor.get_data())
                                                            
