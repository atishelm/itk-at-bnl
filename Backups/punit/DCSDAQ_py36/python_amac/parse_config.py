import re


def amac_config(configuration_file):
    port=""
    address=""
    register_data=[]
    with open(configuration_file) as config:
        for line in config:
            if('amac1' in line.lower()[:7]):
                amac_data=re.findall(r"\d",line)
                port=amac_data[1]
                address=amac_data[2]
            
            hexdata=re.findall(r"0x[a-fA-F0-9]+",line)
            for number in hexdata:
                register_data.append(number)
            if(len(register_data)>=25):
                break

    
    return (port,address,register_data)

#turn 32 bit numbers to  map of register:data
def process_data(register_data):
    register=40
    register_data_map={}
    for bit32 in register_data:
        hex_number=int(bit32,16)
        for i in range(4):
            register_data_map[register]=hex_number & 255
            hex_number=hex_number>>8
            register+=1
        
    return register_data_map


if __name__=="__main__":
    configuration="example_config.det"
    port,address,register_data=amac_config(configuration)
    print(process_data(register_data))
    
