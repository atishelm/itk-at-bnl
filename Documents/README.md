# Documents

The purpose of this directory is to keep track of useful documents for reference of different ITk strips components, procedures, etc. Some links to documents are also included below. 

- [QC: Thermal/Electrical QC Proc. - Modules](https://edms.cern.ch/ui/file/2228451/3.7/QC_Thermal_Electrical_QC_Procedures_-_Modules.pdf)
