import pyaudio
import wave
import sys
import time 
import sys

wavFile = sys.argv[1]
startTime = int(sys.argv[2])

class AudioFile:
    chunk = 1024

    def __init__(self, file):
        """ Init audio stream """ 
        self.wf = wave.open(file, 'rb')
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(
            format = self.p.get_format_from_width(self.wf.getsampwidth()),
            channels = self.wf.getnchannels(),
            rate = self.wf.getframerate(),
            output = True
        )

    def play(self):
        """ Play entire file """
        data = self.wf.readframes(self.chunk)
        while data != b'':
            self.stream.write(data)
            data = self.wf.readframes(self.chunk)

    def close(self):
        """ Graceful shutdown """ 
        self.stream.close()
        self.p.terminate()

# Usage example for pyaudio
a = AudioFile(wavFile)
#startTime = 1702680075
seconds = time.time()
print("Seconds since epoch =", seconds)
print("Waiting for",startTime)
while(seconds < startTime):
  seconds = time.time()
a.play()
a.close()
