import mido 
import string
import numpy as np
import time
import sys
import piplates.DAQCplate as DAQC

SECONDS_PER_SLICE = 73.21/32766.
print("SECONDS_PER_SLICE:",SECONDS_PER_SLICE)

#SECONDS_PER_SLICE = 0.00174 * float(3.)
SECONDS_PER_SLICE = 0.0022 * float(3.)

midFile = sys.argv[1]
startTime = int(sys.argv[2])

mid = mido.MidiFile(midFile)

def switch_note(last_state, note, velocity, on_=True):
    # piano has 88 notes, corresponding to note id 21 to 108, any note out of this range will be ignored
    result = [0] * 88 if last_state is None else last_state.copy()
    if 21 <= note <= 108:
        result[note-21] = velocity if on_ else 0
    return result

def msg2dict(msg):
    result = dict()
    if 'note_on' in msg:
        on_ = True
    elif 'note_off' in msg:
        on_ = False
    else:
        on_ = None
    result['time'] = int(msg[msg.rfind('time'):].split(' ')[0].split('=')[1].translate(
        str.maketrans({a: None for a in string.punctuation})))

    if on_ is not None:
        for k in ['note', 'velocity']:
            result[k] = int(msg[msg.rfind(k):].split(' ')[0].split('=')[1].translate(
                str.maketrans({a: None for a in string.punctuation})))
    return [result, on_]

def get_new_state(new_msg, last_state):
    new_msg, on_ = msg2dict(str(new_msg))
    new_state = switch_note(last_state, note=new_msg['note'], velocity=new_msg['velocity'], on_=on_) if on_ is not None else last_state
    return [new_state, new_msg['time']]
def track2seq(track):
    # piano has 88 notes, corresponding to note id 21 to 108, any note out of the id range will be ignored
    result = []
    last_state, last_time = get_new_state(str(track[0]), [0]*88)
    for i in range(1, len(track)):
        new_state, new_time = get_new_state(track[i], last_state)
        if new_time > 0:
            result += [last_state]*new_time
        last_state, last_time = new_state, new_time
    return result

def mid2arry(mid, min_msg_pct=0.1):
    tracks_len = [len(tr) for tr in mid.tracks]
    min_n_msg = max(tracks_len) * min_msg_pct
    # convert each track to nested list
    all_arys = []
    for i in range(len(mid.tracks)):
        if len(mid.tracks[i]) > min_n_msg:
            ary_i = track2seq(mid.tracks[i])
            all_arys.append(ary_i)
    # make all nested list the same length
    max_len = max([len(ary) for ary in all_arys])
    for i in range(len(all_arys)):
        if len(all_arys[i]) < max_len:
            all_arys[i] += [[0] * 88] * (max_len - len(all_arys[i]))
    all_arys = np.array(all_arys)
    all_arys = all_arys.max(axis=0)
    # trim: remove consecutive 0s in the beginning and at the end
    sums = all_arys.sum(axis=1)
    ends = np.where(sums > 0)[0]
    return all_arys[min(ends): max(ends)]

result_array = mid2arry(mid)

x = range(result_array.shape[0])
y = np.multiply(np.where(result_array>0, 1, 0), range(1, 89))
#roughly 32766 slices for 73.21 seconds

#import matplotlib.pyplot as plt
#plt.plot(range(result_array.shape[0]), np.multiply(np.where(result_array>0, 1, 0), range(1, 89)), marker='.', markersize=1, linestyle='')
#plt.plot(x, y, marker='.', markersize=1, linestyle='')
#plt.title("Jingle bells")
#plt.show()

color_dict = {
  "42": [0],
  "44": [0, 1],
  "46": [1],
  "47": [1],
  "49": [1, 2],
  "51": [2],
  "52" : [2],
  "54" : [2],
}

color_dict_rand = {

  0 : [],
  1 : [0],
  2 : [0, 1],
  3 : [0, 2],
  4 : [1],
  5 : [1, 2],
  6 : [0, 2],
  7 : [2],
  8 : [0, 1, 2]

}

seconds = time.time()
print("Seconds since epoch =", seconds)
print("Waiting for",startTime)

while(seconds < startTime):
  seconds = time.time()

current_vals = [-1]
#current_vals = np.delete(y[0], np.where(y[0] == 0)) # remove zeroes
#print(current_vals)
for i in range(0,len(x)):
  t1 = time.time()
  if(i%3!=0): continue
  values = np.delete(y[i], np.where(y[i] == 0))
  if(not np.array_equal(values, current_vals)):
    current_vals = values
    print("Slice number, values: %s, %s"%(i, current_vals))
  
    DAQC.clrDOUTbit(0,0)
    DAQC.clrDOUTbit(0,1)
    DAQC.clrDOUTbit(0,2)
 
    # random
    for pin in color_dict_rand[int(np.random.rand(1)*9)]:
      DAQC.setDOUTbit(0,pin)

    # for scripted LED turn ons
    #for key in color_dict:
    #  if(int(key) in current_vals):
    #    for pin in color_dict[key]:
    #      DAQC.setDOUTbit(0,pin)

  t2 = time.time()
  diff = float(t2) - float(t1)  
  time.sleep(SECONDS_PER_SLICE - diff)
  #if(i>20000): break
