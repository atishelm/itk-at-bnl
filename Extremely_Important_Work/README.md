# Holiday at BNL

Got the magical functions from https://medium.com/analytics-vidhya/convert-midi-file-to-numpy-array-in-python-7d00531890c to produce the base of `PlayColors.py`.

https://www.epochconverter.com/

```
python3 PlayAudio.py wav/jing.wav 1702773440 # start time in seconds since epoch
python3 PlayColors.py mid/jingle-bells-keyboard.mid 1702773440
```
