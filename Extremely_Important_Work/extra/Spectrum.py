import scipy.io.wavfile as wavfile
import numpy as np
import pylab as pl
import time

import matplotlib.pyplot as plt
import numpy as np

import matplotlib.animation as animation

#FILE = "Jingle-Bells.wav"
FILE = "jingle.wav"
rate, data = wavfile.read(FILE)
t = np.arange(len(data[:,0]))*1.0/rate
#pl.plot(t, data[:,0])
#pl.show()

N_SEC = max(t)
N_DATA_ENTRIES = len(data)
N_SLICES = 2

fig, ax = plt.subplots()

p = 20*np.log10(np.abs(np.fft.rfft(data[:2048, 0])))
f = np.linspace(0, rate/2.0, len(p))
#pl.plot(f, p)
#pl.xlabel("Frequency(Hz)")
#pl.ylabel("Power(dB)")
#pl.show()

plot = ax.plot(f, p)[0]

def update(frame, data, entries_per_slice):
  data_slice_upper = int((frame + 1) * entries_per_slice)
  data_slice_lower = int((frame) * entries_per_slice)
  print("data_slice_upper:",data_slice_upper)
  print("data_slice_lower:",data_slice_lower)
  #p = 20*np.log10(np.abs(np.fft.rfft(data[:5000, 0])))
  p = 20*np.log10(np.abs(np.fft.rfft(data[:data_slice_upper, data_slice_lower])))
  f = np.linspace(0, rate/2.0, len(p))
  plot.set_xdata(f)
  plot.set_ydata(p)
  return (plot)

entries_per_slice = N_DATA_ENTRIES / N_SLICES
#print("ENTRIES_PER_SLICE:",ENTRIES_PER_SLICE)

print("N_entries:",N_DATA_ENTRIES)

ani = animation.FuncAnimation(fig=fig, func=update, fargs=(data,entries_per_slice,), frames=N_SLICES, interval=30)
plt.show()

#p = 20*np.log10(np.abs(np.fft.rfft(data[:2048, 0])))
#f = np.linspace(0, rate/2.0, len(p))
#pl.plot(f, p)
#pl.xlabel("Frequency(Hz)")
#pl.ylabel("Power(dB)")
#pl.show()
