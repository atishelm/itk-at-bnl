import mido 
import string
import numpy as np
import time
import sys

#print(sys.argv[0])
#print(sys.argv[1])

startTime = int(sys.argv[1])

mid = mido.MidiFile('jingle-bells-keyboard.mid')

def switch_note(last_state, note, velocity, on_=True):
    # piano has 88 notes, corresponding to note id 21 to 108, any note out of this range will be ignored
    result = [0] * 88 if last_state is None else last_state.copy()
    if 21 <= note <= 108:
        result[note-21] = velocity if on_ else 0
    return result

def msg2dict(msg):
    result = dict()
    if 'note_on' in msg:
        on_ = True
    elif 'note_off' in msg:
        on_ = False
    else:
        on_ = None
    result['time'] = int(msg[msg.rfind('time'):].split(' ')[0].split('=')[1].translate(
        str.maketrans({a: None for a in string.punctuation})))

    if on_ is not None:
        for k in ['note', 'velocity']:
            result[k] = int(msg[msg.rfind(k):].split(' ')[0].split('=')[1].translate(
                str.maketrans({a: None for a in string.punctuation})))
    return [result, on_]

def get_new_state(new_msg, last_state):
    new_msg, on_ = msg2dict(str(new_msg))
    new_state = switch_note(last_state, note=new_msg['note'], velocity=new_msg['velocity'], on_=on_) if on_ is not None else last_state
    return [new_state, new_msg['time']]
def track2seq(track):
    # piano has 88 notes, corresponding to note id 21 to 108, any note out of the id range will be ignored
    result = []
    last_state, last_time = get_new_state(str(track[0]), [0]*88)
    for i in range(1, len(track)):
        new_state, new_time = get_new_state(track[i], last_state)
        if new_time > 0:
            result += [last_state]*new_time
        last_state, last_time = new_state, new_time
    return result

def mid2arry(mid, min_msg_pct=0.1):
    tracks_len = [len(tr) for tr in mid.tracks]
    min_n_msg = max(tracks_len) * min_msg_pct
    # convert each track to nested list
    all_arys = []
    for i in range(len(mid.tracks)):
        if len(mid.tracks[i]) > min_n_msg:
            ary_i = track2seq(mid.tracks[i])
            all_arys.append(ary_i)
    # make all nested list the same length
    max_len = max([len(ary) for ary in all_arys])
    for i in range(len(all_arys)):
        if len(all_arys[i]) < max_len:
            all_arys[i] += [[0] * 88] * (max_len - len(all_arys[i]))
    all_arys = np.array(all_arys)
    all_arys = all_arys.max(axis=0)
    # trim: remove consecutive 0s in the beginning and at the end
    sums = all_arys.sum(axis=1)
    ends = np.where(sums > 0)[0]
    return all_arys[min(ends): max(ends)]

result_array = mid2arry(mid)

#print("result_array.shape[0]:",result_array.shape[0])
#print("length:",len(result_array.shape[0]))

x = range(result_array.shape[0])
y = np.multiply(np.where(result_array>0, 1, 0), range(1, 89))
#roughly 32766 slices for 73.21 seconds

SECONDS_PER_SLICE = 73.21/32766.
print("SECONDS_PER_SLICE:",SECONDS_PER_SLICE)

SECONDS_PER_SLICE = 0.0018

#import matplotlib.pyplot as plt
#plt.plot(range(result_array.shape[0]), np.multiply(np.where(result_array>0, 1, 0), range(1, 89)), marker='.', markersize=1, linestyle='')

#for j in range(0,30):
  #y = np.delete(y, j)
  #y = y.remove(j)
#plt.plot(x, y, marker='.', markersize=1, linestyle='')
#plt.title("Jingle bells")
#plt.show()

seconds = time.time()
print("Seconds since epoch =", seconds)
print("Waiting for",startTime)
while(seconds < startTime):
  seconds = time.time()

current_val = -1
for i in range(0,len(x)):
  #print("Highest val:",max(y[i]))
  high_val = max(y[i])
  if(current_val != high_val): 
    current_val = high_val
    print("value:",current_val)
  #print("x, y: %s, %s"%(x[i], y[i]))
  time.sleep(SECONDS_PER_SLICE)
  if(i>10000): break
