# One time SSH login

Everyone loves typing their passwords over and over - it is one of the great joys of life. However, in case you prefer to open multiple terminals without having to retype your password each time you ssh, follow the instructions below.

## lxplus

The following example is used for opening multiple terminals through which you ssh into `lxplus` while only having to enter your password the first time, however this can be used for any ssh connection. 

First, include the following in your `~/.ssh/config`:

```
# Setup the connection re-use
ControlMaster auto
# Path to Control Socket
#       %l: local host name
#       %h: target host name
#       %p: port
#       %r: remote login
ControlPath ~/.ssh/master_%l_%h_%p_%r


##################################
# The following lines are included in the sections on lxplus, and other hosts
# You may want to include them more globally but this may affect existing
# settings you may have in your config file
##################################
# Use SSHv2 only
#Protocol 2
# Forward you SSH key agent so that it can be used on further hops
#ForwardAgent yes

# For X11
ForwardX11 yes
#ForwardX11Trusted no
ForwardX11Trusted yes

# In case there is kerberos configured locally
# NOTE: option GSSAPITrustDns may no longer be available in releases of SSH on
#       more recent Mac OSX versions, where you should simply comment it out
#GSSAPITrustDns yes
#GSSAPIAuthentication yes
#GSSAPIDelegateCredentials yes
##################################

Host lxplus.cern.ch aiadm.cern.ch lxtunnel.cern.ch lxtunnel lxplus aiadm
        HostName lxplus.cern.ch
        # Specify the remote username if different from your local one
        User <lxplus-username>
        Compression yes
        # Use SSHv2 only
        Protocol 2
        # Forward you SSH key agent so that it can be used on further hops
        ForwardAgent yes
        # For X11
        ForwardX11 yes
        #ForwardX11Trusted no
        # In case there is kerberos configured locally
        # NOTE: option GSSAPITrustDns may no longer be available in releases of SSH on
        #       more recent Mac OSX versions, where you should simply comment it out
        #GSSAPITrustDns yes
        #GSSAPIAuthentication yes
        #GSSAPIDelegateCredentials yes
```

Then after logging into lxplus with one terminal, a socket file should be created at `~/.ssh/master_%l_%h_%p_%r` as specified in the ssh config file. If you can then open a new terminal and ssh again without having to enter your password, you've done it! 
