"""
1 March 2023
Abraham Tishelman-Charny

The purpose of this python module is to create an interactive session for communicating with devices via serial protocol.

Note - if you don't close your device at the end with ser.close(), you may need to open a new terminal to open and communicate with your device.

https://pyserial.readthedocs.io/en/latest/shortintro.html

Example usage:
python3 -i OpenSerial.py
> ser.write(b"*IDN?\r\n")
> ser.readline()
> ser.readline()
> ser.close()
"""

# imports 
import serial 
import struct

# path to serial device
path = "/dev/serial/by-id/usb-Linux_4.9.220-2.8.7+g57229263ff65_with_2184000.usb_Gadget_Serial_v2.4-if00"

# open the serial device
ser = serial.Serial(path,timeout=1)

# Extra commands that can be run if you uncomment and don't run in interactive mode

"""
# send the identity command (note, you need to check your device's programmer's manual to ensure the command syntax is correct)
ser.write(b"*IDN?\r\n")

# print the output from the command
print(ser.readline())
print(ser.readline())

# other examples and commands
#print(ser.write(b":READ:CHAN:STAT? (@1)\r\n"))
#print(ser.readline())
#status = ser.readline()
#print("status:",status)
#i = struct.unpack('>H', status[:2])[0]
#i = int.from_bytes(status, byteorder='big')
#i &= 0xFFFF
#print("i:",i)
#status_bits = "{0:08b}".format(status)
#print("status in bits:",status_bits)

# close the device when you're done
ser.close()
"""
