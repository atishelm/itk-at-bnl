# Set up persistent device naming 

When connecting a device to a PC or pi, it will be given a default name as seen in the `/dev/` folder, for example: `/dev/ttyUSB0` or `/dev/ttyACM1`. If you unplug and replug in the same device, there is no guarantee that its name in the `/dev/` folder will be the same. This is unideal for configuration files which look for a device name, as a changing device name requies a changing configuration file.

The solution to this is to set a *persistent* device name - every time you plug in a certain device, you will see its same meaningful name every time, e.g. `/dev/MyDevice` such that a configuration file can always look for `/dev/MyDevice`. 

This is done by obtaining unique identifier information from a given device. The full detailed instructions can be found [here](https://rolfblijleven.blogspot.com/2015/02/howto-persistent-device-names-on.html#steps), but the quick steps are shown below:

## Quick steps 

```
dmesg | grep ttyUSB
```

or 

```
ls -lrt /dev/ttyUSB*
```

to see which USB slots most recently had something plugged in. To obtain information unique to the device connected to USB0, one can enter:

```
udevadm info -a -n /dev/ttyUSB0
```

and use that information in the `/etc/udev/rules.d/99-usb-serial.rules` file to create a symlink. 

Load the rule: 

```
sudo udevadm trigger
```

See if your symlink is there:

```
ls -lrt /dev/
```
