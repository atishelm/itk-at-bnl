# Machine setups

The purpose of this directory is to keep track of files used to set up and configure different machines in the BNL clean rooms. Some files are common to all PCs and raspberry pis, and some are specific to certain setups. 

Common files:
* `Common-Bookmarks.html`: A set of common bookmarks to be imported on a given machine, to have a common bookmarks toolbar among all PCs. 
* `bash_aliases`: A set of common bash aliases that may be useful on any machines (to be defined upon sourcing of `~/.bashrc`.

Setup specific files:
* `bnl.ini`: This is the hardware configuration file read by the coldjig software. In the coldjig s/w, it's located in `coldjiglib2/configs/US_Barrel/`, for example: [[ref.]](https://gitlab.cern.ch/ColdJigDCS/coldjiglib2/-/blob/e48abd79faf97aa0217a2b322e9a083ba7f5dbc5/configs/US_Barrel/bnl.ini). Its purpose is to define the hardware used at your setup to be read by the coldjig s/w. 
* `bnl_influx.ini`: This is the influx configuration file read by the coldjig s/w. It's located in `coldjiglib2/configs/US_Barrel/`, for example: [[ref.]](https://gitlab.cern.ch/ColdJigDCS/coldjiglib2/-/blob/e48abd79faf97aa0217a2b322e9a083ba7f5dbc5/configs/US_Barrel/bnl_influx.ini). Its purpose is to define the influx DB parameters in order to upload coldjig info to influx DB.
* `config_BNL.conf`: This is the configuration file used for the coldbox controller GUI. It is located in `coldbox_controller_webgui/configs/`, for example: [[ref.]](https://gitlab.cern.ch/ColdJigDCS/coldbox_controller_webgui/-/blob/c47515aac6286b44f790a9d0f133a10f141226d2/configs/config_BNL.conf). Its purpose is to provide configuration information for the webGUI which monitors and displays the coldjig software outputs.
* `99-usb-serial.rules`: A file to be placed at `/etc/udev/rules.d/` in order to create persistent naming of devices plugged into a pi or PC. 
