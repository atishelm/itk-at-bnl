# Raspiberry Pi setup

Some useful links for setting up the pi:

https://www.raspberrypi.com/documentation/computers/os.html

includes proper `apt` setup:

```
sudo apt update
sudo apt upgrade
```

Allow ssh capabilities so you can ssh into the pi from your PC:

sudo raspi-config
interface option
enable.

if trying to add repo for python3.8

https://raspberrypi.stackexchange.com/questions/116118/updating-python-3-7-to-3-8-on-raspberry-pi
