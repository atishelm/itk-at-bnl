# Reception test coldbox

The purpose of this directory is to keep track of files specific to the reception test coldbox at BNL. This is the coldbox setup used for testing components received at BNL such as modules, and for performing IV curve measurements of silicon sensors. Because the hardware or its unique identifiers may differ from the thermal cycling coldbox in the BNL testing clean room, it may require hardware configuration files unique to itself, which will be stored here.

## Network configuration

Define network interfaces in `/etc/sysconfig/network-scripts/`

examples are in this directory. E.g. `ifcfg-ISEG`.