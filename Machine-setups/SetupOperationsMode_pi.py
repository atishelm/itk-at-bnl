"""
Abraham Tishelman-Charny
24 July 2023

The purpose of this script is to set up git branches properly to work in an operational mode, i.e. one that should work for thermal cycling, rather than a development mode.

Usage: python3 SetupOperationsMode.py
"""

import os
import subprocess

commands = [
    ["cd", "/home/pi/Desktop/coldbox_controller_webgui"],
    ["git", "checkout", "PS_Removal_and_shuntingOption"],
    ["cp", "configs/config_BNL_Box3.conf", "configs/config_BNL.conf"],
    ["cd", "/home/pi/Desktop/coldjiglib2"],
    ["git", "checkout", "Auto_Shunted_Cycling"],
    ["cp", "configs/US_Barrel/bnl_Box3.ini", "configs/US_Barrel/bnl.ini"],
    #["gui"]
]

cwd = "/home/pi/Desktop"
for c in commands:
    command = " ".join(c)
    print("$", command)
    if(c[0] == "cd"):
        cwd=c[1]
    else:
        #subprocess.run(f"bash -i -c '{command}'", shell=True)
        subprocess.run(c, cwd=cwd)
