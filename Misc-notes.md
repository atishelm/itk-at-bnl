## Website links 

In case one wants to make a website using CERN website hosting, for example an ITk strips general documentation website, here are some relevant links for doing that:

- https://webeos.cern.ch/k8s/cluster/projects (for creating a project for EOS to host)
- https://how-to.docs.cern.ch/gitlabpagessite/create_site/ (mkdocs documentation)

Making a website hosted by GitHub example. E.g. this website: https://atishelmanch.github.io/

is created by the files here:

- https://github.com/atishelmanch/atishelmanch.github.io

## Image size reducer 

For the GitLab repo image: https://www.reduceimages.com/
