Checking status of refrigeration...
status: b'OK\rE042=+000128.!\rOK\rF050=+000002.!\r'
Turning off chiller...
pi@raspberrypi:~/Desktop/coldjiglib2/Hardware/Chiller $ python3
Python 3.7.3 (default, Oct 31 2022, 14:04:00) 
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import serial
>>> from serial import *
>>> import time
>>> chiller=serial.Serial("/dev/serial/by-id/usb-FTDI_Chipi-X_FT6EBZGO-if00-port0",9600,SEVENBITS,parity=PARITY_NONE,stopbits=STOPBITS_ONE,timeout=1)
>>> chiller
Serial<id=0xb66455f0, open=True>(port='/dev/serial/by-id/usb-FTDI_Chipi-X_FT6EBZGO-if00-port0', baudrate=9600, bytesize=7, parity='N', stopbits=1, timeout=1, xonxoff=False, rtscts=False, dsrdtr=False)
>>> chiller.write("START\n\r".encode())
7
>>> chiller.write("REFR?\n\r".encode())
7
>>> chiller.readline()
b'OK!\rOK\rF050=+000001.!\r'
>>> chiller.readline()
b''
>>> chiller.write("REFR?\n\r".encode())
7
>>> chiller.readline()
b'OK\rF050=+000002.!\r'
>>> chiller.readline()
b''
>>> chiller.write("REFR?\n\r".encode())
7
>>> chiller.write("REFR?\n\r".encode())
7
>>> chiller.readlines)_
  File "<stdin>", line 1
    chiller.readlines)_
                     ^
SyntaxError: invalid syntax
>>> chiller.readlines()
[b'OK\rF050=+000002.!\rOK\rF050=+000002.!\r']
>>> chiller.readlines()[-1]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
>>> chiller.write("REFR?\n\r".encode())
7
>>> chiller.readlines()[-1]
b'OK\rF050=+000002.!\r'
>>> chiller.write("REFR?\n\r".encode())
7
>>> chiller.readlines()[-1].split("\r")[-2]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: a bytes-like object is required, not 'str'
>>> chiller.write("REFR?\n\r".encode())
7
>>> chiller.readlines()[-1].split("\r")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: a bytes-like object is required, not 'str'
>>> chiller.write("REFR?\n\r".encode())
7
>>> chiller.readlines()[-1]
b'OK\rF050=+000002.!\r'
>>> chiller.write("REFR?\n\r".encode())
7
>>> str(chiller.readlines()[-1])\
... 
KeyboardInterrupt
>>> str(chiller.readlines()[-1])
"b'OK\\rF050=+000002.!\\r'"
>>> chiller.write("REFR?\n\r".encode())
7
>>> str(chiller.readlines()[-1]).split("\r")[-2]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
>>> chiller.write("REFR?\n\r".encode())
7
>>> str(chiller.readlines()[-1]).split("\r")
["b'OK\\rF050=+000002.!\\r'"]
>>> chiller.write("REFR?\n\r".encode())
7
>>> str(chiller.readlines()[-1]).split("\\r")
["b'OK", 'F050=+000002.!', "'"]
>>> chiller.write("REFR?\n\r".encode())
7
>>> str(chiller.readlines()[-1]).split("\\r")[-2]
'F050=+000002.!'
>>> chiller.write("REFR?\n\r".encode())
7
>>> str(chiller.readlines()[-1]).split("\\r")[-2]
'F050=+000002.!'
>>> chiller.write("PUMP?\n\r".encode())
7
>>> str(chiller.readlines()[-1]).split("\\r")[-2]
'F046=+000255.!'
>>> chiller.write("PUMP?\n\r".encode())
7
>>> chiller.readlines()
[b'OK\rF046=+000255.!\r']
>>> chiller.write("ALARMH?\n\r".encode())
9
>>> chiller.readlines()
[b'OK\rF001=+0049.96!\r']
>>> chiller.write("ALARML?\n\r".encode())
9
>>> chiller.readlines()
[b'OK\rF002=-0050.01!\r']
>>> chiller.write("ALMCODE?\n\r".encode())
10
>>> chiller.readlines()
[b'OK\rF076=+000000.!\r']
>>> chiller.write("RR?\n\r".encode())
5
>>> chiller.readlines()
[b'OK\rF054=+0000.00!\r']
>>> chiller.readlines()
[]
>>> chiller.write("RR?\n\r".encode())
5
>>> chiller.readlines()
[b'OK\rF054=+0000.00!\r']
>>> chiller.write("RR?\n\r".encode())
5
>>> data = chiller.readline()
d>>> data.decode()
'OK\rF054=+0000.00!\r'
>>> chiller.write("RR?\n\r".encode())
5
>>> data = chiller.readline()
>>> decode = data.decode()
>>> decode
'OK\rF054=+0000.00!\r'
>>> decode = decode[decode.index("=")+1:data.index("!")]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: argument should be integer or bytes-like object, not 'str'
>>> chiller.write("RR?\n\r".encode())
5
>>> data = chiller.readline()
>>> status = data[data.index("=")+1:data.index("!")]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: argument should be integer or bytes-like object, not 'str'
>>> 
>>> 
>>> 
>>> data
b'OK\rF054=+0000.00!\r'
>>> data =data.decode()
>>> status = data[data.index("=")+1:data.index("!")]
>>> status
'+0000.00'
>>> chiller.write("REFR?\n\r".encode())
7
>>> data = chiller.readline()
>>> data =data.decode()
>>> status = data[data.index("=")+1:data.index("!")]
>>> status
'+000002.'
>>> chiller.write("REFR?\n\r".encode())
7
>>> data = chiller.readline()
>>> data =data.decode()
>>> status = data[data.index("=")+1:data.index("!")]
>>> status
'+000000.'
>>> chiller.write("ALARMH?\n\r".encode())
9
>>> data =data.decode()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'str' object has no attribute 'decode'
>>> data = chiller.readline()
>>> data
b'OK\rF001=+0049.96!\r'
>>> chiller.write("ALARML?\n\r".encode())
9
>>> data = chiller.readline()
d>>> data
b'OK\rF002=-0050.01!\r'
>>> chiller.write("REFR?\n\r".encode())
7
>>> data = chiller.readline()
>>> data
b'OK\rF050=+000002.!\r'
>>> chiller.write("REFR?\n\r".encode())
7
>>> data = chiller.readline()
>>> data
b'OK\rF050=+000000.!\r'
>>> chiller.write("REFRSW\n\r".encode())
8
>>> chiller.write("REFRSW=-1\n\r".encode())
11
>>> chiller.write("REFRHRS?\n\r".encode())
10
>>> data = chiller.readline()
>>> data
b'E022=+000006.!\rOK!\rOK\rF078=+000698.!\r'
>>> chiller.write("REFRSW?\n\r".encode())
9
>>> data = chiller.readline()
>>> data
b'OK\rF051=-000001.!\r'
>>> chiller.write("RERF?\n\r".encode())
7
>>> data = chiller.readline()
>>> data
b'E020=+000003.!\r'
>>> import readline; print('\n'.join([str(readline.get_history_item(i + 1)) for i in range(readline.get_current_history_length())]))
exit
exit()
exit
exit()
import site
site.getsitepackages()
exit()
import site
site.getsitepackages()
exit()
2**8
256*12
exit()
import os
os.system("git checkout Auto_Shunted_Cycling")
exit()
ser.write(b"*IDN?\r\n")
exit()
ser.write(b"*IDN?\r\n")
ser.readline()
exit()
ser.write(b"*IDN?\r\n")
ser.readline()
ser.close()
exit()
ser.write(b"*IDN?\r\n")
ser.close()
exit()
ser.write(b"*IDN?\r\n")
ser.readline()
ser.read_all()
exit()
ser.write(b"*IDN?\r\n")
ser.readline()
ser.close()
exit()
import influxdb_client
exit()
import serial
from serial import *
import time
chiller=serial.Serial("/dev/serial/by-id/usb-FTDI_Chipi-X_FT6EBZGO-if00-port0",9600,SEVENBITS,parity=PARITY_NONE,stopbits=STOPBITS_ONE,timeout=1)
chiller
chiller.write("START\n\r".encode())
chiller.write("REFR?\n\r".encode())
chiller.readline()
chiller.write("REFR?\n\r".encode())
chiller.readline()
chiller.write("REFR?\n\r".encode())
chiller.readlines)_
chiller.readlines()
chiller.readlines()[-1]
chiller.write("REFR?\n\r".encode())
chiller.readlines()[-1]
chiller.write("REFR?\n\r".encode())
chiller.readlines()[-1].split("\r")[-2]
chiller.write("REFR?\n\r".encode())
chiller.readlines()[-1].split("\r")
chiller.write("REFR?\n\r".encode())
chiller.readlines()[-1]
chiller.write("REFR?\n\r".encode())
str(chiller.readlines()[-1])\
str(chiller.readlines()[-1])
chiller.write("REFR?\n\r".encode())
str(chiller.readlines()[-1]).split("\r")[-2]
chiller.write("REFR?\n\r".encode())
str(chiller.readlines()[-1]).split("\r")
chiller.write("REFR?\n\r".encode())
str(chiller.readlines()[-1]).split("\\r")
chiller.write("REFR?\n\r".encode())
str(chiller.readlines()[-1]).split("\\r")[-2]
chiller.write("REFR?\n\r".encode())
str(chiller.readlines()[-1]).split("\\r")[-2]
chiller.write("PUMP?\n\r".encode())
str(chiller.readlines()[-1]).split("\\r")[-2]
chiller.write("PUMP?\n\r".encode())
chiller.readlines()
chiller.write("ALARMH?\n\r".encode())
chiller.readlines()
chiller.write("ALARML?\n\r".encode())
chiller.readlines()
chiller.write("ALMCODE?\n\r".encode())
chiller.readlines()
chiller.write("RR?\n\r".encode())
chiller.readlines()
chiller.write("RR?\n\r".encode())
chiller.readlines()
chiller.write("RR?\n\r".encode())
data = chiller.readline()
data.decode()
chiller.write("RR?\n\r".encode())
data = chiller.readline()
decode = data.decode()
decode

chiller.write("RR?\n\r".encode())
data = chiller.readline()
status = data[data.index("=")+1:data.index("!")]
data
data =data.decode()
status = data[data.index("=")+1:data.index("!")]
status
chiller.write("REFR?\n\r".encode())
data = chiller.readline()
data =data.decode()
status = data[data.index("=")+1:data.index("!")]
status
chiller.write("ALMCODE?\n\r".encode())
data = chiller.readline()
data =data.decode()
status = data[data.index("=")+1:data.index("!")]
status
chiller.write("ALARMH?\n\r".encode())
data =data.decode()
data = chiller.readline()
data
chiller.write("ALARML?\n\r".encode())
data = chiller.readline()
data
chiller.write("REFR?\n\r".encode())
data = chiller.readline()
data
chiller.write("REFR?\n\r".encode())
data = chiller.readline()
data
chiller.write("REFRSW\n\r".encode())
chiller.write("REFRSW=-1\n\r".encode())
chiller.write("REFRHRS?\n\r".encode())
data = chiller.readline()
data
chiller.write("REFRSW?\n\r".encode())
data = chiller.readline()
data
chiller.write("RERF?\n\r".encode())
data = chiller.readline()
data
import readline; print('\n'.join([str(readline.get_history_item(i + 1)) for i in range(readline.get_current_history_length())]))
