# Coldbox timing 

The purpose of this directory is to store files used to characterize the Read/Set timings of the BNL coldbox 1.

Example usage (may not work out of the box):

```
cd itk-at-bnl/Module-testing/ColdboxTiming
python3 CharacterizeTiming.py --dataDirec twoHoursData --PlotTimes
python3 CharacterizeTiming.py --dataDirec twoHoursData --PlotAverages
```
