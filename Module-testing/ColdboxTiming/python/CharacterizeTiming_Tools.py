"""
1 November 2022
Abraham Tishelman-Charny

The purpose of this module is to define parameters for CharacterizeTiming. 
"""

# parameters 
ol = "/eos/user/a/atishelm/www/ITk/Coldjig-Timing/"
hist_color = 'C0'

Value_types = [
    "Read", "Set"
]

HARDWARE = [
    "Chiller",
    "SHT85_left",
    "SHT85_right",
    "NTC0",
    "NTC1",
    "NTC2",
    "NTC3",
    "NTC4",
    "NTC5",
    "HAFFlow_1",
    "INSTEK1",
    "INSTEK2",
    "DewPoint1",
    "DewPoint2",
]