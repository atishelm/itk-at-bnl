"""
Script to automatically take most recent files, sort them by module, and upload them to CERNBOX.

python3 CernboxUploadProduction.py --username eduden --time '17 Nov 2023 15:00'
python3 CernboxUploadProduction.py --username eduden --time '17 Nov 2023 15:00' --end_time '17 Nov 2023 16:00'
"""
import os
from pathlib import Path
import itk_pdb.dbAccess as dbAccess
import argparse

if __name__ == '__main__':
    from __path__ import updatePath
    updatePath()

#username="eduden"
#time="'17 Nov 2023 15:00'" #the earliest time you want to pull tests from

parser = argparse.ArgumentParser()
parser.add_argument("--username", type=str, default="./input", help="lxplus username")
parser.add_argument("--time", type=str, default="", help="earliest time you want to pull files from")
parser.add_argument("--end_time", type=str, default="", help="earliest time you want to pull files from")
options = parser.parse_args()

username = options.username
time = options.time
end_time = "" + options.end_time

pathtoIVs="/home/qcbox2/Desktop/itk-at-bnl/docs/IV_Curves/plots"
filepath="/home/qcbox2/Desktop/itsdaq-sw/DAT"
tempstorage=filepath+"/recentfiles"

#make a temporary directory to put all the files from the time we care about
print("Throwing all of the files for upload into " + filepath + "/recentfiles")
if not os.path.exists(tempstorage):
    os.system("mkdir " + tempstorage)
else:
    os.system("rm " + tempstorage + "/*")

if end_time == "":
    os.system("cp `find " + pathtoIVs + " -type f -newermt '" + time +"'` " + tempstorage)
    os.system("cp `find " + filepath + "/ps" + " -type f -newermt '" + time +"'` " + tempstorage)
    os.system("cp `find " + filepath + "/results" + " -type f -newermt '" + time +"'` " + tempstorage)
else:
    os.system("cp `find " + pathtoIVs + "/ -type f -newermt '" + time +"' ! -newermt '" + end_time + "'` " + tempstorage)
    os.system("cp `find " + filepath + "/ps" + " -type f -newermt '" + time +"' ! -newermt '" + end_time + "'` " + tempstorage)
    os.system("cp `find " + filepath + "/results" + " -type f -newermt '" + time +"' ! -newermt '" + end_time + "'` " + tempstorage)

print("Compiling files")
filelist=[]
pathlist = Path(tempstorage)
for file in pathlist.glob("*"):
    if ("20USBHX2001546" not in str(file)) and ("20USBML1235349" not in str(file)): 
        filelist.append(str(file))

#Find out the serial number for each hybrid tested
MSNs=[]
HSNs=[None]*16 #I want to keep the same index for hybrids and modules. Max 16 modules uploaded at once
for file in filelist:
    if "USBM" in file:
        MSN="20USBM" + file.split("20USBM")[1][0:8]
        if MSN not in MSNs:
            MSNs.append(MSN)
    elif "USBHX" in file:
        HSN="20USBHX" + file.split("20USBHX")[1][0:7]
        if HSN not in HSNs:
            print("Finding parent module for hybrid "+ HSN)
            d = {
                  
                      "component": HSN,
                      "noTests": "false",
                }
            result = dbAccess.doSomething("getComponent",method='GET',data=d)
            for parent in result["parents"]:
                if parent['componentType']['code']=="MODULE":
                    MSN=parent['component']['serialNumber']
                    if MSN not in MSNs:
                        MSNs.append(MSN)
                    print(MSNs)
                    HSNs[MSNs.index(MSN)]=HSN

#Get local ID for each module

LocalIDs=[]
for SN in MSNs:
    print("Finding LocalID for module " + SN)
    d = {
                  
                      "component": SN,
                      "noTests": "false",
                }
    moduleinfo = dbAccess.doSomething("getComponent",method='GET',data=d)
    #should be index 1 for LBNL, 0 for other institutes
    id_index = 0
    if "LBNL" in moduleinfo['institution']['code']:
        id_index = 1
    print(moduleinfo['institution'])
    LocalIDs.append(moduleinfo['properties'][id_index]['value'])

#sort files by module so we can scp them together
directorynames=[]
for i in range(len(MSNs)):
    directorynames.append(LocalIDs[i] + "_" + MSNs[i])
for file in filelist:
    SN="20US" + file.split("20US")[1][0:10]
    if SN in MSNs:
        directory=directorynames[MSNs.index(SN)]
    elif SN in HSNs:
        print(HSNs)
        directory=directorynames[HSNs.index(SN)]
    if not os.path.exists(tempstorage + "/" + directory):
        os.system("mkdir " + tempstorage + "/" + directory)
        os.system("cp index.php " + tempstorage + "/" + directory)
    os.system("mv " + file + " " + tempstorage + "/" + directory)

#create a folder for the module on cernbox if it doesn't exist and copy files over
for i in range(len(MSNs)):
    print("Copying files for module "+ directorynames[i]+"onto CERNBox")
    newdirectorypath="/eos/user/a/atishelm/www/ITk/Production/" + directorynames[i]
    #os.system("ssh " + username + "@lxplus.cern.ch 'mkdir -p " + newdirectorypath +"'")
    os.system("scp " + tempstorage + "/" + directorynames[i] + "/* " + username + "@lxplus.cern.ch:" + newdirectorypath)
