# Module configs from DB 

The scripts and python modules in this directory are used to obtain `.det` hybrid configuration files used by ITSDAQ when module testing. The latest version of the ASIC configuration obtention script should be around [here](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts/-/blob/master/strips/modules/getAsicConfig.py).

Example usage:

* Enter names of modules from which you want to get x hybrid config file, and FUSE ID. Needs to be local IDs as defined in the database, fill these in `Modules.yaml`.
* Run the script: `python3 GetDBInfo.py --ConfigFile Modules.yaml`
