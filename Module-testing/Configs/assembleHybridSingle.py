#!/usr/bin/env python3

### Author: Ian Dyckes
### For assembling ASICs to hybrid based on fuse IDs.
## Normally lives in the hybrids folder of production_database_scripts/strips


from __future__ import print_function #import python3 printer if python2 is used
import argparse, json, os, sys
import numpy as np
import uproot
sys.path.append("../../") # path to dir containing itk_pdb 
import itk_pdb.dbAccess as dbAccess
sys.path.insert(1, '../modules/')
#from getAsicConfig import getHybridAssemblyByLocalName
from getAsicConfig import allChildrenAssembled

def main():


    ### arguements
    parser = argparse.ArgumentParser(description='For assembling chips to a panel of STAR Hybrid Assemblies based on fuse IDs.')
    parser.add_argument('--panel', default=None, help='Serial name of hybrid panel with STAR Hybrid Assemblies already assembled.')
    parser.add_argument('--json', default=None, help='JSON file with fuseIDs of all chips on this panel.  Created by getAsicFuseIDs.cpp in ITSDAQ (does not exist in master yet).')
    parser.add_argument('--root', default=None, help='ROOT file with fuseIDs of all chips on this panel.  Created by any(?) ITSDAQ test.')
    parser.add_argument('--verbose', default=False, action='store_true', help='Increase verbosity on DB interactions.')
    parser.add_argument('--hybridSN', default=None, help='SN of single hybrid you wish to assemble')

    parser.add_argument('--dryRun', default=False, action='store_true', help='Perform a dry run.  Do not update anything in the DB.  Read only.')
    args = parser.parse_args()

    ### DB access.
    if os.getenv("ITK_DB_AUTH"):
        dbAccess.token = os.getenv("ITK_DB_AUTH")
    if args.verbose:
        dbAccess.verbose = True

        
    ### Make sure the fuse IDs are provided either from the JSON file or directly from the ROOT file.
    if args.json:
        with open(args.json,'r') as f:
            print(f"Reading chip fuse IDs from JSON: {args.json}")
            fuseJSON = json.load(f)
    elif args.root:
        print(f"Reading chip fuse IDs directly ftorm ROOT file: {args.root}")
        fuseJSON = ROOT_to_JSON(args.root)
    else:
        sys.exit("Please provide either the ROOT file or JSON file containing the fuse IDs of all chips on this panel!\nExiting!")
        
    ### Print fuseJSON
    print("\nFound the following fuse IDs:")
    print(json.dumps(fuseJSON, indent=4))
    
        
    ### Get hybrid info
    print(f"Getting information for hybrid SN {args.hybridSN} from the DB.\n")
    try:
        hybridInfo = dbAccess.doSomething("getComponent", method="GET",  data={"component":args.hybridSN})
    except:
        sys.exit("Failed to get panel info! Exiting!")

    ### Get serial numbers and slots of all hybrids on the panel
    print("\nSuccessfully found hybrid panel in the DB.")
    print("Will now iterate through its children to find which STAR Hybrid Assemblies are assembled in which slots.")
    print("NB:")
    print("To determine the slot, this script relies on the 'HYBRID_POSITION' property of each assembled child hybrid!")
    print("   Since this is a string (as opposed to an int), we have to be a bit careful parsing it.")
    print("This script does NOT rely on the child slot's 'order' property!")
    print("There are a couple reasons for this:")
    print("   1) The slot's order property is not visible from the web interface.")
    print("   2) For barrel panels, the order property resets between X-specific and XY-ambidextrous slots!")
    print("         X-specific slots of 'type':'X' while XY-ambidextrous slots have 'type':null.")
    print("         Both have order parameters of 0-2, as do the powerboards.")
    print('')
    hybridDict = {}
    pbDict = {}
    hybridDict[0]=hybridInfo


    print(json.dumps(hybridDict,indent=4))


    ####################
    # Start assembling # 
    ####################

    print('\n###########')
    print('###########')
    print('###########')

    ### Iterate through the hybrids in the fuse ID JSON.
    print(f"\nIterating through the fuse ID json {args.json} to assemble chips to hybrids based on the fuseIDs read by ITSDAQ.")
    for slot,fuseDict in fuseJSON.items():

        ### Print the fuseIDs of the chips on this hybrid.
        print('\n#####\n')
        print(f"Hybrid in slot {slot} has the following chips assembled:")
        print(json.dumps(fuseDict, indent=4))

        ### Get just get slot number.  Drop "module_"a
        slot = slot.split('_')[-1]

        ### See if there is a hybrid in this panel slot according to the DB.
        try:            
            hybridSerial = args.hybridSN
        except:
            print(f"ITSDAQ says this hybrid is in slot {slot}, but according to the DB, there are no hybrids in this slot!")
            print(f"According to the DB, the filled slots are:\n{hybridDict.keys()}")
            print("Perhaps one of the following is wrong:")
            print("   1) This is the wrong panel for this fuseID JSON file.")
            print("   2) The panel needs to be updated in the DB to accurately reflect which hybrids are assembled.")
            print("   3) The 'position' property of the hybrid on the panel is not a simple (string of an) int.")
            sys.exit("Please fix this.  Exiting!")
        
        print(f"According to the database, this should be hybrid {hybridSerial}")

        ### Pull the hybrid's info from the DB.
        print("Pulling this hybrid's info from the DB.")
        try:
            hybridInfo = dbAccess.doSomething("getComponent", method="GET",  data={"component":hybridSerial,})
        except:
            sys.exit("Failed to pull this hybrid's info from the DB.  Exiting!")

        # ### Check if hybrid already has chips assembled
        # if allChildrenAssembled(hybridInfo, "ABCSTARV1"):
        #     print("All ABCSTARV1 chips are already assembled.")
        # elif allChildrenAssembled(hybridInfo, "HCCSTARV1"):
        #     print("HCCSTARV1 chip is already assembled.")

        ### Iterate through chips
        for chip,fuse in fuseDict.items():

            print('---')
            
            ####### 
            # ABC #
            ####### 
            if 'chan' in chip:
                
                ### Get just the slot number.  Drop "fuseid_readback_chan_"
                chipPosition = chip.split('_')[-1] 
                print(f"ABC fuseID: {fuse}, position: {int(chipPosition)-1}")
                
                ### Check fuse ID length
                if not len(fuse)==8:
                    print("The length of this chip's fuseID is unexpected.  Expect a length of 8 when in hex.")
                    sys.exit("Exiting!")
                    
                ### Get this chip's info from the DB                    
                shortFuse = fuse[2:]
                altID = "ABC"+shortFuse
                print(f"Looking for chip in DB with alternative identifier: {altID}")
                try:
                    chipInfo = dbAccess.doSomething("getComponent", method="GET",  data={"component":altID,'alternativeIdentifier':True})
                    print("Found the chip!")
                except:
                    sys.exit("Failed to find this chip in the DB.  Exiting!")

                ### Check institute.
                hybridInstitution = hybridInfo['institution']['code']
                hybridLocation = hybridInfo['currentLocation']['code']
                chipLocation = chipInfo['currentLocation']['code']
                if not chipLocation==hybridInstitution and not chipLocation==hybridLocation:
                    print(f"Chip is located at {chipLocation}, but the hybrid is currently located at {hybridLocation} and belongs to {hybridInstitution}.")
                    sys.exit("The chip location must match one of these hybrid institutions in order to assemble it to the hybrid.  Exiting!")

                ### Check if this chip is already assembled to a hybrid.
                parentHybrids = [parent for parent in chipInfo['parents'] if parent['componentType']['code']=='HYBRID_ASSEMBLY' and parent['component'] ]
                if len(parentHybrids)==1: # this chip is already assembled to a hybrid.
                    parentHybrid = parentHybrids[0]
                    parentHybridSerial = parentHybrid['component']['serialNumber']
                    print(f"This chip is already assembled to a hybrid with serial {parentHybridSerial}!!!")
                        
                    ### check if the hybrid to which this chip is already assembled matches the hybrid you are trying to assemble.
                    if parentHybridSerial == hybridSerial: # yes it is already assembled to the right hybrid.
                        print("This chip is already assembled to the correct hybrid... ", end='')
                        ### now check if it is assembled in the correct position as well.
                        assembledInfo = [child for child in hybridInfo['children'] if child['component']] # get all assembled children
                        assembledInfo = [child for child in assembledInfo if child['component']['serialNumber']==chipInfo['serialNumber']][0] # get assembled chip matching this one.
                        assembledPosition = [prop['value'] for prop in assembledInfo['properties'] if prop['code']=='ABC_POSITION'][0]                        
                        try:
                            assembledPosition = str(int(assembledPosition)) # checking that this is just a string of an integer.
                        except:
                            try:
                                assembledPosition = str(int(assembledPosition.split('_')[-1])) # in case the positions are e.g. ABC_X_9
                            except:
                                print(f"Can not parse the position property for the already assembled chip in the DB: {assembledPosition}")
                                print('Expect either e.g. "9" or "ABC_X_9".')
                                print("Cannot check if this chip you are trying to assemble by fuseID is already assembled in the DB to the correct hybrid in the correct position.")
                                print("Moving on...")
                        if assembledPosition == chipPosition:
                            print("And it's in the correct position!")
                        else:
                            print("But it's in the wrong position!!!")
                            print(f"Already assembled to position {assembledPosition}... but should be assembled to position {chipPosition}!!!")

                    ### It's aleady assembled to a different hybrid!
                    else: 
                        print('This chip is already assembled to a different hybrid!!!')

                    ### Either way, no more assembly to do right now, so continue to the next chip.
                    continue

                ### Chip is not already assembled to a hybrid.
                ### But should also check if this hybrid has another chip in this slot already.  After previous check, it should not be this chip.
                abcstarv1Children = [child for child in hybridInfo['children'] if child['type']['code']=="ABCSTARV1" and child['component']]
                alreadyFilledPositions = [([prop['value'] for prop in child['properties'] if prop['code']=='ABC_POSITION'][0],child['component']['serialNumber']) for child in abcstarv1Children ]
                if len(alreadyFilledPositions)>0:
                    print("This hybrid already has the following chips assembled (position, serial):")
                    print(alreadyFilledPositions)
                    if chipPosition in np.array(alreadyFilledPositions)[:,0]:
                        print("This hybrid already has another chip assembled in this position!")
                        continue
                else:
                    print("No chips are already assembled to this hybrid.")
                    
                ### Need to pick a slot on the hybrid.  Should probably try to match the slot's 'order' property to the ABC position...
                ###    But 'order' starts at 0, but for PPB, chip positions start at 1.
                ###    Maybe pick order = position -1.
                order = int(chipPosition) - 1
                slotID = [child['id'] for child in hybridInfo['children'] if child['type']['code']=='ABCSTARV1' and child['order']==order][0]
                print(f"Slot info: order = {order}, ID = {slotID}")

                ### Build properties dict for upload
                childProps = {
                    #"ABC_POSITION": chipPosition,
                    "ABC_POSITION": str(order),
                    "WAFER_NUMBER": None, # skip for now.  "WAFER_NUMBER" is only in the history of the ABC.  It is not a property of the ABC itself...
                    "POSITION_ON_WAFER": None # skip for now.  "POSITION_ON_WAFER" is only in the history of the ABC.  It is not a property of the ABC itself...
                }
                
                ### Finally, if this point is reached, the ABC should be assembled to the hybrid in the position written in the fuseID JSON.
                print(f"Finally, assembling {altID} to the specified position ({chipPosition}) to the specified hybrid ({hybridSerial}) on the specified hybrid ({args.hybridSN}).")
                print(json.dumps({'parent':hybridSerial,'slot':slotID,'child':chipInfo['serialNumber'],'properties':childProps}, indent=4))
                if not args.dryRun:
                    try:        
                        result = dbAccess.doSomething(action='assembleComponentBySlot',method='POST',data={'parent':hybridSerial,'slot':slotID,'child':chipInfo['serialNumber'],'properties':childProps})
                    except:
                        print("\n---")
                        sys.exit("Assembly failed!  Exiting!")
                    print('Successfully assembled chip to hybrid!')
                        
            ####### 
            # HCC #
            ####### 
            else: 
                print(f"HCC fuseID: {fuse}:")
                
                ### Check fuse ID length
                if not len(fuse)==8 and not len(fuse)==6:
                    print("The length of this chip's fuseID is unexpected.  Expect a length of 6 or 8 when in hex.")
                    sys.exit("Exiting!")
                    
                ### Get this chip's info from the DB
                if len(fuse)==8:
                    shortFuse = fuse[2:]
                else:
                    shortFuse = fuse
                altID = "HCC"+shortFuse
                print(f"Looking for chip in DB with alternative identifier: {altID}")
                try:
                    chipInfo = dbAccess.doSomething("getComponent", method="GET",  data={"component":altID,'alternativeIdentifier':True})
                    print("Found the chip!")
                except:
                    sys.exit("Failed to find this chip in the DB.  Exiting!")

                ### Check institute.
                hybridInstitution = hybridInfo['institution']['code']
                hybridLocation = hybridInfo['currentLocation']['code']
                chipLocation = chipInfo['currentLocation']['code']
                if not chipLocation==hybridInstitution and not chipLocation==hybridLocation:
                    print(f"Chip is located at {chipLocation}, but the hybrid is currently located at {hybridLocation} and belongs to {hybridInstitution}.")
                    sys.exit("The chip location must match one of these hybrid institutions in order to assemble it to the hybrid.  Exiting!")

                ### Check if this chip is already assembled to a hybrid.
                parentHybrids = [parent for parent in chipInfo['parents'] if parent['componentType']['code']=='HYBRID_ASSEMBLY' and parent['component']]
                # print(json.dumps(parentHybrids, indent=4))
                if len(parentHybrids)==1: # this chip is already assmebled to a hybrid.
                    parentHybrid = parentHybrids[0]
                    parentHybridSerial = parentHybrid['component']['serialNumber']
                    print(f"This chip is already assembled to a hybrid with serial {parentHybridSerial}!!!")
                        
                    ### check if the hybrid to which this chip is already assembled matches the hybrid you are trying to assemble.
                    if parentHybridSerial == hybridSerial: # yes it is already assembled to the right hybrid.
                        print("This chip is already assembled to the correct hybrid...")

                    ### It's aleady assembled to a different hybrid!
                    else: 
                        print('This chip is already assembled to a different hybrid!!!')

                    ### Either way, no more assembly to do right now, so continue to the next chip.
                    continue

                ### Chip is not already assembled to a hybrid.
                ### But should also check if this hybrid has a chip in this slot already.  After previous check, it should not be this chip.
                hccv1Children = [child for child in hybridInfo['children'] if child['type']['code']=="HCCSTARV1" and child['component']]
                if len(hccv1Children)>0:
                    print(f"This hybrid already has the following chip assembled: {json.dumps(hccv1Children, indent=4)}")
                else:
                    print("No chips are already assembled to this hybrid.")

                ### Build properties dict for upload
                childProps = {
                    "FUSE_ID": shortFuse,
                    "WAFER_NUMBER": [prop['value'] for prop in chipInfo['properties'] if prop['code']=="WAFER_NAME"][0], # use WAFER_NAME property of HCC?
                    "POSITION_ON_WAFER": [prop['value'] for prop in chipInfo['properties'] if prop['code']=="DIE_INDEX"][0] # use DIE_INDEX property of HCC?
                }
                
                ### Finally, if this point is reached, the ABC should be assembled to the hybrid in the position written in the fuseID JSON.
                print(json.dumps({'parent':hybridSerial,'child':chipInfo['serialNumber'],'properties':childProps}, indent=4))
                if not args.dryRun:
                    try:        
                        result = dbAccess.doSomething(action='assembleComponent',method='POST',data={'parent':hybridSerial,'child':chipInfo['serialNumber'],'properties':childProps})
                    except:
                        sys.exit("Failed when trying to assemble HCC!  Exiting!")
                    print('Successfully assembled chip to hybrid!')

        
    ###
    print("Done assembling hybrids.")

    
################################
################################
################################

def ROOT_to_JSON(filename):

    ### Open ROOT file to configuration/ TDirectory
    f = uproot.open(filename+":configuration")

    ### Get keys for TObjStrings.     
    fuseIDKeys = sorted([key for key in f.keys() if 'module_' in key and 'fuseid_readback' in key]) 

    ### Get all the "modules" (hybrids) that are assembled to the panel.
    modules = sorted(list(set([key.split('/')[0] for key in fuseIDKeys])))

    ###
    ### Format a bit better into a dict/JSON
    ###
    
    ### Schema:
    ### {
    ###    "module_0": {
    ###       "fuseid_readback": "7040086b",
    ###       "fuseid_readback_chan_1": "8940985e",
    ###       ...
    ###    },
    ###    "module_1": {
    ###       "fuseid_readback": "70400921",
    ###       "fuseid_readback_chan_1": "8940983f",
    ###       ...
    ###    },
    ###   ...
    ### }

    ### Each key is e.g. 'module_1/fuseid_readback_chan_10;1', so drop 'module_1' and the cycle (';1')
    fuseJSON = {module:{key.split('/')[-1].split(';')[0]:f[key] for key in fuseIDKeys if module in key} for module in modules}

    # ### More explicit
    # fuseJSON = {}
    # for module in modules:
    #     thisModuleDict = {}
    #     for key in fuseIDKeys:
    #         if module not in key:
    #             continue
    #         chipKey = key.split('/')[-1].split(';')[0] # from e.g. 'module_1/fuseid_readback_chan_10;1', drop 'module_1/' and the cycle (';1')
    #         chipValue = f[key]
    #         thisModuleDict[chipKey] = chipValue
    #     fuseJSON[module] = thisModuleDict

    ### Return
    return fuseJSON

    
################################
################################
################################


if __name__ == "__main__":
    main()

