"""
28 December 2022
Abraham Tishelman-Charny

The purpose of this python module is to plot, compare, and analyze noise from ITk strips modules.

# Example usage:

# Plot ratio of files chosen explicitly

python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/Commissioning/Box2_RoomTemp_Chuck2/SCIPP-PPB_LS-016_RC_441_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/Commissioning/Box3_RoomTemp/SCIPP-PPB_LS-016_RC_47_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_016_Box2_Chuck2_RoomTemp,SCIPP_PPB_LS_016_Box3_Chuck1_RoomTemp,SCIPP_PPB_LS_016_RoomTemp_Comparison_Chucks12" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/ --makeSeriesPlot --makeHist

python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box2_Chuck1/results/SCIPP-PPB_LS-012_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box3/results/SCIPP-PPB_LS-012_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_012_Box2_Chuck1,SCIPP_PPB_LS_012_Box3,SCIPP_PPB_LS_012_Comparison_Chucks_1_1" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist
python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box2_Chuck2/results/,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box3/results/SCIPP-PPB_LS-012_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_012_Box2_Chuck1,SCIPP_PPB_LS_012_Box3,SCIPP_PPB_LS_012_Comparison_Chucks_1_1" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist

python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box2/results/SCIPP-PPB_LS-012_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box3/results/SCIPP-PPB_LS-012_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_012_Box2,SCIPP_PPB_LS_012_Box3,SCIPP_PPB_LS_012_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist

python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box2/results/SCIPP-PPB_LS-012_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box3/results/SCIPP-PPB_LS-012_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_012_Box2,SCIPP_PPB_LS_012_Box3,SCIPP_PPB_LS_012_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist
python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-016_box2/results/SCIPP-PPB_LS-016_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-016_box3/results/SCIPP-PPB_LS-016_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_016_Box2,SCIPP_PPB_LS_016_Box3,SCIPP_PPB_LS_016_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist
python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-019_box2/results/SCIPP-PPB_LS-019_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-019_box3/results/SCIPP-PPB_LS-019_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_019_Box2,SCIPP_PPB_LS_019_Box3,SCIPP_PPB_LS_019_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist
python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-023_box2/results/SCIPP-PPB_LS-023_RC_309_7.txt,/eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-023_box3/results/SCIPP-PPB_LS-023_RC_28_7.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_023_Box2,SCIPP_PPB_LS_023_Box3,SCIPP_PPB_LS_023_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/ --makeSeriesPlot --makeHist

# Other
python3 Module-Analysis.py --plotTogether --dataFromITSDAQ
python3 Module-Analysis.py --plotPerModule --dataFromCSV
python3 Module-Analysis.py --plotPerModule --dataFromITSDAQ --inDir /eos/user/a/atishelm/www/ITk/Third_Box/RoomTemperature/SCIPP-PPB_LS-012_box2/results/ --perSide --makeSeriesPlot --makeHist

"""

# Comparing cold tests with and without shunting:
# python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/PPB2/TC/All/BNL-PPB2-MLS-220_RC_229_6.txt,/eos/user/a/atishelm/www/ITk/PPB2/TC/All/BNL-PPB2-MLS-220_RC_229_22.txt" --perSide --ratioFilesLabels "BNL_PPB2_MLS_220_ColdTestNoShunting,BNL_PPB2_MLS_220_ColdTestWithShunting,BNL_PPB2_MLS_220_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/PPB2/TC/ShuntingComparison/ --makeSeriesPlot --makeHist --inDir /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/

# Death by thermal cycle study commands:
#python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/BNL-PPB2-MLS-210_RC_94_25.txt,/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data_2/BNL-PPB2-MLS-210_RC_138_166.txt" --perSide --ratioFilesLabels "BNL_PPB2_MLS_210_ColdTest1,BNL_PPB2_MLS_210_ColdTest101,BNL_PPB2_MLS_210_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/NoiseComparisons/ --makeSeriesPlot --makeHist
#python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/BNL-PPB2-MLS-211_RC_94_25.txt,/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data_2/BNL-PPB2-MLS-211_RC_138_134.txt" --perSide --ratioFilesLabels "BNL_PPB2_MLS_211_FirstColdTest,BNL_PPB2_MLS_211_LastDayColdTest,BNL_PPB2_MLS_211_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/NoiseComparisons/ --makeSeriesPlot --makeHist
#python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/SCIPP-PPB_LS-016_RC_115_224.txt,/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/SCIPP-PPB_LS-016_RC_115_256.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_016_1,SCIPP_PPB_LS_016_2,SCIPP_PPB_LS_016_Compare" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/NoiseComparisons/ --makeSeriesPlot --makeHist

# python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/BNL-PPB2-MLS-210_RC_94_25.txt,/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data_2/BNL-PPB2-MLS-210_RC_138_166.txt" --perSide --ratioFilesLabels "BNL_PPB2_MLS_210_ColdTest1,BNL_PPB2_MLS_210_ColdTest101,BNL_PPB2_MLS_210_Comparison" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/NoiseComparisons/ --makeSeriesPlot --makeHist
# python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/SCIPP-PPB_LS-016_RC_94_25.txt,/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data_2/SCIPP-PPB_LS-016_RC_138_166.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_016_ColdTest5,SCIPP_PPB_LS_016_ColdTest105,SCIPP_PPB_LS_016_Compare" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/NoiseComparisons/ --makeSeriesPlot --makeHist
# python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/SCIPP-PPB_LS-019_RC_94_25.txt,/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data_2/SCIPP-PPB_LS-019_RC_138_166.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_019_ColdTest5,SCIPP_PPB_LS_019_ColdTest105,SCIPP_PPB_LS_019_Compare" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/NoiseComparisons/ --makeSeriesPlot --makeHist

# 94, 25 was first cold test of TC to death
# 138, 166 was final

# Compare 3 results
#python3 Module-Analysis.py --plotRatio --ratioFiles "/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/SCIPP-PPB_LS-016_RC_115_224.txt,/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/SCIPP-PPB_LS-016_RC_115_256.txt,/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/SCIPP-PPB_LS-016_RC_115_288.txt" --perSide --ratioFilesLabels "SCIPP_PPB_LS_016_before,SCIPP_PPB_LS_016_during,SCIPP_PPB_LS_016_after,SCIPP_PPB_LS_016_Compare" --dataFromITSDAQ --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/NoiseComparisons/ --makeSeriesPlot --makeHist

#/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/BNL-PPB2-MLS-210_RC_94_25.txt # first cold test of 210
#/eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data_2/BNL-PPB2-MLS-210_RC_138_134.txt # last cold test of 210

# /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/BNL-PPB2-MLS-211_RC_97_6.txt # First cold test of 211 at Vbias = -250V

# Plot one module
# python3 Module-Analysis.py --plotPerModule --dataFromITSDAQ --inDir /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/ --perSide --makeSeriesPlot --makeHist --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Plots
# python3 Module-Analysis.py --plotPerModule --dataFromITSDAQ --inDir /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data/ --perSide --makeSeriesPlot --makeHist --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Plots
# BNL-PPB2-MLS-210_RC_122_230.txt
# python3 Module-Analysis.py --plotPerModule --dataFromITSDAQ --inDir /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Data_2/ --perSide --makeSeriesPlot --makeHist --ol /eos/user/a/atishelm/www/ITk/Death_By_Thermal_Cycle/Plots/


# imports 
import pandas as pd 
import numpy as np 
import os 
import csv 
import mplhep as hep 
from matplotlib import pyplot as plt 
plt.style.use(hep.style.ATLAS) # customization: https://mplhep.readthedocs.io/en/latest/api.html
from python.Module_Analysis_Tools import * 
from python.SetOptions import options 

# Command line flag options 
plotPerModule = options.plotPerModule
plotTogether = options.plotTogether
inDir = options.inDir 
ol = options.ol 
dataFromITSDAQ = options.dataFromITSDAQ
dataFromCSV = options.dataFromCSV
makeSeriesPlot = options.makeSeriesPlot
makeHist = options.makeHist
perSide = options.perSide
NoInteractiveMode = options.NoInteractiveMode
plotRatio = options.plotRatio
ratioFiles = options.ratioFiles.split(',')
ratioFilesLabels = options.ratioFilesLabels.split(',')

if(NoInteractiveMode): 
    import matplotlib
    matplotlib.use('Agg') # turn off interactive mode

if(dataFromITSDAQ + dataFromCSV != 1):
   raise Exception("Need to choose either dataFromITSDAQ or dataFromCSV")

# Create output directory if it doesn't already exist
if(not os.path.exists(ol)):
    print("Creating output directory:",ol)
    os.system("mkdir -p %s"%(ol))
    os.system("cp %s/../index.php %s"%(ol, ol)) # assumes one directory up from 'ol' exists

# Functions 
def Save_Figures(fig, fileTypes_, OUTNAME_, plotType):
    for fileType in fileTypes_:
        outname = "%s_%s.%s"%(OUTNAME_, plotType, fileType)
        print("")
        print("Saving figure:",outname)
        print("")
        fig.savefig(outname)
    plt.close()    

def Make_Hist(y_vals_, y_label, ymaxIncrement, Nentries, upperText, quantity_nonlfn_label, fileTypes, OUTNAME_, log_):
    # histogram (values integrated over time)
    fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
    counts, bins = np.histogram(y_vals_)
    avg, stdev = np.mean(y_vals_), np.std(y_vals_)
    ax.hist(bins[:-1], bins, weights=counts)
    hep.atlas.text("ITk Strips Internal")    
    ax.set_xlabel(y_label)
    ax.set_ylabel("Entries")

    if(log_): 
        plt.yscale('log')
        OUTNAME_ += "_log"
        ymaxIncrement = 10

    # set ymax higher to contain plot labels
    ymin, ymax = ax.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    ax.set_ylim(ymin, Incremented_Ymax)

    # Upper left text 
    plt.text(
        0.06,
        0.95,
        "\n".join([
            "$\mu$ = %s" % float('%.4g' % avg),
            "$\sigma$ = %s" % float('%.4g' % stdev),
            "$N_{entries}$ = %s"%(Nentries)
        ]
        ),
        horizontalalignment='left',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )

    # Upper right text 
    plt.text(
        0.94, 
        0.95,
        "\n".join([
            upperText,
            quantity_nonlfn_label
        ]
        ),
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )
    fig.tight_layout()
    Save_Figures(fig, fileTypes, OUTNAME_, "history")

    # Save avg and stdev 
    csv_outname = '%s_stats.csv'%(OUTNAME_)
    print("Saving csv file:",csv_outname)
    row = [avg, stdev]
    f = open(csv_outname,'w')
    writer = csv.writer(f)
    writer.writerow(row)
    f.close()

def Make_Plots(x_vals_, x_label, y_vals_, y_vals_err_, OUTNAME_, Module_, Value_type_, SET_NUMBER_, add_side_number, makeSeriesPlot, makeHist):
    unit_dict = {
        "Current" : "$\mu$A",
        "Voltage" : "mV",
        "Gain" : "",
        "vt50" : "",
        "Input_noise" : r"$e^{-}$"
    }

    yunit = unit_dict[Value_type_]
    xmin, xmax = min(x_vals_), max(x_vals_)

    # params 
    fileTypes = ["png", "pdf"]
    Nentries = len(y_vals_)
    ymaxIncrement = 0.333
    quantity_nonlfn_label = Value_type_.replace("_", " ")
    if(yunit == ""): y_label = quantity_nonlfn_label # if unitless, no brackets
    else: y_label = r"%s [%s]"%(quantity_nonlfn_label, yunit)

    if(makeSeriesPlot):
        # series (values vs x quantity)
        fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
        
        if(y_vals_err_ is not None):
            ax.errorbar(x_vals_, y_vals_, yerr=y_vals_err_, xerr=None, marker='o', markersize=2, elinewidth=0.5, linewidth=0.5)
        #else: ax.plot(x_vals_, y_vals_)
        else: ax.plot(x_vals_, y_vals_, linewidth=0.05) # very thin line
        hep.atlas.text("ITk Strips Internal")
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)

        # set ymax higher to contain plot labels
        ymin, ymax = ax.get_ylim()
        Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
        ax.set_ylim(ymin, Incremented_Ymax)

        # Upper right text 
        if(add_side_number): upperText = "%s, Stream %s"%(Module_,SET_NUMBER_)
        else: upperText = "%s"%(Module_)

        plt.text(
            0.94,
            0.95,
            "\n".join([
                upperText,
                quantity_nonlfn_label
            ]
            ),
            horizontalalignment='right',
            verticalalignment='top',
            fontweight = 'bold',
            transform=ax.transAxes
        )

        ax.set_xlim(xmin, xmax)
        fig.tight_layout()
        Save_Figures(fig, fileTypes, OUTNAME_, "series")
        logs = [0]
        if(makeHist): 
            for log in logs:
                Make_Hist(y_vals_, y_label, ymaxIncrement, Nentries, upperText, quantity_nonlfn_label, fileTypes, OUTNAME_, log)

def Make_Combined_Plot(x_label, all_module_vals_, y_errs_, x_vals_, modules_, quantity_, side_, OUTNAME_, perSide_, plotLog_):
    
    unit_dict = {
        "Current" : "$\mu$A",
        "Voltage" : "mV",
        "Gain" : "",
        "vt50" : "",
        "Input_noise" : r"$e^{-}$"
    }

    yunit = unit_dict[quantity_]

    # parameters 
    fileTypes = ["png", "pdf"]
    N_modules = len(all_module_vals_)
    xmin, xmax = min(x_vals_), max(x_vals_)
    ymaxIncrement = 0.6
    quantity_nonlfn_label = quantity_.replace("_", " ")

    # make ratio plot 
    fig, ax = plt.subplots(2, figsize=(6, 4), sharex=True, dpi=100, gridspec_kw={'height_ratios': [3, 1]})

    lower = ax[1]
    upper = ax[0]

    if(yunit == ""): y_label = quantity_nonlfn_label # if unitless, no brackets
    else: y_label = r"%s [%s]"%(quantity_nonlfn_label, yunit)

    # to get local IDs correct without having to worry about dashes not allowable as tokens for names
    Module_Label_Dict = {
        "SCIPP_PPB_LS_012_Box2" : "SCIPP-PPB_LS-012_Box2",
        "SCIPP_PPB_LS_012_Box3" : "SCIPP-PPB_LS-012_Box3",

        "SCIPP_PPB_LS_016_Box2" : "SCIPP-PPB_LS-016_Box2",
        "SCIPP_PPB_LS_016_Box3" : "SCIPP-PPB_LS-016_Box3",

        "SCIPP_PPB_LS_019_Box2" : "SCIPP-PPB_LS-019_Box2",
        "SCIPP_PPB_LS_019_Box3" : "SCIPP-PPB_LS-019_Box3",

        "SCIPP_PPB_LS_023_Box2" : "SCIPP-PPB_LS-023_Box2",
        "SCIPP_PPB_LS_023_Box3" : "SCIPP-PPB_LS-023_Box3",   

        "BNL_PPB2_MLS_210_ColdTest1" : "BNL module 1 Cycle 1",                     
        "BNL_PPB2_MLS_210_ColdTest101" : "BNL module 1 Cycle 101", 

        "SCIPP_PPB_LS_016_ColdTest5" : "SCIPP module 1 Cycle 1",                     
        "SCIPP_PPB_LS_016_ColdTest105" : "SCIPP module 1 Cycle 101",   

        "SCIPP_PPB_LS_019_ColdTest5" : "SCIPP module 2 Cycle 1",                     
        "SCIPP_PPB_LS_019_ColdTest105" : "SCIPP module 2 Cycle 101",                     

        "SCIPP-PPB_LS-016" : "SCIPP module 1",                     
        "SCIPP-PPB_LS-019" : "SCIPP module 2",                     
    }

    #module_names = []

    colors, labels = [], []

    for m_i, module_vals in enumerate(all_module_vals_):
        if(modules_[m_i] in Module_Label_Dict): module_label = Module_Label_Dict[modules_[m_i]]
        else: module_label = modules_[m_i]
        if(m_i == 0): InitialModuleLabel = "%s"%(module_label.split('_')[-1])
        ThisModuleLabel = "%s"%(module_label.split('_')[-1])

        #module_names.append(module_label)

        if(y_errs_ is not None):
            upper.errorbar(x_vals_, module_vals, yerr=y_errs_[m_i], linestyle="-", marker="o", label = module_label, linewidth=0.25, markersize=0.2)
            
        else: 
            #upper.plot(x_vals_, module_vals, 'o-', label = module_label, linewidth=0.25, markersize=0.25)
            upper.plot(x_vals_, module_vals, 'o-', label = module_label, linewidth=0.25, markersize=0.25)
            color = upper.get_lines()[-1].get_c()
            colors.append(color)
            labels.append(module_label)

        module_vals = np.array(module_vals, dtype=float)
        all_module_vals_[0] = np.array(all_module_vals_[0], dtype=float)

        ratio_vals = np.divide(module_vals, all_module_vals_[0], out=np.zeros_like(module_vals), where=all_module_vals_[0]!=0)
        lower.plot(x_vals_, ratio_vals, 'o', linewidth=0, markersize=0.25)

        # make histogram of values 
        Nentries = len(module_vals)
        if(perSide_): upperText = "%s, Stream %s"%(module_label, side_)
        else: upperText = "%s"%(module_label)
        OUTNAME_hist = "%s_%s_hist"%(OUTNAME_, ThisModuleLabel)
        
        logs = [0]

        for log in logs: Make_Hist(module_vals, ThisModuleLabel, ymaxIncrement, Nentries, ThisModuleLabel, quantity_nonlfn_label, fileTypes, OUTNAME_hist, log)

        # and ratio, only if it's not the first module since that will have all entries = 1 by definition
        if(m_i != 0):
            OUTNAME_hist = "%s_%s_Ratio_hist"%(OUTNAME_, module_label)
            ratio_label = "%s / %s"%(ThisModuleLabel, InitialModuleLabel)
            for log in logs: Make_Hist(ratio_vals, ratio_label, ymaxIncrement, Nentries, ratio_label, quantity_nonlfn_label, fileTypes, OUTNAME_hist, log)

    
    lower.set_xlabel(x_label)
    upper.set_ylabel(y_label)    

    upper.set_xlim(xmin, xmax)
    lower.set_xlim(xmin, xmax)

    lower.axhline(y=1, color='black', linestyle='dashed', linewidth=0.25)

    firstModuleName = modules_[0]

    ratioLabelDict = {
        "ColdTest1" : "Cycle 1",
        "ColdTest5" : "Cycle 1"
    }

    ratio_Label = ratioLabelDict[firstModuleName.split("_")[-1]]

    lowerYlabel = "\n".join([
        "Ratio to",
        ratio_Label
    ])

    lower.set_ylabel(lowerYlabel, fontsize=10)

    if(plotLog_):
        OUTNAME_full = "%s_log"%(OUTNAME_)
        ymaxIncrement_Total = ymaxIncrement*10.
    else:
        OUTNAME_full = OUTNAME_
        ymaxIncrement_Total = ymaxIncrement

    custom = 1
    custom_ymin_upper = 700
    custom_ymax_upper = 1300
    custom_ymin_lower = 0.8
    custom_ymax_lower = 1.2

    if(custom):
        upper.set_ylim(custom_ymin_upper, custom_ymax_upper)
        lower.set_ylim(custom_ymin_lower, custom_ymax_lower)
    else:
        # set ymax higher to contain plot labels
        ymin, ymax = upper.get_ylim()
        Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement_Total
        if(plotLog_):
            upper.set_yscale('log')        
        upper.set_ylim(ymin, Incremented_Ymax)    
    #upper.legend(fontsize=7.5, loc = 'upper center')

    # Add horizontal lines to upper plot if desired 
    # Maximum allowed LS noise leve: 1243 ENC
    # Target noise: 779 (uncovered) 824 (covered) LS modules
    upper.axhline(y=1243, color='#cc0000', linestyle='dashed', linewidth=1.5)
    #upper.axhline(y=824, color='#6aa84f', linestyle='dashed', linewidth=1.5)
    upper.axhline(y=779, color='#6aa84f', linestyle='dashed', linewidth=1.5)

    # legendFontSize = 12
    legendFontSize = 9
    #legend = upper.legend(fontsize=legendFontSize, loc = 'upper left')
    #if(not plotLog): 
        #print("No scientific notation")
        #upper.ticklabel_format(style='plain') # remove scientific notation

    hep.atlas.text("ITk Strips Preliminary", ax=upper)

    #for legobj in legend.legendHandles:
        #legobj.set_linewidth(2.0)

    fig.tight_layout()

    if(perSide_):
        # Stream number 
        plt.text(
            0.06,
            0.85,
            "\n".join([
                "Stream %s"%(int(side_))
            ]
            ),
            horizontalalignment='left',
            verticalalignment='top',
            fontweight = 'bold',
            transform=upper.transAxes,
            fontsize = 14
        )   

    print("colors:",colors)
    print("labels:",labels)

    PlotLabelDict = {

    }

    ModuleName = ""
    if("BNL module 1 " in labels[0]): ModuleName = "BNL module 1"
    elif("BNL module 2 " in labels[0]): ModuleName = "BNL module 2"
    elif("SCIPP module 1 " in labels[0]): ModuleName = "SCIPP module 1"
    elif("SCIPP module 2 " in labels[0]): ModuleName = "SCIPP module 2"

    # upper left text
    plt.text(
        #0.06,
        0.94,
        0.85,
        ModuleName,
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=upper.transAxes,
        fontsize = 14
    )  

    plt.text(
        0.94,
        0.73,
        labels[0].replace(ModuleName, ""),
        color = colors[0],
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=upper.transAxes,
        fontsize = 14
    )  

    plt.text(
        0.94,
        0.61,
        labels[1].replace(ModuleName, ""),
        color = colors[1],
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=upper.transAxes,
        fontsize = 14
    )  

    Save_Figures(fig, fileTypes, OUTNAME_full, "series")    

# go through modules just to get values into lists/arrays
for module_i, module in enumerate(modules): # assumes data is in form of txt files output from ITSDAQ
    print("On module:",module)
    module_str = module.replace("-","_")

    # data from standard ITSDAQ output files 
    if(dataFromITSDAQ):
        inFile = "%s/%s_%s_%s_%s.txt"%(inDir, module, RESULT_TYPE, RUN_NUMBER, SCAN_NUMBER)
        df = pd.read_csv(inFile, delimiter="\t")
        #print("df:",df)

        # for each quantity, make a plot for the module
        Nstrips = 1280 # per side
        #Nstrips = 394240 # hack for huge input files which combine a lot of files
        for quantity in quantities:
            #print("On quantity:",quantity)
            vals = np.array(df[quantity].tolist())
            channels = np.array(df["#chan"].tolist())

            if(perSide):
                strips_0_vals = vals[:Nstrips] # values for first set of strips
                strips_1_vals = vals[Nstrips:] # values for second set of strips 
                strips_0_channels = channels[:Nstrips]
                strips_1_channels = channels[Nstrips:]
            else: # just set them to the same thing
                strips_0_vals = vals[:] # values for first set of strips
                strips_1_vals = vals[:] # values for second set of strips 
                strips_0_channels = channels[:]
                strips_1_channels = channels[:]                

            # save values for later in case you want to make a combined plot 
            exec("%s_%s_0_y_vals = np.copy(strips_0_vals)"%(module_str, quantity)) # module, quantity, side 
            exec("%s_%s_1_y_vals = np.copy(strips_1_vals)"%(module_str, quantity)) # module, quantity, side     
            
            strips_0_channels = np.copy(strips_0_channels)
            strips_1_channels = np.copy(strips_1_channels)           

            quant_label = quantity_dict[quantity]

            if(plotPerModule):
                if(perSide):
                    for SET_NUMBER in ["0", "1"]:
                        # make two types of plots: distribution of values, and value vs. channel number 
                        OUTNAME = '%s/%s_%s_%s'%(ol, module, quant_label, SET_NUMBER)
                        exec("These_channels = strips_%s_channels"%(SET_NUMBER))
                        exec("These_vals = strips_%s_vals"%(SET_NUMBER))
                        vals_rms = None
                        x_label = "Channel number"
                        add_side_num = 1
                        Make_Plots(These_channels, x_label, These_vals, vals_rms, OUTNAME, module, quant_label, SET_NUMBER, add_side_num, makeSeriesPlot, makeHist)
                else:
                    # One plot, combine sides
                    SET_NUMBER = 1
                    OUTNAME = '%s/%s_%s_%s'%(ol, module, quant_label, SET_NUMBER)
                    exec("These_channels = strips_%s_channels"%(SET_NUMBER))
                    exec("These_vals = strips_%s_vals"%(SET_NUMBER))
                    vals_rms = None
                    #x_label = "Channel number"
                    x_label = r"$Channel_{N}{\times}Measurement_{N}$"
                    add_side_num = 0
                    These_channels = [i for i in range(Nstrips)]
                    Make_Plots(These_channels, x_label, These_vals, vals_rms, OUTNAME, module, quant_label, SET_NUMBER, add_side_num, makeSeriesPlot, makeHist)


    # data from custom CSV file
    elif(dataFromCSV):
        inFile = "%s/%s_%s.csv"%(inDir, module, RESULT_TYPE)
        df = pd.read_csv(inFile, delimiter=",", header=None)
        print("df",df)
        quant_column_dict = { # columns with quantity and its RMS
            "Current" : [2, 3],
            "Voltage" : [4, 5]
        }
        # for each quantity, make a plot for the module
        for quantity in quantities:
            #print("On quantity:",quantity)
            val_col, val_rms_col = quant_column_dict[quantity]

            vals = np.array(df[val_col].tolist())
            vals_rms = np.array(df[val_rms_col].tolist())

            if(quantity == "Current"): 
                vals = np.array([i/1000. for i in vals]) # convert nA to muA.
                vals_rms = np.array([i/1000. for i in vals]) # convert nA to muA.

            quant_label = quantity_dict[quantity]
            # make two types of plots: distribution of values, and value vs. time (x axis value time now not channel number)
            times = np.array(df[0].tolist())
            initial_time = times[0] 
            times = np.array([time-initial_time for time in times])

            # save values in case you want to plot them later
            exec("%s_%s_y_vals = np.copy(vals)"%(module_str, quantity))
            exec("%s_%s_y_vals_rms = np.copy(vals_rms)"%(module_str, quantity))
            exec("%s_%s_x_vals = np.copy(times)"%(module_str, quantity))

            if(plotPerModule):
                OUTNAME = "%s/%s_%s"%(ol, module, quant_label)
                SET_NUMBER = "SET_NUMBER_BLANK"
                x_label = "time [s]"
                add_side_num = 0
                Make_Plots(times, x_label, vals, vals_rms, OUTNAME, module, quant_label, SET_NUMBER, add_side_num, makeSeriesPlot, makeHist)

if(plotTogether):
    if(perSide): 
        sides = ["0", "1"]
        OUTNAME = '%s/%s_%s_%s'%(ol, "AllModules", quant_label, side) # wrong? side number here?
    else: 
        sides = ["NoSides"]    
        OUTNAME = '%s/%s_%s'%(ol, "AllModules", quant_label)    
    for quantity in quantities:
        print("On quantity:",quantity)
        for side in sides:
            print("On side:",side)
            all_module_vals = []
            all_module_vals_rms = []
            for module in modules:
                module_str = module.replace("-","_")
                if(dataFromITSDAQ):
                    x_label = "Channel number"
                    exec("these_y_vals = %s_%s_%s_y_vals"%(module_str, quantity, side))
                    these_y_vals_rms = None
                    exec("these_x_vals = strips_%s_channels"%(side)) # taking most recent channel vals. assuming same for all modules
                elif(dataFromCSV):
                    x_label = "time [s]"
                    exec("these_y_vals = %s_%s_y_vals"%(module_str, quantity))
                    exec("these_y_vals_rms = %s_%s_y_vals_rms"%(module_str, quantity))
                    exec("these_x_vals = %s_%s_x_vals"%(module_str, quantity))
                all_module_vals.append(these_y_vals)
                all_module_vals_rms.append(these_y_vals_rms)
            quant_label = quantity_dict[quantity]
            if(dataFromITSDAQ): 
                all_module_vals_rms = None

            OUTNAME = '%s/%s_%s_%s'%(ol, "AllModules", quant_label, side)
            for plotLog in [0,1]:
                Make_Combined_Plot(x_label, all_module_vals, all_module_vals_rms, these_x_vals, modules, quant_label, side, OUTNAME, perSide, plotLog)

# for just choosing files by hand
if(plotRatio):

    # Structure values into lists
    for file_i, file in enumerate(ratioFiles):
        fileLabel = ratioFilesLabels[file_i]
        df = pd.read_csv(file, delimiter="\t")
        #print("df:",df)
        Nstrips = 1280 # per side
        for quantity in quantities:
            print("On quantity:",quantity)

            vals = np.array(df[quantity].tolist())
            channels = np.array(df["#chan"].tolist())

            if(perSide):
                strips_0_vals = vals[:Nstrips] # values for first set of strips
                strips_1_vals = vals[Nstrips:] # values for second set of strips 
                strips_0_channels = channels[:Nstrips]
                strips_1_channels = channels[Nstrips:]
            else: # just set them to the same thing
                strips_0_vals = vals[:] # values for first set of strips
                strips_1_vals = vals[:] # values for second set of strips 
                strips_0_channels = channels[:]
                strips_1_channels = channels[:]                

            # save values for later in case you want to make a combined plot 
            exec("%s_%s_0_y_vals = np.copy(strips_0_vals)"%(fileLabel, quantity)) # fileLabel, quantity, side 
            exec("%s_%s_1_y_vals = np.copy(strips_1_vals)"%(fileLabel, quantity)) # fileLabel, quantity, side     
        
            strips_0_channels = np.copy(strips_0_channels)
            strips_1_channels = np.copy(strips_1_channels)           

            quant_label = quantity_dict[quantity]

    if(perSide): 
        sides = ["0", "1"]
    else: 
        sides = ["NoSides"]    
    
    for quantity in quantities:
        print("On quantity:",quantity)
        for side in sides:
            print("On side:",side)
            all_module_vals = []
            all_module_vals_rms = []

            for file_i, file in enumerate(ratioFiles):
                fileLabel = ratioFilesLabels[file_i]                
                print("fileLabel:",fileLabel)
                if(dataFromITSDAQ):
                    x_label = "Channel number"
                    exec("these_y_vals = %s_%s_%s_y_vals"%(fileLabel, quantity, side))
                    these_y_vals_rms = None
                    exec("these_x_vals = strips_%s_channels"%(side)) # taking most recent channel vals. assuming same for all modules
                elif(dataFromCSV):
                    x_label = "time [s]"
                    exec("these_y_vals = %s_%s_y_vals"%(fileLabel, quantity))
                    exec("these_y_vals_rms = %s_%s_y_vals_rms"%(fileLabel, quantity))
                    exec("these_x_vals = %s_%s_x_vals"%(fileLabel, quantity))
                all_module_vals.append(these_y_vals)
                all_module_vals_rms.append(these_y_vals_rms)
            
            quant_label = quantity_dict[quantity]
            if(dataFromITSDAQ): 
                all_module_vals_rms = None

            Ratio_Label = ratioFilesLabels[-1] # reserve final label for name of ratio plot

            OUTNAME = '%s/%s_%s_%s'%(ol, Ratio_Label, quant_label, side)
            for plotLog in [0]:
                Make_Combined_Plot(x_label, all_module_vals, all_module_vals_rms, these_x_vals, ratioFilesLabels, quant_label, side, OUTNAME, perSide, plotLog)