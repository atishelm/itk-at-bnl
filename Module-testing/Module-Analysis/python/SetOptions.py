"""
28 December 2022
Abraham Tishelman-Charny

The purpose of this python module is to set run options for the Module-Analysis module.
"""

# Imports 
import argparse 

# Create parser and get command line arguments 
parser = argparse.ArgumentParser()

# Things you can plot
parser.add_argument("--makeSeriesPlot", action="store_true", help="Plot series of each quantity")
parser.add_argument("--makeHist", action="store_true", help="Plot histogram of each quantity")

# Ways you can plot those things
parser.add_argument("--plotPerModule", action="store_true", help="Plot quantities for each module")
parser.add_argument("--plotTogether", action="store_true", help="Plot comparison of modules together")
parser.add_argument("--perSide", action="store_true", help="Plot data for each LS strip side separate (Streams 0 and 1)")
parser.add_argument("--plotRatio", action="store_true", help="Plot the ratio of a quantity between two inputs.") 

# Where the data is coming from
parser.add_argument("--inDir", type=str, default="/eos/user/a/atishelm/www/ITk/Thermal-Cycling/HV-Stability/data/", help="Directory with data to input")
parser.add_argument("--ratioFiles", type=str, default="file_1.txt,file_2.txt", help="Comma separated string of input files to take ratios between")
parser.add_argument("--ratioFilesLabels", type=str, default="FirstFile,SecondFile", help="Comma separated string of labels for ratio files")
parser.add_argument("--dataFromITSDAQ", action="store_true", help="Input data is of txt format from ITSDAQ output")
parser.add_argument("--dataFromCSV", action="store_true", help="Custom data in CSV format")

# Plotting options
parser.add_argument("--NoInteractiveMode", action="store_true", default=True, help="Run matplotlib in non-interactive mode")
parser.add_argument("--ol", type=str, default="./plots", help="Output location for plots.") 

# Save the argument values
options = parser.parse_args()