# BNL coldboxes 

The purpose of this document is to keep track of useful information / commands for the BNL testing clean room coldboxes.

sshing into raspberry pis:

```
ssh pi@169.254.115.144 # Sshing into the rasp pi at coldbox 1 (closest to testing cold room door, used for IV curves)

ssh pi@10.2.250.137 # rasp pi at coldbox 2 (second from the door, currently used for thermocycling)
```

Files that often need to be touched in case IP addresses get switched up / changed:

```
coldbox_controller_webgui/configs/config_BNL.conf
coldjiglib2/configs/US_Barrel/bnl_influx.ini
```

Grafana data source, influx DB, URL for box 2 before we switched computers: `http://169.254.115.145:8086`

## Coldbox 1 (closest to testing room entrance)

Currently sshing into pi with `ssh pi@10.2.250.137`. This is via wifi rather than ethernet as this is the IP of the wlan connection of the pi. 

RaspberryPi ifconfig output:

```
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet6 fe80::e98:c924:3364:af2  prefixlen 64  scopeid 0x20<link>
        ether dc:a6:32:49:3f:22  txqueuelen 1000  (Ethernet)
        RX packets 8  bytes 672 (672.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 98  bytes 8688 (8.4 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 707577  bytes 241051094 (229.8 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 707577  bytes 241051094 (229.8 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wlan0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.2.250.137  netmask 255.255.0.0  broadcast 10.2.255.255
        inet6 fe80::c8b1:ffc1:4d12:cdf7  prefixlen 64  scopeid 0x20<link>
        ether dc:a6:32:49:3f:23  txqueuelen 1000  (Ethernet)
        RX packets 578117  bytes 215017057 (205.0 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 714205  bytes 190753220 (181.9 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

receptionteststation machine ifconfig output:

```
enp0s31f6: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 168.254.115.145  netmask 255.255.0.0  broadcast 168.254.255.255
        inet6 fe80::ffeb:7cb0:7d1b:ff72  prefixlen 64  scopeid 0x20<link>
        ether b0:4f:13:09:52:7a  txqueuelen 1000  (Ethernet)
        RX packets 6  bytes 674 (674.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 117  bytes 17063 (16.6 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 16  memory 0x70280000-702a0000  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 194  bytes 31682 (30.9 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 194  bytes 31682 (30.9 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:36:4b:55  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wlp0s20f0u7: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.2.252.61  netmask 255.255.0.0  broadcast 10.2.255.255
        inet6 fe80::eb76:41e9:8391:c2c0  prefixlen 64  scopeid 0x20<link>
        ether 9c:ef:d5:fc:28:48  txqueuelen 1000  (Ethernet)
        RX packets 592001  bytes 144114328 (137.4 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 486199  bytes 195292530 (186.2 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

## Coldbox 2 (second from testing room entrance)

Current ssh into coldbox 2 is via wifi (wlan0) rather than ethernet (eth0). Ssh from qcbox machine into coldbox 2 raspberry pi with: `ssh pi@10.2.231.5`. This relies on `wlan0` of pi from `ifconfig` to read:

```
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 169.254.115.144  netmask 255.255.0.0  broadcast 169.254.255.255
        inet6 fe80::9b1e:7c0c:625:6a19  prefixlen 64  scopeid 0x20<link>
        ether dc:a6:32:1b:55:52  txqueuelen 1000  (Ethernet)
        RX packets 28  bytes 1680 (1.6 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 99  bytes 15273 (14.9 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 10326  bytes 4427872 (4.2 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 10326  bytes 4427872 (4.2 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wlan0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.2.231.5  netmask 255.255.0.0  broadcast 10.2.255.255
        inet6 fe80::2067:19eb:3714:70ed  prefixlen 64  scopeid 0x20<link>
        ether dc:a6:32:1b:55:53  txqueuelen 1000  (Ethernet)
        RX packets 10426  bytes 2089494 (1.9 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 14722  bytes 16918288 (16.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

and `ifconfig` for the qcbox to read:

```
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        ether 18:60:24:a5:5d:18  txqueuelen 1000  (Ethernet)
        RX packets 12  bytes 768 (768.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 253  bytes 21206 (20.7 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 16  memory 0xb0100000-b0120000  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 2186  bytes 163713 (159.8 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2186  bytes 163713 (159.8 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:a2:0b:2f  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wlp0s20f0u10: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.2.243.0  netmask 255.255.0.0  broadcast 10.2.255.255
        inet6 fe80::9eef:d5ff:fefc:13da  prefixlen 64  scopeid 0x20<link>
        ether 9c:ef:d5:fc:13:da  txqueuelen 1000  (Ethernet)
        RX packets 85653  bytes 59844768 (57.0 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 76432  bytes 17677376 (16.8 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

then for the ColdBoxGUI you would navigate to `http://10.2.231.5:5003` and ensure that `coldbox_controller_webgui/configs/config_BNL.conf` (config file in the coldbox S/W) sets the `gui_server` variable to `10.2.231.5` for the GUI to work, and to get Grafana to work make sure the `dash` is set to `http://10.2.231.5:3000/d/ZKemEcPGk/coldjig?orgId=1&refresh=5s`, and in `coldjiglib2/configs/US_Barrel/bnl_influx.ini` make sure the `IP` is set to `10.2.231.5`.

## Grafana

For issues with grafana, e.g. to change the data source (sometimes you may have to change the IP of the InfluxDB data source if you are moving computers and raspbery pis around, or are changing IPs by hand), one way to access is typically port 3000 of the IP the coldbox config is set to upload to, example: `http://169.254.115.144:3000/`. Then to change data source configs, log in (check bottom left hand options for logging in): 

```
username: admin
password: Ask someone who knows :) (Abe should know)
```