from .. import Hardware 
import logging
import serial
from serial import *
from .SPS_lib import SPS_Chiller
import logging
logger = logging.getLogger(__name__)


class SPS(Hardware.Hardware):
    def __init__(self,name):
        super().__init__(name)
        self.__temperature = 0.0
        self.__pump = None
        self.__chiller_state='OFF'
        self.__name = name


    def initialise(self,parameters) :
        """
        Initialise hardware
        """
        self.__usb_port= parameters['Location']
        try:
            self.__chiller=SPS_Chiller(self.__usb_port)
        except serial.SerialException:
            print("can't open the chiller, is it connected on "+self.__usb_port+" ?")
        self.__chiller.SetRampRate(0.0)
        #self.__chiller.TurnOn()
    
    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict['chiller.set_temperature'] = 20.0
        data_dict['chiller.temperature'] = 20.0
        data_dict['chiller.set_state']='OFF'
        data_dict['chiller.read_state']=self.__chiller_state
 

        
    def Read(self, data_dict) :
        """
        Read out all values and update data_dict
        """
        data_dict['chiller.temperature'] = self.__chiller.GetTemperature()
        data_dict['chiller.read_state'] = self.__chiller_state
        self.__chiller.SetRampRate(0.0)


    def Set(self, data_dict) :
        """
        Read data_dict for new chiller settings & apply them
         """
        self.__chiller.SetTemperature(data_dict['chiller.set_temperature'])
        if data_dict['chiller.set_state']=='ON':# and self.__chiller_state=="OFF":
            self.__chiller.TurnOn()
            self.__chiller_state='ON'
            self.__chiller.SetRampRate(0.0)
        elif data_dict['chiller.set_state']=='OFF':# and self.__chiller_state=="ON":
            self.__chiller.TurnOff()
            self.__chiller_state='OFF'
        #self.set_pump(data_dict['chiller.set_pump'])
        self.__chiller.SetRampRate(0.0)


    def Shutdown(self) :
        """
        Called to release resources
        """
        self.__chiller.TurnOff()
        self.__chiller_state='ON'
        logging.info(f'Shutting down {self.name}')
        #print(f'Shutting down {self.name}')
        # CALL ANY METHOD TO RELEASE RESOURCES IF NEEDED


    def set_temperature(self,temperature) :
        logging.debug(f"SPS - set tempetature {temperature} ")
        self.__chiller.SetTemperature(temperature)
        self.__chiller.SetRampRate(0.0)

    def temperature(self) :
        temp_now= self.__chiller.GetTemperature()
        logging.debug(f"SPS - temperature is {temp_now}")
        self.__chiller.SetRampRate(0.0)
        return temp_now
   
    def set_pump(self,pump) :
        #might have to add something later

        pass
   
    def pump(self) :
        """
        is this something for the SPS chiller?
        """
        pass

    def set_flow(self,flow) :
        pass

    def flow(self) :
        pass
   
    def alarm(self) :
        pass
