#include "runtimeCommon.h"

#include "TFitResult.h"
#include "TMemFile.h"

#include "AMAC.h"
#include "AMAC_common.h"
#include "AMAC_detmap.h"
#include "AMACStar.h"
#include "AMACv2_final.h"
#include "AMACStar_0514.h"

#ifdef ITSDAQ_FULL_LINK
#include "AMACv2_final_0514.h"
#include "AMACv2_final_65420.h"
#include "AMACv2_final_lpGBT.h"

#include "AMACStar_0514.h"
#include "AMACStar_65420.h"
#include "AMACStar_lpGBT.h"
#endif

#include "../khvdll/TIVScan.h"

struct IVRecord {
  float hv_current;
  float hv_voltage;
  float hv_set_voltage;

  float amac_current_mean;
  float amac_current_rms;
  float amac_voltage_mean;
  float amac_voltage_rms;
  int amac_range;
};

static const float NO_BD_CONST = 9.99e37;

std::vector<std::unique_ptr<Json> >
    AMAC_IVScan(Float_t vStart, Float_t vStop, Float_t vStep, Float_t vEnd,
                Float_t iLimit, Float_t iSlope, Float_t iLimitAMAC, Int_t msDelay, Int_t nRepeat, Bool_t rampDownAtCompliance, Int_t nDR) {

  std::vector<std::unique_ptr<Json> > jsonvector;
  jsonvector.clear();

  auto vMax = std::max(std::max(vStart, vStop), vEnd);
  if(vMax > 551) {
    std::cout << "AMAC_IVScan: ABORT: Maximum voltage limited to 550\n";
    return jsonvector;
  }

  // Check length of map vector matches expected number of AMACs
  // if(hvmap_final.size() != AMAC_final.size()){
  //   std::cout << "AMAC_IVScan: ABORT: map vector has wrong length\n";
  //   return jsonvector;
  // }

  std::vector<TDCSDevice*> hvvector;
  for (Int_t i = 0; i < nHVSupplies; i++) {
    hvvector.push_back(HVSupplies[i]);
  }

  // Calculate the number of drain resistors associated with with HV channel
  std::vector<int> drvector;
  std::vector<bool> hvInCompliance;
  for(size_t i=0; i<hvvector.size(); i++){
    drvector.push_back(0);
    hvInCompliance.push_back(false);
  }

  Int_t nMapped = 0;
  for(size_t a=0; a<AMAC_final.size(); a++){
    const auto &info = AMAC_final[a];
    if((info.hvmap>-1) && (info.hvmap < (int) hvvector.size())){
      Int_t myDR = 0;
      if (nDR<0){ // auto
        if((info.nabcs==16)||(info.nabcs==18)){ // R4 or R5
          myDR = 2;
        }else{
          myDR = 1;
        }
      }else{ // manual
        myDR = nDR;
      }
      drvector[info.hvmap]+= myDR;
      nMapped++;
    }
  }

  std::cout << "AMAC_IVScan: drain resistor vector:";
  for(size_t a=0; a<drvector.size(); a++){
    std::cout << drvector[a] << ", ";
  }
  std::cout << std::endl;

  if(nMapped == 0){
    std::cout << "AMAC_IVScan: ABORT: AMACs have not been mapped to HV channels\n";
    return jsonvector;
  }

  if(AMAC_common_checkUnique()==false){
    std::cout << "AMAC_IVScan: ABORT: AMAC ids are not unique\n";
    return jsonvector;
  }

  e->scannum ++;

  // TURN OFF DCDC JUST FOR AMAC OP AMP SATURATED MODULES
  AMACStar_DCDC(-1,0);

  {
    std::cout << "Calibrating AMAC\n";
    if(g != nullptr) {
      switch(load_amac_type) {
        case POWERCHIP_AMAC2:
#ifndef ITSDAQ_FULL_LINK
          gROOT->ProcessLine("AMACv2_final_0514_calScan(1)");
#else
          AMACv2_final_0514_calScan(1);
#endif
          break;
        case POWERCHIP_AMAC_STAR:
#ifndef ITSDAQ_FULL_LINK
          gROOT->ProcessLine("AMACStar_0514_calScan(1)");
#else
          AMACStar_0514_calScan(1);
#endif
          break;
      }
    }
    else if(h != nullptr) {
      switch(load_amac_type) {
        case POWERCHIP_AMAC2:
#ifndef ITSDAQ_FULL_LINK
          gROOT->ProcessLine("AMACv2_final_65420_calScan(1)");
#else
          AMACv2_final_65420_calScan(1);
#endif
          break;
        case POWERCHIP_AMAC_STAR:
#ifndef ITSDAQ_FULL_LINK
          gROOT->ProcessLine("AMACStar_65420_calScan(1)"); 
#else
          AMACStar_65420_calScan(1);
#endif
          break;
      }
    }
    else if(e->HsioCheckFirmware("FW_CONNECTION_EOS")){
      switch(load_amac_type) {
        case POWERCHIP_AMAC2:
#ifndef ITSDAQ_FULL_LINK
          gROOT->ProcessLine("AMACv2_final_lpGBT_calScan(1)");
#else
          AMACv2_final_lpGBT_calScan(1);
#endif
          break;
        case POWERCHIP_AMAC_STAR:
#ifndef ITSDAQ_FULL_LINK
          gROOT->ProcessLine("AMACStar_lpGBT_calScan(1)"); 
#else
          AMACStar_lpGBT_calScan(1);
#endif
          break;
      }
    }
#ifndef ITSDAQ_FULL_LINK
    gROOT->ProcessLine("AMAC_internalCalibrations()"); 
#else
    AMAC_internalCalibrations();
#endif
  }

  int amac_readings = 1000;

	
  if(vStep>0) {
    // supplied as positive increment
    // flip polarity if vStop<vStart
    if(vStop<vStart) vStep = -vStep;
  } else {
    // supplied as negative increment
    // flip polarity if vStop>vNow
    if(vStop>vStart) vStep = -vStep;
  }

  Bool_t done = false;
  Bool_t hitComp = false;
  Float_t vRequest = vStart;

  std::vector<IVRecord> iv_amac;
  std::vector<bool> amacInCompliance;
  std::vector<std::vector<IVRecord> > iv_data;
  for (uint32_t iAmac =0; iAmac < AMAC_final.size(); iAmac++){
    iv_data.push_back(iv_amac);
    amacInCompliance.push_back(false);
  }


  std::string ok_string;

  TIVScan scanner = TIVScan(hvvector, drvector);
  e->Signals().Connect("Abort()", "TIVScan",
                       &scanner, "Abort()");

  bool append = false;
  while((!done)&&(!e->abort)&&(!hitComp) && ok_string.empty()) {
    if(vStep>0){
      if(vRequest>=vStop){
        vRequest = vStop;
        done = true;
      }
    }else{
      if(vRequest<=vStop){
        vRequest = vStop;
        done = true;
      }
    }

    // Let the (non AMAC) IV scan handle the ramping
    scanner.IVScanWithPB(vRequest,vRequest,vStep,vRequest,iLimit,iSlope,msDelay,append, 0);

    // Measure AMAC IDET (op-amp) offsets if first point is 0V)
    if((vRequest == 0.0)&&(append == false)){
      int32_t nRanges = 2;
      std::cout << "Determining AMAC IDET offsets for " << nRanges << " ranges whilst we are at 0V" << std::endl;

      if(load_amac_type==POWERCHIP_AMAC2){
        AMACv2_final_readIDETOffsets(-1, amac_readings, nRanges);
      }

      else if (load_amac_type==POWERCHIP_AMAC_STAR){
        AMACStar_readIDETOffsets(-1, amac_readings, nRanges);
      }
    }
    append = true;

    // Did any channel hit compliance?
    for(size_t i=0; i<hvvector.size(); i++){
      if(scanner.getHitIt(i)){
        hvInCompliance[i] = true;
        hitComp = true;
      } 
    }

    uint32_t nReadings = 1;

    // repeat stability test at stop voltage
    if((done)&&(!e->abort)) {
      nReadings += nRepeat;

      std::cout << "AMAC_IVScan: will repeat final voltage for " << nRepeat << " more measurements\n";
    }


  
    for (uint32_t iReading = 0; iReading < nReadings && ok_string.empty(); iReading++) {

      bool printValue = false;
      bool doAutoRange = true;

      if(load_amac_type==POWERCHIP_AMAC2){
        // NB returns measured current, but we use stored value instead
        AMACv2_final_readIDET(-1, amac_readings, printValue, doAutoRange);
      }

      else if (load_amac_type==POWERCHIP_AMAC_STAR){
        AMACStar_readIDET(-1, amac_readings, printValue, doAutoRange);
      }

      else {//not implemented
        std::cout<<"AMAC_IVScan: ERROR: Unrecognized AMAC type"<<std::endl;
        if(hvvector.size()>0) {
          return jsonvector;
        }
      }

      for (uint32_t iAmac =0; iAmac < AMAC_final.size(); iAmac++){
        const auto &info = AMAC_final[iAmac];

        // use amac to check if we've surpassed compliance
        // note that iLimit is in uA and sensorImeanA is in A
        if (info.sensorImeanA*1e6>iLimitAMAC) {
	  std::cout<<"AMAC_IVScan: WARNING: AMAC " << iAmac << " in compliance at " << vRequest << "V (current ";
          std::cout<< info.sensorImeanA*1e6 << "uA vs limit " << iLimitAMAC << "uA)" << std::endl;
          amacInCompliance[iAmac] = true;
          hitComp = true;
        }

        IVRecord record;
 
        // Power supply stuff
        uint32_t iHV = info.hvmap;
        record.hv_set_voltage = vRequest;
        record.hv_voltage = scanner.getLastX(iHV);
        record.hv_current = scanner.getLastY(iHV);

        record.amac_range = info.sensorIrange;
        record.amac_voltage_mean = info.sensorImeanmV;
        record.amac_voltage_rms = info.sensorIrmsmV;

        //current measured by amac in A
        record.amac_current_mean = info.sensorImeanA * 1e9;
        record.amac_current_rms = info.sensorIrmsA * 1e9;

        iv_data[iAmac].push_back(record);
      }
    }

    vRequest += vStep;
  }

  if((hitComp == false) && (vEnd!=vStop) && (!e->abort)){
    std::cout<<"AMAC_IVScan: INFO: ramping to endpoint " << vEnd << "V\n";
    
    // Let the (non AMAC) IV scan handle the ramping
    scanner.IVScanWithPB(vStop,vEnd,(vStop-vEnd),vEnd,iLimit,iSlope,msDelay,append,0);
    for(size_t i=0; i<hvvector.size(); i++){
      if(scanner.getHitIt(i)){
        hvInCompliance[i] = true;
        hitComp = true;
      } 
    }
    
  }

  if (((hitComp == true) && (rampDownAtCompliance == true)) || (e->abort)){
    if(e->abort){
      std::cout<<"AMAC_IVScan: WARNING: ramping down due to ABORT\n";
    }else{
      std::cout<<"AMAC_IVScan: WARNING: ramping down due to compliance violation(s)\n";
    }

    Int_t nOK=0;
    for(size_t iHV=0; iHV<hvvector.size(); iHV++){
      hvvector[iHV]->RampBG(0,-1,2); // ramp down at 20V/s, compliance unchanged
      nOK++;
    }
    if(nOK != hvvector.size()) return jsonvector;

    // Wait for the threads to complete
    Int_t nBusy = 99;
    while(nBusy){
      nBusy = 0;
      for(size_t iHV=0; iHV<hvvector.size(); iHV++){
        if(hvvector[iHV]->Busy()) nBusy++;
      }
    }
  }
 
  if(hitComp || e->abort) {
    ok_string += "ABORT";
    if(hitComp) {
      ok_string += " (hit compliance)";
    }
    if(e->abort) {
      ok_string += " (user request)";
    }
  } else {
    ok_string = "Successful";
  }
  std::cout << "AMAC_IVScan completed: " << ok_string << std::endl;

  e->Signals().Disconnect("Abort()");

  std::string tmpFile = Form("%s/data/temp_IVinfo.root",e->sct_var);
  gSystem->Unlink(tmpFile.c_str());
  TMemFile tmpfile(tmpFile.c_str(), "RECREATE");
  e->WriteScanInfo();
  {
    for (int n=0; n<=e->GetLastPresentId(); n++){

      char mname[32];
      e->GetModuleName(n,mname);
      //auto tn = e->m[n]->name;
      auto mns = std::unique_ptr<TObjString>(new TObjString(mname));
      tmpfile.cd();
      mns->Write("ModuleName");
    }
  }

  for(size_t iAmac = 0; iAmac < AMAC_final.size(); iAmac++){

    int module = -1;
    auto j = AMAC_detmap_firstDet(iAmac);
    if(j == -1) {
      std::cout<<"AMAC_IVScan: WARNING: AMAC mapping is not known.  Did you run AutoConfig()?\n";
    }else{
      module = j;
    }
    if(module == -1){
      std::cout<<"AMAC_IVScan: WARNING: using iAMAC instead\n";
      module = iAmac;
    }
      
    // Can change things later
    int pass = 0;
    int problem = 0;
    std::unique_ptr<Json> json_data
      = MakeResultsJson("AMAC_IVSCAN",
                        &tmpfile, 
                        module, e->runnum, e->scannum,
                        pass, problem);

    {
      // Copy system info (git tag etc from ROOT file)
      TDirectoryFile *info = (TDirectoryFile*) tmpfile.Get("system_info");
      TList *keyList = (TList*) info->GetListOfKeys();
      TIter next(keyList);
      TKey *key;
      while ((key=(TKey*)next())) {
        TObjString *prop = (TObjString*)key->ReadObj();
        json_data->WriteString(Form("/properties/system_info/%s", key->GetName()),
                             prop->GetName());
      }
    }

    json_data->WriteInteger("/properties/AMAC_READINGS",amac_readings);
    // Record the one that might cause compliance defect
    json_data->WriteInteger("/properties/scan_info/PS_ILIMIT", iLimit);
    json_data->WriteInteger("/properties/scan_info/AMAC_ILIMIT", iLimitAMAC);

    for(size_t i=0; i<iv_data[iAmac].size(); i++) {
      auto &r = iv_data[iAmac][i];
      json_data->WriteFloat(Form("/properties/scan_info/SET_VOLTAGE/%d", (int)i), r.hv_set_voltage);
      json_data->WriteFloat(Form("/results/VOLTAGE/%d", (int)i), r.hv_voltage); //supply voltage
      json_data->WriteFloat(Form("/results/PS_CURRENT/%d", (int)i), r.hv_current); //supply current (in uA)

      //current measured by amac
      json_data->WriteFloat(Form("/results/CURRENT/%d", (int)i), r.amac_current_mean);
      json_data->WriteFloat(Form("/results/CURRENT_RMS/%d", (int)i), r.amac_current_rms); 
      // These are the actual measurement, but current is through constant resistor
      // json_data->WriteFloat(Form("/amac_voltage_mean/%d", (int)i), r.amac_voltage_mean);
      // json_data->WriteFloat(Form("/amac_voltage_rms/%d", (int)i), r.amac_voltage_rms);
      json_data->WriteInteger(Form("/properties/AMAC_CURRENT_RANGE/%i",(int)i),r.amac_range);
    }
 
    json_data->WriteString("/final_status", ok_string);

    jsonvector.push_back(std::move(json_data));
  }

  // TURN ON DCDC JUST FOR AMAC OP AMP SATURATED MODULES
  AMACStar_DCDC(-1,1);

  return jsonvector;
}

//Function to calculate BDV, adapted from python function in sensor IV upload scripts
float BDVcalc(std::vector<double> I, std::vector<double> V, const unsigned int sm_window=2, float bd_limit=5.5, 
Bool_t allow_running_bd=true) {

	//skip zeros, fix offset, and take absolute value
	int j=0;
  float offset = I[0];
	for (size_t i=0; i<I.size();i++){
		V[i]=fabs(V[i]);
		I[i]=fabs(I[i]-offset);
		if (I[i]!=0 || V[i]!=0){
			j=j+1;}
	}
	if (j==0){
		std::cout<<"No data"<<std::endl;
		// Assume the worst
		return 0;
        } 


	int idx1=0;
	for (size_t i=0; i<I.size();i++){
		if (I[i]!=0 || V[i]!=0){
			V[idx1] = V[i]; //after this, we only want to call V and I up to index j
			I[idx1] = I[i];
			idx1++;
		}
	}	
	
	//create an array of averages of voltages and currents and try to do fit to each subset
	std::vector<float> V_avg(j-sm_window+1);
	std::vector<float> I_avg(j-sm_window+1);
	std::vector<float> dIdV(j-sm_window+1);

		for (size_t i=0;i<j-sm_window+1;i++){
		float V_sub[sm_window];
		float V_sum=0;
		float I_sub[sm_window];
		float I_sum=0;
		TGraph g;
			for (size_t n=i;n<i+sm_window;n++){
				V_sub[n-i]=V[n]; 
				V_sum +=V_sub[n-i];
				I_sub[n-i]=I[n];
				I_sum +=I_sub[n-i];
				g.SetPoint(n-i,V_sub[n-i],I_sub[n-i]);
			}
		V_avg[i]=V_sum/sm_window;
		I_avg[i]=I_sum/sm_window;
		TFitResultPtr fit_result = g.Fit("pol1","Q S");
		float slope = fit_result->Parameter(1);
		dIdV[i]=slope;	
		}

	//get running BDV and compare
	std::vector<float> bd_limit_running(j-sm_window+1);
	for (size_t i=0;i<j-sm_window+1;i++){
		if (allow_running_bd && V_avg[i]>500){
			bd_limit_running[i]=bd_limit+(V_avg[i]-500)/100;
		}
		else{bd_limit_running[i]=bd_limit;}
	}

	Int_t n=0; //get length of V_avg_BD
	for (size_t i=0;i<j-sm_window+1;i++){
		if (dIdV[i]/(I_avg[i]/V_avg[i])>bd_limit_running[i]){
		n=n+1;}
	}
	
	std::vector<float> V_avg_BD(n); 
	Int_t m=0; //index for filling V_avg_BD
	Int_t BDVloc=-1; //index in V_avg where BDV criteria is first met
	for (size_t i=0;i<j-sm_window+1;i++){
		if (dIdV[i]/(I_avg[i]/V_avg[i])>bd_limit_running[i]){
			V_avg_BD[m]=V_avg[i];
			m=m+1;
			if (BDVloc==-1){BDVloc=i;}
		}
	}

	//Estimate BDV
	if (m==0){
		// No breakdown found
		return NO_BD_CONST;
        }

	if (dIdV[0]/(I_avg[0]/V_avg[0])>bd_limit_running[0]){
	return V[0];} //if breakdown occurs at first point, just choose this point as the bdv
	
	std::vector<float> BDV(n);
	for (Int_t i=0;i<n;i++){
		BDV[i]=(V_avg_BD[i]+V_avg[BDVloc-1])/2;
		BDVloc=BDVloc+1;
	}
	
	return BDV[0];
}

//Function to calculate I_500V, normalized by active area (nA/cm^2)
float I_500Vcalc(std::vector<double> I, std::vector<double> V,const char* moduletype){
	
  float offset = I[0];
	for (size_t i=0; i<I.size();i++){
		V[i]=fabs(V[i]);
		I[i]=fabs(I[i]-offset);
	}
	
	float activearea; //active area for each type of module in cm^2
	if (strncmp(moduletype,"BARREL_LS_MODULE", 16)){activearea=93.63;}
	else if (strncmp(moduletype,"BARREL_SS_MODULE", 16)){activearea=93.63;}
	else if (strncmp(moduletype,"R0", 2)){activearea=89.9031;}
	else if (strncmp(moduletype,"R1", 2)){activearea=89.0575;}	
	else if (strncmp(moduletype,"R2", 2)){activearea=74.1855;}	
	else if (strncmp(moduletype,"R3", 2)){activearea=80.1679;}	
	else if (strncmp(moduletype,"R4", 2)){activearea=2*87.4507;}	
	else if (strncmp(moduletype,"R5", 2)){activearea=2*91.1268;}
	else {
		std::cout<<"Module type not recognized, I_500V not normalized by active area"<<std::endl;
		activearea=1;
	}
	

	//rounding to nearest multiple of 10 to find index of V=500V
	int n=0;
	for (size_t i=0; i<I.size();i++){
		int roundedn;
		int intn=floor(V[i]);
		float decn=V[i]-floor(V[i]);
		if (intn%10<5){
			roundedn=(intn/10)*10;
		}
		else if (n%10==5){
			if (decn>0) {roundedn=((intn+10)/10)*10;}
			else {roundedn=(intn/10)*10;}
		}		
		else {roundedn=((intn+10)/10)*10;}
		if (roundedn==500){
			n=i;
			break;
		}
		
	}
	
	float I_500V;
	if (n==0){
		std::cout<<"Data not taken at 500V"<<std::endl;
		I_500V=0.0;
	}
	else {I_500V=I[n]/activearea;}
	
	return I_500V;
	
	
}

//Function that creates output file, also takes a few amac measurements for the output file
void AMACScanOutput(uint32_t iAmac, const char* moduleSN, const char* testtype,const char* sensor_type,const char* moduleStage,
	    Float_t temp,Float_t humidity,const char* comments, Json * amacdata){

  //this function needs to be revisited to output the correct format and add analysis
  int module = AMAC_detmap_firstDet(iAmac);
  if(module == -1) {
    std::cout<<"AMACScanOutput: WARNING: AMAC mapping is not known.  Did you run AutoConfig()?\n";

    std::cout<<"AMACScanOutput: WARNING: using iAMAC instead\n";
    module = iAmac;
  }

  //char moduleSN[32];
  //e->GetModuleName(module, moduleSN);

//get date and time
     std::string datetime = NowTimeString();

//get amac fuse id and amac temperature values
     std::string efuseid;
     float x_hybrid_temp = -999.0f;
     float y_hybrid_temp = -999.0f;
     float pb_temp = -999.0f;
     float ptat_temp = -999.0f;
     if(load_amac_type==POWERCHIP_AMAC2){
        uint32_t fuse_reg = AMACv2_final_readReg(iAmac,31,false).at(0);
        efuseid = Form("%06x", fuse_reg);
        AMAC_reportADCs(iAmac);

        TModuleDCS *dcs = e->m[module]->DCS;
        x_hybrid_temp = dcs->Value("AMAC_NTCx");
        y_hybrid_temp = dcs->Value("AMAC_NTCy");
        pb_temp = dcs->Value("AMAC_NTCpb");

        // PTAT not implemented at the moment
      }


      else if (load_amac_type==POWERCHIP_AMAC_STAR){
	AMACStar_writeReg(iAmac, 32,0x10000);
	AMACStar_readPTATzero(iAmac,100,1);

        uint32_t fuse_reg = AMACStar_readReg(iAmac,31,false).at(0);
        efuseid = Form("%06x", fuse_reg);
        AMAC_reportADCs(iAmac);

        TModuleDCS *dcs = e->m[module]->DCS;
        x_hybrid_temp = dcs->Value("AMAC_NTCx");
        y_hybrid_temp = dcs->Value("AMAC_NTCy");
        pb_temp = dcs->Value("AMAC_NTCpb");
        ptat_temp = dcs->Value("AMAC_PTAT");
      }

     int defect_index = 0;
	  
//get results from I_500V calculation
	auto arrayV = amacdata->ReadFloatArray("/results/VOLTAGE");
	auto arrayI = amacdata->ReadFloatArray("/results/CURRENT");
	auto arrayEI = amacdata->ReadFloatArray("/results/CURRENT_RMS");
        std::vector<double> arrayEV;
        arrayEV.clear();

        for(size_t j=0; j<arrayV.size(); j++){
          std::cout << j << " " << arrayV[j] << " " << arrayI[j] << " " << arrayEI[j] << std::endl;
          arrayEV.push_back(0.5); //??
        } 

	auto final_status = amacdata->ReadString("/final_status");
	amacdata->RemoveFromObject("", "final_status");

	bool PassStatus = true;
	bool ProblemStatus = false;

	if (final_status != "Successful") {
	  PassStatus = false;
          ProblemStatus = true;

	  auto props = ListDefectJson(*amacdata, defect_index++,
				      "IV_STATUS",
				      "IV did not complete successfully");

	  amacdata->WriteString(props + "/info", final_status);
	}

	float I_500=I_500Vcalc(arrayI, arrayV, sensor_type);
	  
//get results from BDV calculation
	float BDV = BDVcalc(arrayI, arrayV);
	if (BDV==NO_BD_CONST || BDV>500) {PassStatus = true;}
	else {
          PassStatus = false;

          auto props = ListDefectJson(*amacdata, defect_index++,
                                      "BREAKDOWN",
                                      "Breakdown early");

          amacdata->WriteFloat(props + "/VBD", BDV);
        }

      // skip first 2 chars if they are "SN"
      std::string compSN = moduleSN;
      if(compSN.find("SN")==0){
        compSN = compSN.substr(2);
      }

      amacdata->WriteString("/component", compSN.c_str());
      amacdata->WriteString("/testType",testtype);
      amacdata->WriteString("/date",datetime);
      amacdata->WriteBool("/passed",PassStatus);
      amacdata->WriteBool("/problems", ProblemStatus);
      amacdata->WriteString("/properties/sensor_type",sensor_type);
      if(efuseid.size()) {
        amacdata->WriteString("/properties/det_info/AMAC_FuseID",efuseid); 
      }

      // not null
      if(comments && !std::string(comments).empty()) {
        amacdata->WriteString("/comments/0",comments);
      }

      amacdata->WriteFloat("/results/TEMPERATURE",temp); 

      amacdata->WriteFloat("/results/I_500V",I_500);
      amacdata->WriteFloat("/results/HUMIDITY",humidity); 
      amacdata->WriteFloat("/results/VBD",BDV);

      // Record expected module stage in filename
      std::string basename = Form("%s_%s", moduleSN, moduleStage);

      SaveJsonResults(*amacdata, basename.c_str(), e->runnum, e->scannum);

      //plot the IV automatically for convenience
      double* pointV = &arrayV[0];
      double* pointI = &arrayI[0];
      double* pointEV = &arrayEV[0];
      double* pointEI = &arrayEI[0];
	  
      std::unique_ptr<TGraph> ivGraph(new TGraphErrors(arrayV.size(),pointV,pointI,pointEV,pointEI));
      std::unique_ptr<TCanvas> cIV(new TCanvas());

      ivGraph->Draw();
      ivGraph->GetHistogram()->SetXTitle("Bias Voltage (V)");
      ivGraph->GetHistogram()->SetYTitle("Current (nA)");

      if(efuseid.size()==0) {
        efuseid = "unknown";
      }
      std::string plotname = Form("AMAC %d (%s): %s %s %d-%d", iAmac, efuseid.c_str(), moduleSN, moduleStage, e->runnum, e->scannum);
      ivGraph->SetTitle(plotname.c_str());

      std::string fname(Form("%s/ps/IVScan_%s_%s_%d_%d.pdf",e->sct_var,moduleSN,moduleStage,e->runnum, e->scannum));
      std::cout<<"Saving pdf to file"<<fname<<std::endl;
      cIV->Print(fname.c_str(),"pdf");

      return;
}
