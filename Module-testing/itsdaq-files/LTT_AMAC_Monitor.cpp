#ifdef ITSDAQ_BUILD_CHECK
#include "AMACv2_final.h"
#include "AMACStar.h"
#include "AMACStar_0514.h"
#include "ST.h"

#include "runtimeCommon.h"

#endif

void ReadAmac2() {
  AMACv2_final_reportADCs(-1);
}

void ReadAmacStar() {
  AMACStar_reportADCs(-1);
}

void Influx_AMAC_READ() {
  std::cout << "Start AMAC monitoring loop\n";
  while(1){
    sleep(5);

    if(load_amac_type == POWERCHIP_AMAC2) {
      ReadAmac2();
    }
    else
      if(load_amac_type == POWERCHIP_AMAC_STAR) {
    
    ReadAmacStar();
    } else {
    std::cout << "AMAC is not configured, aborting\n";
    break;
      }
  }
}

  void StartupAmac2() {
  // Cal still needs indirection at the moment
  if(g != nullptr) {
    loadmacro("AMACv2_final_0514");
  }

  AMACv2_final_configureAMACs(1);
  if(g != nullptr) {
    gROOT->ProcessLine("AMACv2_final_0514_calScan(1)");
  }
  AMACv2_final_internalCalibrations();
  }

void StartupAmacStar() {
  // Cal still needs indirection at the moment
  if(g != nullptr) {
    loadmacro("AMACStar_0514");
  }
  
  AMACStar_configureAMACs(1);
  if(g != nullptr) {
    gROOT->ProcessLine("AMACStar_0514_calScan(1)");
  }
  AMACStar_internalCalibrations();
}

void Influx_AMAC_Startup() {
  std::cout << "Startup for AMAC monitoring\n";

  if(load_amac_type == POWERCHIP_AMAC2) {
    StartupAmac2();
  } else if(load_amac_type == POWERCHIP_AMAC_STAR) {
    
    StartupAmacStar();
  }
}

void LTT_AMAC_Monitor()
{
    Stavelet();
       	
    //AMACStar_initialiseAMACs({0},{0}) ; // hardcoded - to be changed 
    AMACStar_initialiseAMACs({0,0,0,0},{0,1,2,3}) ; // hardcoded - to be changed 
    
    Influx_AMAC_Startup();
    Influx_AMAC_READ();
}
