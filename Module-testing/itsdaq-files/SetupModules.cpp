// 26 January 2023 
// Abraham Tishelman-Charny
//
// The purpose of this macro is to setup modules for testing.

void SetupModules(int N_modules){
	loadmacro("AMAC_common");
	loadmacro("AMACStar");
	loadmacro("AMACStar_0514");
	if(N_modules==1) AMACStar_initialiseAMACs({0},{0});
	else if(N_modules==2) AMACStar_initialiseAMACs({0,0},{0,1});
	else if(N_modules==3) AMACStar_initialiseAMACs({0,0,0},{0,1,2});
	else if(N_modules==4) AMACStar_initialiseAMACs({0,0,0,0},{0,1,2,3});
	AMACStar_configureAMACs(1);
	AMACStar_configureAMACs(0);
	AMACStar_0514_calScan(1);
	AMACStar_internalCalibrations();
	AMACStar_configureAMACs(1);
	std::cout << "Calibration complete." << std::endl;
}
