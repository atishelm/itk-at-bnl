"""
14 April 2023
Abraham Tishelman-Charny

The purpose of this python module is to readout SHT temperatures and humidities for debugging purposes.
"""

import smbus
import time

bus_i = 3
bus = smbus.SMBus(int(bus_i))
address=0x44

bus.write_byte_data(address, 0x24, 0x00)

#self.__bus.write_byte_data(self.__address, 0x24, 0x00)
time.sleep(0.1) #This is so the sensor has tme to preform the mesurement and write its registers before you read it

# Read data back, 8 bytes, temperature MSB first then lsb, Then skip the checksum bit then humidity MSB the lsb.

data0 = bus.read_i2c_block_data(address, 0x00, 8)

print("data0:",data0)

t_val = (data0[0]<<8) + data0[1] #convert the data
h_val = (data0[3] <<8) + data0[4]     # Convert the data
T = ((175.72 * t_val) / 65536.0 ) - 45 #do the maths from datasheet
H = ((100 * h_val) / 65536.0 )

print("t_val:",t_val)
print("h_val:",h_val)
print("T:",T)
print("H:",H)
