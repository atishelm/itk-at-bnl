/*
26 July 2023
Abraham Tishelman-Charny

This function notifies people via email if a step is ready for them to perform. This is triggered every time a cell is edited in the spreadsheet.
When the cell is edited, check:
  - If a hybrid is ready for wirebonding 
  - If a module is ready for wirebonding
  - If a module is ready for testing
*/

function checkCase(columnName, firstColumnToCheck, firstDesiredValue, secondColumnToCheckIndex, secondDesiredValue, emailAddresses, subject, message, rowData, editedCellData){
  if((columnName === firstColumnToCheck) && (editedCellData == firstDesiredValue) && (rowData[secondColumnToCheckIndex] == secondDesiredValue) ){
    MailApp.sendEmail(emailAddresses, subject, "", {htmlBody: message});
    return 1;
  }
}

function sendMailEdit(e){
 
  // Get the edited row and column
  let editedRow = e.range.getRow();
  let editedColumn = e.range.getColumn(); // One-indexed

  // Initialize variables
  let columnToCheck = -1; // Define column to check for changes
  let desiredValue = ""; // Define the desired value to trigger the email

  // Spreadsheet info, info of row that was edited
  let sheet = e.source.getActiveSheet();
  let rowData = sheet.getRange(editedRow, 1, 1, sheet.getLastColumn()).getValues()[0];
  let columnData = sheet.getRange(1, editedColumn, sheet.getLastRow(), 1).getValues();
  columnData = columnData.flat(); // Convert 2D array to 1D array
  let sheetName = sheet.getName();

  // Create the link to the edited cell
  let spreadsheetUrl = e.source.getUrl();
  let editedCellLink = spreadsheetUrl + "#gid=" + sheet.getSheetId() + "&range=" + sheet.getRange(editedRow, editedColumn).getA1Notation();

  // to be updated for different cases
  let emailAddresses = "";
  let subject = "";
  let message = "";

  // Edit was made in Production Module Assembly sheet. 
  if(sheetName === "Production Module Assembly"){

    // Variables needed for all cases in this sheet
    columnName = columnData[2]; // index depends on sheet
    const module_local_ID = rowData[4];
    const module_serial_number = rowData[5];

    // Check cases 
    // Get module wirebonding email addresses, subject, and message body
    [emailAddresses, subject, message] = ModuleWirebondingMessage(module_local_ID, module_serial_number, editedCellLink);

    // Check if the "Module Wirebonded" column was marked as "To Do Next", AND that the metrology has already been marked as done.
    checkCase(columnName, "Module Wirebonded", "To Do Next", 15, "Done", emailAddresses, subject, message, rowData, e.value);

    // Check if the "Module Metrology" column was just marked as "Done", and the Module Wirebonded column was already marked as To Do Next
    checkCase(columnName, "Module Metrology", "Done", 16, "To Do Next", emailAddresses, subject, message, rowData, e.value);

    // Get module testing email addresses, subject, and message body
    [emailAddresses, subject, message] = ModuleTestingMessage(module_local_ID, module_serial_number, editedCellLink);

    // Check if the "Visual Inspection" column was just marked as "To Do Next", and the Module Wirebonded column is already marked as Done
    checkCase(columnName, "Visual Inspection", "To Do Next", 16, "Done", emailAddresses, subject, message, rowData, e.value);

    // Check if the "Module Wirebonded" column was just marked as "Done", and the "Visual Inspection" column is already marked as "To Do Next"
    checkCase(columnName, "Module Wirebonded", "Done", 17, "To Do Next", emailAddresses, subject, message, rowData, e.value);
  } // end of Production Module Assembly sheet

  // Edit was made in Hybrid Assembly sheet. 
  if(sheetName === "Hybrid Assembly"){

    // Variables needed for all cases in this sheet
    columnName = columnData[1]; // index depends on sheet
    const hybrid_substrate_number = rowData[4];
    const hybrid_serial_number = rowData[5];

    // Get module wirebonding email addresses, subject, and message body
    [emailAddresses, subject, message] = HybridWirebondingMessage(hybrid_substrate_number, hybrid_serial_number, editedCellLink);

    // Check if the "Wirebonded" column was marked as "To Do Next", AND that "On Test Panel" has already been marked as done.
    checkCase(columnName, "Wirebonded", "To Do Next", 10, "Done", emailAddresses, subject, message, rowData, e.value);

    // Check if the "On Test Panel" column was marked as "Done", AND that "Wirebonded" column has already been marked as To Do Next.
    checkCase(columnName, "On Test Panel", "Done", 13, "To Do Next", emailAddresses, subject, message, rowData, e.value);
  } // end of Hybrid Assembly sheet

  // Edit was made in Module Reception sheet. 
  if(sheetName === "Module Reception"){

    // Variables needed for all cases in this sheet
    columnName = columnData[2]; // row 3 for this sheet.
    const module_local_ID = rowData[4];
    const module_serial_number = rowData[5];

    [emailAddresses, subject, message] = ModuleReceptionTestingMessage(module_local_ID, module_serial_number, editedCellLink)

    // Check if the "Visual Inspected" column was marked as "To Do Next"
    checkCase(columnName, "Visual Inspected", "To Do Next", 8, "To Do Next", emailAddresses, subject, message, rowData, e.value);
  } // end of Module Reception sheet

}