
// Modules
function ModuleTestingMessage(module_local_ID, module_serial_number, editedCellLink) {
    emailAddresses = "emilyduden@brandeis.edu,abraham.tishelman.charny@cern.ch";
    //emailAddresses = "abraham.tishelman.charny@cern.ch";
    subject = "[ITk at BNL] - " + module_local_ID + " (" + module_serial_number + ") is ready for testing";
    message = "<p style='word-wrap: break-word;'></p>" +
                    "Dear Emily and Abe,<br><br>" + 
                    module_local_ID + " (" + module_serial_number + ") is now ready for testing. Please test at your earliest convenience.<br><br>" + 
                    "Remember to perform the following steps:<br><br>" + 
                    "- Inspect the guard/bias rings under a miscroscope to make sure there are no obvious issues. Mark this column appropriately in the spreadsheet<br>"+
                    "- (Maybe) Take a high resolution photo, upload somewhere.<br>"+
                    "- Take an AMAC IV at room temperature, upload to the database, mark this column appropriately in the spreadsheet\<br>"+
                    "- Take a room temperature electrical test, upload to the database, fille in the HCCStar fuseID, mark this column appropriately in the spreadsheet<br>"+
                    "- Take an AMAC IV at cold temperature, (no need to upload this to the database), mark this column appropriately in the spreadsheet<br>"+
                    "- Thermal cycle this module at your earliest convenience and at an opportune time. Feel free to wait for a batch of four modules to thermal cycle at once<br><br>"+
                    "If any of these steps results in 'Problems', indicate so in the appropriate spreadsheet column and leave a meaningful and detailed description in the Comments column for this module.<br><br>"+
                    "Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your reply will not be seen." + 
                    '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';
    return [emailAddresses, subject, message];
}

function ModuleReceptionTestingMessage(module_local_ID, module_serial_number, editedCellLink) {
    emailAddresses = "emilyduden@brandeis.edu,abraham.tishelman.charny@cern.ch";
    //emailAddresses = "abraham.tishelman.charny@cern.ch";
    subject = "[ITk at BNL] - Module " + module_local_ID + " (" + module_serial_number + ") is ready for testing";
    message = "Dear Emily and Abe,<br><br>" + 
              module_local_ID + " (" + module_serial_number + ") is now ready for testing. Please test at your earliest convenience.<br><br>" + 
              "Remember to perform the following steps as part of the reception test:<br><br>" + 
              "- Take a high resolution photo, upload somewhere, mark this column as Done in the spreadsheet<br>"+
              "- Take an AMAC IV at room temperature, upload to the DB, mark this column appropriately in the spreadsheet<br>"+
              "- Take a room temperature electrical test, upload to the DB, fill in the HCCStar fuseID, mark this column appropriately in the spreadsheet<br>"+
              "- If all tests pass, mark this module as Ready in the Status column<br><br>"+
              "If any of these steps results in 'Problems', leave a meaningful and detailed description in the Comments column for this module.<br><br>"+
              "Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your reply will not be seen." + 
              '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';
    return [emailAddresses, subject, message];
}

function ModuleWirebondingMessage(module_local_ID, module_serial_number, editedCellLink) {
    emailAddresses = "cmusso@bnl.gov,wfielitz@bnl.gov,abraham.tishelman.charny@cern.ch";
    //emailAddresses = "abraham.tishelman.charny@cern.ch";
    subject = "[ITk at BNL] - " + module_local_ID + " (" + module_serial_number + ") is ready for wirebonding";
    message = "Dear Chris and Will,<br><br>" + 
                    module_local_ID + " (" + module_serial_number + ") is now ready for wirebonding. Please wirebond at your earliest convenience." + 
                    " When you're finished, remember to update the spreadsheet and upload any required files to the database. <br><br>Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your message will not be seen." + 
                    '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';
    return [emailAddresses, subject, message];
}

// Hybrids
function HybridWirebondingMessage(hybrid_substrate_number, hybrid_serial_number, editedCellLink) {
    emailAddresses = "cmusso@bnl.gov,wfielitz@bnl.gov,abraham.tishelman.charny@cern.ch";
    //emailAddresses = "abraham.tishelman.charny@cern.ch";
    subject = "[ITk at BNL] - Hybrid " + hybrid_substrate_number + " (" + hybrid_serial_number + ") is ready for wirebonding";
    message = "Dear Chris and Will,<br><br>" + 
              "Hybrid " + hybrid_substrate_number + " (" + hybrid_serial_number + ") is now ready for wirebonding. Please wirebond at your earliest convenience." + 
              " When you're finished, remember to update the spreadsheet and upload any required files to the database. <br><br>Thank you,<br>BNL_ModuleStatus spreadsheet <br><br>--------------------------<br><br>This is an automatic email. Please do not reply to it - if you do, your message will not be seen." + 
              '<br><br>This email was trigged by the following cell being edited: <a href="'+editedCellLink+'">Link_To_Spreadsheet</a>';
    return [emailAddresses, subject, message];
}