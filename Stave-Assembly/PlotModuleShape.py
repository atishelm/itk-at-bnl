"""
28 August 2023 
Abraham Tishelman-Charny

The purpose of this plotter is to display the 3D X,Y,Z of a module from a stave confocal measurement.

Example usage: 
- Alter input file name in this file, alter selections as desired, then run:
python3 PlotModuleShape.py 
"""

# Imports
import pandas as pd 
import numpy as np
from PlotMetrology import Make3DPlot, SaveCSV

# parameters
makeSelections = 0
absRange = 1
microns = 1 # mm to microns conversion

files = [
	["csv/BNL-PPB2-MLS-210_LS_N.csv", "BNL_Module_1"],
	["csv/SCIPP-PPB2-LS-016_LS_N.csv", "SCIPP_Module_1"],
	["csv/SCIPP-PPB2-LS-019_LS_N.csv", "SCIPP_Module_2"]
]

for f_info in files:
	print("f_info",f_info)

	f = f_info[0] # input file
	Label = f_info[1] # label for naming outputs

	# get the data
	data = pd.read_csv(f)


	if(makeSelections):
		# make selections by eye
		uplim 	= 710
		lowlim 	= 670 
		xmax	= 720
		xmin	= 630
		zmax 	= 540
		zmin 	= 0
		mask = np.logical_or(data['X'] > lowlim, data['X'] < uplim)
		mask = np.logical_and(mask, data['X'] > xmin)
		mask = np.logical_and(mask, data['X'] < xmax)
		mask = np.logical_and(mask, data['Z'] < zmax)
		mask = np.logical_and(mask, data['Z'] > zmin)

		data = data[mask]

	# set zmin to zero based on minimum value
	if(absRange):
		data_min = min(data['Z'])
		print("data['Z']:",data['Z'])
		data['Z'] = [float(val - data_min) for val in data['Z'] if type(val) != str]

	if(microns):
		data['Z'] = [val*1000. for val in data['Z'] if type(val) != str]

	# cuts data in multiple dataframes to plot non-contiguous meshes
	#datas = [data[data['X'] < 680], data[data['X'] > 700]]
	datas = [data[data['Y'] < -50], data[data['Y'] > -25]]

	all_vals = [val for data in datas for val in data['Z'] if type(val) != str]
	zmin, zmax = min(all_vals), max(all_vals)

	# make the plot
	if(microns):
		Z_label = "Height [um]"
	else:
		Z_label = "Height [mm]"
	Make3DPlot(datas, Label, zmin, zmax, Z_label)

	# save an output csv file which contains the data after selections are applied
	out_file = "csv/%s.csv"%(Label)
	X, Y, Z = np.array(data['X']), np.array(data['Y']), np.array(data['Z'])
	data = ["%s,%s,%s"%(X[i], Y[i], Z[i]) for i in range(0, len(Z))]
	SaveCSV(out_file, data)
