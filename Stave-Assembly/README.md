# Stave Assembly

Contains Confocal_GUI with adjusted filter regions. Version 0 uploaded later.

## PlotModuleShape.py 

To use `PlotModuleShape.py`, first ensure you have all of the imported python modules installed - see the imports in `PlotMetrology.py` (You can also just try running `PlotModuleShape.py` or `PlotMetrology.py` and your terminal will let you know.

After preparing your environment, you need a csv file with 3 columns as input, corresponding to X, Y, and Z positions. See the file in the `csv` folder as an exmaple. At the time of plotting, a 3D plot will be produced, and you can click and rotate in order to save the plot with different orientations.