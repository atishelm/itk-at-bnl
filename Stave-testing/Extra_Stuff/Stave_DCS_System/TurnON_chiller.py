import serial
from serial import *
import time

class Chiller:
    def __init__(self,location="/dev/ttyUSB0",timeout=1):
        self.chiller=serial.Serial(location,9600,SEVENBITS,parity=PARITY_NONE,stopbits=STOPBITS_ONE,timeout=1)

    def TurnOn(self):
        query="START\n\r"
        self.chiller.write(query.encode())

    def TurnOff(self):
        query="STOP\n\r"
        self.chiller.write(query.encode())
   
    def IsOn(self):
        query="START?\n\r"
        self.chiller.write(query.encode())
        status = str(self.chiller.readline())
        #status_dict = {
        #    b'OK\rF060=+000000.!\r' : "OFF",
        #    b'OK\rF060=-000001.!\r' : "ON"
        #}
        print("status:",status)
        if("F060=+000000.!" in status): return "OFF"
        elif("F060=-000000.!" in status): return "ON"
        else: print("Do not have expected output from chiller query")

        #return status_dict[s

if __name__ == "__main__":
    
    chiller = Chiller(location="/dev/serial/by-id/usb-FTDI_Chipi-X_FT1JF9YA-if00-port0")

    # Turn on the chiller
    

    STATUS = chiller.IsOn()
    exit()
    print("Chiller status:",STATUS)
    #chiller.TurnOn()

    print("Chiller has been turned on.")
    
    STATUS = chiller.IsOn()
    print("Chiller status:",STATUS)
    time.sleep(20)

    #chiller.TurnOff()
    
    
    #STATUS = chiller.IsOn()
    #print("Chiller status:",STATUS)
    
    
    #print("Chiller has been turned off.")

