#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This program reads out IST HYT sensors(HYT-221, HYT-271, HYT-939)
#
# License: Public Domain/CC0
# Original source: https://github.com/joppiesaus/python-hyt/
#
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Humidity:
#   0x0000      -       0x3FFFF  hex
#   0           -       16383    dec
#   0           -       100      % RH
#
# Temperature:
#   0x0000      -       0x3FFF   hex
#   0           -       16383    dec
#  -40          -       125      °C
#   233.15      -       298.15   °K
#   ???         -       ???      °F
#
#  |  byte 0  |  byte 1  |  byte 2  |  byte 3  |  
#  |---------------------|---------------------|
#  |      Humidity       |     Temperature     |
#  |---------------------|---------------------|
#  | 2 bit |   14 bit    |   14 bit    | 2 bit |
#  |-------|-------------|-------------|-------|
#  | state |    data     |    data     | dummy |
#      |
#      +-----------------------+
#      |   bit 0   |   bit 1   |
#      | CMode bit | stale bit |
#      +-----------------------+
#
# CMode bit: if 1 - sensor is in "command mode"
# Stale bit: if 1 - no new value has been created since the last reading
#
# RH = (100 / (2^14 - 1)) * RHraw
# T  = (165 / (2^14 - 1)) * Traw - 40
#
# crappy ascii picture from top to see pinout:
#  ---
# |~ ~|
# | O |
# |___|
# ||||
# |||+-SCL
# ||+--VDD
# |+---GND
# +----SDA
#
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


import time
import smbus
import math

class hyt221():

    def __init__(self, muxAddress=0x73, muxChannel=1, hytAddress = 0x28):
        '''
        This stores the parameters needed to set up the MUX IC on the inBurn piHat
        '''

        if(muxChannel==0):
            self.muxCommand=0x04
        elif(muxChannel==1):
            self.muxCommand=0x05
        elif(muxChannel==2):
            self.muxCommand=0x06
        elif(muxChannel==3):
            self.muxCommand=0x07

        self.bus = smbus.SMBus(1)
        self.delay = 60.0 / 1000.0 # 50-60 ms delay. Without delay, it doesn't work.
        self.muxAddress = muxAddress
        self.hytAddress = hytAddress

    def single_shot(self,rep='HIGH'):

        try :
            # setup MUX 
            self.bus.write_byte_data(self.muxAddress,0x04,self.muxCommand)

            self.bus.write_byte(self.hytAddress, 0x00) # send some stuff
            time.sleep(self.delay) # wait a bit
            reading = self.bus.read_i2c_block_data(self.hytAddress, 0x00, 4) # read the bytes

        except OSError as error :
            print(error)
            print("Try again...")

            self.bus.write_byte_data(self.muxAddress,0x04,self.muxCommand)

            self.bus.write_byte(self.hytAddress, 0x00) # send some stuff
            time.sleep(self.delay) # wait a bit
            reading = self.bus.read_i2c_block_data(self.hytAddress, 0x00, 4) # read the bytes

        # clear MUX 
        self.bus.write_byte_data(self.muxAddress,0x04,0)

        # Mask the first two bits
        relh = ((reading[0] & 0x3F) * 0x100 + reading[1]) * (100.0 / 16383.0)
        # Mask the last two bits, shift 2 bits to the right
        temp = 165.0 / 16383.0 * ((reading[2] * 0x100 + (reading[3] & 0xFC)) >> 2) - 40

        # stolen from sht85.py
        t_range = 'water' if temp >= 0 else 'ice'
        tn = dict(water=243.12, ice=272.62)[t_range]
        m = dict(water=17.62, ice=22.46)[t_range]

        if relh > 0:
            dewp = tn * (math.log(relh / 100.0) + (m * temp) / (tn + temp))/ (m - math.log(relh / 100.0) - m * temp / (tn + temp))
        else:
            dewp = -273.15
        
        return round(temp,4), round(relh,4), round(dewp,4)

	
