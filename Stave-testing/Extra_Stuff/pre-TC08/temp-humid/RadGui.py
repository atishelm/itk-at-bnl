from PyQt5.QtWidgets import QMainWindow, QApplication,QWidget
from PyQt5 import uic
from PyQt5.QtGui import *
from PyQt5.QtCore import Qt,pyqtSlot
from PyQt5 import QtCore
import numpy as np
import sys
from collections import deque
from read_rpi import rpi
import graphyte
from record_star_data import loop_chips

Ui_MainWindow, QtBaseClass = uic.loadUiType("RadGui.ui")
from tec import tec
#from sht85 import sht85
import threading
import time

def write_csv(dictionary,filename="eviroment_data.csv"):
    with open(filename,"a") as csv:
        for key,value in dictionary.items():
            csv.write(key)
            csv.write(":")
            csv.write(item)
            csv.write(",")
        csv.write("\n")
        
def check_stability(temperature_deque):
    tolerance=1.5
    return np.std(temperature_deque)<tolerance
graphyte.init("127.0.0.1")
class MyApp(QMainWindow):
    def __init__(self):
        super(MyApp, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        #self.ui.TemperatureSlider11.setTickPosition(2)
        #self.ui.TemperatureSlider11.setTickInterval(5)

        #temperature sliders
        self.ui.TemperatureSlider11.setMinimum(-15)
        self.ui.TemperatureSlider11.setMaximum(25)
        self.ui.TemperatureSlider11.setValue(20)
        self.ui.TemperatureDisplay11.setText(str(self.ui.TemperatureSlider11.value()))
        self.ui.TemperatureSlider11.valueChanged.connect(self.TempSlider)
        self.ui.TemperatureSlider11.sliderReleased.connect(self.SetTempFromSlider)

        self.ui.TemperatureSlider12.setMinimum(-15)
        self.ui.TemperatureSlider12.setMaximum(25)
        self.ui.TemperatureSlider12.setValue(20)
        self.ui.TemperatureDisplay12.setText(str(self.ui.TemperatureSlider12.value()))
        self.ui.TemperatureSlider12.valueChanged.connect(self.TempSlider)
        self.ui.TemperatureSlider12.sliderReleased.connect(self.SetTempFromSlider)

        self.ui.TemperatureSlider13.setMinimum(-15)
        self.ui.TemperatureSlider13.setMaximum(25)
        self.ui.TemperatureSlider13.setValue(20)
        self.ui.TemperatureDisplay13.setText(str(self.ui.TemperatureSlider13.value()))
        self.ui.TemperatureSlider13.valueChanged.connect(self.TempSlider)
        self.ui.TemperatureSlider13.sliderReleased.connect(self.SetTempFromSlider)

        self.ui.TemperatureSlider14.setMinimum(-15)
        self.ui.TemperatureSlider14.setMaximum(25)
        self.ui.TemperatureSlider14.setValue(20)
        self.ui.TemperatureDisplay14.setText(str(self.ui.TemperatureSlider14.value()))
        self.ui.TemperatureSlider14.valueChanged.connect(self.TempSlider)
        self.ui.TemperatureSlider14.sliderReleased.connect(self.SetTempFromSlider)

        self.ui.sensorButton.clicked.connect(self.start_monitoring)
        self.chip11_chiller=tec("/dev/ttyUSB1",230400,1)
        self.chip12_chiller=tec("/dev/ttyUSB2",230400,2)
        self.chip13_chiller=tec("/dev/ttyUSB3",230400,3)
        self.chip14_chiller=tec("/dev/ttyUSB4",230400,4)

        try:
          self.chip11_chiller.default_setting()
        except:
            pass
        try:
          self.chip12_chiller.default_setting()
        except:
            pass
        try:
          self.chip13_chiller.default_setting()
        except:
            pass
        try:
          self.chip14_chiller.default_setting()
        except:
            pass
        self.chip11_temp=20
        self.chip12_temp=20
        self.chip13_temp=20
        self.chip14_temp=20

        self.chip11_humid=19
        self.chip12_humid=20
        self.chip13_humid=21
        self.chip14_humid=22

        self.ui.humidity11.setValue(self.chip11_humid)
        self.ui.humidity12.setValue(self.chip12_humid)
        self.ui.humidity13.setValue(self.chip13_humid)
        self.ui.humidity14.setValue(self.chip14_humid)

        self.pill2kill = threading.Event()
        self.monitor=SensorMonitor("Sensor Thread 1",self.pill2kill,[self.chip11_chiller,self.chip12_chiller,self.chip13_chiller,self.chip14_chiller])
        self.monitor.humid_sig.connect(self.UpdateHumidity)
        self.monitor.temp_sig.connect(self.UpdateTemperature)
        self.ABCKiller=threading.Event()
        self.ABCMonitor=ABCStarMonitor("ABC Monitor thread 1",self.ABCKiller)
        self.ui.ABCStarButton.clicked.connect(self.ABCStarMonitor)
    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)

        y_pos=55
        x_pos=170
        spacing=100
        
        temp_11=self.chip11_temp

        grad1 = QLinearGradient(x_pos,y_pos,x_pos+25,y_pos+165)
        grad1.setColorAt(1,QColor(255,0,0))
        grad1.setColorAt((25.0001-temp_11)/40.1+0.001,QColor(170,0,80))
        grad1.setColorAt(0,QColor(0,0,255))
        qp.setBrush(grad1)
        qp.drawRect(x_pos,y_pos,25,165)
        qp.end()

        qp2 = QPainter()
        qp2.begin(self)
        temp_12=self.chip12_temp
        grad2 = QLinearGradient(x_pos+spacing,y_pos,x_pos+spacing+25,y_pos+165)
        grad2.setColorAt(1,QColor(255,0,0))
        grad2.setColorAt((25.0001-temp_12)/40.1+0.001,QColor(170,0,80))
        grad2.setColorAt(0,QColor(0,0,255))
        qp2.setBrush(grad2)
        qp2.drawRect(x_pos+spacing,y_pos,25,165)
        qp2.end()

        qp3 = QPainter()
        qp3.begin(self)
        temp_13=self.chip13_temp
        grad3 = QLinearGradient(x_pos+spacing*2,y_pos,x_pos+spacing*2+25,y_pos+165)
        grad3.setColorAt(1,QColor(255,0,0))
        grad3.setColorAt((25.0001-temp_13)/40.1+0.001,QColor(170,0,80))
        grad3.setColorAt(0,QColor(0,0,255))
        qp3.setBrush(grad3)
        qp3.drawRect(x_pos+2*spacing,y_pos,25,165)
        qp3.end()

        
        qp4 = QPainter()
        qp4.begin(self)
        temp_14=self.chip14_temp
        grad4 = QLinearGradient(x_pos+spacing*3,y_pos,x_pos+spacing*3+25,y_pos+165)
        grad4.setColorAt(1,QColor(255,0,0))
        grad4.setColorAt((25.001-temp_14)/40.1+.001,QColor(170,0,80))
        grad4.setColorAt(0,QColor(0,0,255))
        qp4.setBrush(grad4)
        qp4.drawRect(x_pos+spacing*3,y_pos,25,165)
        
        qp4.end()

    def start_monitoring(self):
        if not self.pill2kill.is_set():
            self.pill2kill.set()
            self.monitor.start()
        else:
            self.pill2kill.clear()

    def TempSlider(self):
        self.ui.TemperatureDisplay11.setText(str(self.ui.TemperatureSlider11.value()))
        self.ui.TemperatureDisplay12.setText(str(self.ui.TemperatureSlider12.value()))
        self.ui.TemperatureDisplay13.setText(str(self.ui.TemperatureSlider13.value()))
        self.ui.TemperatureDisplay14.setText(str(self.ui.TemperatureSlider14.value()))

        self.update()
    def SetTempFromSlider(self):
        try:
            self.chip11_chiller.set_temp(int(self.ui.TemperatureSlider11.value()))
        except:
            print("didnt set chip 11 temp")
        try:
            self.chip12_chiller.set_temp(int(self.ui.TemperatureSlider12.value()))
        except:
            print("didnt set chip 12 temp")
        try:
            self.chip13_chiller.set_temp(int(self.ui.TemperatureSlider13.value()))
        except:
            print("didnt set chip 13 temp")
        try:
            self.chip14_chiller.set_temp(int(self.ui.TemperatureSlider14.value()))
        except:
            print("didnt set chip 14 temp")

        
    @pyqtSlot(int,float)
    def UpdateHumidity(self,chip_id,value):
        print("updating humidity")
        print(chip_id,value)
        if chip_id == 1:
            self.ui.humidity11.setValue(value)
        if chip_id == 2:
            self.ui.humidity12.setValue(value)
        if chip_id == 3:
            self.ui.humidity13.setValue(value)
        if chip_id == 4:
            self.ui.humidity14.setValue(value)
        self.update()
    @pyqtSlot(int,float)
    def UpdateTemperature(self,chip_id,value):
        print("updating temperature")
        print(chip_id,value)
        if chip_id == 1:
            self.chip11_temp=value
        if chip_id == 2:
            self.chip12_temp=value
        if chip_id == 3:
            self.chip13_temp=value
        if chip_id == 4:
            self.chip14_temp=value
        self.update()

    def ABCStarMonitor(self):
        if not self.ABCKiller.is_set():
            self.ABCKiller.set()
            self.ABCMonitor.start()
        else:
            self.ABCKiller.clear()

class SensorMonitor(QtCore.QThread):
    humid_sig = QtCore.pyqtSignal(int,float)
    temp_sig= QtCore.pyqtSignal(int,float)
    error_sig=QtCore.pyqtSignal(str)
    def __init__(self,threadID,stop_event,tec_list):
        super(QtCore.QThread, self).__init__()
        self.threadID = threadID
        self.temperature_deque=deque()
        self.tec_list=tec_list
        self.stop_event=stop_event
        self.pi=rpi("/dev/ttyUSB0")
        graphyte.init("127.0.0.1")
    def run(self):
        print("Starting " + str(self.threadID))
        self.monitor()

    def monitor(self):
        while self.stop_event.is_set():
            time.sleep(1)
            
            for tec in self.tec_list:
                if tec:
                    try:
                        tec_temp=tec.get_temp()
                        print(tec_temp)
                        print(tec.chip_id)
                        graphyte.send("chip1"+str(tec.chip_id)+".tec_temperature",tec_temp)
                        if(tec_temp > 25):
                            print("high temperature, not recording")
                        else:
                            self.temp_sig.emit(tec.chip_id,tec_temp)
                    except Exception as e:
                        print(e)
                        print("problem with TEC")

            try:
                sht_data=self.pi.get_data()
            except:
                sht_data=''
            while sht_data['chip']:
                chip_id=sht_data['chip']
                sht_temp=sht_data['Temperature']
                sht_hum=sht_data['Humidity']
                graphyte.send("chip1"+str(chip_id[-1])+".humidity",sht_hum)
                graphyte.send("chip1"+str(chip_id[-1])+".air_temperature",sht_temp)

                print(sht_data)
                self.humid_sig.emit(int(chip_id[-1]),float(sht_hum))
                time.sleep(0.1)
                if sht_hum > 30:
                    self.tec_list[int(chip_id[-1])-1].set_temp(20)
                try:
                    sht_data=self.pi.get_data()
                except:
                    sht_data=''
        print("stopped")


class ABCStarMonitor(QtCore.QThread):
    def __init__(self,threadID,stop_event):
        super(QtCore.QThread, self).__init__()
        self.threadID = threadID
        self.stop_event=stop_event

    def run(self):
        print("starting"+self.threadID)
        while self.stop_event.is_set():
            loop_chips()
            time.sleep(600)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
    
