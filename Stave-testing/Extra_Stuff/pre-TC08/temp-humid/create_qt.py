import sys

name=sys.argv[1]
pyname=name.split(".")[0]
pyname+=".py"
with open(pyname,'w') as pyfile:
    pyfile.write("from PyQt5.QtWidgets import QMainWindow, QApplication\n")
    pyfile.write("from PyQt5 import uic\n")
    pyfile.write("import sys\n")
    pyfile.write("Ui_MainWindow, QtBaseClass = uic.loadUiType(\""+name+"\")\n")
    pyfile.write("\n\n")
    pyfile.write("class MyApp(QMainWindow):\n")
    pyfile.write("    def __init__(self):\n")
    pyfile.write("        super(MyApp, self).__init__()\n")
    pyfile.write("        self.ui = Ui_MainWindow()\n")
    pyfile.write("        self.ui.setupUi(self)\n")
    pyfile.write("\n\n")
    pyfile.write("if __name__ == \"__main__\":\n")
    pyfile.write("    app = QApplication(sys.argv)\n")
    pyfile.write("    window = MyApp()\n")
    pyfile.write("    window.show()\n")
    pyfile.write("    sys.exit(app.exec_())\n")
    



