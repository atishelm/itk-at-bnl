import subprocess
from subprocess import Popen, PIPE
import time
import os
from six.moves import input
import re
import datetime


def record_single_chip(chip,tid):
    filename=chip+"_fmc_data.txt"
    os.chdir("/home/grosin/Radiation/"+chip)
    print(os.getcwd())
    subprocess.call("source activate.sh",shell=True,executable="/bin/bash")
    os.chdir("itsdaq-sw")
    print(os.getcwd())
    p = Popen("source RUNITSDAQ.sh",stdin=PIPE,shell=True,executable="/bin/bash")
    p.stdin.write(("f->writeMonitorData(\""+str(filename)+"\","+str(tid)+");\n").encode())
    p.communicate(".q".encode())



def x_time(begin,now):
    return (now-begin)/datetime.timedelta(seconds=1)

def get_tid(chip):
    dose=2.5
    start_time=datetime.datetime(2019,2,4,14,9,30)
    now=datetime.datetime.now()
    return x_time(start_time,now)*2.5/3600

def loop_chips():
    chips=["ABCStar11","ABCStar12","ABCStar13","ABCStar14"]
    for chip in chips:
        tid=get_tid(chip)
        record_single_chip(chip,tid)
        os.chdir("/home/grosin/Radiation")
        time.sleep(5)
if __name__=="__main__":
    loop_chips()
