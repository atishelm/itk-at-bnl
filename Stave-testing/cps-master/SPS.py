import serial
import time

class SPS:

    ##### Initialization #####
    def __init__(self,location="/dev/ttyUSB0",timeout=1):
        self.chiller=serial.Serial(location,9600,serial.SEVENBITS,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,timeout=1)

    #### General communication functions #####
    def sendQuery(self, query):
        self.chiller.readlines()
        self.chiller.write(query.encode())
        data = self.chiller.readline()
        # ADD CHECK ON NUMBER OF LINES
        data = str(data.decode())
        #print("data before parse:",data)
        response = data[data.index("=")+1:data.index("!")]
        #print("data after parse:",response)
        return response

    def sendCommand(self, command):
        self.chiller.write(command.encode())

    ##### Commands #####
    def turnOn(self):
        query="START\n\r"
        self.sendCommand(query)

    def turnOff(self):
        query="STOP\n\r"
        self.sendCommand(query)
        
    ##### Queries #####
    def isOn(self):
        query = "START?\n\r"
        data  = self.sendQuery(query)
        status = data[data.index("=")+1:data.index("!")]
        if("+000000." in status):
            return "OFF"
        elif("-000001." in status):
            return "ON"
        else:
            print("Do not have expected output from chiller query")
            return "ERROR"

if __name__ == "__main__":
    
    chiller = SPS(location="/dev/serial/by-id/usb-FTDI_Chipi-X_FT1JF9YA-if00-port0")
    #chiller.turnOn()
    chiller.turnOff()
    STATUS = chiller.isOn()
    print(STATUS)
    exit()
