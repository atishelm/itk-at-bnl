import smbus
from smbus2 import SMBus
import qwiic

MUXboard = qwiic.QwiicTCA9548A()
MUXboard.disable_channels([0,1,2,3,4,5,6,7])

class sht85:
    def __init__(self, channel, MUXboard, bus=1,address=0x44):
        
        self.bus=smbus.SMBus(bus)
        self.address = address
        self.channel = channel
        self.MUXboard = MUXboard

    @staticmethod
    def initPorts(bus, address=0x44):
    #check which ports have someting connected to it
        ports = []
        smb = smbus.SMBus(bus)
        for i in range(8):
            try:
                if i == 0:
                    MUXboard.enable_channels([i])
                else:
                    MUXboard.enable_channels([i])
                    MUXboard.disable_channels([i-1])
                smb.write_byte_data(address, 0x24, 0x00)
            except:
                pass
            else:
               ports.append(sht85(i,bus,address))
        MUXboard.disable_channels([7])

        return ports
        
    
    def get_data(self):
        self.MUXboard.disable_channels([0,1,2,3,4,5,6,7])
        self.MUXboard.enable_channels([self.channel])
        #Write the read sensor command
        self.bus.write_byte_data(self.address, 0x24, 0x00)
        time.sleep(0.5) #This is so the sensor has tme to preform the mesurement and write its registers before you read it\
        
        # Read data back, 8 bytes, temperature MSB first then lsb, Then skip the checksum bit then humidity MSB the lsb.
        data0 = self.bus.read_i2c_block_data(self.address, 0x00, 8)
        t_val = (data0[0]<<8) + data0[1] #convert the data
        h_val = (data0[3] <<8) + data0[4]     # Convert the data
        T = ((175.72 * t_val) / 65536.0 ) - 45 #do the maths from datasheet
        H = ((100 * h_val) / 65536.0 )
        return {"temperature": T,"humidity": H, "dewpoint": dewPoint(T, H), "channel": self.channel}
        #self.Upload_To_Influx_hum(current_time, ifuser = "abe", ifpass = "abe_influxdb")

    @staticmethod
    def Upload_To_Influx_hum(time, data, ifuser = "abe", ifpass = "abe_influxdb"):

        measurement_name = "humidity"
        
        fields = {}
        for d in data:         
            field_name = "humid%d"%(d['channel'])
            print(f'This is field {field_name} humidity {d["humidity"]}')
            fields[field_name] = d['humidity']
            
        ifclient = InfluxDBClient(INFLUXDB_HOST,INFLUXDB_PORT,ifuser,ifpass,INFLUXDB_DB)
        body = [
                {
                "measurement" : measurement_name,
                "time": time,
                "fields": fields
                }
            ]

        ifclient.write_points(body)
            
    '''
        def sensor_data(self):
        ports = self.initPorts()
        print(ports)
        while True:
        #prints out data from the connected sensors
            for i in range(len(ports)):
                if i == 0:
                    print("\n\n\nSensor 1")
                    test.enable_channels([ports[i]])
                    test.disable_channels([len(ports)-1])
                    print(self.get_data())
                else:
                    print("\nSensor %d" %(i+1))
                    test.enable_channels([ports[i]])
                    test.disable_channels([ports[i-1]])
                    print(self.get_data())
            time.sleep(1)
        '''

