import qwiic
import smbus
import time
import numpy as np

def dew_point(temperature, humidity):
    A = 17.27
    B = 237.7
    
    #protection against div by 0
    if humidity == 0:
        return -999
    alpha = ((A * temperature) / (B + temperature)) + np.log(humidity / 100.0)
    dewpoint = (B * alpha) / (A - alpha)
    return dewpoint


def dewPoint(temp, rh):
        #Values and formulas from datasheet
        if temp > 0:
            m = 17.62
            Tn = 243.12
        else:
            m = 22.46
            Tn = 272.62
        
        #protection against div by 0
        if rh == 0:
            return -999
            
        return Tn * (np.log(rh/100) + (m * temp)/(Tn + temp))/(m - np.log(rh/100) - (m * temp)/(Tn * temp)) 


MUXboard = qwiic.QwiicTCA9548A()
MUXboard.disable_channels([0,1,2,3,4,5,6,7])

class sht85:
    def __init__(self, channel, bus=1,address=0x44):
        
        self.bus=smbus.SMBus(bus)
        self.address = address
        self.channel = channel

    def get_data(self):
        MUXboard.disable_channels([0,1,2,3,4,5,6,7])
        MUXboard.enable_channels([self.channel])
        #Write the read sensor command
        self.bus.write_byte_data(self.address, 0x24, 0x00)
        time.sleep(0.5) #This is so the sensor has tme to preform the mesurement and write its registers before you read it\

        # Read data back, 8 bytes, temperature MSB first then lsb, Then skip the checksum bit then humidity MSB the lsb.

        data0 = self.bus.read_i2c_block_data(self.address, 0x00, 8)
        t_val = (data0[0] <<8) + data0[1] #convert the data
        h_val = (data0[3] <<8) + data0[4]     # Convert the data
        T = ((175.72 * t_val) / 65536.0 ) - 45 #do the maths from datasheet
        H = ((100 * h_val) / 65536.0 )

        #print("data0:",data0)
        #print("t_val:",t_val)
        #print("h_val:",h_val)
        #print("T:",T)
        #print("H:",H)

        data = {"temperature": T,"humidity": H, "dewpoint": dewPoint(T, H)} #, "channel": channel}

        return data


bus = 1
address = 0x44

ports = []
smb = smbus.SMBus(bus)
for i in range(8):
    try:
        if i == 0:
            MUXboard.enable_channels([i])
        else:
            MUXboard.enable_channels([i])
            MUXboard.disable_channels([i-1])
        smb.write_byte_data(address, 0x24, 0x00)
    except:
        pass
    else:
       ports.append(sht85(i,bus,address))

MUXboard.disable_channels([7])

print("ports:",ports)

for i in range(len(ports)):
    print("\nSensor %d" %(i+1))
    print(ports[i].get_data())


