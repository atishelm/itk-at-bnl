import matplotlib.pyplot as plt
import numpy as np

import mplhep as hep 
plt.style.use(hep.style.ATLAS)

# https://stackoverflow.com/questions/47205918/plot-multiple-stacked-bar-in-the-same-figure

bar_colors = [(187/255, 39/255, 26/255), (209/255, 109/255, 106/255), (238/255, 205/255, 205/255), (220/255, 233/255, 213/255)]

failed_color = (222/255, 157/255, 155/255)
passed_color = (157/255, 195/255, 132/255)

import pandas as pd
strategies = ("Hysol", "Wide-gap (Hysol)", "Wide-gap (SE4445)", "Interposers")
x = np.arange(len(strategies))  # the label locations

#fig, ax = plt.subplots(layout='constrained', dpi = 150, figsize=(15, 10))
#fig, ax = plt.subplots(dpi = 150, figsize=(15, 10))
#fig, ax = plt.subplots(dpi = 150, figsize=(25,4))
fig, ax = plt.subplots(dpi = 150, figsize=(25,8))

width = 1

# Number of passed modules per temperature
df_passed = pd.DataFrame(dict(
    m35=[0,0,0,0], # assumes ordering: Hysol, Widegap_Hysol, Widegap_SE4445, Interposers
    m45=[0,0,0,0],
    m70=[0,0,0,0],
))

# Number of failed modules per termperature
df_failed = pd.DataFrame(dict(
    m35=[0,0,0,0], # assumes ordering: Hysol, Widegap_Hysol, Widegap_SE4445, Interposers
    m45=[0,0,0,0],
    m70=[0,0,0,0]
))

# Staves

# US 8 14 Hysol, 14 Wide-gap Hysol. Ever brought to -70 at RAL?

# 14 hysol side
df_passed.m35[0] += 14
df_failed.m35[0] += 0

df_passed.m45[0] += 11
df_failed.m45[0] += 3

df_passed.m70[0] += 1
df_failed.m70[0] += 13

# 14 wide-gap hysol side
df_passed.m35[1] += 14 
df_failed.m35[1] += 0

df_passed.m45[1] += 10
df_failed.m45[1] += 3

df_passed.m70[1] += 3
df_failed.m70[1] += 10

# US 12
df_passed.m35[2] += 28
df_failed.m35[2] += 0

df_passed.m45[2] += 28
df_failed.m45[2] += 0

df_passed.m70[2] += 0
df_failed.m70[2] += 0

# US 14 (28 interposer modules)
df_passed.m35[3] += 28
df_failed.m35[3] += 0

df_passed.m45[3] += 28
df_failed.m45[3] += 0

df_passed.m70[3] += 0
df_failed.m70[3] += 0

# UK 38 (14 Hysol)
df_passed.m35[0] += 14
df_failed.m35[0] += 0

df_passed.m45[0] += 13
df_failed.m45[0] += 1

df_passed.m70[0] += 5
df_failed.m70[0] += 9 # tested to -70?

# UK 40 (14 Hysol?) Tested at -70?
df_passed.m35[0] += 14
df_failed.m35[0] += 0

df_passed.m45[0] += 9
df_failed.m45[0] += 4

#df_passed.m70[0] += 0
#df_failed.m70[0] += 0

# UK 43 (14 interposer modules)
df_passed.m35[3] += 14
df_failed.m35[3] += 0

df_passed.m45[3] += 14
df_failed.m45[3] += 0

df_passed.m70[3] += 14
df_failed.m70[3] += 0

temps = ["m35", "m45", "m70"]
initial_positions = [0,4,8,12]

print("df_passed:",df_passed)
print("df_failed:",df_failed)

# -35C
plt.bar([1,5,9,13], df_passed.m35, width=width, align='edge', color=passed_color, bottom=0, label = "Passed", rasterized=True)
plt.bar([1,5,9,13], df_failed.m35, width=width, align='edge', color=failed_color, bottom=df_passed.m35, label = "Suspected or confirmed fracture", rasterized=True)

# -45C
plt.bar([2,6,10,14], df_passed.m45, width=width, align='edge', color=passed_color, bottom=0, rasterized=True)
plt.bar([2,6,10,14], df_failed.m45, width=width, align='edge', color=failed_color, bottom=df_passed.m45, rasterized=True)

# -70C
plt.bar([3,7,11,15], df_passed.m70, width=width, align='edge', color=passed_color, bottom=0, rasterized=True)
plt.bar([3,7,11,15], df_failed.m70, width=width, align='edge', color=failed_color, bottom=df_passed.m70, rasterized=True)

temp_text_size = 30

plt.text(1+width/2, df_passed.m35[0]+df_failed.m35[0], "-35\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)
plt.text(5+width/2, df_passed.m35[1]+df_failed.m35[1], "-35\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)
plt.text(9+width/2, df_passed.m35[2]+df_failed.m35[2], "-35\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)
plt.text(13+width/2, df_passed.m35[3]+df_failed.m35[3], "-35\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)

plt.text(2+width/2, df_passed.m45[0]+df_failed.m45[0], "-45\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)
plt.text(6+width/2, df_passed.m45[1]+df_failed.m45[1], "-45\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)
plt.text(10+width/2, df_passed.m45[2]+df_failed.m45[2], "-45\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)
plt.text(14+width/2, df_passed.m45[3]+df_failed.m45[3], "-45\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)

plt.text(3+width/2, df_passed.m70[0]+df_failed.m70[0], "-70\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)
plt.text(7+width/2, df_passed.m70[1]+df_failed.m70[1], "-70\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)
plt.text(11+width/2, df_passed.m70[2]+df_failed.m70[2], "-70\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)
plt.text(15+width/2, df_passed.m70[3]+df_failed.m70[3], "-70\u00B0C", horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=temp_text_size)

max_val = (df_failed.to_numpy() + df_passed.to_numpy()).max()
ax.set_xticks([2.5,6.5,10.5,14.5], strategies, fontsize = 40)#, labelpad=20)
ax.set_ylabel("Number of modules", fontsize = 40)

plt.tick_params(axis='x', pad=10)
#plt.legend(loc='upper center', fontsize = 40, bbox_to_anchor=(0.01, 1.075))
#plt.legend(loc='upper center', fontsize = 40, bbox_to_anchor=(0.01, 1.075))
plt.legend(loc='upper center',fontsize = 40)

ax.set_ylim(0, max_val*1.6)
ax.set_xlim(0, 17)
fig.tight_layout(pad=2.0)
ax.tick_params(axis=u'x', which=u'both',length=0) #va='top')
ax.tick_params(axis='both', which='both', labelsize=35)

#plt.tick_params(axis='x', labelpad=10)
#plt.text(
#  0.06, 0.95, "Modules tested down to", horizontalalignment='left', verticalalignment='top', fontweight='bold', transform=ax.transAxes, fontsize = 20
#)

plt.savefig("FractureSummary.png")
plt.savefig("FractureSummary.pdf")
#plt.show()
