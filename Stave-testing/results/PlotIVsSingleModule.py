"""
21 February 2024
Abraham Tishelman-Charny

The purpose of this python module is to plot IV curves. Repurposed from original single IV plotter.

Example usage:
python3 PlotIVsSingleModule.py --inDir Mod4Warm --IV_Type module


Example usage:
python3 IV_Curves.py --inFile data_bypassAMAC/SCIPP-PPB_LS-015_RoomTemp_IV_bypassAMAC.json --IV_Type module --ol .

python3 PlotIVs.py --inDir data --IV_Type module

for f in ./data_bypassAMAC/*; do python3 PlotIVs.py --inFile ${f} --IV_Type module --ol plots_bypassAMAC; done
"""

# imports 
import json 
import pandas as pd 
import numpy as np 
import os 
import csv 
import matplotlib 
matplotlib.use('Agg') # turn off interactive mode
import matplotlib.pyplot as plt

import mplhep as hep 
plt.style.use(hep.style.ATLAS) # customization: https://mplhep.readthedocs.io/en/latest/api.html
from python.SetOptions import options 

# Command line flag options 
inFile = options.inFile
inDir = options.inDir
IV_Type = options.IV_Type
ol = options.ol 
debug = options.debug

if(inFile is not None and inDir is not None):
    raise Exception("Choose either inFile or inDir, not both")

# Create output directory if it doesn't already exist
if(not os.path.exists(ol)):
    print("Creating output directory:",ol)
    os.system("mkdir -p %s"%(ol))
    os.system("cp %s/../index.php %s"%(ol, ol)) # assumes one directory up from 'ol' exists

# Functions 
def Save_Figures(fig, fileTypes_, OUTNAME_, plotType):
    for fileType in fileTypes_:
        outname = "%s_%s.%s"%(OUTNAME_, plotType, fileType)
        print("")
        print("Saving figure:",outname)
        print("")
        fig.savefig(outname)
    plt.close()    

def Make_IV_Plot(VOLTAGE_vals_, CURRENT_vals_, CURRENT_RMS_vals_, PS_CURRENT_vals_, x_label_, y_label_, OUTNAME_, component_, TEMPERATURE_, debug, include_PSinfo_, bypassAMAC_, ATLAS_label_, simpleName_):
    if(debug): print("CURRENT_RMS_vals_:",CURRENT_RMS_vals_)

    # Subtract the y intercept from all values (offset)
    val_0 = float(CURRENT_vals_[0])
    CURRENT_vals_ = [float(val_i - val_0) for val_i in CURRENT_vals_]
    AbsVal = 0
    if(AbsVal): CURRENT_vals_ = np.abs(CURRENT_vals_)
    print(CURRENT_vals_)
    # params 
    fileTypes = ["png", "pdf"]
    Nentries = len(CURRENT_vals_)
    #ymaxIncrement = 0.425
    ymaxIncrement = 0.1

    # IV curve plot 
    fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
    #hep.atlas.text("ITk Strips %s"%(ATLAS_label_), loc=0)

    if(include_PSinfo_): PS_ax = ax.twinx()
    if(bypassAMAC_): AMAC_data_color = "black"
    else: AMAC_data_color = "C0"
    PS_data_color = "C1"

    ax.errorbar(VOLTAGE_vals_, CURRENT_vals_, yerr=CURRENT_RMS_vals_, xerr=None, marker='o', markersize=2, elinewidth=1, linewidth=0.5, color = AMAC_data_color)
    ax.set_xlabel(x_label_)
    ax.set_ylabel(y_label_, color=AMAC_data_color)
    ax.spines['left'].set_color(AMAC_data_color)
    if(include_PSinfo_): ax.spines['right'].set_color(PS_data_color)
    ax.yaxis.label.set_color(AMAC_data_color)
    ax.tick_params(axis='y', which='both', colors=AMAC_data_color) # both = major and minor

    # plot power supply current on same plot w/ different y-axis
    if(debug):
        print("VOLTAGE_vals_",VOLTAGE_vals_)
        print("PS_CURRENT_vals_",PS_CURRENT_vals_)
    
    if(include_PSinfo_):
        PS_ax.plot(VOLTAGE_vals_, PS_CURRENT_vals_, color = PS_data_color, marker='o', markersize=2, linewidth=0.5)
        PS_ax.set_ylabel(r"PS current [${\mu}$A]", color=PS_data_color)
        PS_ax.spines['left'].set_color(AMAC_data_color)
        PS_ax.spines['right'].set_color(PS_data_color)
        PS_ax.yaxis.label.set_color(PS_data_color)
        PS_ax.tick_params(axis='y', which='both', colors=PS_data_color) # both = major and minor

    if(include_PSinfo_): PS_ax.spines['top'].set_color('black')
    ax.spines['top'].set_color('black')
    if(include_PSinfo_): PS_ax.spines['bottom'].set_color('black')
    ax.spines['bottom'].set_color('black')

    # set ymax higher to contain plot labels
    ymin, ymax = ax.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    #ax.set_ylim(ymin, Incremented_Ymax)

    #plt.ylim(ymin, 60000)
    #plt.xlim(-450, 0)

    upperText = "%s"%(component_)

    if(type(TEMPERATURE_) == str): TEMPERATURE_ = TEMPERATURE_.replace("_", " ")
    
    middleText = "Module IV curve"
    if(simpleName_):
        simpleNames = {
            "BNL-PPB2-MLS-210" : "BNL module 1",
            "SCIPP-PPB_LS-016" : "SCIPP module 1",
            "SCIPP-PPB_LS-019" : "SCIPP module 2",
            "" : "",
        }

        if(upperText) in simpleNames:
            upperText = simpleNames[upperText]


        if(type(TEMPERATURE_) == str): TEMPERATURE_ = TEMPERATURE_.replace("bypass AMAC", " ")

        middleText = "IV curve"

    plt.text(
        0.94,
        0.95,
        "\n".join([
            upperText,
            middleText,
            f"{TEMPERATURE_} \N{DEGREE SIGN}C"
        ]
        ),
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )

    #plt.ylim(ymin, 60000)
    #plt.xlim(-450, 0)
    if(include_PSinfo_): PS_ax.ticklabel_format(style='plain')
    ax.ticklabel_format(style='plain')
    fig.tight_layout()
    Save_Figures(fig, fileTypes, OUTNAME_, "IVCurve")    


def Make_Combined_IV_Plot(all_VOLTAGE_vals_, all_CURRENT_vals_, all_CURRENT_RMS_vals_, all_PS_CURRENT_vals_, x_label_, y_label_, OUTNAME_, components_, ATLAS_label_, inDir_, removed_modules_,runnums_):
    print("Make combined IV plot")

    cmap = plt.get_cmap('turbo')
    #cmap = plt.get_cmap('Blues')
    #cmap = plt.get_cmap('winter')
    #cmap = plt.get_cmap('autumn')
    #colors = [""]
    alpha = 1.0
    N_curves = len(all_VOLTAGE_vals_)

    labelMap = {
         "20USBML1234969" : "LBNL_PPB2_LS_33",
         "20USBML1234971" :"LBNL_PPB2_LS_35",
         "20USBML1234973": "LBNL_PPB2_LS_40",
         "20USBML1234975": "LBNL_PPB2_LS_42",
         "20USBML1234976": "LBNL_PPB2_LS_43",
         "20USBML1234977": "LBNL_PPB2_LS_44",
         "20USBML1234979": "LBNL_PPB2_LS_46",
         "20USBML1234981": "LBNL_PPB2_LS_48",
         "20USBML1234984": "LBNL_PPB2_LS_50",
         "20USBML1234985": "LBNL_PPB2_LS_51",
         "20USBML1234853": "BNL-PPB2-MLS-241",
         "20USBML1234959": "BNL-PPB2-MLS-233",
         "20USBML1234961": "BNL-PPB2-MLS-235",
         "20USBML1234963": "BNL-PPB2-MLS-237",
         "SCIPP-PPB_LS-008" : "SCIPP-PPB_LS-008",
         "20USBML1234761" : "SCIPP-PPB_LS-008"
    }

    runMap = {
        "20231120_1266" : "Before fracture",
        "20240119_158" : "After fracture",
        "1266_1" : "Before fracture",
        "158_2" : "After fracture",
        "408_17": "Cold 13",
        "401_18": "Cold 12",
        "376_36": "Cold 11",
        "358_1": "Cold 10",
        "344_18": "Cold 9",
        "339_18": "Cold 8",
        "327_51": "Cold 7",
        "327_33": "Cold 6",
        "317_102": "Cold 5",
        "317_52": "Cold 4",
        "317_18": "Cold 3",
        "302_18": "Cold 2",
        "300_6": "Cold 1",
        "413_17": "Cold 14",
        "418_17": "Cold 15",
        "425_17": "Cold 16",
        "435_17": "Cold 17",
        "438_17": "Cold 18",
        "446_17": "before slave loaded",
        "548_36": "Cold 1",
        "548_37": "Cold 1 (30 mn later)",
        "601_17": "Cold 2",
        "650_18": "Cold 3",
        "664_25": "Cold 4",
        "669_41": "Cold 5",
        "680_18": "Cold 6",
        "691_1": "Cold 7",
        "729_23": "Cold 8",
        "761_1": "Cold 9",
        "835_1": "Cold 10",
        "871_18": "Cold 11",
        "871_70": "Cold 12",
        "875_18": "Cold 13 (10 more cycles)",
        "877_2": "Cold 14 (10 more cycles)",
        "877_75": "Cold 15 (10 more cycles)",
        "885_1": "Cold 16 (10 more cycles)",
        "601_18": "Cold 2",
        "650_1": "Cold 3",
        "664_8": "Cold 4",
        "669_24": "Cold 5",
        "680_1": "Cold 6",
        "681_1": "Cold 7",
        "724_331": "Cold 8",
        "756_1": "Cold 9",
        "827_1": "Cold 10",
        "871_1": "Cold 11",
        "871_53": "Cold 12",
        "875_1": "Cold 13 (10 more cycles)",
        "877_3": "Cold 14 (10 more cycles)",
        "877_58": "Cold 15 (10 more cycles)",
        "885_2": "Cold 16 (10 more cycles)",
        "447_1": "before slave loaded",
        "548_19": "RT 1",
        "582_73": "RT 2",
        "584_1": "RT 3",
        "648_1": "RT 4",
        "669_17": "RT 6",
        "659_1": "RT 5",
        "671_1": "RT 7",
        "678_17": "RT 8",
        "680_35": "RT 9",
        "723_1": "RT 10",
        "750_1": "RT 11",
        "789_1": "RT 12",
        "824_19": "RT 13",
        "869_18": "RT 14",
        "871_43": "RT 15",
        "873_18": "RT 16",
        "875_26": "RT 17 (after 10 cycles)",
        "877_45": "RT 18 (after 10 cycles)",
        "877_100": "RT 19( after 10 cycles)",
        "885_26": "RT 20 (after 10 cycles)",
        "548_1": "RT 1",
        "582_57": "RT 2",
        "584_20": "RT 3",
        "648_2": "RT 4",
        "666_1": "RT 6",
        "671_2": "RT 7",
        "675_1": "RT 8",
        "680_25": "RT 9",
        "718_1": "RT 10",
        "739_1": "RT 11",
        "778_1": "RT 12",
        "824_19": "RT 13",
        "869_19": "RT 14",
        "871_25": "RT 15",
        "871_78": "RT 16",
        "875_27": "RT 17 (10 more cycles)",
        "877_26": "RT 18 (10 more cycles)",
        "877_82": "RT 19 (10 more cycles)",
        "885_28": "RT 20 (10 more cycles)",
        "871_79": "RT 16",
        "875_28": "RT 17 (10 more cycles)",
        "877_27": "RT 18 (10 more cycles)",
        "877_83": "RT 19 (10 more cycles)",
        "885_36": "RT 20 (10 more cycles)",
        "659_1": "RT 5",
        "409_18": "RT 15",
        "402_18": "RT 14",
        "398_18": "RT 13",
        "376_1": "RT 12",
        "358_18": "RT 11",
        "355_2": "RT 10",
        "344_1": "RT 9",
        "339_1": "RT 8",
        "327_34": "RT 7",
        "325_1": "RT 6",
        "317_69": "RT 5",
        "317_35": "RT 4",
        "317_1": "RT 3",
        "302_1": "RT 2",
        "294_1": "RT 1",
        "410_19": "RT 16",
        "414_19": "RT 17",
        "415_18": "RT 18",
        "423_18": "RT 19",
        "433_18": "RT 20",
        "436_33": "RT 21",
        "443_18": "RT 22",
        "447_18": "RT 23",
    }

    # params 
    fileTypes = ["png", "pdf"]
    ymaxIncrement = 0.0
    AMAC_data_color = "C0"
    PS_data_color = "C1"

    # Figure and axis
    # fig, ax = plt.subplots(1, figsize=(10, 4), dpi=200)
    #fig, ax = plt.subplots(1, figsize=(6, 4), dpi=200)
    fig, ax = plt.subplots(1, figsize=(5.5, 4), dpi=200)
    #hep.atlas.text("ITk Strips %s"%(ATLAS_label_), loc=0)

    max_y_values = [np.max(all_CURRENT_vals_[i]) for i in range(N_curves)]
    #sorted_modules = np.argsort(max_y_values)
    sorted_modules = sorted(range(len(runnums_)), key=lambda i: tuple(map(int, runnums_[i].split('_'))))

    lines = []
    indicies_to_remove = []

    for i, module_i in enumerate(sorted_modules):
        removeModule = 0
        CURRENT_vals_ = all_CURRENT_vals_[module_i]
        VOLTAGE_vals_ = all_VOLTAGE_vals_[module_i]
        CURRENT_RMS_vals_ = all_CURRENT_RMS_vals_[module_i]
        PS_CURRENT_vals_ = all_PS_CURRENT_vals_[module_i]
        component_ = components_[module_i]
        runnum_ = runnums_[module_i]

        # Subtract the y intercept from all values (offset)
        val_0 = float(CURRENT_vals_[0])
        if float(CURRENT_vals_[1])<float(CURRENT_vals_[0]):
            val_0 = float(CURRENT_vals_[1])
        CURRENT_vals_ = [float(val_i - val_0) for val_i in CURRENT_vals_]
        AbsVal = 0
        if(AbsVal): CURRENT_vals_ = np.abs(CURRENT_vals_)

        for moduleToRemove in removed_modules_:
            if(moduleToRemove in runnum_):
                indicies_to_remove.append(i)
                removeModule = 1

        if(not removeModule):
            localID = labelMap[component_]
            if(runnum_ not in runMap.keys()):
                    print(runnum_)
                    runlabel = "NoRunLabelFound"
                    print("Cannot find",runnum_)
            else:
                runlabel = runMap[runnum_]
        else: localID, runlabel = "", ""
        if(removeModule): label = "_"
        else: 
            label = f"{runlabel}"
        #color = cmap((i+4) / (N_curves+4))

        ##### hack
        #####
        if(i==0): color = "green"
        elif(i==1): color = "darkred"

        # proper way to do it
        #color = cmap((i) / (N_curves))
        



        #if i==1:
        #    color = cmap(0.8)
        alpha = 0.9
        if(removeModule): plt.autoscale(False)
        print(ax.get_ylim())
        #print(CURRENT_vals_)
        #thisLine = ax.errorbar(VOLTAGE_vals_, CURRENT_vals_, yerr=CURRENT_RMS_vals_, xerr=None, marker='o', markersize=1.5, elinewidth=1, linewidth=0.5, label = label, color = color, alpha=alpha)
        thisLine = ax.errorbar(VOLTAGE_vals_, CURRENT_vals_, yerr=CURRENT_RMS_vals_, xerr=None, marker='o', markersize=5, elinewidth=2, linewidth=2, label = label, color = color, alpha=alpha)
        lines.append(thisLine)
        print("i",i,"lims:",ax.get_ylim())
        if(i == (len(sorted_modules) - 1)): 
            ymin, ymax = ax.get_ylim()
            print("ymin:",ymin)
            print("ymax:",ymax)
        plt.autoscale(True)
        #if(i==0 and removeModule): ax.set_ylim(0,1)

    for indexToRemove in indicies_to_remove: # remove line while preserving color scheme
        lines[indexToRemove].remove()

    ax.set_xlabel(x_label_)
    ax.set_ylabel(y_label_) #, color=AMAC_data_color)
    ax.tick_params(axis='y', which='both') #colors=AMAC_data_color) # both = major and minor


    ax.spines['top'].set_color('black')
    ax.spines['bottom'].set_color('black')

    # set ymax higher to contain plot labels
    # ymin, ymax = ax.get_ylim()
    #ymax=1000 #change y max for my nefarious purposes
    #Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    #ax.set_ylim(0, Incremented_Ymax)
    #ax.set_xlim(-400,0)

    upperText = f"{inDir_}"
    upperText = upperText.replace("_", " ")
    middleText = "Module IV curves"
    lowerText = ""
    if(len(removed_modules_) > 0):
        lowerText = "Modules removed: "
        for removedModule in removed_modules_: 
            removedModule 
            lowerText += f"{removedModule}, "
        lowerText = lowerText[:-2]

    plotUpperText = 0

    if(plotUpperText):

        plt.text(
            0.94,
            0.95,
            "\n".join([
                upperText,
                middleText,
            ]
            ),
            horizontalalignment='right',
            verticalalignment='top',
            fontweight = 'bold',
            transform=ax.transAxes
        )

        plt.text(
            0.94,
            0.78,
            lowerText,
            horizontalalignment='right',
            verticalalignment='top',
            # fontweight = 'bold',
            transform=ax.transAxes,
            fontsize = 8
        )

    ax.ticklabel_format(style='plain')
    ax.set_yscale('log')
    ax.legend(loc='best', fontsize = 15)
    #ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize = 15)
    
    #handles, labels = ax.get_legend_handles_labels()
    #ax.legend(handles[::-1], labels[::-1], loc='center left', bbox_to_anchor=(1, 0.5), fontsize = 7) #, title='Line', loc='upper left')
    #ax.legend(handles[::-1], labels[::-1], loc='center left', bbox_to_anchor=(1, 0.5), fontsize = 20) #, title='Line', loc='upper left')
    #ax.legend(handles[::-1], labels[::-1], loc='center left', bbox_to_anchor=(1, 0.5), fontsize = 20) #, title='Line', loc='upper left')
    
    ax.set_xlim(-100,0)
    #ax.set_yscale('log')

    fig.tight_layout()
    Save_Figures(fig, fileTypes, OUTNAME_, "IVCurve")  

# parameters, common to inFile and inDir methods
include_PSinfo = 0
RC_filter = 0 # if RC filter present, reduce PS voltage to get sensor bias voltage
absval = 0
bypassAMAC = 0
ATLAS_label = "Internal" # Preliminary, Internal, ...
simpleName = 0

dataset_names = [
            "CURRENT", "CURRENT_RMS", # from AMAC (or bypass)
            "PS_CURRENT", "VOLTAGE" # from PS
        ]

#xlabel, ylabel = "PS Voltage [V]", "Sensor Current [nA]"
#xlabel, ylabel = "Voltage [V]", "Current [nA]"
xlabel, ylabel = "Voltage [V]", "Current [nA]"
if(bypassAMAC): ylabel = "Sensor Current [nA]"

if(inFile is not None):
    fileTypes = ["png", "pdf"]
    print("in file:",inFile)
    f = open(inFile)
    data = json.load(f)
    if(debug): print("data:",data)

    component = data["component"]
    TEMPERATURE = data["results"]["TEMPERATURE"]
    if(debug):
        print("component:",component)
        print("TEMPERATURE:",TEMPERATURE)

    for dataset_name in dataset_names:
        exec("{}_vals = data['results']['{}']".format(dataset_name, dataset_name))
        if(debug):
            exec("print('{}_vals:',{}_vals)".format(dataset_name, dataset_name))

    OUTNAME = "{}/{}_{}degC".format(ol, component, TEMPERATURE)

    if("bypass" in OUTNAME): 
        bypassAMAC = 1
        absval = 1
        include_PSinfo = 0
    if(absval):
        #CURRENT_vals = [abs(i) for i in CURRENT_vals]
        CURRENT_vals = [abs(i) for i in CURRENT_vals]
        #CURRENT_vals /= 1000.
    if(RC_filter):
        VOLTAGE_vals = [i*10./11. for i in VOLTAGE_vals]
    
    #CURRENT_vals /= 1000.

    Make_IV_Plot(VOLTAGE_vals, CURRENT_vals, CURRENT_RMS_vals, PS_CURRENT_vals, xlabel, ylabel, OUTNAME, component, TEMPERATURE, debug, include_PSinfo, bypassAMAC, ATLAS_label, simpleName) # include component as text on plot 

if(inDir is not None):
    
    modules_to_skip=[
        # "1234971"
    ]
    
    OUTNAME = f"{ol}/{inDir}_IVs_Combined"
    files = [f"{inDir}/{f_}" for f_ in os.listdir(inDir)]
    print("files:",files)
    all_VOLTAGE_vals, all_CURRENT_vals, all_CURRENT_RMS_vals, all_PS_CURRENT_vals, components, RunNums = [], [], [], [], [], []
    removed_modules = []
    for file in files:
        skip = 0
        for module_to_skip in modules_to_skip: 
            if(module_to_skip in file): 
                print("Skipping",file)
                f = open(file)
                data = json.load(f)
                component = data["component"]
                removed_modules.append(component)
                skip = 0 # don't actually skip it, preserve colors
        if(skip): continue
        f = open(file)
        data = json.load(f)
        component = data["component"]
        TEMPERATURE = data["results"]["TEMPERATURE"]
        if "ON_CORE" in file:
            RunNum = file.split("_")[4]+"_"+file.split("_")[5]
        else:
            RunNum = file.split("_")[3]+"_"+file.split("_")[4]
        print(RunNum)
        for dataset_name in dataset_names:
            exec("{}_vals = data['results']['{}']".format(dataset_name, dataset_name))
            if(debug):
                exec("print('{}_vals:',{}_vals)".format(dataset_name, dataset_name))

        if(absval):
            CURRENT_vals = [abs(i) for i in CURRENT_vals]
        if(RC_filter):
            VOLTAGE_vals = [i*10./11. for i in VOLTAGE_vals]

        # hack
        #CURRENT_vals = [value/1000. for value in CURRENT_vals]

        all_VOLTAGE_vals.append(VOLTAGE_vals)
        all_CURRENT_vals.append(CURRENT_vals)
        all_CURRENT_RMS_vals.append(CURRENT_RMS_vals)
        all_PS_CURRENT_vals.append(PS_CURRENT_vals)
        components.append(component)
        RunNums.append(RunNum)
    #print(all_CURRENT_vals)
    Make_Combined_IV_Plot(all_VOLTAGE_vals, all_CURRENT_vals, all_CURRENT_RMS_vals, all_PS_CURRENT_vals, xlabel, ylabel, OUTNAME, components, ATLAS_label, inDir, removed_modules,RunNums)

