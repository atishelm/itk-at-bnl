# imports 
import json 
import pandas as pd 
import numpy as np 
import os 
import csv 
import matplotlib 
# matplotlib.use('Agg') # turn off interactive mode
import matplotlib.pyplot as plt
import mplhep as hep 
plt.style.use(hep.style.ATLAS) # customization: https://mplhep.readthedocs.io/en/latest/api.html
from matplotlib.ticker import MaxNLocator

def MakePlot(Tests, strip_numbers, TestNotes, legendOutside, plotName, cmap, alpha):
    ATLAS_label = "Internal"
    fig, ax = plt.subplots(1, figsize=(9, 4), dpi=200)
    hep.atlas.text("ITk Strips %s"%(ATLAS_label), loc=0)
    N_lines = len(Tests)

    for i, Test in enumerate(Tests):
        these_strip_numbers = strip_numbers.copy()
        data = df.loc[:, Test]
        testNote = TestNotes[Test]
        label = f"{Test}, {testNote}"
        color = cmap((i+2) / (N_lines+2))

        ymin, ymax = plt.ylim()
        if("RT" in Test):
            ymax = 3000 # set y max to desired value
            plt.ylim(ymin, ymax)

        plt.plot(these_strip_numbers, data, '-o', label = label, markersize = 5, color=color, alpha=alpha)

        # remove a given point 
        # plt.plot(these_strip_numbers[:14], data[:14], '-o', label = label, markersize = 5, color=color, alpha=alpha)
        # plt.plot(these_strip_numbers[15:], data[15:], '-o', label = None, markersize = 5, color=color, alpha=alpha)
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.ticklabel_format(style='plain')

    # legend

    # outside plot
    if(legendOutside):
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles[::-1], labels[::-1], loc='center left', bbox_to_anchor=(1, 0.5), fontsize = 9) #, title='Line', loc='upper left')

    # in plot
    else:
        ymin, ymax = plt.ylim()
        ymaxIncrement = 0.75
        Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement # to fit legend
        ax.set_ylim(ymin, Incremented_Ymax)
        ax.legend(loc='upper left', fontsize = 9)
    
    # decorate
    plt.xlabel("Strip number")
    plt.ylabel("Input noise [ENC]")
    ymin, ymax = plt.ylim()
    plt.vlines(1046, ymin, ymax, colors='k', linestyles='dashed')
    plt.ylim(ymin, ymax)
    plt.xticks(rotation='vertical')

    # finish
    fig.tight_layout()
    plt.show()
    fig.savefig(f"{plotName}_plot.png")
    fig.savefig(f"{plotName}_plot.pdf")
    plt.close()

# get the data
df = pd.read_csv("Noise.csv")
strip_numbers = df.loc[:, 'Strip number']

# choose the tests you want to plot
ColdTests = ["Cold 9 (before suspected crack)", 
             "Cold 10 (suspected crack)", 
             "Cold 11", 
             "Cold 12", 
             "Cold 13", 
             "Cold 14", 
             "Cold 15", 
             "Cold 16", 
             "Cold 17", 
             "Cold 18", 
             "Cold 19"
             ]

WarmTests = ["RT 10 (before suspected crack)",
             "RT 11 (after suspected crack)",
             "RT 12",
             "RT 13",
             "RT 14", 
             "RT 15", 
             "RT 16", 
             "RT 17", 
             "RT 18",
             "RT 19", 
             "RT 20",
             "RT 22",
             "RT 23",
            ]

# notes for each test
TestNotes = {
    "Cold 9 (before suspected crack)" : "-350V 10PG",
    "Cold 10 (suspected crack)" : "-350V 10PG",
    "Cold 11" : "-140V 3PG",
    "Cold 12" : "-140V 3PG",
    "Cold 13" : "-140V 3PG",
    "Cold 14" : "-140V 3PG",
    "Cold 15" : "-140V 3PG",
    "Cold 16" : "-140V 3PG",
    "Cold 17" : "-140V 3PG",
    "Cold 18" : "-140V 3PG",
    "Cold 19" : "-140V 3PG",
    "RT 10 (before suspected crack)" : "-350V 10PG",
    "RT 11 (after suspected crack)" : "-140V 3/10PG",
    "RT 12" : "-140V 3/10PG",
    "RT 13" : "-140V 3/10PG",
    "RT 14" : "-140V 3PG",
    "RT 15" : "-126V 3PG",
    "RT 16" : "-100V 3PG",
    "RT 17" : "-50V 3PG",
    "RT 18" : "-45V 3PG",
    "RT 19" : "-50V 3PG",
    "RT 20" : "-50V 3PG",
    "RT 21" : "-50V 3PG",
    "RT 22" : "-100V 3PG",
    "RT 23" : "-100V 3PG"
}

# colors
cold_cmap = plt.get_cmap('Blues')
warm_cmap = plt.get_cmap('Reds')
alpha = 1.0

# params
legendOutside = 1
plotName = ""

# make the plot
plotName = "Cold"
MakePlot(ColdTests, strip_numbers, TestNotes, legendOutside, plotName, cold_cmap, alpha)

plotName = "Warm"
MakePlot(WarmTests, strip_numbers, TestNotes, legendOutside, plotName, warm_cmap, alpha)