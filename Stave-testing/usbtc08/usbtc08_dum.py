

USBTC08_CHANNEL_CJC = 0
USBTC08_CHANNEL_1 = 1
USBTC08_CHANNEL_2 = 2
USBTC08_CHANNEL_3 = 3
USBTC08_CHANNEL_4 = 4
USBTC08_CHANNEL_5 = 5
USBTC08_CHANNEL_6 = 6
USBTC08_CHANNEL_7 = 7
USBTC08_CHANNEL_8 = 8

USBTC08_UNITS_CENTIGRADE = 0
USBTC08_UNITS_FAHRENHEIT = 0
USBTC08_UNITS_KELVIN = 0
USBTC08_UNITS_RANKINE = 0

USBTC08_ERROR_OK = "No error occurred"
USBTC08_ERROR_OS_NOT_SUPPORTED = "The driver does not support the current operating system"
USBTC08_ERROR_NO_CHANNELS_SET = "A call to usb_tc08_set_channel() is required"
USBTC08_ERROR_INVALID_PARAMETER = "One or more of the function arguments were invalid"
USBTC08_ERROR_VARIANT_NOT_SUPPORTED = "The hardware version is not supported Download the latest driver"
USBTC08_ERROR_INCORRECT_MODE = "An incompatible mix of legacy and non-legacy functions was called (or usb_tc08_get_single() was called while in streaming mode)"
USBTC08_ERROR_ENUMERATION_INCOMPLETE = "Function usb_tc08_open_unit_async() was called again while a background enumeration was already in progress"
USBTC08_ERROR_NOT_RESPONDING = "Cannot get a reply from a USB TC-08"
USBTC08_ERROR_FW_FAIL = "Unable to download firmware"
USBTC08_ERROR_CONFIG_FAIL = "Missing or corrupted EEPROM"
USBTC08_ERROR_NOT_FOUND = "Cannot find enumerated device"
USBTC08_ERROR_THREAD_FAIL = "A threading function failed"
USBTC08_ERROR_PIPE_INFO_FAIL = "Can not get USB pipe information"
USBTC08_ERROR_NOT_CALIBRATED = "No calibration date was found"
USBTC08_EROOR_PICOPP_TOO_OLD = "An old picoppsys driver was found on the system"
USBTC08_ERROR_PICO_DRIVER_FUNCTION = "Undefined error"
USBTC08_ERROR_COMMUNICATION = "The PC has lost communication with the device"

sizeof_USBTC08_INFO = 0
USBTC08_MAX_INFO_CHARS = 128
USBTC08_MAX_CHANNELS = 8
USBTC08_MAX_SAMPLE_BUFFER = 16
USBTC08_PROGRESS_PENDING = 0
USBTC08_PROGRESS_FAIL = 1
USBTC08_PROGRESS_COMPLETE = 2
USBTC08_MAX_VERSION_CHARS = 128
USBTC08LINE_DRIVER_VERSION = 0
USBTC08LINE_KERNEL_DRIVER_VERSION = 1
USBTC08LINE_HARDWARE_VERSION = 2
USBTC08LINE_VARIANT_INFO = 0
USBTC08_MAX_SERIAL_CHARS = 128
USBTC08LINE_BATCH_AND_SERIAL = 3
USBTC08LINE_CAL_DATE = 4
USBTC08_MAX_DATE_CHARS = 128

def setbuf (buf, maxlen, s):
    l = min(len(s), maxlen)
    for i in range(l):
        buf[i] = ord(s[i])
    return l

class tc08dum:
    pass
def USBTC08_INFO():
    return tc08dum()
def usb_tc08_open_unit_async():
    return tc08dum()
def usb_tc08_close_unit(self):
    return tc08dum()
def usb_tc08_get_unit_info2(h, buf, maxlen, item):
    if item == USBTC08LINE_DRIVER_VERSION:
        return setbuf (buf, maxlen, 'driver_vers')
    elif item == USBTC08LINE_KERNEL_DRIVER_VERSION:
        return setbuf (buf, maxlen, 'kern_driver_vers')
    elif item == USBTC08LINE_HARDWARE_VERSION:
        return setbuf (buf, maxlen, 'hw_vers')
    elif item == USBTC08LINE_BATCH_AND_SERIAL:
        return setbuf (buf, maxlen, 'batch_and_serial')
    elif item == USBTC08LINE_CAL_DATE:
        return setbuf (buf, maxlen, 'calibration_date')
    return 0
def usb_tc08_set_channel(h, chanl, tc):
    return 1
def usb_tc08_set_mains(h, i):
    return 60
def usb_tc08_get_minimum_interval_ms(h):
    return 20
def usb_tc08_get_single(h, cb, f, u):
    return 1
def usb_tc08_run (h, i):
    return 1
def usb_tc08_get_temp_deskew (h, tempbuffer, timebuffer, maxlen, flags, channel, unit, x):
    tempbuffer[0] = 22
    timebuffer[0] = 0
    return 1

def usb_tc08_open_unit_progress():
    return USBTC08_PROGRESS_COMPLETE, 1, None
def usb_tc08_get_last_error(self):
    return tc08dum()
def charArray(maxn):
    return [0]*maxn
floatArray = charArray
intArray = charArray
shortArray = charArray
