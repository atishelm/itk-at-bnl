# ABCStar

The ABCStar (ATLAS Binary Chip with a Star layout) is an ASIC used to determine if hits are registered on individual strips. An image is shown below:

<img width="500" src="/Images/Hardware/ABCStar.jpg">

The specifications for the ABCStar can be [[downloaded here]](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/blob/master/Documents/Specifications/ABCStar_Spec.pdf?ref_type=heads), or viewed below:

<iframe src="https://atishelm.web.cern.ch/atishelm/ITk/itk-at-bnl/ABCStar_Spec.pdf" 
        style="border: 0;" 
        width="1000" 
        height="1000" 
        frameborder="0" 
        scrolling="yes">
</iframe>