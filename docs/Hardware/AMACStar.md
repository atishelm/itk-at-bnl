# AMACStar

The AMACStar (Autonomous Monitoring and Control chip with a Star layout) is an ASIC housed on powerboards used for many functionalities, including powering the Hybrid on/off, measuring sensor current, monitoring hybrid NTC temperature, opening and closing the ganfet, and more.

An image is shown below:

<img width="250" src="/Images/Hardware/AMACStar.png">

The specifications for the AMACStar can be [[downloaded here]](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/raw/master/Documents/Specifications/AMACStar_Spec.pdf?ref_type=heads&inline=false), or viewed below:

<iframe src="https://atishelm.web.cern.ch/atishelm/ITk/itk-at-bnl/AMACStar_Spec.pdf" 
        style="border: 0;" 
        width="1000" 
        height="1000" 
        frameborder="0" 
        scrolling="yes">
</iframe>

The register map can be [[downloaded here]](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/raw/master/Documents/Specifications/AMACStarRegistersMap.xlsx?ref_type=heads), or viewed on [[Google sheets here]](https://docs.google.com/spreadsheets/d/1ABy-n2Exh8UaL9MwV7VHPbh_SJLMDinf/edit?usp=sharing&ouid=105416624867850906028&rtpof=true&sd=true), or viewed below:

<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ256oTlD0j0w1lRBBLAAjngp-rZQxz2zcBddvc19ty73PFQ7apCUBbYB_cxKH7eQ/pubhtml" 
        style="border: 0;" 
        width="1000" 
        height="1000" 
        frameborder="0" 
        scrolling="yes">
</iframe>