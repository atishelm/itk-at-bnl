# HCCStar

The HCCStar (Hybrid Controller Chip with a Star layout) is an ASIC used to control ABCStars. 

The specifications for the HCCStar can be [[downloaded here]](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/blob/master/Documents/Specifications/HCCStar_Spec.pdf?ref_type=heads), or viewed below:

<iframe src="https://atishelm.web.cern.ch/atishelm/ITk/itk-at-bnl/HCCStar_Spec.pdf" 
        style="border: 0;" 
        width="1000" 
        height="1000" 
        frameborder="0" 
        scrolling="yes">
</iframe>