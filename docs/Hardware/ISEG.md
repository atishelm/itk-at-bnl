# ISEG

The ISEG HV PSU is used for biasing our ITk sensors. 

## Interlocks

The ISEGs connected to the three module testing boxes (Box 1, Box 2, Box 3) are all equipped with hardware interlocks. When the coldbox is opened, the ISEG is automatically thrown into an ERROR state on all of its channels. This probably ramps down the HV to 0 no matter what value it was at (to be confirmed).

Additionally, the ISEGs are run in constant current mode. This means if the compliance (set current) value is reached, the ISEG will attempt to keep the current constant while adjusting the voltage to do so. Note that if you change the ISEG settings, it's possible to change to a trip interlock mode. If you change to this mode, if the set current value is reached, the ISEG will go into a trip state and ramp down voltage to zero. 

[[Link to ISEG manual]](https://iseg-hv.com/download/AC_DC/SHR/iseg_manual_SHR_en.pdf)

## Firmware

To check firmware version on ISEG:

Settings -> Device Info

If the ISEG does not appear to be scanning properly (for example, ramping down 10-20V before ramping up to the next step) you might try updating the software and firmware on the ISEG. You can check which version of both you have by navigating on the ISEG to Control>Device Setup. Update the software by downloading the software from [this link](https://iseg-hv.com/download/?dir=SOFTWARE/iCS/). The link also contains instructions for the download--you will have to type the ISEG IP into your browser to upload the new software. Firmware can be downloaded from the same link and uploaded in a similar way, but you will need to navigate to the hardware tab on the ISEG GUI in your browser to finish the installation.

Let it be known that I don't know what I'm doing.

Click the ethernet connection logo on the upper right of the PC. Click "real tech ethernet connection". Select "ISEG".

When something gets unplugged, click the settings under the ethernet logo. Click Network->ISEG->Settings->IPv4, update the address with what's on the iseg except change the last digit for your own use. Just make sure you have the same subnet (aaa.bbb.ccc).

Enter the address on the ISEG into your browser to launch the webserver. Do not put http or https before the address.

Make sure power_supplies.json has the updated port.

## Problems

If when running ITSDAQ, it just says "Made it!" and quits, your power supply connection is not set up properly. You can verify this by typing the IP address in your browser and see if you can connect to the webserver. This could  be because the ISEG needs to be power cycled before the IP address it is set at sticks. You can see if this works by trying to ping the ISEG IP before and after power cycling.
