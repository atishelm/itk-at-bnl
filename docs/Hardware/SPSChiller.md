# SPS Chiller

The SPS chiller is used to control the temperature of modules and staves, and is shown below:

<img width="500" src="/Images/Hardware/SPS_Chiller.png">

The following are the hardware limits set in the SPS chillers, separate for the module testing and stave testing setups:

Module testing:

<img width="500" src="/Images/Hardware/SPS_Settings_Module.png">

Stave testing:

<img width="500" src="/Images/Hardware/SPS_Settings_Stave.png">