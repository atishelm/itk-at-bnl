# lpGBT

<img width="500" src="/Images/Hardware/lpGBTlayoutImage.png">

The lpGBT (Low Power Giga Bit Transceiver) is a radiation tolerant ASIC used to implement optical links between the stave and FPGA for DAQ. More introductory information can be found here:

[[lpGBT introduction]](https://lpgbt.web.cern.ch/lpgbt/v1/introduction.html)

More information and a full manual on the lpGBT versions v0 and v1 can be found here:

[[lpGBT website]](https://lpgbt.web.cern.ch/lpgbt/)