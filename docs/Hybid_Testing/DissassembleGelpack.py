import json, os, sys

sys.path.append("../../") # path to dir containing itk_pdb 
import itk_pdb.dbAccess as dbAccess
sys.path.insert(1, '../modules/')

import numpy as np


if os.getenv("ITK_DB_AUTH"):
    dbAccess.token = os.getenv("ITK_DB_AUTH")



gelpackSN='20USGDA1175005'

d = {

        "component": gelpackSN,
        "noTests": "true",

    }

result = dbAccess.doSomething("getComponent",method='GET',data=d)

for child in result["children"]:
    if child["component"] is not None:
        SN = child["component"]["serialNumber"]

        d2 = {
                "parent": gelpackSN,
                "child": SN,
                "trashed": "false",
            }

        result2 = dbAccess.doSomething(action="disassembleComponent",method='POST',data=d2)

        print("dissassembled component " + str(SN) + " from gelpack")


