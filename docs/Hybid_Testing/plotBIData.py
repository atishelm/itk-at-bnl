import os
import argparse
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
import tarfile
import datetime
import json
from glob import glob
import seaborn as sns
from matplotlib.ticker import MultipleLocator



#Loads data from RC files into a dictionary.  data[file_name][parameter (ie gain)][channel] = value at channel for parameter in file_name
def load_data(hybrid_RC):
    data = {};
    
    for file in hybrid_RC:
         
        try:
            with open(file, "r" ) as rF:
                lLines = rF.readlines()
            data[file] = [];
            ch_state = [];


            for i,aL in enumerate(lLines):
                clData = aL.strip()
                
                if len(clData) == 0 or clData.startswith("#"):
                    continue;      
                els = clData.split()
                #Calculate Output Noise
                output_noise = float(els[4]) * float(els[2]) / 6250 
                data[file].append( [float(els[0]), float(els[1]), float(els[2]), float(els[3]), float(els[4]), output_noise] )
                ch_state.append(" ".join(els[5:]));
            data[file] = np.array(data[file]).transpose().tolist();
            data[file].append(ch_state);
            
      
        except Exception:
            print(" error processing file " + file + ", exiting...")
            return
    
    #Obtain number of chips in hybrid 
    chips = int(len(data[hybrid_RC[0]][0]) / 256)
    return data, chips;




#Calculates basic parameter statistics such as the average and SD for each channel, each run, and in total
def calc_param_stats(data, chips, param, param_name, run_num, h, percent_runs = 1, displayPlot = True, savePlot = False):
     
    param_data = [];
    avg_run = [];
    org_run_num = np.arange(len(run_num));
    
    
    
    for file in data:
        param_data.append(data[file][param]);
    #For run statistics
    param_data_run = np.array(param_data)[np.argsort(run_num)];
    param_data_run_sect = []; 
    for i in range(0, int(1/percent_runs)):
        param_data_run_sect.append(param_data_run[int((i * percent_runs) * len(param_data_run)) : int((i + 1) * percent_runs * len(param_data_run))]);
   
     
    #For channel statistics
    param_data_ch = np.transpose(param_data_run);
    param_data_ch_init = np.transpose(param_data_run_sect[0]);
    param_data_ch_final = np.transpose(param_data_run_sect[-1]);
   
    param_data_ch_sect = [];
    for i in range(0, int(1/percent_runs)):
        param_data_ch_sect.append(np.transpose(param_data_run_sect[i]));

    
    #For chip statistics
    param_data_group1 = np.swapaxes(np.split(np.transpose(param_data_ch[0 : int(len(param_data_ch) / 2) ]), chips, axis = 1), 0 ,1);   
    param_data_group2 = np.swapaxes(np.split(np.transpose(param_data_ch[int(len(param_data_ch) / 2) : ]), chips, axis = 1), 0, 1);
    param_data_chip = np.concatenate((param_data_group1, param_data_group2), axis = 2)

    param_data_chip_sect = [];
    
    for i in range(0, int(1/percent_runs)):
        param_data_group1_sect = np.swapaxes(np.split(np.transpose(param_data_ch_sect[i][0 : int(len(param_data_ch) / 2) ]), chips, axis = 1), 0, 1);
        param_data_group2_sect = np.swapaxes(np.split(np.transpose(param_data_ch_sect[i][int(len(param_data_ch) / 2) : ]), chips, axis = 1), 0, 1);
        param_data_chip_sect.append(np.concatenate((param_data_group1_sect, param_data_group2_sect), axis = 2));

    
    #Stats per run
    param_data_run_avg = np.average(param_data_run, axis = 1);
    param_data_run_std = np.std(param_data_run, axis = 1);
   
 
    #Stats per chip
    param_data_chip_avg = np.transpose(np.average(param_data_chip, axis = 2));
    param_data_chip_std = np.transpose(np.std(param_data_chip, axis = 2));

       
    param_data_chip_avg_sect = [];
    param_data_chip_std_sect = [];
    
    for i in range(0, int(1/percent_runs)):
        param_data_chip_sect_inter = np.swapaxes(param_data_chip_sect[i],0,1)
        param_data_chip_sect_inter = param_data_chip_sect_inter.reshape(param_data_chip_sect_inter.shape[0], param_data_chip_sect_inter.shape[1] * param_data_chip_sect_inter.shape[2])
        param_data_chip_avg_sect.append(np.average(param_data_chip_sect_inter, axis = 1));
        param_data_chip_std_sect.append(np.std(param_data_chip_sect_inter, axis = 1));
    
    param_data_chip_avg_sect_transpose = np.transpose(param_data_chip_avg_sect);
    param_data_chip_std_sect_transpose = np.transpose(param_data_chip_std_sect);
    

    
    #Chip Pull Statistics
    param_data_chip_pull_stats = (param_data_chip_avg_sect[4] - param_data_chip_avg_sect[-1]) / param_data_chip_std_sect[4];
   
    
    if(displayPlot or savePlot): 
        
        #AveragePerRun Plots
        for i in range(0,chips):
            plt.plot(org_run_num, param_data_chip_avg[i], marker = 'o', label = f"Chip {i}");
            #np.save(f"{h}_{param_name}_Chip_{i}.npy", param_data_chip_avg[i], allow_pickle = True);
        plt.plot(org_run_num, param_data_run_avg, marker = 'o', color = "black", label = f"All Channels");
        #np.save(f"{h}_{param_name}_All_Channels.npy", param_data_run_avg, allow_pickle=True);
        plt.title(f"Average of the {param_name} per Test for {h}");
        plt.xlabel("Test");
        plt.ylabel(f"Average {param_name}")
        plt.legend();
        
        if(savePlot):
            plt.savefig(f"{h}/{h}-Avg{param_name}_Per_Test"); 
        if(displayPlot):
            plt.show();

        plt.clf(); 
 
        #SDPerRun Plots
        for i in range(0, chips):
            plt.plot(org_run_num, param_data_chip_std[i], marker = 'o', label = f"Chip {i}");
        plt.plot(org_run_num, param_data_run_std, marker = 'o', color = 'black', label = f"All Channels");       
        plt.title(f"Standard Deviation of the {param_name} per Test for {h}");
        plt.xlabel("Test");
        plt.ylabel("Standard Deviation");
        plt.legend();
        
        if(savePlot):
            plt.savefig(f"{h}/{h}-SD{param_name}_Per_Test");
        if(displayPlot):
            plt.show();

        plt.clf();
        
        #AveragePerChip Plots
        plt.plot(np.arange(len(param_data_chip_avg_sect[0])), param_data_chip_avg_sect[1], 'o', label = f"Average of the {param_name} per Chip Over First {100 * percent_runs}% of Tests");
        plt.plot(np.arange(len(param_data_chip_avg_sect[1])), param_data_chip_avg_sect[1], 'o', label = f"Average of the {param_name} per Chip Over Second {100 * percent_runs}% of Tests"); 
        plt.plot(np.arange(len(param_data_chip_avg_sect[-1])), param_data_chip_avg_sect[-1], 'o', label = f"Average of the {param_name} per Chip Over Last {100 * percent_runs}% of Tests");
        plt.xlabel("Chip");
        plt.ylabel(f"Average {param_name}");    
        plt.title(f"Average {param_name} per Chip for {h}");
        plt.legend();

        if(savePlot):
            plt.savefig(f"{h}/{h}-Avg{param_name}_Per_Chip_Over_SubsetofTests");
        
        if(displayPlot):
            plt.show();
        
        plt.clf();
        
        #SDPerChip Plots
        plt.plot(np.arange(len(param_data_chip_std_sect[0])), param_data_chip_std_sect[0], 'o', label = f"Standard Deviation of the {param_name} per Chip Over First {100 * percent_runs}% of Tests");
        plt.plot(np.arange(len(param_data_chip_std_sect[1])), param_data_chip_std_sect[1], 'o', label = f"Standard Deviation of the {param_name} per Chip Over Second {100 * percent_runs}% of Tests");
        plt.plot(np.arange(len(param_data_chip_std_sect[-1])), param_data_chip_std_sect[-1], 'o', label = f"Standard Deviation of the {param_name} per Chip Over Last {100 * percent_runs}% of Tests");
        plt.xlabel("Chip");
        plt.ylabel(f"Standard Deviation of the {param_name}");
        plt.title(f"Standard Deviation of the {param_name} per Chip for {h}");
        plt.legend();
        
        if(savePlot):
            plt.savefig(f"{h}/{h}-SD{param_name}_Per_Chip_Over_SubsetofTests")
        
        if(displayPlot):
            plt.show();
       
        plt.clf();


        #5% Chunk AveragePerChip
        for i in range(0, len(param_data_chip_avg_sect_transpose)):
            plt.plot(np.arange(int(1/percent_runs)), param_data_chip_avg_sect_transpose[i] / param_data_chip_avg_sect_transpose[i][-1], marker = 'o', label = f"Chip {i}");
        plt.xlabel(f"ith {percent_runs * 100}% of Tests");
        plt.ylabel("Average/Last Average");
        plt.title(f"Average {param_name} for Each Chip Over a Subset of Tests");
        plt.legend();

        if(savePlot):
            plt.savefig(f"{h}/{h}-Avg{param_name}OfChip_Per_SubsetofTests");
    
        if(displayPlot):
            plt.show();

        plt.clf();


        #5% Chunk StdPerChip
        for i in range(0, len(param_data_chip_std_sect_transpose)):
            plt.plot(np.arange(int(1/percent_runs)), param_data_chip_std_sect_transpose[i] / param_data_chip_std_sect_transpose[i][-1], marker = 'o', label = f"Chip{i}");
        plt.xlabel(f"ith {percent_runs * 100}% of Tests");
        plt.ylabel("Standard Deviation / Last Standard Deviation");
        plt.title(f"SD of the {param_name} for Each Chip Over a Subset of Tests");
        plt.legend();

        if(savePlot):
            plt.savefig(f"{h}/{h}-SD{param_name}OfChip_Per_SubsetofTests");
        
        if(displayPlot):
            plt.show();

        plt.clf();

    return [org_run_num, param_data_chip_avg, param_data_run_avg, param_data_chip_std, param_data_run_std, param_data_chip_avg_sect, param_data_chip_std_sect, param_data_chip_pull_stats];
    


#Finds the number of bad channels per run
#Returns an array in proper run-order
def num_bad_channel(data, run_num, displayPlot = True, savePlot = False):
    
    plt.clf();
    num_bad_ch = [];
    
    for file in data:
        #Finds the number of good channels and subtracts it from the total        
        num_g_ch = data[file][6].count("OK");
        num_g_ch += data[file][6].count('unbonded');
        
        num_bad_ch.append((len(data[file][6]) - num_g_ch));
    num_bad_ch = np.array(num_bad_ch)[np.argsort(run_num)];
    org_run_num = np.arange(len(run_num));
    if(displayPlot or savePlot):
        plt.plot(org_run_num, num_bad_ch, 'o', label = f'{h}');
        plt.xlabel("Run Number");
        plt.ylabel("Number of Bad Channels");
        plt.title("Bad Channel vs Run Histogram");
        plt.legend();

        if(displayPlot):
            plt.show();
        if(savePlot):
            plt.savefig(f"{h}/{h}-Bad_Channel_vs_Run");
        
    return [org_run_num, num_bad_ch];


#Returns an array corresponding to a hist of bad channels (ordered by channel)
def bad_ch_hist(data, h, displayPlot = True, savePlot = False):
    data_list = [];
    bad_ch_hist = [];
    
    for file in data:
        data_list += [data[file][6]];
    
    data_list = np.transpose(np.array(data_list)).tolist();
    
    for lists in data_list:
        num_g_ch = lists.count("OK");
        num_g_ch += lists.count("unbonded");    
        bad_ch_hist.append(len(lists) - num_g_ch);
    
    channels = np.arange(len(bad_ch_hist));
    plt.clf();
    if(displayPlot or savePlot):
        plt.plot(channels, bad_ch_hist, 'o', label = f'{h}');
        plt.xlabel("Channel");
        plt.ylabel("Number of Bad Channels");
        plt.title("Bad Channel Histogram");
        plt.legend();
 
        if(savePlot):
            plt.savefig(f"{h}/{h}-Bad_Channel_Hist");
        if(displayPlot):
            plt.show();
    
    return [channels, bad_ch_hist];


def bad_ch_desc(data, run_num, h, useThresh = False, lower_thresh = 10, upper_thresh = 150, display = True, save = False):
    data_list = [];
    bad_ch_desc = [];
    ordered_run_num = np.sort(run_num);

    for file in data:
        data_list.append(data[file][6]);
    #ordered numpy array
    data_list = np.array(data_list);
    data_list_order = np.argsort(run_num)
    data_list = data_list[data_list_order]; 
    data_list = np.transpose(data_list);

    #Locate when a channel is flagged as bad
    for lists in data_list:
        good_loc = np.char.find(lists, "OK");
        unbonded_loc = np.char.find(lists, "unbonded");
        bad_loc = (np.where(good_loc == -1, 1, 0) * np.where(unbonded_loc == -1, 1, 0))
        bad_loc_ind = np.argwhere(bad_loc);
        num_runs = len(bad_loc_ind.flatten());

        bad_labels = np.array(lists)[bad_loc_ind].flatten()
        bad_loc_runs = ordered_run_num[bad_loc_ind].flatten();
        if(useThresh):
            if(num_runs >= lower_thresh and num_runs <= upper_thresh):
                bad_ch_desc.append([bad_labels, bad_loc_runs]);
            else:
                bad_ch_desc.append([[],[]]);
            
        else:        
            bad_ch_desc.append([bad_labels, bad_loc_runs]);
       
    
    if(display):
        run = 0;
        for i in range(0, len(bad_ch_desc)):
            didPrint = False;
            for j in range(0, len(bad_ch_desc[i][0])):     
                print(f"Channel: {i}     Run: {bad_ch_desc[i][1][j]}     Status: {bad_ch_desc[i][0][j]}");
                didPrint = True;
             
            if(didPrint):
                print("\n");
               
    if(save):
        run = 0;
        with open(f"{h}/{h}_Bad_Channel_Classifications.txt", 'w') as f:
            for i in range(0, len(bad_ch_desc)):
                didPrint = False;
                for j in range(0, len(bad_ch_desc[i][0])):
                    f.write(f"Channel: {i}     Run: {bad_ch_desc[i][1][j]}     Status: {bad_ch_desc[i][0][j]}\n");
                    didPrint = True;
                if(didPrint):
                    f.write("\n\n");
                


def bad_ch_2DHist(data, run_num, h, displayPlot = True, savePlot = False):
    
    bad_loc_list = [];
    i = 0;
    for file in data:
        good_loc = np.char.find(data[file][6], "OK");
        unbonded_loc = np.char.find(data[file][6], "unbonded");
        bad_loc_temp = (np.where(good_loc == -1, 1, 0) * np.where(unbonded_loc == -1, 1, 0)).tolist();
        bad_loc_temp = [np.argwhere(bad_loc_temp).flatten(), run_num[i] * np.ones(len(np.argwhere(bad_loc_temp).flatten()))];
        i = i + 1;
        bad_loc_list.append(bad_loc_temp);
 
    bad_loc = np.concatenate(bad_loc_list, axis = 1);
    
    if(displayPlot or savePlot):
        plt.clf();
        plt.plot(bad_loc[0], bad_loc[1], 'o', color = 'b', label = f"{h}-Bad Channel");
        plt.xlabel("Channel");
        plt.ylabel("Run");
        plt.title(f"Bad Channel vs Run 2D Histogram");
        plt.legend();
        
        if(savePlot):
            plt.savefig(f"{h}/{h}_2DHist_Bad_Channel_vs_Run");
        
        if(displayPlot):
            plt.show();

def bad_ch_freq(data, h, run_num, freq_min = 0, freq_max = -1, displayPlot = True, savePlot = False):
    bad_hist = bad_ch_hist(data,h, displayPlot = False, savePlot = False);
    freq = [];
    num_runs = len(run_num);
   
    for i in range(0, num_runs + 1):
        freq.append(len(np.extract(np.array(bad_hist[1]) == i, np.array(bad_hist[1]))));
    
    freq_failed = np.sum(freq[int(len(freq) * 0.15):]);
     

    if(displayPlot or savePlot):
        plt.clf();
        plt.yscale("log");
        plt.plot((np.arange(len(freq[freq_min:freq_max])) + freq_min) / num_runs, freq[freq_min:freq_max], 'o', label = f"{h}\n{freq_failed} Channels Failed");
        plt.xlabel("Times a Channel is Flagged Bad / Number of Runs")
        plt.ylabel("Number of Channels");
        plt.title(f"Frequency of No. of Times a Channel is Bad Normalized to No. of Runs");
        plt.legend();
        
        if(displayPlot):
            plt.show();
        
        if(savePlot):
            plt.savefig(f"{h}/{h}_Freq_Bad_Channel_Hist");
    
    return [np.arange(len(freq)) / num_runs, freq, freq_failed]
    
    

def save_json2tar(directory, hybrids, test_num, tar_name):

    print("Saving files into a tarfile...")
    date = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    name = f'{tar_name}_{date}'
    with tarfile.open(f'{name}.tar.gz', "w:gz") as tar_handle:
        for t_num in test_num: 
            for hybrid in hybrids:
                for extension in ["json","txt"]:
                  files = Path(directory).glob(f"**/{hybrid}*_{t_num}_*.{extension}")
                  for file in files:
                     if extension=="json":
                        os.system(f"sed -i -e 's/NEXT/PPA/g' {file}")
                     tar_handle.add(str(file), arcname = "%s/results/%s" % (name,str(file).split('/')[-1]));
    tar_handle.close();
    
def get_list_hybrids(results_dir):

  unique_hybrid_list = []

  hybrid_list = glob("%s/*_RC_*.txt" % results_dir)
  for h in hybrid_list:
      hybrid = h.split("/")[-1].split("_RC_")[0]
      if not hybrid in unique_hybrid_list:
          unique_hybrid_list.append(hybrid)
  return unique_hybrid_list


def obtain_3PG_RC(hybrid, results_dir):
    Files_3PG_Test = [];
    Files_RC_Test = [];

    RC_Files_Run = glob(f"{results_dir}/{hybrid}*RC*");
    run_num = [];
    for files in RC_Files_Run:
        run_num.append(int(files.split("_")[-2]));
    
    run_num = np.sort(run_num);
    run_num = np.unique(run_num);

    for r in run_num:

        RC_Files_ScanNum = glob(f"{results_dir}/{hybrid}*RC_{r}_*");
        scan_num = [];
    
        for files in RC_Files_ScanNum:
            scan_num.append(int(files.split("_")[-1].split(".")[0]));
        scan_num = np.sort(scan_num);
        
        #Check to see if any 3PG tests have no corresponding RC test
        scan_diff = (scan_num[1:] - scan_num[:-1])
        pair_loc = np.argwhere(np.where(scan_diff == 3, np.ones(len(scan_diff)), np.zeros(len(scan_diff))));
        
        pair_loc = np.concatenate((pair_loc, pair_loc + 1), axis = 1);

        single_3PG = scan_num[np.delete(np.arange(len(scan_num)), pair_loc.flatten())];        
        
        pair_loc = np.transpose(pair_loc);
        pair_3PG = scan_num[pair_loc[0]];
        
        #List of scan numbers for the 3PG and RC tests
        ScanNum_3PG = np.sort(np.concatenate((single_3PG, pair_3PG)));
        ScanNum_RC = np.sort(scan_num[pair_loc[1]]);
        for i in ScanNum_3PG:
            Files_3PG_Test.append(f"{results_dir}/{hybrid}_RC_{r}_{i}.txt");        
    
        for i in ScanNum_RC:
            Files_RC_Test.append(f"{results_dir}/{hybrid}_RC_{r}_{i}.txt")

    Test_Order_3PG = np.arange(len(Files_3PG_Test));
    Test_Order_RC = np.arange(len(Files_RC_Test));
    
    return [Files_3PG_Test, Files_RC_Test], [Test_Order_3PG, Test_Order_RC] 



def get_JSON(results_dir, h, test):
    json_files = glob(f"{results_dir}/{h}_*_{test}.json")
    for i in json_files:
        print(i);



#Start of script

parser = argparse.ArgumentParser();
parser.add_argument("results_directory", help = "Points to the Directory with the Results from Testing");
args = parser.parse_args();

results = args.results_directory;
print("The results are in: ", results);
 
hybrids = get_list_hybrids(results)

print("******************************************")
print(f" Detected the following {len(hybrids)} hybrids: " )
for h in hybrids:
    print(" ---> %s " % h)

num = 0

display = False;
save = True;

hybrid_chips = [];

output_noise_params = [];
input_noise_params = [];
vt_50_params = [];
gain_params = [];

freq = [];

#Calculates statistics for each hybrid

for i, h in enumerate(hybrids):
   
    RC_Files, test_num = obtain_3PG_RC(h, results);
    data, chips = load_data(RC_Files[0]);
    run_num = test_num[0];
   
    #np.save(f"{h}_RC_Files",RC_Files[0], allow_pickle = True);  

    hybrid_chips.append(chips);

    if not os.path.exists(h):
        os.makedirs(h);

    print(f"On hybrid {i}: {h}");
    
    output_noise_params_array = calc_param_stats(data, chips, 5, "Output_Noise", run_num, h, percent_runs = 0.05, displayPlot = display, savePlot = save);
    output_noise_params.append(output_noise_params_array);
    
    input_noise_params_array = calc_param_stats(data, chips, 4, "Input_Noise", run_num, h, percent_runs = 0.05, displayPlot = display, savePlot = save);
    input_noise_params.append(input_noise_params_array);

    vt_50_params_array = calc_param_stats(data, chips, 3, "VT_50", run_num, h, percent_runs = 0.05, displayPlot = display, savePlot = save);
    vt_50_params.append(vt_50_params_array);

    gain_params_array = calc_param_stats(data, chips, 2, "Gain", run_num, h, percent_runs = 0.05, displayPlot = display, savePlot = save);
    gain_params.append(gain_params_array);

    num_bad_channel(data, run_num, displayPlot = display, savePlot = save);
    
    bad_ch_hist(data, h, displayPlot = display, savePlot = save);
    
    bad_ch_desc(data, run_num, h, useThresh = False, lower_thresh = 3, upper_thresh = 10, display = display, save = save);
    
    bad_ch_2DHist(data, run_num, h, displayPlot = display, savePlot = save);
    plt.clf(); 
    
    freq_array = bad_ch_freq(data, h, run_num, freq_min = 0, freq_max = -1, displayPlot = display, savePlot = save);
    
    freq.append(freq_array);

    plt.clf();






if not os.path.exists("Holistic_Plots"):
    os.makedirs("Holistic_Plots");



def pull_stats(params, param_name, num_bins, bin_width, displayPlot = False, savePlot = False):
    plt.clf();

    pull_stats = np.array([]);

    for array in params:
        pull_stats = np.concatenate((pull_stats, array[-1]))
 
   
    pull_stats_values, pull_stats_bins, patches1 = plt.hist(pull_stats, np.arange(num_bins + 1) * bin_width - (num_bins + 1) * bin_width / 2);
    plt.title(f"{param_name} Pull Statistics for {len(hybrids)} Hybrids");
    plt.xlabel("Pull Value");
    plt.ylabel("Number of Entries");
    print(f"Average of {param_name} pull_stats: ", np.average(pull_stats));
    
    if(displayPlot):
        plt.show();
    if(savePlot):
        plt.savefig(f"Holistic_Plots/{param_name}_Pull_Statistics.png");

    plt.clf();

    #np.save(f"{param_name}_pull_stats_values.npy", pull_stats_values, allow_pickle = True);
    #np.save(f"{param_name}_pull_stats_bins.npy", pull_stats_bins, allow_pickle = True);

#Calculates statistics for all hybrids grouped together


pull_stats(input_noise_params, "Input Noise", 19, 0.1, displayPlot = display, savePlot = save);
pull_stats(gain_params, "Gain", 19, 0.1, displayPlot = display, savePlot = save);
pull_stats(output_noise_params, "Output Noise", 19, 0.1, displayPlot = display, savePlot = save);
pull_stats(vt_50_params, "VT 50", 19, 0.1, displayPlot = display, savePlot = save);



#Displays the frequency a channel is marked as bad

total_freq = np.zeros(len(freq[0][0]));
freq_failed = [];

for array in freq:
    total_freq = total_freq + np.array(array[1]);
    freq_failed.append(array[2])

plt.yscale("log");
plt.xlabel("Times a Channel is Flagged Bad / Number of Runs");
plt.ylabel("Number of Channels");
plt.title(f"Frequency of No. of Times a Channel is Bad Normalized to No. of Runs");
plt.plot(freq[0][0], total_freq, 'o', label = f"{len(hybrids)} Hybrids - {np.sum(freq_failed)} Channels Failed in Total");

plt.legend();


if(display):
    plt.show();

if(save):
    plt.savefig(f"Holistic_Plots/Frequency_of_Bad_Channels.png");

plt.clf();


#Displays how many channels failed per hybrid



for i in range(0, len(freq)):
    plt.plot([i], [100 * freq_failed[i]/(256 * hybrid_chips[i])], 'o', color = 'b');
plt.xlabel("Hybrid");
plt.ylabel("(100 * Channels Failed / Total Number of Channels)%");
plt.title("Percentage of Channels that Fail per Hybrid");


if(display):
    plt.show();
if(save):
    plt.savefig(f"Holistic_Plots/Channel_Failure_vs_Hybrid.png");


plt.clf();
