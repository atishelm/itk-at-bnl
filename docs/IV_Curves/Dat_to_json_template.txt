{
    "component": "{component}",
    "properties": {
        "scan_info": {
            "AMAC_ILIMIT": 100,
            "PS_ILIMIT": 155,
            "SET_VOLTAGE": {SET_VOLTAGE}
        },
        "sensor_type": "BARREL_LS_MODULE"
    },
    "results": {
        "CURRENT": {CURRENT},
        "CURRENT_RMS": {CURRENT_RMS},
        "HUMIDITY": 0.0,
        "I_500V": 0.0,
        "PS_CURRENT": {PS_CURRENT}
        ,
        "TEMPERATURE": {TEMPERATURE},
        "VBD": 124.76174926757813,
        "VOLTAGE": {VOLTAGE}
    },
    "runNumber": "42-1",
    "testType": "MODULE_IV_AMAC"
}
