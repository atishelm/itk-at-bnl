"""
28 December 2022
Abraham Tishelman-Charny

The purpose of this python module is to set run options for the Module-Analysis module.
"""

# Imports 
import argparse 

# Create parser and get command line arguments 
parser = argparse.ArgumentParser()
parser.add_argument("--debug", action="store_true", help="Extra print statement for figuring out why this (for some unimaginable reason) does not work out of the box.")
parser.add_argument("--inFile", type=str, default=None, required=True, help="Input json file from ITSDAQ output.")
parser.add_argument("--IV_Type", type=str, required=True, help="Type of IV - sensor, or module")
parser.add_argument("--ol", type=str, default="./plots", help="Output location for plots.")
options = parser.parse_args()