# Alma Linux 9

Almalinux 9 is now the recommended operating system to use for ITk PCs, as it will come with *active support* until 31 May 2027, and *security support* until 31 May 2032. [[ref]](https://endoflife.date/almalinux). Active support generally means reported bugs and security issues are fixed, and regular point releases are made. Security support generally means a minor release updates are made only for critial issues.

As we need reliable OS versions during the full ITk production period, we must run on AL9.

## From USB

If you don't care at all about preserving your existing file system, or you have a brand new PC with no OS installed, you can install AL9 by first installing the image onto a USB stick and booting the PC from USB.

Ensure you have a USB device that works, installed the full DVD ISO (rather than the boot version of the ISO), and plug the USB into the back of the PC. See some instructions here:

https://wiki.almalinux.org/LiveMedia.html#how-to-download-and-write-images \
https://tuxcare.com/blog/how-to-upgrade-from-centos-7-to-almalinux-9/

## Upgrade current OS

The huge advantage of upgrading your PC's OS is that you can preserve the entire file system. This is extremely convenient if you want to keep your files, configurations, software, users, etc etc etc in-tact through the OS upgrade. 

Before proceeding: *back up all directories with important files, e.g. data, influx data*. You never know what can go wrong during OS upgrades:

Example for backing up things (https://serverfault.com/questions/120431/how-to-backup-a-full-centos-server), command:

```
sudo tar -zcvpf Backup_influx.tar.gz --directory /bkg --exclude=/Backup_influx.tar.gz .
```

### Centos 8 to AL8

First upgrade Centos 8 to alma linux 8 (same "Major" release 8):

https://wiki.almalinux.org/documentation/migration-guide.html#how-to-migrate \
https://github.com/AlmaLinux/almalinux-deploy

If influx repos aren't playing nice:

https://computingforgeeks.com/how-to-install-influxdb-on-fedora/

If other things don't play nice:

https://github.com/AlmaLinux/almalinux-deploy/issues/153

### AL8 to AL9

For upgrades between major releases, e.g. almalinux 8 to almalinux 9, you need to use ELevate:

[[ELevate]](https://wiki.almalinux.org/elevate/) / 
[[ELevate quickstart]](https://wiki.almalinux.org/elevate/ELevate-quickstart-guide.html)

<img src="/Images/ELevate.png" alt="ELevate tree">

See some possibly useful debugging below:

Checking default kernel: [[link]](https://almalinux.discourse.group/t/newer-kernel-update-not-being-used/1970/3).

Remove 'newer' kernels if necessary:

[https://discussion.fedoraproject.org/t/how-can-i-delete-a-kernel-from-boot/75855](https://discussion.fedoraproject.org/t/how-can-i-delete-a-kernel-from-boot/75855) / 
[https://linuxhint.com/delete-old-unused-kernels-centos-8/](https://linuxhint.com/delete-old-unused-kernels-centos-8/)

Note that here is how you can make sure the proper kernel versions are uninstalled:

```
sudo dnf list --installed | grep kernel
```

you may see something like:

```
abrt-addon-kerneloops.x86_64                       2.10.9-24.el8.alma                                         @appstream   
kernel.x86_64                                      4.18.0-513.5.1.el8_9                                       @baseos      
kernel-core.x86_64                                 4.18.0-513.5.1.el8_9                                       @baseos      
kernel-core.x86_64                                 4.18.0-522.el8                                             @baseos      
kernel-headers.x86_64                              4.18.0-513.5.1.el8_9                                       @baseos      
kernel-modules.x86_64                              4.18.0-513.5.1.el8_9                                       @baseos      
kernel-modules.x86_64                              4.18.0-522.el8                                             @baseos      
kernel-tools.x86_64                                4.18.0-513.5.1.el8_9                                       @baseos      
kernel-tools-libs.x86_64                           4.18.0-513.5.1.el8_9                                       @baseos   
```

notice that `kernel-core.x86_64` has two versions: `4.18.0-513.5.1.el8_9` and `4.18.0-522.el8`. To delete only `4.18.0-522.el8` (e.g. to avoid an error from `leapp preinstall` that the latest installed kernel version is not in use) you would run:

```
sudo dnf remove kernel-core-4.18.0-522.el8
sudo reboot
```

If you have file conflicts, e.g.:

```
Error: Transaction test error:
  file /usr/share/man/man8/ebtables-nft.8.gz from install of iptables-nft-1.8.8-6.el9_1.x86_64 conflicts with file from package iptables-ebtables-1.8.5-10.el8_9.x86_64
```

Simply delete the package and allow the correct version to install without conflicts:

```
sudo dnf remove iptables-ebtables 
<previous command> e.g. sudo leapp upgrade
```

### Centos 7 to AL9

May be less likely to work: https://tuxcare.com/blog/how-to-upgrade-from-centos-7-to-almalinux-9/

# ROOT

Example way to install a latest/custom `root` version (the c++ based physics tool, nothing to do with a PC's `root` privileges).

Latest version:

```
sudo yum install root
```

Custom version (at one point was necessary to install `6.26` as ITSDAQ couldn't compile with `6.28`) - (Thanks to Juergen Thomas):

```
Login as admin and create and install directory:

$ su  
$ cd /usr/local  
$ mkdir root\_6.26  

$ sudo yum install git make cmake gcc-c++ gcc binutils 
$ sudo yum libX11-devel libXpm-devel libXft-devel libXext-devel python openssl-devel

$ yum install cmake  
$ yum install python-devel  
$ yum install -y gcc-g++ boost-devel  
$ yum install -y libSM libICE libX11 libXext libXpm libXft  

$ yum install epel-release  
$ yum config-manager --set-enabled crb  
$ yum install redhat-lsb-core gcc-gfortran pcre-devel
$ yum install mesa-libGL-devel mesa-libGLU-devel glew-devel ftgl-devel mysql-devel   
$ yum install fftw-devel cfitsio-devel graphviz-devel libuuid-devel   
$ yum install avahi-compat-libdns\_sd-devel openldap-devel python-devel python3-numpy
$ yum install libxml2-devel gsl-devel readline-devel qt5-qtwebengine-devel 
$ yum install R-devel R-Rcpp-devel R-RInside-devel  

$ git clone --branch v6-26-00-patches https://github.com/root-project/root.git root\_src
$ mkdir root\_build root\_install && cd root\_build  
$ cmake -DCMAKE\_INSTALL\_PREFIX=../root\_install ../root\_src # Check cmake configuration output for warnings or errors  
## Note that for v6-24-00-patches you may need "-Dxrootd=OFF -Dbuiltin_xrootd=OFF" added to the cmake command above
$ cmake --build . -- install -j4 # if you have 4 cores available for compilation  
 
Note: This takes 30min+
Run the setup script:

$ source ../root\_install/bin/thisroot.sh # or thisroot.{fish,csh}
```

# Installing some software

https://docs.influxdata.com/influxdb/v2/install/?t=Linux

https://www.linuxcloudvps.com/blog/how-to-install-grafana-on-almalinux-9/

# Super user

Make your new user a super user:

https://linux.how2shout.com/add-user-to-sudoers-or-sudo-group-in-almalinux-8/

remember to restart after doing this to make changes take effect.

# Backup Influx v1 data

```
influxd backup -portable /path/to/backup-destination
```

https://docs.influxdata.com/influxdb/v1/administration/backup_and_restore/

# Migrating influx data

The important lesson learned: Migrating data is different from backup/restore. Migrating is when you want to move influx data from one source (e.g. on PC 1 running influx instance 1) to another source (e.g. PC 2 running influx instance 2). If running influxv2, create a line protocol from the data:

```
su
influxd inspect export-lp --bucket-id fb57c73e71b21dca --engine-path ~/.influxdbv2/engine --output-path export.lp
```

Get the bucket ID from:

```
influx bucket list --org BNL
```

where org is your organization. 

Put the `lp` file on a USB or hard drive, connect the USB or hard drive to PC 2, transfer the `lp` file to your PC, and run:

```
influx write --bucket Box3 --file export.lp
```

where you can specify the bucket to add this data to.

# Converting influx v1 data to influx v2

https://docs.influxdata.com/influxdb/v2/install/upgrade/v1-to-v2/manual-upgrade/

commands like this seem to get me somewhere:

```
sudo influx_inspect export -database coldjig -datadir /var/lib/influxdb/data/ -waldir /var/lib/influxdb/wal/ -out file.lp -lponly # influx v1
influx write --file file.lp --bucket coldjig # influx v2
```


removing lines that cannot be parsed:

```
COMM,RECEIVER=ITSDCS,SENDER=COLDJIG,SETUP=BNL COMMAND="LV_ON" 1694114854461604831
COMM,RECEIVER=ITSDCS,SENDER=COLDJIG,SETUP=BNL COMMAND="INIT" 1694114869578074875
COMM,RECEIVER=ITSDCS,SENDER=COLDJIG,SETUP=BNL COMMAND="AMAC_CALIBRATION" 1694114884676646661
COMM,RECEIVER=ITSDCS,SENDER=COLDJIG,SETUP=BNL COMMAND="HYBRID_ON" 1694114889713776209
COMM,RECEIVER=ITSDCS,SENDER=COLDJIG,SETUP=BNL COMMAND="AMAC_CALIBRATION" 1694114899795456772
COMM,RECEIVER=ITSDCS,SENDER=COLDJIG,SETUP=BNL DATA="" 1694114849413201188
COMM,RECEIVER=ITSDCS,SENDER=COLDJIG,SETUP=BNL DATA="" 1694114854461604831
COMM,RECEIVER=ITSDCS,SENDER=COLDJIG,SETUP=BNL DATA="" 1694114869578074875
[qcbox@localhost test]$ influx write --bucket coldjig --file file_end.txt
Error: failed to write data: 400 Bad Request: unable to parse '\"builddate\": \"2023-09-07 19:10:09 +0000 (Thu, 07 Sep 2023)\",': invalid field format
unable to parse '\"revision\": \"itsdaq_23_3_2023-661-gfb8cd7e77\"': invalid field format
unable to parse '}': missing fields
unable to parse '}': missing fields
unable to parse '" 1694114395468881434': invalid field format
[qcbox@localhost test]$ grep -v "builddate" "file_end.txt" > "shorter_file_end.txt"
[qcbox@localhost test]$ influx write --bucket coldjig --file shorter_file_end.txt
Error: failed to write data: 400 Bad Request: unable to parse '\"revision\": \"itsdaq_23_3_2023-661-gfb8cd7e77\"': invalid field format
unable to parse '}': missing fields
unable to parse '}': missing fields
unable to parse '" 1694114395468881434': invalid field format
[qcbox@localhost test]$ grep -v "revision" "shorter_file_end.txt" > "shorter_file_end_.txt"
[qcbox@localhost test]$ influx write --bucket coldjig --file shorter_file_end_.txt
Error: failed to write data: 400 Bad Request: unable to parse '}': missing fields
unable to parse '}': missing fields
unable to parse '" 1694114395468881434': invalid field format
[qcbox@localhost test]$ grep -v "}" "shorter_file_end_.txt" > "shorter_file_end__.txt"
[qcbox@localhost test]$ influx write --bucket coldjig --file shorter_file_end__.txt
Error: failed to write data: 400 Bad Request: unable to parse '" 1694114395468881434': invalid field format
[qcbox@localhost test]$ grep -v "1694114395468881434" "shorter_file_end__.txt" > "shorter_file_end___.txt"
[qcbox@localhost test]$ influx write --bucket coldjig --file shorter_file_end___.txt

```
