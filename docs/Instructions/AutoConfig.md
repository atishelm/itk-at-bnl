# Use AutoConfig to configure ASICs on modules

Whenever we are testing a hybrid or something that has a hybrid, the function AutoConfig can be used to configure the ASICs on the powerboard and hybrid.

Good information about AutoConfig can be found in [Cole's slides](https://docs.google.com/presentation/d/1DD2VgvwdBLEvlVhigQS_S_mh89LrH3-U8EgsRd3cjyE/edit#slide=id.g237fb8842e4_7_40)

The full function is AutoConfig(bool power_down=false, bool skip_download=true, bool skip_hvmap=false).

The first argument determines whether the DCDC is turned off after configuration. The second determinese whether we attempt to pull ASIC specific config info from the database. The third determines whether we skip the mapping of high voltage channels based on AMACs.

When you download ASIC config files (second argument = false), information for each ABC on the hybrids you have loaded is stored to asic_info.json. This file is overwritten each time you run AutoConfig with skip_download=false. 

To run AutoConfig, first make sure production_database_scripts is pulled to the same directory as itsdaq-sw. Then:

1. In production_database_scripts, run 'python3 get_token.py'.

2. Run the command printed out by this function.

3. Navigate back to the itsdaq and edit the DAT/config/st_system_config.dat file as follows (depending on how many modules you have):

Module    0  1  1   0   0   -1  50  50   JaneDoe0 Barrel
Module    1  1  1   0   8   -1  50  50   JaneDoe1 Barrel
Module    2  1  1   0   16  -1  50  50   JaneDoe2 Barrel
Module    3  1  1   0   24  -1  50  50   JaneDoe3 Barrel

Note that we have used the "JaneDoeN" format so that AutoConfig knows we want to search the database for the appropriate config parameters for these modules. Since ITSDAQ does not have a config file titled "JaneDoe0.det", it will try to open the file "default_Barrel.det", which is located in the DAT/config folder. This sets the configuration for the HCC. You may have to adjust the crossbonding register. You can change the "Barrel" field if you want ITSDAQ to look at other config files for the HCC. 


4. run 'rid' (alias for 'source RUNITSDAQ.sh')

AutoConfig(false, true, false) will run at startup. Ideally, you already see all the HCCs and ABCs are read. But to make sure that the ABCs are using the correct ocnfig info:

5. Run AutoConfig(false,false,false). This will take a bit longer since it is pulling config info for each ABC from the database. You only have to run this once for the correct ABC info to be used whenever AutoConfig() is called in the future as long as you don't run AutoConfig(false,true,false) with other modules and overwrite your asic_info.json file.


6. You should see that all your LV currents make sense (~280mA) and that all the HCCs and ABCs are read by AutoConfig. You can try reading all the fuse IDs to be sure: st_read_fuse_ids().


