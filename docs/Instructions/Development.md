# Development using Git

[Git](https://en.wikipedia.org/wiki/Git) is an amazing tool that makes order out of chaos. However, it is a beast that takes patience to learn how to tame. This page contains useful tips and tricks in one place so you don't have to google the same question over and over.

## Switching git branches

You may want to switch your git branch to test the latest version of someone's (or your) merge request. You can do this by running

'git checkout  <desired branch>' 

'git pull -X theirs origin <desired branch>'

You may get a message saying you have unresolved conflicts when you try to checkout. You can temporarily stash these changes by doing 'git stash .', but remember to do 'git stash pop' to undo this before checking out your main branch again.

To check which branch you are in, do 'git branch'. Make sure you know what the most commonly used branch is on a machine before you checkout a new one so you can leave the machine as you found it.

To checkout a specific file from a branch, do:

'git checkout origin/[branch name] -- [file path]'

## New local branch without keeping current branch commits

To create a new local branch based on a remote branch, without keeping any of the current commits on your active branch, run:

Prior to Git 2.23:

```
git checkout -b test <name of remote>/test
```

More information here: https://stackoverflow.com/questions/1783405/how-do-i-check-out-a-remote-git-branch

## New local branch based on tag

```
git checkout -b <newBranchName> <tagName>
```

More information here: https://stackoverflow.com/questions/10940981/how-to-create-a-new-branch-from-a-tag

## Deleting local/remote branches

Deleting local branches in git:

```
$ git branch -d feature/login
```

Deleting remote branches in git:

```
$ git push origin --delete feature/login
```

Additional information: https://www.git-tower.com/learn/git/faq/delete-remote-branch

## Push local branch to remote but changing its name

https://github.com/jiffyclub/blog-posts/blob/master/git-pushing-to-a-remote-branch-with-a-different-name.md

```
git push origin local-name:remote-name
```

## Update list of remote branches (prune)

```
git remote update origin --prune
```

More info: https://stackoverflow.com/questions/36358265/when-does-git-refresh-the-list-of-remote-branches


## Check for differences between files via branch or version

To check the difference between your local version and the most recent commit in the same branch, simply:

```
$ git diff [file path]
```
To check the difference between your checked out branch and a branch of your choice:

```
$ git diff [branch name] [file path]
```

## Stash changes and bring them back

When switching between branches, you may want to stash your changes with


```
$ git stash .
```

or bring them back with

```
$ git stash pop
```
