# Download hybrid configuration files

In order to run electrical tests, you need a configuration file for each hybrid involved in the test. These are stored on the ITk database, but can be extracted using python scripts. One can obtain them for the desired modules with the following steps:

1) Clone the repository, and change to the configurations folder in order to have access to the required scripts:

```
cd <workingArea>
git clone https://gitlab.cern.ch/atishelm/itk-at-bnl.git # via HTTPS
git clone ssh://git@gitlab.cern.ch:7999/atishelm/itk-at-bnl.git # via SSH
cd itk-at-bnl/Module-testing/Configs
```

2) Edit the `Modules.yaml` file. This should contain a list of the local IDs of the modules you want hybrid configuration files for. An example `Modules.yaml` file is as follows:

```
module_local_names:
  [
    LBNL_PPB_LS_1,
    LBNL_PPB_LS_2,
    BNL-PPB2-MLS-106,
    BNL-PPB2-MLS-108,
    SCIPP-PPB_LS-007,
    SCIPP-PPB_LS-008,
  ]
```

Corresponding to six long strip modules - 2 from LBNL, 2 from BNL, and 2 from SCIPP. 

3) Then simply run the script with `python3 GetDBInfo.py`. Note that if you are missing the required python packages, you may need to install them onto your machine: 

```
pip3 install requests
yum install python-requests
```

If this works properly, you should see your configuration files in the output directory `hybrid_configs`. To use them with an ITSDAQ instance, you should then move the files to the proper ITSDAQ folder: 

```
cp hybrid_configs/* /path/to/itsdaq-sw/DAT/config/
```


## Notes

The latest version of the ASIC configuration obtention script should be around [here](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts/-/blob/master/strips/modules/getAsicConfig.py).
