# FELIX 

Please refer to this [gitlab repository](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/tree/master) for all custom configurations and scripts used for this setup. 

## Setup

### Description

The various components of the setup are:

- **FELIX board**: ours is a 48 channel FLX 712, integrated into our PC.
- **VLDB+ board**: it houses the VTRx and lpGBT, which take care of the electrical $\leftrightarrow$ optical conversion. Our lpGBT is a version 1 (at the time of writing, many institutes still have v0). 
- **piGBT**: custom RaspberryPi used to configure the lpGBT over I2C. 
- **'Nikhef' breakout board**: it is the intermediary between the VLDB+ and the module. The design comes from Nikhef, but this particular board is from a batch ordered by Aram Apyan (Brandeis) from PalPilot (PCB fabrication) and East End Assembly (PCB assembly).  
- **Module**: ours is a full module with powerboard (including AMAC), HCC, and 10 ABCs. It is also possible to do this work with partial modules (e.g. < 10 ABCs, no powerboard). 

### Diagram

<img width="800" src="/Images/FELIX/Setup_diagram.png" alt="Diagram of the setup">

*Diagram of the setup*

### Pictures

<img width="500" src="/Images/FELIX/FELIX.jpg" alt="FELIX">

*FELIX board*

<img width="500" src="/Images/FELIX/Module.jpg" alt="Module">

*Module*

<img width="500" src="/Images/FELIX/VLDB_Nikhef_piGBT.jpg" alt="VLDB+, 'Nikhef' breakout board, and piGBT">

*VLDB+, 'Nikhef' breakout board, and piGBT*

<img width="500" src="/Images/FELIX/piGBT.jpg" alt="piGBT">

*piGBT*

## Patching the Nikhef board

Some patches need to be applied to the Nikhef board before it can work.

### Hardware patch

To remove an AC-coupling effect, capacitors C44-C51 (on the back side of the board) need to be shorted. 
Below is a snippet of the schmetic showing the capacitors affected. 
In the new batch ordered by Aram, the manufacturer replaced capacitors C46-C51 with 0 Ohm resistors, but not C44-C45. Therefore, this still needs to be done by hand. The way to do this is just to solder over the capacitors. 

<img width="800" src="/Images/FELIX/Nikhef_board_patch_schematic.png" alt="Portion of the Nikhef board schematic showing the affected capacitors">

*Portion of the Nikhef board schematic showing the affected capacitors*

<img width="500" src="/Images/FELIX/Breakout_board_preFix.jpg" alt="Back of the Nikhef board before the patch (broad view)">

*Back of the Nikhef board before the patch (broad view)*

<img width="500" src="/Images/FELIX/Breakout_board_preFix_zoom.png" alt="Back of the Nikhef board before the patch (zoomed-in view)">

*Back of the Nikhef board before the patch (zoomed-in view)*

<img width="500" src="/Images/FELIX/Breakout_board_postFix.jpg" alt="Back of the Nikhef board after the patch">

*Back of the Nikhef board after the patch*

## Programming the Nikhef board

The MAX10 FPGA (the square black chip at the center of the Nikhef board) needs to be programmed with firmware before use.

### Acquire a USB Blaster

Loading the firmware onto the FPGA requires a USB Blaster, as shown below:

<img width="500" src="/Images/FELIX/USB_blaster_front.jpg" alt="USB blaster (front)">

*USB blaster (front)*

<img width="500" src="/Images/FELIX/USB_blaster_back.jpg" alt="USB blaster (back)">

*USB blaster (back)*

### Install the Quartus software

Download the software with

```
wget https://cdrdv2.intel.com/v1/dl/downloadStart/684219/684234?filename=Quartus-lite-21.1.0.842-linux.tar
```
Then, unzip the tarball and run the  `setup.sh` script.

Once installed, Quartus can be launched by running 
```
[installation_path]/quartus/bin/quartus
```

### Download the firmware

The firmware is found in this [git repository](https://gitlab.cern.ch/atlas-tdaq-felix/strip-vldb-plus-adapter-board/-/tree/master?ref_type=heads). Clone the repository:
```
git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-felix/strip-vldb-plus-adapter-board.git
```
then navigate to the firmware folder:
```
cd strip-vldb-plus-adapter-board/firmware/
```

### Install the firmware

#### Detect the USB Blaster

Go to Tools > Programmer.  
Under "Hardware setup", check that "USB-Blaster" is present and selected, then close this window.

<img width="800" src="/Images/FELIX/Quartus_hardware_setup.jpg" alt="Quartus hardware setup window">

*Quartus Hardware Setup window*

Click on "AutoDetect". You should now see a sketch of an Intel FPGA.

<img width="800" src="/Images/FELIX/Quartus_programmer.jpg" alt="Quartus programmer window">

*Quartus Programmer window after FPGA has been detected*

#### (Optional) Make small changes to firmware

A few changes can be applied to the firmware which will allow the input and output signals of the MAX10 to be diverted to the SMA connectors on the Nikhef board. These can then be directly connected to a scope. This prevents having to probe by hand while debugging. This is what the connection will ultimately look like:

<img width="500" src="/Images/FELIX/SMA_connection.png" alt="SMA connection between Nikhef board and scope">

*Direct connection between Nikhef board and scope using SMA connectors (from setup at Argonne National Lab)*

Go to File > Open Project.  
Select Adaptorboard.qpf.  
After opening, a message about quartus versions might show up. Click yes.  
Double click on the project file. It should open Adapterboard.vhd.

<img width="800" src="/Images/FELIX/Quartus_setup_vhd.png" alt="Quartus select vhd window">

*Quartus Window when selecting vhd file*

Navigate to line 163, 179 and 208 and implement the highlighted changes.

<img width="800" src="/Images/FELIX/Quartus_sma_changes_1.png" alt="Quartus changes for SMA (1)">

<img width="800" src="/Images/FELIX/Quartus_sma_changes_2.png" alt="Quartus changes for SMA (2)">

<img width="800" src="/Images/FELIX/Quartus_sma_changes_3.png" alt="Quartus changes for SMA (3)">

*Changes to the Adapterboard.vhd file*

Click the "Save" icon at the top of the page.  
Click "Start compilation" (the "play" icon at the top of the page) and wait until it reaches 100% (see botton right corner).

<img width="800" src="/Images/FELIX/Quartus_start_compilation.png" alt="Quartus start compilation">

<img width="800" src="/Images/FELIX/Quartus_compilation_finished.png" alt="Quartus compilation finished">

*Compile the changes*

#### Program the FPGA

Go to Tools > Programmer > Add File.  
Select the file `strip-vldb-plus-adapter-board/firmware/output_files/Adapterboard.sof` or `strip-vldb-plus-adapter-board/firmware/output_files/Adapterboard.pof`. If the .sof file is used, the firmware will need to be re-loaded after every power cycle. Using the .pof file loads the firmware to memory, which means that it will still be present after a power cycle. In most cases, the .pof file should be used.  
Ensure that "Program/Configure" is checked and click "Start". The progress bar on the top right should reach "100% (Successful)".

<img width="800" src="/Images/FELIX/Quartus_programmer.jpg" alt="Quartus programmer window">

*Quartus Programmer window after the firmware has been successfully uploaded*

After this, the blue LED near the MAX10 on the Nikhef board should turn on.

## Checking switches on the VLDB+ and Nikhef board

### VLDB+ switches

The images below show the positions of the switches on our VLDB+ board:

<img width="500" src="/Images/FELIX/VLDB_switches_FMC.jpg" alt="VLDB+ switches 1-4">

*VLDB+ switches 1-4 (related to FMC)*

<img width="500" src="/Images/FELIX/VLDB_switches_I2Cpullups.jpg" alt="VLDB+ switch 5">

*VLDB+ switche 5 (related to I2C)*

<img width="500" src="/Images/FELIX/VLDB_switches_misc.jpg" alt="VLDB+ switches 6-8">

*VLDB+ switches 6-8*



Explanation about a few relevant switches (from [VLDB+ manual](https://vldbplus.web.cern.ch/manual/index.html)):

| Switch number | Name on board | Long name | Current setting | Notes |
| ---           | ---  | ---     | ---             | ---   |
| SW5 | I2C pull-ups | Pull-up resistors for I2C | 00111100 | Would recommend not touching this. |
| SW6 | ADR0-3 | I2C address | 0000 | If 0 is provided, the default address 0b1110000 = 112 will be used. |
| SW7 | MODE0-3 | Operation mode | 1111 = transciever | Transciever means bi-directional communication, other options are transmitter or receiver (uni-directional). |
| SW8 | SC-I2C/BOOTCNF1  | I2C control | 1 = ON | Needs to be ON when using piGBT to configure the lpGBT, OFF otherwise. |
| SW8 | LOCKMODE | Lock mode | 1 | Controls how to get the clock. 1 means getting the clock from the data stream (necessary when in transciever mode), 0 means external clock. |
| SW10 | | Power source | Down = Mode 1 | Mode 1 (down) = external power supply -> need to connect directly to PSU, Mode 2 (up) = FMC -> can get power through FMC if Nikhef board is powered, OFF (middle). |

### Nikhef board switches

The image below shows the positions of the switches on our Nikhef board:

<img width="500" src="/Images/FELIX/Nikhef_switches.jpg" alt="Nikhef switches">

*Nikhef board switches*

## Setting up FELIX software and drivers

The FELIX software and drivers can be downloaded from this [webpage](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/releases.html). Follow the instructions in the [FELIX user manual](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-doc/felix-user-manual/5.x/5_software_installation.html) for installing them. 

The versions we are using:

- Software: release 5.1.1 (`el9-gcc13-lcg104b`), name of folder: `felix-05-01-01-rm5-stand-alone-x86_64-el9-gcc13-opt`. 
- Drivers: release 4.20.0 (`rpm-el9`), name of file: `tdaq_sw_for_Flx-4.20.0-2dkms.noarch.rpm`. 

Once everything is set up, one should source `felix-05-01-01-rm5-stand-alone-x86_64-el9-gcc13-opt/x86_64-el9-gcc13-opt/setup.sh` every time a new terminal is opened (see [setupFELIX.sh](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/FELIX/setupFELIX.sh?ref_type=heads)). 

## Flashing FELIX firmware

A list of bitfiles for the FELIX firmware can be downloaded from this [webpage](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/firmware/Bitfiles_development/). The one we are using is `FLX712_STRIPS_24CH_TTCCLK_GIT_phase2-release-5.1_rm-5.1_494_240718_0552`. 

To flash the firmwre, there are 2 options:

1. **Command line**:  
```
fflashprog -f 3 <firmware_path>.mcs prog
sudo shutdown now
```
Then restart the computer by pushing on the power button. 

2. **Vivado**:  

This option requires inserting a JTAG into the FELIX, as shown below. 

<img width="500" src="/Images/FELIX/JTAG.jpg" alt="JTAG">

*JTAG*

<img width="500" src="/Images/FELIX/JTAG_in_FELIX.jpg" alt="JTAG in FELIX">

*JTAG inserted in FELIX board*


The Vivado program can be downloaded from this [webpage](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/2024-1.html), under the "Vivado (HW developer)" tab, then "Vivado Lab Solutions". After unzipping the folder, run the executable and follow the instructions on the installer. Then, install the drivers by following the instructions on this [webpage](https://docs.amd.com/r/en-US/ug973-vivado-release-notes-install-license/Installing-Cable-Drivers) ("Install Cable Drivers for Linux"). To (re)launch Vivado, run `vivado_lab&`. 

To flash the firmware, follow the instructions in [this page of the FELIX user manual](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-doc/felix-user-manual/5.x/4_firmware_programming.html) (Section 4.2.3). Then reboot the computer. 

Note: when uploading the firmware through Vivado, a reboot is necessary, but when uploading by `fflashprog`, one needs to shut down then turn on.

At this point, it should be possible to run the following FELIX commands:

- `flx-init`: initializes FELIX.
- `flx-info`: provides information about the FELIX card, firmware version, ...  
- `flx-info pod`: shows the amount of light present on each optical channel.
- `flx-info gbt`: shows which channels are aligned.

## Connecting the fibers

Current setup:

- FELIX -> box 1: MTP 
- Inside box 1: MTP gets split into 48 very thin "breakout" fibers. 
- Box 1 -> box 2: 2 x LC 
- Inside box 2: The 2 LC get recombined into an MTP. 
- Box 2 -> lpGBT: MTP 

(See [this website](https://community.fs.com/article/mtpmpo-vs-traditional-lc-fiber-cables-which-one-to-choose.html) for the difference between MTP and LC)

<img width="500" src="/Images/FELIX/Fibers_box1.jpg" alt="Box 1 (top view)">

*Box 1 (top view)*

<img width="500" src="/Images/FELIX/Fibers_box1_side.jpg" alt="Box 1 (side view)">

*Box 1 (side view)*

<img width="500" src="/Images/FELIX/Fibers_box2.jpg" alt="Box 2">

*Box 2*


### How to determine where to plug the fibers?

Use the `flx-info pod` command to guide this process. The output should look something like this:

<img width="500" src="/Images/FELIX/flx-info_pod_output.jpg" alt="Example output from the flx-info pod command">

*Example output from the flx-info pod command*

Note: The `flx-info pod` command will always return a non-zero value for TX if FELIX is on, because it measures the amount of light that is sent out by FELIX (regardless of whether it comes back).

For box 1, we can see light coming from the right half of the outputs: this corresponds to the TX lines. Therefore, the other half of the outputs (left) correspond to RX lines. We need to connect one LC to TX and one to RX. In principle, it doesn't matter which of the 6 groups of slots for TX and RX are used, this will just change which GBT channel the light will show up on (and therefore which channel to talk to in subsequent steps). In our case, we put the fibers in the 1st and 7th groups starting from the right, second slot from the bottom in both cases, which corresponds to GBT channel 0. 
 
For box 2, we need to turn on the VLDB+, which should produce light coming from the lpGBT. By looking at the outputs of box 2, we see that light arrives in the top slot of the 3rd group from the left. Since this is the RX direction, we need to connect the LC from the RX side of box 1 (the one that didn’t have light) to this location in box 2. The TX fiber needs to go in the slot directly following the RX when counting from bottom to top and left to right, i.e. the bottom slot of the 4th group from the left. 

The `flx-info pod` command should now show a non-zero RX value for one of the channels. 

## Configuring the lpGBT using the piGBT

When the piGBT is powered, its IP address, SSID, and password will show up on the screen. To connect to the piGBT via Wifi, do the following:

- Check the switches on the piGBT: SW1 ("MASTER MODE") should be set to "DIS" and SW2 ("Wifi") should be set to "ON".  
- On the computer, search for a Wifi network called `PIGBT_<SSID>`.   
- Connect to the network by entering the password displayed on the piGBT.  
- In a browser, open `http://<IP_address>:8080`, where `<IP_address>` is the one displayed on the piGBT. In our case, the IP address is `121.224.4.10`. 

For other ways to connect to the piGBT, see the [piGBT manual](https://pigbt.web.cern.ch/manual/quick_start.html).

To configure the lpGBT, follow these steps:

- In the web interface (`http://<IP_address>:8080`), select "Real lpGBT", then under "Select Hardware" choose "VLDB+ board". In the "I2C adress" dropdown, one option should be available (in our case it is 112, see SW6 on the VLDB+) .
- In the "Core" page (the first one that shows up), set "LpGBT Mode" to "TRX", "Tx Data Rate" to "10Gbps", and "Tx Encoding" to "FEC5", as seen in the screenshot below. 
- Go to the "Registers" page, click "Load", and select the lpGBT configuration file (for us, it is lpgbt_LS_M_RH_V1_Pri.cnf from [itsdaq-sw/config](https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw/-/blob/master/config/lpgbt_LS_M_RH_V1_Pri.cnf?ref_type=heads)).

At this point, a green check mark at the top right of the page should appear, indicating that the lpGBT has found a lock. 

<img width="800" src="/Images/FELIX/piGBT_interface_core.png" alt="piGBT interface core">

*piGBT interface - Core page*

<img width="800" src="/Images/FELIX/piGBT_interface_registers.png" alt="piGBT interface registers">

*piGBT interface - Registers page*

A few notes: 

- One change was necessary in the lpGBT configuation compared to the version on git: an inversion of the high speed data output lines is necessary, which is controlled by the lpGBT register highSpeedDataOutInvert (documented [here](https://lpgbt.web.cern.ch/lpgbt/v1/registermap.html#x036-chipconfig)). This corresponds to bin 7 of register 0x36. The value of this register was changed from 0x00 to 0x80.  
- We changed the readout speed from 640 Mbps (default) to 320 Mbps by changing the values of registers [200 to 206](https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw/-/blob/master/config/lpgbt_LS_M_RH_V1_Pri.cnf?ref_type=heads#L202-207) from 0x5a to 0x56. Otherwise, we were not able to communicate with the HCC (under investigation).

## Enabling elinks

To enable all TX and RX elinks for EC, IC, and data, one needs to run a series of `flx-config` commands. The bash script [felix_config_enable_elinks_oneLink.sh](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/FELIX/felix_config_enable_elinks_oneLink.sh) (adapted from [this one](https://gitlab.cern.ch/itk-strips-at-sr1/itk-strips-felix-setup-scripts/-/blob/master/felix_config_enable_elinks.sh?ref_type=heads) in SR1), can be used.

To run, do:

```
source felix_config_enable_elinks_oneLink.sh 0 00 320
```
The first argument is the FELIX device, the second is the GBT link (the one that shows up as aligned when running `flx-info gbt`, make sure to use 2 digits) and the third option is the readout speed: 320 or 640 Mbps. Note that the readout speed here should match the one used in the lpGBT configuration. 

To view the elink configuration GUI, run `elinkconfig&` then click on "Read Cfg". You can enable/disable elinks from this GUI by checking/unchecking the boxes then clicking on "Generate/Upload". 

A few notes:

- All elink numbers are shown in hexadecimal.  
- The TX elinks are not configurable from the GUI. This means that even if they are shown as enabled, this may not be the case. This is why it is important to run `felix_config_enable_elinks_oneLink.sh` (or the individual `flx-config` commands inside this script).   
- You can select a different FELIX device with the drop down menu on the top left that says "FLX-device: 0 (712, STRIP)".  
- You can select a different GBT link with the box labeled "Link" on the top left.  

<img width="800" src="/Images/FELIX/Screenshot_elinkconfig.png" alt="Elinkconfig interface">

*Elink configuration interface*

## Running felix-star

More information about felix-star can be found in the [FELIX user manual](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-doc/felix-user-manual/5.x/1_overview.html) (sections 7, 8, and 9). The `supervisord` orchestrator requires a config file: [supervisord_vldbp_bnl.conf](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/FELIX/supervisord_vldbp_bnl.conf?ref_type=heads). The only changes necessary for a different setup are the paths and the `NETWORK_IP`. The main useful commands are:

```
supervisord -c ~/felix-star/supervisord_vldbp_bnl.conf #start orchestrator
supervisorctl status #check what is currently running
supervisorctl start daq:* #start all the DAQ nodes
supervisorctl stop #stop the process
supervisorctl restart #restart the process
supervisorctl shutdown #shut down orchestrator
```
Alternatively, all these commands (except starting and shutting down the orchestrator) can be done via a web interface. In our case, the url is: [http://127.0.0.1:9001](http://127.0.0.1:9001). A screenshot of this interface is found below:

<img width="800" src="/Images/FELIX/Supervisord_interface.png" alt="Supervisord interface">

*Supervisord web interface*

Note: whenever the elink configuration changes, you must re-start felix-star by doing `supervisorctl restart daq:*`, otherwise it will not be able to subscribe to the desired elinks. 

## Configuring the AMAC

### Installing the AMAC code
The code needed to communicate with the AMAC is found in [dcs-tests](https://gitlab.cern.ch/itk-strips-at-sr1/dcs-tests). The instructions from the README can be followed. However, the following points should be noted:

In general, we do not need python bindings, so this option can be omitted.  

In the cmake command, change the `DCMAKE_INSTALL_PREFIX` path to match your file system. 

After running `make install`, binaries are created in the directory specified by `DCMAKE_INSTALL_PREFIX` (e.g. `/opt/atlas/usr/local/bin`). It is useful to create symlinks from the binaries in `/opt/atlas/usr/local/bin/` and `/opt/atlas/usr/local/scripts/` to `~/bin` using the `ln -s` command (see README). It is also useful to create a symlink between `dcs-tests/build/bin/endeavour2` and `~/bin`. Now, the executables such as `endeavour2` can be run from any directory. 

### Running endeavour

The executable that will be used to talk to the AMAC is `endeavour2` (the `2` is necessary when running felix-star, otherwise `endeavour` should be used). This requires a config file, [cfg_endeavour_link00.json](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/AMAC/cfg_endeavour_link00.json), which we adapted from the one used in SR1. It is found in `/opt/atlas/etc/cfg_endeavour.json` on the BNL lab computer 

A few explanations:  

- `tx_elink_n` and `rx_elink_n` are typically 20 and 28 if you are on GBT link 0. Otherwise, these will need to be adjusted. Check the numbers (in hexadecimal) in the "EC" boxes in the elinkconfig interface.  
- `lock_filename` can in principle point to any file (typically empty) on the file system. This is required to prevent multiple processes from interfering with each other.  
- `LOCAL_IP_OR_INTERFACE` is set to `lo` here, since we are running on the same machine as the FELIX. If this is not the case, the IP address of the FELIX should be specified. You can find it by running `ifconfig`.  
- `BUS_DIR` should be changed to match your file system.  
- `connectorID` corresponds to the FELIX device number.  
- `link_rx` and `link_tx` correspond to the GBT link (the one that shows up as aligned when running `flx-info gbt`).  
- `elink_rx` and `elink_tx` are the same as `rx_elink_n` and `tx_elink_n`, respectively.  
- I don't think the `icnetio` section is needed, but I have not tried without.


To start communicating with the AMAC, run:
```
endeavour2 -s -p -c /opt/atlas/etc/cfg_endeavour_link00.json -i 0 setid idpads 0 -d
```
A brief explanation of the arguments:

- `-s`: should be included when running felix-star
- `-p`: uses new EC protocol (in FELIX firmware as of summer 2024)      
- `-c`: path to the endeavour config file (shown above)   
- `-i`: communication ID  
- `setid`: action to carry out  
- `idpads`: bond ID 
- `-d`: verbose output 

Before running `setid`, the AMAC will only respond to its bond ID. After running `setid`, the AMAC can be addressed with its communication ID. In principle, the bond ID should be 0 by default (for a barrel module not on a stave). Note that the AMAC will only respond to the first `setid` it receives after power up. Any subsequent `setid` will lead to a "timeout".

Some other commands that can be used for testing the AMAC communication:

Read a register (e.g. 42):
```
endeavour2 -s -p -c /opt/atlas/etc/cfg_endeavour_link00.json -i 0 read 42
```
This should return a value for the content of register 42.

Turn on DCDC:
```
endeavour2 -s -c /opt/atlas/etc/cfg_endeavour_linnk00.json -i 0 write 41 0x1
```
This should cause the current drawn by the module to increase. 

Once you have confirmed that the AMAC communication is working, send the full configuration by running this [powerUP_amac_star.sh](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/AMAC/powerUP_amac_star.sh) script:
```
source powerUP_amac_star.sh 0 /opt/atlas/etc/cfg_endeavour_link00.json
```
The first argument is the AMAC communication ID. If you have not yet run `setid` and you want to run it at the same time, add `setid` as a third argument.

At BNL, we made an alternative version [powerUP_amac_star_stepbystep.sh](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/AMAC/powerUP_amac_star_stepbystep.sh), where we separated each register write into its own endeavour command. We found this to be more stable, but it may or may not be necessary for other setups.  

After running `powerUP_amac_star.sh`, our current increases from 50 mV to 220 mV. 

## Running scans

### Installing YARR

```
git clone https://gitlab.cern.ch/YARR/YARR.git
cd YARR
git checkout devel
mkdir build
cd build
cmake3 .. -DYARR_CONTROLLERS_TO_BUILD="Spec;Emu;FelixClient" -DBUILD_TESTS=on
make -j4
make install
```

### Running YARR

#### test_star application

The first thing that is needed to run YARR is a "controller configuration": [controller_config_felix_client.json](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/YARR_configs/controller_config_felix_client.json)

Make sure to adapt the `busDir` to match your file system. The only other parameters which you might need to change are `localIPOrInterface` if your FELIX card is not in your local machine, and `connectorID` if you are not using FELIX device 0 (see explanation on `cfg_endeavour_link00.json` above). 

The [`test_star`](https://gitlab.cern.ch/YARR/YARR/-/blob/devel/src/libStar/app/test_star.cpp?ref_type=heads) application can be used as a first step to check the communication. To keep things clean, you can create a `YARR/run` directory from which to run everything. From inside `run`, do

```
../build/bin/test_star ~/YARR_configs/controller_config_felix_client.json -r 0 -t 1 -V Star_vH1A1
```
A brief explanation of the arguments:

- The first argument is the path to the controller configuration (make sure to adapt this to your file system).    
- `-r`: RX elinks to probe, in decimal. Each HCC corresponds to one RX elink. For a single-long strip module, the HCC will probably be on RX 0. For a short-strip module, the two HCCs will probably be on elinks 0 and 4. If you want, you can include all possible elinks here (all the ones that are checked in the elinkconfig GUI) until you figure out which one is correct.  
- `-t`: TX elinks to probe, in decimal. One TX elink controls multiple HCCs. For a single module, this will likely be 1. If you want, you can include other possivle elinks (1, 6, 11, 16 - the ones labeled as "LCB cmd." on the elinkconfig GUI) until you figure out which one is correct.  
- `-V`: the versions of your chips. For example, `vH1A1` means HCC version 1 and ABC version 1. 

By default, this will run the "Full" sequence of tests: check HPRs, probe HCCs, test HCC registers, configure HCC, test hit counts, test data packets static, and test data packets full. See the script for more options of test sequences. In general, it is not advised to run a single tests on its own (i.e. not part of a sequence).

A successful output for the HCC-related tests (check HPRs, probe HCCs, test HCC registers, configure HCC) is found below. 

<img width="800" src="/Images/FELIX/Screenshot_test_star_HCC.png" alt="Output from test_star">

*Successful output for HCC-related tests in test_star*

The `probeHCCs` test is of particular interest because it gives us the tx and rx elinks as well as the e-fuse ID for the HCC(s) on the module. One these elinks are known, all other RX elinks should be disabled from the elinkconfig GUI, or using the script [felix_config_enable_elinks_oneLink_oneHCC.sh](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/FELIX/felix_config_enable_elinks_oneLink_oneHCC.sh?ref_type=heads).

Currently, the ABC-related tests are still failing for us. 


#### Obtaining the connectivity and chip configurations

To run an actual scan, one needs additional configuration files: the connectivity config and the chip config. The connectivity config specifies the TX and RX elinks for each HCC as well as the path to the chip config. The chip config contains all the register values for an HCC and its associated ABCs. 

One way to obtain these configurations is by using the corresponding ITSDAQ configurations and translating them to the YARR format. This can be done with the [`scan-tools-for-yarr`](https://gitlab.cern.ch/itk-strips-at-sr1/scan-tools-for-yarr/-/tree/master) repository:
```
git clone https://gitlab.cern.ch/itk-strips-at-sr1/scan-tools-for-yarr.git
```
The script we need is `run_YARR_singlemodule.py`. This script requires a configuration file, which is found in `configs_barrel/scanSetup_singlemodule.json`. The fields should be relatively self-explanatory. A few notes:

- The `Strip_type` determines how many HCCs are considered ("LS" = 1 HCC, "SS" = 2 HCCs).  
- `HCC_crossed_bonds` determines which set of delays to use depending on whether the HCC has crossed bonds or not (this information should be available in the production database).  
- `ITSDAQ_Config_names` is the name of the chip config from ITSDAQ. It is a list, since there could be 2 HCCs.  
- `fuse_ids` should correspond to the e-fuse ID(s) obtained from the `probeHCCs` step in `test_star`.  
- `HCC_IDs` are the desired HCC communication IDs. This tells YARR to set a new communication ID for the HCC when configuring (similar to `setid` for the AMAC). 

To just obtain the configurations without running a scan, do
```
python3 run_YARR_singlemodule.py -c configs_barrel/scanSetup_singlemodule.json --no_scan
```

The resulting config files in our case are found in [Module_PPB_LS_connectivity.json](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/YARR_configs/Module_PPB_LS_connectivity.json) and [Module_PPB_LS_hcc0.json](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/blob/master/YARR_configs/Module_PPB_LS_hcc0.json).

Note: The readout speed might need to be modified in `Importdefinitions.py` to match your FELIX and lpGBT configurations. For 320 Mbps, the HCC registers `OPmode` and `OPmodeC` should have value "00030001", whereas for 640 Mbps it should be "00030011".

#### Running a scan

Below is an example command for running a scan (from the `YARR/run` directory):
```
../build/bin/scanConsole -s ../configs/scan/star/std_regDump.json -c ~/YARR_configs/Module_PPB_LS_connectivity.json -r ~/YARR_configs/controller_config_felix_client.json
```
A brief explanation of the arguments:

- `-s`: scan configuration file. A few examples are found in `YARR/configs/scan/star`. It contains the details of the parameters of the scan itself. The example here `std_regDump.json` will just print out the values of the HCC and ABC registers.   
- `-c`: path to the connectivity config (see previous step).  
- `-r`: path to controller config.  

Another option is to use `run_YARR_singlemodule` (from `scan-tools-for-yarr`):
```
python3 run_YARR_singlemodule.py -c configs_barrel/scanSetup_singlemodule.json
```
In this case, the scan config should be specified in `scanSetup_singlemodule.json`. The YARR path might need to be modified inside `run_YARR_singlemodule.py` to match your file system. 

## Probing signals with an oscilloscope

Sometimes, it is helpful for debugging to look at the various signals going through the system at different places. 

### On the VLDB+

To see the signal coming OUT of the lpGBT, one can use a miniHDMI breakout board, as shown below. 

<img width="500" src="/Images/FELIX/miniHDMI_breakout.jpg" alt="miniHDMI breakout">

*miniHDMI breakout board*

After configuring everything, unplug the HDMI cable (the one coming out of the VLDB+ and going to the Nikhef board) on the Nikhef board side, and plug it into the miniHDMI breakout board. Place the scope probes on pins 5 and 6. While sending the `endeavour2` command (preferably in a loop), the commands going TO the AMAC should be visible on the scope. It should look something like this:

<img width="500" src="/Images/FELIX/Scope_AMACin_miniHDMI.jpg" alt="AMAC commands from miniHDMI">

*AMAC commands coming out of lpGBT, probed on miniHDMI breakout board*


### On the modules

On the module, the data and AMAC lines can be probed from the miniDisplayPort connectors, as shown below.

<img width="500" src="/Images/FELIX/miniDP_module_probePoints.jpg" alt="Probe points on module">

*Oscilloscope probe points on miniDP connectors on module*

[This diagram](https://twiki.cern.ch/twiki/pub/AtlasPublic/DAQloadVLDBplusAtSR1/Module_board.png) from the SR1 VLDB+ Twiki is helpful in understanding the mapping.

The pictures below show what we saw on the scope for the various lines and commands:

<img width="500" src="/Images/FELIX/Scope_R3L1_module.jpg" alt="R3L1 from module">
<img width="500" src="/Images/FELIX/Scope_LCB_module.jpg" alt="LCB from module">
<img width="500" src="/Images/FELIX/Scope_clock_module.jpg" alt="Clock from module">

*R3L1, LCB, and clock signals, probed on miniDP on the module*

<img width="500" src="/Images/FELIX/Scope_AMACin_module_setid.jpg" alt="AMACin setid from module">

*AMAC in setid signal, probed on miniDP on the module*

<img width="500" src="/Images/FELIX/Scope_AMACin_module_reg.jpg" alt="AMACin read register from module">

*AMAC in read register signal, probed on miniDP on the module*

## Common problems and solutions

### Unable to clone git repositories with ssh

Follow these steps:

1. Generate an ssh key:  

    ```
    ssh-keygen -t rsa -b 4096 -C "<my_cern_email_>"
    ```

2. Add key to SSH agent:   

    ```
    eval "$(ssh-agent -s)"
    ssh-add ~/.ssh/id_rsa
    ```
    
3. Add key to gitlab.cern.ch:  

    ```
    cat ~/.ssh/id_rsa.pub
    ```

    Copy the key, go to "ssh settings" on the gitlab webpage, click "add new key" and paste it into the box. 

4. Add the lines below to `.ssh/config`: 

    ```
    Host github.com
    HostName gitlab.cern.ch
    User git
    IdentityFile ~/.ssh/id_rsa
    ```

5. Ensure the right permissions:  

    ```
    chmod 700 ~/.ssh
    chmod 600 ~/.ssh/id_rsa 
    ```

### Need to upgrade to Alma9

We did this via a USB drive that already had Alma9 on it (see the [Alma Linux 9 page](/Instructions/AlmaLinux.md)). Before starting, we backup up the data we wanted to keep to a different partition on the PC, which we believed would not be affected. We then followed this [step-by-step guide](https://linux.web.cern.ch/almalinux/alma9/stepbystep/), except that we applied manual partitioning. If you need to do this, I suggest to talk to an IT expert. 

### LCG compatibility

After upgrading to Alma9, we were getting an error message about not finding LCG when running `source felix-05-00-04-rm5-stand-alone-x86_64-el9-gcc13-opt/setup.sh`. Our solution may or may not be be generalizable.

We did the following:
```
locmap --enable cvmfs
locmap --configure all
```
(the [instructions](https://linux.web.cern.ch/almalinux/alma9/stepbystep/) made it seem like this was only necessary if we had NOT installed the "CERN Software Development Workstation", but it seems that is not the case). 

However, this did not completely solve the problem. In the end, what did the trick was adding the following lines to the `/etc/cvmfs/default.local` file:
```
CVMFS_REPOSITORIES=cvmfs-config.cern.ch, atlas-condb.cern.ch,atlas-nightlies.cern.ch,atlas-online-nightlies.cern.ch,atlas.cern.ch,atlas-pixel-daq.cern.ch,sft.cern.ch
CVMFS_HTTP_PROXY="http://cvmfs-cache.sdcc.bnl.gov:3128|http;DIRECT"
```

### Conflicting git versions

After upgrading to Alma9, we were getting the error "remote-https is not a git command" when trying to clone a repository. In our case, this was due to conflicting git versions. The easiest solution is to open a new terminal and NOT set up the FELIX software.

### FELIX card not found

If the FELIX card is not found when doing `flx-init` or `flx-info`, check whether the firmware uses bifurcation (see [documentation](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/releases.html)). If it does, the BIOS of the PC might need to be modified. 

### Unable to upload lpGBT configuration

If you are getting an "out of range" error when trying to upload the lpGBT registers on the piGBT interface, this likely means no data is being received. Check the fiber connections. 

One trick to check if the configuration worked (without having a lock): upload the registers, go to another page in the piGBT interface, then come back to "registers" page. If they changed back to their default, the configuration did not work. 

### The lpGBT is locked but the channel does not align on FELIX 

If the lpGBT is locked (green check mark on the piGBT interface), but is shown as not aligned when doing `flx-info gbt`, an inversion of the high speed data output lines is necessary. This is controlled by the lpGBT register `highSpeedDataOutInvert`. For lpGBT v1, this corresponds to bin 7 of register 0x36 (see [documentation](https://lpgbt.web.cern.ch/lpgbt/v1/registermap.html#x036-chipconfig)). If the value of the register is currently 0x00, change it to 0x80.

### Cannot start felix-star nodes 

If you are getting a "spawn error" when trying to start any of the felix-star nodes, run the following:
```
mkfifo ~/felix-star/stats_tohost_0
mkfifo ~/felix-star/stats_toflx_0
```

### Missing libraries for AMAC code

If you are getting an error about zmq not being found when trying to install `dcs-tests`, run the following:
```
sudo dnf install zeromq-devel
```

### Unable to run endeavour

If you are getting a "Permission denied" error when trying to run endeavour, change the permissions (`chmod 777`) on the lock files.

### Not getting any data back from the AMAC

Check inversions of the EC line in FELIX and the lpGBT.

#### On FELIX:

To check whether an inversion is applied, run:
```
flx-config -d 0 getraw -b 2 -r 0xD000 
```
If this returns 0x000, this means the inversion is not applied. If it returns 0x008, it is applied. To change the value of the register, run:
```
flx-config -d 0 setraw -b 2 -r 0xD000 -o 0 -w 64 -v <new_value>
```
where `<new_value>` would be 0x000 for removing the inversion and 0x008 for applying it. 

#### On lpGBT:

The inversion is controlled by the `EPTXEcInvert` subregister. For lpGBT v1, this corresponds to bit 2 of register 0x0ac (`EPTXEcChnCntr`) (see [documentation](https://lpgbt.web.cern.ch/lpgbt/v1/registermap.html#x0ac-eptxecchncntr)). If this bit is 1, the inversion is being applied.

Either both or neither of the FELIX and lpGBT registers need to be applying the inversion (this still needs to be checked). 

Note: the lpGBT v0, the `EPTXEcInvert` subregister corresponds to bit 5 of register 0x0a8 (`EPTXControl`) (see [documentation](https://lpgbt.web.cern.ch/lpgbt/v0/registermap.html#x0a8-eptxcontrol)).

#### Summary

FELIX: 

- register `0xD000` = `0x000` = no inversion  
- register `0xD000` = `0x008` = inversion

lpGBT v1:

- register `0x0ac` = `0x0e1` = no inversion  
- register `0x0ac` = `0x0e5` = inversion

### FELIX not able to subscribe to elinks

If you are getting the message "Error while creating info from bus: Resource currently not available" when running YARR, this likely means that the TX elinks are not enabled. Enable them with the `phase2_enable_elinks.sh` script, or with `flx-config`, then restart felix-star (see sections "Enabling elinks" and "Running felix-star" above).


## Useful links  

- [Gitlab repository](https://gitlab.cern.ch/elebouli/single-module-felix-bnl/-/tree/master) with custom scripts and configurations used for this setup at BNL
- [Twiki](https://twiki.cern.ch/twiki/bin/view/AtlasPublic/DAQloadVLDBplusAtSR1) from single module setup at SR1
- Old SR1 [Twiki](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/FelixStripsTestsAtSR1) (mostly outdated, but could be helpful for certain things)
- [LBL ALTAS Wiki page](https://atlaswiki.lbl.gov/strips/felixdaq): we used it primarily for the instructions regarding programming the MAX10 on the Nikhef board.
- [VLDB+ manual](https://vldbplus.web.cern.ch/manual/index.html)
- [piGBT manual](https://pigbt.web.cern.ch/manual/)
- [lpGBT manual](https://lpgbt.web.cern.ch/lpgbt/)
- [Gitlab](https://gitlab.cern.ch/atlas-tdaq-felix/strip-vldb-plus-adapter-board) page for Nikhef board design (schematics are in `Doc/Adapterboard/Adapterboard_sch.pdf`)
- [FELIX user manual](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-doc/felix-user-manual/5.x/1_overview.html)
