# Firmware Installation in Module Coldbox

The FW in module coldbox is installed through the SD card on the FPGA board. 

## How to install/change FW version

Pull out the MicroSD and read it with any computer using a SD card reader. There should only be 1 .bit file for the FW which is installed in the card right now.
Format the card (this is a safe way to make sure the file is properly deleted) and then choose the FW version from the link below.

https://www.hep.ucl.ac.uk/~warren/upgrade/firmware/?C=M;O=D\

You only need to download the ".bit" file. And that should be it.
Reinstall the card in the FPGA board and power cycle it. On the display, you should see the version you have installed.

P.S. For now, the version which works with ITSDAQ is "https://www.hep.ucl.ac.uk/~warren/upgrade/firmware/nexysv_itsdaq_vb5fb_FDP_STAR.bit". This is an old version from 2022. Dont update this unless you absolutely know you have to.

