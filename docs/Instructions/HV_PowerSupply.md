# Setting up HV Power Supplies

The PSU units used to deliver HV to the stave are ISEG SHR units, hereby referred to as ISEG. Each ISEG unit powers one side of a stave.

## Setting up connection

### Ethernet

On the ISEG, you can manually set an IP. If replacing a previous ISEG, I recommend using the same IP as previous. This saves you messing with ITSDAQ settings

If you're having trouble connecting when you first set things up, try a factory reset of the ISEG

### Connecting to the iCS2 interface

Once the connection is enstablished, open a web browser and write the device IP in the address bar (without https://). You will be prompted to enter username and password.

As of Feb 2025, these are 

* [169.254.5.113](https://169.254.5.113/en/control) (Master side)
* [169.254.5.114](http://169.254.5.114/en/control) (Slave side)

## Current testing settings

* **Polarity: Negative**
    * Very important! In `iCSconfig` → hardware
* voltage ramp: 0.05%/s * Vnom
* current ramp: 20%/s * Inom
* Channel Iset (current limits)
    * Channel 0 (Modules 11-13/25-27): 180μA
    * Channel 1 (Modules 4-7/18-21): 230μA
    * Channel 2 (Modules 8-10/22-24): 180μA
    * Channel 3 (Modules 0-3/14-17): 230μA

## Current software and firmware versions

Tests are currently (Feb 2024) ran using the following software and firmware on the ISEG units:

* product database version: 20220208
* iCS version: 2.12.3 20230620_1250
* Firmware: N04C2
    * Master Side: 01.86 (`N04C2_186.hex`)
    * Slave Side: 01.89 (`N04C2_189.hex`)

## Links
- [ISEG software for updates](https://iseg-hv.com/download/?dir=SOFTWARE/iCS)
- [Documentation for ISEG API (iCService)](https://iseg-hv.com/download/SOFTWARE/iCS/doc/iCSservice/iCSapiWebsocket_Docu.html)
- [Python library for ICService communication](https://iseg-hv.com/download/SOFTWARE/iCS/iCSpython/ics-python3.pdf)
- [Documentation for ICService configuration](https://iseg-hv.com/download/SOFTWARE/iCS/doc/iseg_iCS2_en.pdf)
