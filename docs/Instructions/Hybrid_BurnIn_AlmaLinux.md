# Hybrid Burn-In Station Instructions

## Useful Links and Groups

### Hybrid Burn-In Website Links
* [BNL_ModuleStatus spreadsheet](https://docs.google.com/spreadsheets/d/1PgVL5i0sD9h57ORrsTRfMN7Rfhz91kjiC8jE2fEcbeQ/edit#gid=2146007184)
* [Production Database](https://itkpd-test.unicorncollege.cz/)
* [BNL Uploader](https://bnl-uploader.app.cern.ch/)
* [InfluxDB](http://localhost:8086/orgs/ee0b4ab0edd1fdc1)
* [Grafana](http://localhost:3000/?orgId=1)
* [ITSDAQ Documentation](https://atlas-strips-itsdaq.web.cern.ch/index.html)
* [Production Database Scripts](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts)
* [Geneys2 FPGA Bit files](https://www.hep.ucl.ac.uk/~warren/upgrade/firmware/?C=M;O=D)
* [Hybrid Burn-in twiki](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/HybridBurnInCrate#Software_links)
* [ITSDAQ](https://gitlab.cern.ch/mbasso/itsdaq-sw)
* [ITSDAQ tutorial](https://indico.cern.ch/event/1223749/#17-itsdaq-tutorial-session)

### e-groups to join 
* atlas-upgrade-itk-strip-triggerDAQ
* atlas-itk-production-database

## Components of the Hybrid Burn-In Crate
### Front of Crate
<img width="613" src="/Images/Burn_In/FrontOfCrate.png" alt="Front Image of Crate">

### Back of Crate
<img width="670" src="/Images/Burn_In/BackOfCrate.png" alt="Back Image of Crate">


## Burn-in Crate Setup

The hybrid burn-in station is located in the back-right of the testing clean room. There is a spare burn-in crate on the optics table near Box1 for module testing

1. Plug in the barrel plug for the fans and FPGA 
2. Plug in the JTAG cord (microUSB on the right side of the ethernet port on the FPGA board)
3. Plug in the ethernet cord (the light on the right side should blink green)
4. Plug in the voltage supply in the back of the crate

*In standard operation, we leave the ethernet, JTAG, and voltage supply plugged in.*

## Hybrid Test Panel Setup
* Handling the panels:
    * The panels have very tiny wirebonds all over the board. Be careful not to touch them! 
        * Hold the panels on the edges or from underneath
        * Do not touch the gold parts
        * *Ask for help if you are unsure on how to handle the panels*
    * Test panels will be in the large desiccator cabinet or in the clear desiccator next to the cabinet 
* Installing the powerboards:
    * Ask a tech or postdoc or Stefania to show you how to install the powerboards
        * Make sure all pins are lined up correct, and apply firm pressure directly downwards to install
    * Installing powerboards incorrectly will damage the wirebonds or the powerboard itself
    * **The powerboards have unique ID numbers (0, 1, 4). You cannot put two powerboards with the same ID number on one panel** 
* Inserting/removing panels:
    * There are guide rails to ensure the test panels plug in straight
    * Careful not to bump adjacent boards when handling panels inside the crate
    * Inserting may require a firm push to seat
    * **The top slot is particularly tight, exercise care when inserting and removing panels**
* Updating the spreadsheet
    * All module/hybrid information is kept in the [BNL_ModuleStatus spreadsheet](https://docs.google.com/spreadsheets/d/1PgVL5i0sD9h57ORrsTRfMN7Rfhz91kjiC8jE2fEcbeQ/edit#gid=2146007184)
    * Each panel has a local serial number (SN) written somewhere on the test panel
        * The local SN tends to look something like `USA-PAN-034`    
    * Local SN ↔ ATLAS SN information is in the 'Hybrid Test Panels' tab
        * The 'New Panel's SN' column is the ATLAS SN and link to the database
        * The 'Panel' column is the local SN
    * Find the ATLAS SN for your panel(s). You will need it later
    * Individual hybrids are tracked in the 'Hybrid Assembly' tab
        * When testing, find the panel you're burning-in in the 'Test Panel Number' column
        * Update the 'Electrical Test' and 'Burn In' status of all burning-in hybrids to 'In progress'
            * When finished, change 'In progress' to 'Done'
* Once testing is done, place test panels back where you got them

## Doing a Hybrid Burn-in
### Starting up
1. Turn on the computer and login to the user: qcbox
    * Ask a local BNL person for the password
2. Open a terminal, then open a second tab
    * One tab is used for ITSDAQ, the other for AMAC monitoring
3. Change into the Hybrid_BurnIn directory (`cd Hybrid_BurnIn`) in both tabs
4. Set up the environment variables using in **both tabs** with `source setup.sh`
    * Note: you will have to enter the root password
5. Check everything is plugged in properly
6. Turn on the FPGA board
7. In the first tab, program the FPGA board with `source program_FPGA.sh`
8. Turn on the Low Voltage Power supply.
    * If off, turn on power supply by pressing the on/off button
    * Supply power to the FPGA board by pressing the `OUTPUT 1` button
        * There are four lights you need to verify are in the right place
            1. You should see a yellow light next to Constant Voltage (CV). If the light is next to Constant Current (CC), refer to 'How to fix the problem'
                * Make sure this light stays on once you've started ITSDAQ
            2. The Lock button will have a green light. This ensures the current and voltage limits are locked in place. 
                * If you need to change the voltage/current, hold the button for a few seconds to unlock
            3. OUTPUT 1 will have a red light next to it. This shows that the power is being supplied to the crate.
            4. 60V/20A should have a red light next to it. If not, press the button
9. Change into the itsdaq-sw directory (`cd Hybrid_BurnIn`) in both 

### Starting ITSDAQ
1. Update `st_system_config.dat` to match the physical setup. This file is responsible for setting up the hybrids in ITSDAQ
    * The file is located in `itsdaq-sw/DAT/configs`. 
    * The file is already mostly set up, simply comment out/in whichever hybrids are in use
        * It can also be useful to comment which panel is in which slot
2. In the first tab of your terminal, start ITSDAQ
    * This can be done with `source RUNITSDAQ.sh` or the alias `rid`
3. Check to ensure all HCCs and ABCs are being properly read
    * *Note: Usually HCC is a powerboard issue, ABC is a hybrid issue*
    * In the itsdaq gui, diagnostics tab, run `FuseIDReadback`
    * If not found, the first step is to restart ITSDAQ 
    * If this doesn't work, navigate to the "How to fix the problem" section, specifically the "Not all ABCs/HCCs found" subsection
4. Prepare assembly scripts by running `AutoConfig(false,false,false)`
    * The `false,false,false` options tell the script to download information from the database
    * `AutoConfig` will produce a file for each panel, `assembly_script_panel_#.json` where # represents the panel number
5. Assemble ABCs to the hybrids in the database
    * Navigate to the production database scripts directory
        * `Hybrid_BurnIn/production_database_scripts/`
    * Assemble the components to the database with the `assemble_component.py` script
        * `python3 assemble_component.py --json [Assembly_Script_Panel_JSON_FIle] --reference [PanelNumber] --dryRun`
        * **Ensure the `json` file is matched to the proper PanelNumber**
        * The `--dryRun` option skips assembly. Use it first to ensure everything is correct, then remover and rerun
        * Example: `python3 assemble_component.py --json ~/Hybrid_BurnIn/itsdaq-sw/assembly_script_panel_2.json --reference 20USBTG0000197 --dryRun`
    * Assemble all panels to the database
6. Navigate back to the `itsdaq-sw` directory
7. Start ITSDAQ again as a safety/sanity 
    * `source RUNITSDAQ.sh` or `rid`
    * Check for all ABCs and HCCs with `FuseIDReadback`
    * Run `AutoConfig(false,false,false)` to confirm proper assembly
        * Look for something like `ABC45811b (0,0) has parent 20USBHX2001648`
    * You can quit ITSDAQ once this is done
    
### Starting Burn-in
1. To prep for easier merging, ensure the `DAT/results/` folder is empty
    * **DON'T DELETE OLD RESULTS**, move them into the `archivedResults/` folder 
        * Be smart about your organization, name your folders!
2. In one terminal, run `source INFLUX_AMAC.sh` to start fan monitoring
    * The output should show temperature readings of the hybrid burn in crate. Look for something like: 

            AMAC temp. 0(H0, H1, PB): 36.6536, 40.3264, 48.7372
            AMAC temp. 1(H0, H1, PB): 35.4231, 36.2313, 48.7372
            AMAC temp. 2(H0, H1, PB): 36.6536, 36.9995, 48.7372
            AMAC temp. 3(H0, H1, PB): 35.7663, 36.8725, 48.7372
            AMAC temp. 4(H0, H1, PB): 39.3784, 37.9613, 48.7372
            AMAC temp. 5(H0, H1, PB): 39.3236, 37.6292, 48.7372
            AMAC temp. 6(H0, H1, PB): 33.7066, 35.2287, 48.7372
            AMAC temp. 7(H0, H1, PB): 34.1538, 35.5364, 48.7372
            AMAC temp. 8(H0, H1, PB): 34.287, 35.2728, 48.7372
            Read max. temperature 40.3264
    
3. In your other tab, run `source SIMPLE_20_TEST_LOOP.sh`
    * This will start ITSDAQ, run a FullTest, then repeat 
    * Check the `DAT/results/` and `DAT/ps/` folders to ensure tests are being saved properly
4. The tests will stop and ITSDAQ will close after 20 consecutive tests (~7 hours)
    * The script does *not* shut off LV, so that needs to be done manually.
    * Stop `INFULX_AMAC` in the terminal with `Ctrl+Z` then `kill %` to end the process

## Uploading Results
### Merging the test files 
1. To merge all of the files for each test (Strobe Delay, Pedestal Trim, Response Curve, No PPA), run the following commands

        cd ~/Hybrid_BurnIn/itsdaq-sw/contrib/itsdaq-merger 
        python3 control_panel.py -i $SCTDAQ_VAR/results/*STROBE_DELAY_PPA*.json -o $SCTDAQ_VAR/to_upload/ -b --test-type STROBE_DELAY
        python3 control_panel.py -i $SCTDAQ_VAR/results/*RESPONSE_CURVE_PPA*.json -o $SCTDAQ_VAR/to_upload/ -b --test-type RESPONSE_CURVE_PPA
        python3 control_panel.py -i $SCTDAQ_VAR/results/*PEDESTAL_TRIM_PPA*.json -o $SCTDAQ_VAR/to_upload/ -b --test-type PEDESTAL_TRIM_PPA
        python3 control_panel.py -i $SCTDAQ_VAR/results/*NO_PPA*.json -o $SCTDAQ_VAR/NO_PPA/ -b --test-type NO_PPA
    
    * For unknown reasons, `STROBE_DELAY_PPA` fails, so we use `STROBE_DELAY`

### Uploading to the database

Uploading of tests is done via the [BNL Uploader](https://bnl-uploader.app.cern.ch/)!

1. Log in using the same authentication you use for the database
2. Click 'Test'
3. Click 'Hybrid'
4. Click 'Electrical Tests'
5. Drag and drop the `.json` files from `DAT/to_upload`
    * You can upload in as many or as few steps as you would like. I like to do one test type at a time.
6. Click 'Upload'
7. Once tests are uploaded, move the `json` files into the `DAT/uploaded/` folder 

<details><summary>Deprecated Script Uploads</summary>

1. To upload the merged test to the database run the following commands:

            cd ~/Hybrid_BurnIn/production_database_scripts/strips/modules
            python3 upload_results.py --results-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/STROBE_DELAY_PPA/ --to-upload-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/to_upload --upload-done-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/uploaded/ --institution BNL
            python3 upload_results.py --results-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/PEDESTAL_TRIM_PPA/ --to-upload-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/to_upload --upload-done-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/uploaded/ --institution BNL
            python3 upload_results.py --results-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/RESPONSE_CURVE_PPA/ --to-upload-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/to_upload --upload-done-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/uploaded/ --institution BNL
            python3 upload_results.py --results-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/NO_PPA/ --to-upload-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/to_upload --upload-done-dir /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/uploaded/ --institution BNL
        
    * The `--results-dir` is where each of the merged test files are stored. These directories were made in the 'Merging the test files' section (STROBE_DELAY_PPA PEDESTAL_TRIM_PPA RESPONSE_CURVE_PPA NO_PPA).

</details>

## Ending the Burn-In Testing
1. Navigate to the [BNL_ModuleStatus spreadsheet](https://docs.google.com/spreadsheets/d/1PgVL5i0sD9h57ORrsTRfMN7Rfhz91kjiC8jE2fEcbeQ/edit#gid=2146007184)
2. In the 'Hybrid Assembly' tab, update 'Electrical Test' and 'Burn In' from 'In Progress' to 'Done'
3. Move the contents of the `DAT/results` folder to the `archivedResults/` folder
4. Turn off the Output 1 of the LV power supply
5. Turn off the FPGA board
6. Unplug the barrel plug to the FPGA board
7. Return the test panels to the desiccator crate
8. Have a great day :)

## Studying and Characterizing Hybrids
** TO BE UPDATED**

* There's a script from Marcus Wong at UC Santa Cruz to check the quality of hybrids, we need to investigate it further...
* But roughly, from `itsdaq-sw/MarcusScript` run `python plotBIData-4.py '../DAT/results'`

## How to fix the problem 
**Update this section as new problems and solutions are discovered!**

* The first solution is to try restarting ITSDAQ
* The second first solution is to power cycle. Turn everything off, wait a minute, turn everything back on
### Not all ABCs/HCCs found
* Checking wirebonds
    * Wirebond issues usually show up as not-found HCCs, it's rare for individual ABCs to be a bonding issue
    * It could be a physical issue, check with the microscope
    * Look at bonds between the test panel and the hybrids
        * A break is usually obvious
        * A detached bond is harder to see. Check if the foot of the bond is in focus when the backplane is in focus
            * If they're in different planes of focus, the bond might be hovering above the pad
* Try switching powerboards around on the panel
* Single ABCs missing is usually a delay issue
    * A digital test can reveal weirdness in the delays to confirm
    * The default delay is `a`, they're hexadecimal so try `b` or `9` etc. until you either find the ABC or lose more ABCs
    * Within ITSDAQ, try `e->ConfigureVariable(ST_HCC_STAR_ABC_ALL_FINE_DELAY,0xa);e->ExecuteConfigs()`
        * Replace the `a` in `0xa` with your chosen delay value
    * For a more permanent change, you can change R33 and R34 in `DAT/config/default_Barrel_xHCC.det` and restart ITSDAQ
    * According to Peter Phillips, you can also try the “magic HCC R32 value 0x02903520”
* If nothing else works, ask Stefania or the 'Hybrid Burn-in' mattermost channel
    
### Other Issues
* "Failed to read from DAQ board to get initial status, closing connection"
    * Usually this is a network problem, the computer switches which ethernet it is using
        * Check the network settings:
            * In the top right of the desktop, click the icons and navigate to network settings
            * Make sure Ethernet (enp0s31f6) is connected
    * Check for and kill any errant background processes

            ps aux | grep root | grep Stave
            ps aux | grep root | grep LTT
            ps aux | grep 'RUNITSDAQ'
            kill -9 [process numbers]
    
    * Manually reset the FPGA board
        * Underneath the Digilent GENESY2 logo there are two manual reset buttons 
        * Press them
        * Reprogram the board

* The fans are not working (spinning)
    - Check to make sure the power cord is plugged into an outlet
    - Check to make sure the power cord is connected to both the FPGA board and the Arduino board
    - Unplug it, wait a bit, and then plug it back in 

* Error with fans (when running source INFLUX_AMAC.sh)

            Can't read AMACs, setting fans to maximum
            AMAC temp. 0(H0, H1, PB): -1000, -1000, -1000
            AMAC temp. 1(H0, H1, PB): -1000, -1000, -1000
            AMAC temp. 2(H0, H1, PB): -1000, -1000, -1000
            AMAC temp. 3(H0, H1, PB): -1000, -1000, -1000
            AMAC temp. 4(H0, H1, PB): -1000, -1000, -1000
            AMAC temp. 5(H0, H1, PB): -1000, -1000, -1000
            AMAC temp. 6(H0, H1, PB): -1000, -1000, -1000
            AMAC temp. 7(H0, H1, PB): -1000, -1000, -1000
            AMAC temp. 8(H0, H1, PB): -1000, -1000, -1000
            Read max. temperature -999

    * Check that the current is being supplied
        * Is there a red light by OUTPUT1 on the power supply? If not, press the button
        * Check if the CV light is highlighted and not CC
    * If CC has the light, then there is not enough current for the panels. 
        * Each full panel should get roughly 2A. Increase the current limit on the power supply

* After `source SIMPLE_DAQ_LOOP.sh` the terminal shutdowns, or the tab went missing, or it's just not working
    * Rerun the `setup.sh` script and reprogram the FPGA board

* When trying to start ITSDAQ, you see:

            Error: bash: /bin/root: No such file or directory  

    * Rerun the `setup.sh` script in the terminal window you're using

* When trying to start ITSDAQ, you see:

            TST::Startup - Number of modules present: 1
            ...done Startup
            Loading ABCStar test macros...!
            Creating FMC65420 object for ABCStar communication as h
            SYSTEM ALREADY INITIALISED, READING CURRENT SETTINGS
            Warning: timeout while reading EEPROM data!
            Warning: timeout while reading EEPROM data!
            Warning: timeout while reading EEPROM data!
            Warning: timeout while reading EEPROM data!
            Warning: timeout while reading EEPROM data!
            Warning: timeout while reading EEPROM data!

    * The panel is not fully seated in the crate. Push it in.

* Grafana works but you do not see any test results 
    1. In Grafana, click on the Influx Bucket dropdown menu on the dashboard
    2. You will see three options *(pending you only have one API token setup)*: `_monitoring`, `_task`, and `_hybrid-burnin`
    3. Click on `_hybrid-burnin`, you should see test pop-up if you are currently running the burnin (with some time delay). 
        * If you are not running the burn-in, click on the clock (next to the save and setting button) and adjust the time

* Power Supply is reading CC and not CV. 
    * Supply may have automatically set to CC (constant current) mode due to shorts
        * Power off supply
        * Push in/ check all of the cables on the power supply and on the burn-in crate
    * The current limit is not high enough for the number of panels 
        * A fully populated crate consisting of 6 hybrid panels with 36 Barrel hybrids, will require roughly ~10V/12A
    * Power cycle the supply 

* Output from SIMPLE_DAQ_LOOP:

            Uploading results (NO) to InfluxDB 
            Creating json file for results (NO_PPA)
            Using 20USBHX2001647 as SN for DB result files
            File /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/results/SN20USBHX2001647_20240223_60_64_NO_PPA.json opened to save results
            ABCStarNOScurves called for module 5
            Using ROOT Datafile /home/qcbox/Hybrid_BurnIn/itsdaq-sw/DAT/data/strun60_64.root
            scan_start 0.000000, stop 40.000000, npoints 41
            Running simple DAQ loop, will now wait before running the next test
            abort by using the Action -> Abort menu item

    * This is not an error, it's just slow

* The internet is not working 
    * Call IT, sometimes the computers will get kicked off of the BNL internet
    * **Only the IT team can fix this problem**

* Power Supply Error message: 

            Create TTi from json:
            /dev/serial/by-id/usb-TTi_MX_Series_PSU_483216-if00
            TSerialProxy: Error 2 from open: No such file or directory
            TTi::TTi (viOpen) UNKOWN at resource /dev/serial/by-id/usb-TTi_MX_Series_PSU_483216-if00 failed with code 0xffffffff (TSerialProxy: IO Error)

    * Check the connections to the power supply and the back of the hybrid burn-in crate
    * Turn the power supply on/off and check all of the four required lights


