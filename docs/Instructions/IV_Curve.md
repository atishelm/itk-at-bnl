# IV curve instructions

To take a module IV curve via the AMAC, first connect the module you want to take an IV for to a chuck in the coldbox. Connect LV/HV lines, data and AMAC lines.

* Screw grounding screws through the test frame into the chuck (see images below).

<details>

The grounding screws and an appropriate screwdriver should be by the sensor visual inspection setup:

<img src="/Images/GroundingScrewsLocation.jpeg" alt="TC sequence">

The screws can be screwed in the gap on the white spacer, directly into the test frame and chuck:

<img src="/Images/GroundingScrewPlacement.jpeg" alt="TC sequence">

</details>

* Close lid 
* Make sure you are in the correct directory: /home/qcbox/Desktop/itsdaq-sw
* Make sure you're on the correct  git branch by typing 'git branch' in the itsdaq directory. Right now, we want to be in master. Do 'git checkout master' if you are not in this branch.
* Turn on chiller, set to +20C. If the coldjig GUI is not running on a raspberry pi terminal, follow these instructions to turn on the GUI from scratch:


<details>


# Starting the coldjig s/w web gui 

The purpose of these instructions are to explain how to start the coldbox s/w GUI from scratch. This is used to monitor and control the various hardware components of the setup. <br>
<br>
1. If not already done, open a firefox window and a konsole terminal window by (double) clicking the icons on the desktop.<br>
2. On the konsole terminal, type the command `ssh_pi` and press enter. You will then be asked for the password. If you do not know the password, ask Abe or Stefania. <br>
3. If the ssh into the pi was successful, you should see the terminal prompt start with `pi@raspberrypi:~ $`. If this is the case, you can switch to the software working directory by typing and entering the command `gui`. After this, type and enter these commands in order:<br>

```
pipenv shell
source run_BNL.sh
```

If this was successful, you should begin to see some information printout in the window and you can move to step 4.<br>
<br>
If there was already an attempted instance to start the coldjig s/w running before you began, at this stage you may receive the error:<br>

```
OSError: [Errno 98] Address already in use
```

This is because there is a job already using the IP address provided in the configuration file to start an instance of the GUI. You can kill that already existing process in one way by running `ps -a` to list all running processes, and killing the python3 process which is likely the culprit by identifying its "PID" (process ID) from the `ps -a` printout, and running `kill -9 <PID>` to kill the process. After this, try `source run_BNL.sh` again to see if you can start the web gui. <br>
<br>
A possibly related problem occurs when the web gui starts but the "OK" message does not print in the terminal and commands in the gui are not executed. To fix this, try clearing the jobs. Run 'jobs' and then 'kill %1' in the pi.<br>
<br>
4. Open a firefox tab and click on the bookmark in the bookmarks bar of your desired webgui: `Coldjig-GUI-QC` or `Coldjig-GUI-Reception`. If this worked properly, you should see a window load the GUI and a Grafana dashboard. <br>
5. Start the GUI by clicking "Start" in the Control Panel tab. If this worked, you should see additional printout in the konsole where you ran `run_BNL.sh`. If all is working well, the grafana panel should start to update. One can always set the time of the grafana values to be more recent to confirm - e.g. switching absolute time range from "Last 1 hour" to "Last 5 minutes" with the dropdown menu.<br>

</details>

* Edit the config file `<SCTDAQ_VAR>/config/st_system_config.dat` accordingly. On most of the BNL computers, <SCTDAQ_VAR> is the DAT folder. For example if you have one LS module with crossed HCC bonds in the coldbox:

```
Module    0  1  1   0   0   -1  50  50   JaneDoe0 Barrel
```

* Make sure the institution is set in your setup file. You should see a line like `export ITSDAQ_DB_INSTITUTE=BNL`.


* Edit the `<SCTDAQ_VAR>/config/power_supplies.json` accordingly (where $SCTDAQ_VAR is an environment variable you can define in your `~/.bashrc` file, for example with `export SCTDAQ_VAR=/my/itsdaq/directory/`). Then set your config to control a single HV channel, for example:

```
{
    "lv_supplies": [
        {
            "psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0",
            "modName": "BNL Module 0 LV",
            "channel" : 1
        },
        {
            "psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0",
            "modName": "BNL Module 1 LV",
            "channel" : 2
        },
        {
            "psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VBPRE-if00-port0",
            "modName": "BNL Module 2 LV",
            "channel" : 1
        },
        {
            "psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VBPRE-if00-port0",
            "modName": "BNL Module 3 LV",
            "channel" : 2
        }
    ],

    "hv_supplies": [
        {
            "psName": "/dev/serial/by-id/usb-Linux_4.9.220-2.8.7+g57229263ff65_with_2184000.usb_Gadget_Serial_v2.4-if00",
            "modName": "BNL Module 0 HV",
            "output": 1,
            "deviceType" : "ISEG",
            "channel"LBNL_PPB2_LS_47 : 0
        }
    ]
}

```

In this example, all 4 LV channels will be detected by ITSDAQ, but only the HV PS channel 0, which will be used for the IV.

If your modules are loaded from left to right with no gaps, you can copy one of the following files in the DAT/config folder into power_supplies.json:

power_supplies_one.json

power_supplies_two.json

power_supplies_three.json

power_supplies_four.json

* Skip this step if you plan on using AutoConfig. If you have very specific configuration needs or are having AutoConfig issues, you can consider changing 'JaneDoe' to the local id in st_system_config.dat and ['Download hybrid config files'](https://itk-at-bnl.docs.cern.ch/Instructions/DownloadHybridConfigs/). 

* Make sure the LV and HV power supplies are turned on and the HV supply has all errors cleared. 

* Start ITSDAQ in a terminal (`cd itsdaq-sw`, `rid`)


* Switch on LV to supply 11V if it isn't already. The best way to do this is via the ITSDAQ GUI, only if the LV supplies were defined in the `power_supplies.json`. To turn on an LV channel, one would go to `DCS` in the dropdown menu and select `LV On (One Channel)`...

<img src="/Images/LV_ON_ITSDAQ_GUI.png" alt="Turn on an LV channel with the ITSDAQ GUI">

An easy way is to press 'output' on the INSTEK, but it isn't as safe to control the LV manually.

* Make sure all chips are configured and the current on the LV has reached about 280 mA. To do this:

-IF THESE ARE THE LAST MODULES YOU TESTED IN THIS SETUP: AutoConfig will run upon ITSDAQ startup and use the ASIC configuration parameters pulled for each chip from the database previously (they are saved in daq_asics/asic_info.json, which gets overwritten each time you pull from the database with AutoConfig).

-IF THESE ARE NOT THE LAST MODULES YOU TESTED IN THIS SETUP: Run AutoConfig(false,false,false) to configure the ASICs and pull the best parameters for each ABC from the database. You will have to enter your database access codes. Learn more about Autonfig [here](https://itk-at-bnl.docs.cern.ch/Instructions/AutoConfig/)

If there are red printouts saying a chip wasn't read or the current isn't right, do not proceed. If the HCC is present but not the ABCs, it is a crossbonding issue and you need to change "Barrel" to "BarrelUncrossed" in the st_system_config.dat file so that the default configuration file used assumes no cross bonding.



* In ITSDAQ GUI, select `Module (AMAC) IV scan (550V)` from `DCS` dropdown, and fill out accordingly:

<img src="/Images/IV_config.png" alt="ITSDAQ IV configuration">

The current limit should now automatically be 10 uA, which is correct!

Make sure to use the serial number and not local identifier so that the output file has the information necessary for upload. AutoConfig should fill this automatically.

Click `OK` (twice if you have an unconventional entry in the GUI), scan should begin.

* After the scan, make sure the HV is ramped down, then turn off the LV (ideally with the GUI). Then turn off the chiller.

* Upload the resulting json to the database via the bnl uploader.

* Create a beautiful plot of the IV using the [IV_Curves.py script](https://itk-at-bnl.docs.cern.ch/IV_Curves/)

* Upload the beautiful plot and json to CERNBox. You can wait for electrical tests to finish before you do this if applicable. If you do not know how to upload to CERNBox, see below:

<details>

To stay organized during Production, every production module tested at BNL should have a folder on [Abe's website](https://atishelm.web.cern.ch/atishelm/ITk/Production/). These folders are of the structure [LocalId]_[SerialNumber] so that they are easily searchable. A script to upload all results for a test run of 1 or more modules is in the itk-at-bnl git repository, which is pulled in the same directory as itsdaq-sw on every testing computer. The script is located at /Module-testing/Configs/CernboxUploadProduction.py.<br>
<br>
First, create a folder with the [LocalId]_[SerialNumber] format in Abe's CERNBox Production folder. If you do not have permission to do this, ask Abe to add you lxplus account.<br>
<br>
To run the script, navigate to the itk-at-bnl/Module-testing/Configs/ folder and run:<br>
<br>
python3 CernboxUploadProduction.py --username eduden --time '17 Nov 2023 15:00'<br>
<br>
where you change "eduden" to your own lxplus username and the time to the earliest time you want to upload files from. As the script runs, you will first have to enter your database access codes so the script can find the module corresponding to the hybrid SN on electrical test files. Then you will need to enter your lxplus password once for every module you are uploading tests for. If this does not work, ask Emily!

</details>


# Problems:

1. If the ISEG does not appear to be scanning properly (for example, ramping down 10-20V before ramping up to the next step) you might try updating the software and firmware on the ISEG. You can check which version of both you have by navigating on the ISEG to Control>Device Setup (or something like that). Update the software by downloading the software from [this link](https://iseg-hv.com/download/?dir=SOFTWARE/iCS/). The link also contains instructions for the download--you will have to type the ISEG IP into your browser to upload the new software. Firmware can be downloaded from the same link and uploaded in a similar way, but you will need to navigate to the hardware tab on the ISEG GUI in your browser to finish the installation.


