# Electrical tests

When a module is received from another institute, we need to ensure it survived the shipping process properly. We do this by performing a reception test, defined as an electrical test a room temperature.


## Procedure 

1. Make sure coldjig GUI is running - requires one terminal on the raspberry pi running. In production mode, should already have this open by default. If not, follow these instructions to turn on the GUI from scratch:


<details>


# Starting the coldjig s/w web gui 

The purpose of these instructions are to explain how to start the coldbox s/w GUI from scratch. This is used to monitor and control the various hardware components of the setup. <br>
<br>
1. If not already done, open a firefox window and a konsole terminal window by (double) clicking the icons on the desktop.<br>
2. On the konsole terminal, type the command `ssh_pi` and press enter. You will then be asked for the password. If you do not know the password, ask Abe or Stefania. <br>
3. If the ssh into the pi was successful, you should see the terminal prompt start with `pi@raspberrypi:~ $`. If this is the case, you can switch to the software working directory by typing and entering the command `gui`. After this, type and enter these commands in order:<br>

```
pipenv shell
source run_BNL.sh
```

If this was successful, you should begin to see some information printout in the window and you can move to step 4.<br>
<br>
If there was already an attempted instance to start the coldjig s/w running before you began, at this stage you may receive the error:<br>

```
OSError: [Errno 98] Address already in use
```

This is because there is a job already using the IP address provided in the configuration file to start an instance of the GUI. You can kill that already existing process in one way by running `ps -a` to list all running processes, and killing the python3 process which is likely the culprit by identifying its "PID" (process ID) from the `ps -a` printout, and running `kill -9 <PID>` to kill the process. After this, try `source run_BNL.sh` again to see if you can start the web gui. <br>
<br>
A possibly related problem occurs when the web gui starts but the "OK" message does not print in the terminal and commands in the gui are not executed. To fix this, try clearing the jobs. Run 'jobs' and then 'kill %1' in the pi.<br>
<br>
4. Open a firefox tab and click on the bookmark in the bookmarks bar of your desired webgui: `Coldjig-GUI-QC` or `Coldjig-GUI-Reception`. If this worked properly, you should see a window load the GUI and a Grafana dashboard. <br>
5. Start the GUI by clicking "Start" in the Control Panel tab. If this worked, you should see additional printout in the konsole where you ran `run_BNL.sh`. If all is working well, the grafana panel should start to update. One can always set the time of the grafana values to be more recent to confirm - e.g. switching absolute time range from "Last 1 hour" to "Last 5 minutes" with the dropdown menu.<br>

</details>

2. Open box, connect up to 4 modules to chuck connections
3. Screw grounding screws through the test frame into the chuck (see images below).

<details>

The grounding screws and an appropriate screwdriver should be by the sensor visual inspection setup:

<img src="/Images/GroundingScrewsLocation.jpeg" alt="TC sequence">

The screws can be screwed in the gap on the white spacer, directly into the test frame and chuck:

<img src="/Images/GroundingScrewPlacement.jpeg" alt="TC sequence">

</details>
Open box, co
4. Close box, set chiller to +20C via the coldjig GUI.
5. Edit BNL Module spreadsheet to say the electrical test for the chosen modules is in progress.
6. In ITSDAQ DAT/config/st_system_config.dat, define a `Module` line for each hybrid. Notably, the 5th numerical column (s0) corresponds to the module's chuck, where the first chuck is 0 and subsequent chuck's values increase in increments of 8. You can use the 'JaneDoeN' naming schema instead of local ID and AutoConfig will later find the module's serial number.
7. Ensure all power supplies are defined in DAT/config/power_supplies.json - this will be important for tracking LV/HV current/voltage in Influx. If your modules are loaded from left to right with no gaps, you can copy one of the following files in the DAT/config folder into power_supplies.json:

power_supplies_one.json

power_supplies_two.json

power_supplies_three.json

power_supplies_four.json

Turn on (but do not power) the power supplies.

8. Run ITSDAQ using the alias `rid` (for 'source RUNITSDAQ.sh').

9. Supply 11V from the two INSTEK power supplies to the modules. Because ITSDAQ software has interlocks, you should use the ITSDAQ GUI to do this. The "LV On" button is under the DCS tab. If you have trouble, you can press `OUTPUT` on the powersupplies. If nothing happens when you press the button, press 'LOCAL' first. For long strip modules, you should see current values of around 50 mA per channel. 

10. Configure the ASICs so that the LV is drawing about 280 mA. To do this:

-IF THESE ARE THE LAST MODULES YOU TESTED IN THIS SETUP: AutoConfig will run upon ITSDAQ startup and use the ASIC configuration parameters pulled for each chip from the database previously (they are saved in daq_asics/asic_info.json, which gets overwritten each time you pull from the database with AutoConfig).

-IF THESE ARE NOT THE LAST MODULES YOU TESTED IN THIS SETUP: Run AutoConfig(false,false,false) to configure the ASICs and pull the best parameters for each ABC from the database. You will have to enter your database access codes. Learn more about Autonfig [here](https://itk-at-bnl.docs.cern.ch/Instructions/AutoConfig/)

If there are red printouts saying a chip wasn't read or the current isn't right, do not proceed. If the HCC is present but not the ABCs, it is a crossbonding issue and you need to change "Barrel" to "BarrelUncrossed" in the st_system_config.dat file so that the default configuration file used assumes no cross bonding.

11. Ramp the necessary channels to -385V. Do not ramp the channels if the hybrid is not configured. Make sure the current limit is set to 50 uA on the HV power supply before you ramp.

This screenshot shows you how to configure IV Scan with PB compliance (AMAC)

<img src="/Images/ET_IV_AMAC.png" alt="TC sequence">


12. Run the electrical test in the ITSDAQ GUI by doing

    'Test>FullTest'

13. After the test, ramp down the HV first, then LV (using the ITSDAQ gui) and turn off the chiller. Also enter the the HCC fuse ID on the BNL Modules spreadhseet. You can do this easily by doing something like `grep "HCC_FUSE" DAT/results/SCIPP-PPB_LS-038*` to quickly search text files depending on which module you have.

14. Upload the jsons to the database. If you use the BNL uploader, you probably will have to add "_X_BNL" to the title because the uploader looks for files with these strings. Use "_Y_" instead of "_X_" if you have a Y hybrid.

15. Upload all the results to CERNBox. If you are doing both electrical tests and IVs, you can wait to do this until the IVs are also done. If you do not know how to upload results, see below:

<details>

To stay organized during Production, every production module tested at BNL should have a folder on [Abe's website](https://atishelm.web.cern.ch/atishelm/ITk/Production/). These folders are of the structure [LocalId]_[SerialNumber] so that they are easily searchable. A script to upload all results for a test run of 1 or more modules is in the itk-at-bnl git repository, which is pulled in the same directory as itsdaq-sw on every testing computer. The script is located at /Module-testing/Configs/CernboxUploadProduction.py.<br>
<br>
First, create a folder with the [LocalId]_[SerialNumber] format in Abe's CERNBox Production folder. If you do not have permission to do this, ask Abe to add you lxplus account.<br>
<br>
To run the script, navigate to the itk-at-bnl/Module-testing/Configs/ folder and run:<br>
<br>
python3 CernboxUploadProduction.py --username eduden --time '17 Nov 2023 15:00'<br>
<br>
where you change "eduden" to your own lxplus username and the time to the earliest time you want to upload files from. As the script runs, you will first have to enter your database access codes so the script can find the module corresponding to the hybrid SN on electrical test files. Then you will need to enter your lxplus password once for every module you are uploading tests for. If this does not work, ask Emily!

</details>




PROBLEMS:


1. Trying to run an electrical test, you get printouts like "Not found good even in many (10) packets" and the test will run forever if you don't stop it. Also, the LV current only reads about 240 mA after configuration.


    Solution: The module's .det file may not be configured to account for cross bonding on the HCC. Try opening the .det file and changing register 32 to read 0x02900020.

2. When running the test, you get a messasge in red: "hsio_read_event: read_bit_data error".

    Solution: Look at what stream/module the error corresponds to (0,8,16, or 24 depending on location of the module). if it's only 1 or 2 streams, you can comment out that module in DAT/config/st_system_config.dat and redo the test without it. 

3. After staring ITSDAQ, you get a 

    "Failed to read from DAQ board to get initial status, closing connection"

error, preceded by a message about the address already being in use (something like "udp_open:bind failed: Address already in use").

    Solution: You have a process in the background keeping the board busy. Run "ps -a" and look for the process. It might be a root instance. Then do "kill -9 [number1] [number 2] ..." where the numbers are associated with the processes you want to kill. 

4. You have trouble pulling the hybrid config files, getting an error on the line where you try to pull the hybrid SN in GetDBInfo.py. You then discover that the hybrid is not assembled in the database and has no config file yet.

    Solution: First, find someone to yell at for not assembling the hybrid! Then, run an electrical test (for example, strobe delay) using a default config file. On box 2, there is a file called "default.det" that should automatically be used when another config file isn't found. The test should produce a root file in DAT/data/ that you need for the next step.

    Now you can use the root file produced from this test to run an assembly script. There is no maintained script for assembling a single hybrid (that I am aware of) but Emily has a hastily edited version of 'assembleHybridByFuseIDs.py' on box 2 that should work. This script is in production_database_scripts/strips/hybrids on the hybrid burn in setup but I am unsure what git repository it comes from since it is not on master. The script you want to run on box 2 is Desktop/production_database_scripts/strips/hybrids/assembleHybridSingle.py. Using the root file outputted by your electrical test, run (for example):
        python3 assembleHybridSingle.py --root ~/Desktop/itsdaq-sw/DAT/data/strun26_1.root --hybridSN 20USBHX2001311

    Now you can try to pull the config file from the database like you normally do.

