# Remote Connection

As much as we love being in the lab, sometimes it's nice to be able to connect to our clean room PCs and setups from the comfort of our office or home. These instructions explain how to do so. 

## Obtaining a gateway machine account

To start, one needs to have an account on the clean room gateway machine located in the testing clean room. You can obtain this by asking Tom Throwe at `throwe@bnl.gov`, where in your email you should provide your preferred username and a public SSH key which you will use on the machine you will ssh from (you can find an example for how to create a public SSH key [here](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key)). An example email format is as follows (add `abraham.tishelman.charny@cern.ch` in cc): 

```
Dear Tom,

May I have an account on the clean room gateway machine located in the testing clean room? My preferred user name is XXX, and attached to this email is my SSH public key.

Thank you,
XXX
```

## Creating your `.ssh/config` file

After obtaining a username, one should add the following to their `.ssh/config` file from the machine they wish to ssh from:

<details><summary>.ssh/config file</summary>
```
ControlMaster auto
ForwardX11 yes
ForwardX11Trusted yes

# Stave testing PC
Host cleanroom_gateway_tunnel_ForwardStaveTestingPC
Hostname 130.199.20.1
User atishelma
LocalForward 4000 130.199.47.108:4000

# Module testing PCs
Host cleanroom_gateway_tunnel_ForwardBox1
Hostname 130.199.20.1
User atishelma
LocalForward 4000 130.199.47.124:4000

Host cleanroom_gateway_tunnel_ForwardBox2
Hostname 130.199.20.1
User atishelma
LocalForward 4000 130.199.47.110:4000

Host cleanroom_gateway_tunnel_ForwardBox3
Hostname 130.199.20.1
User atishelma
LocalForward 4000 130.199.47.114:4000

# Hybrid burn-in PCs
Host cleanroom_gateway_tunnel_ForwardBurnin
Hostname 130.199.20.1
User atishelma
LocalForward 4000 130.199.47.118:4000

Host cleanroom_gateway_tunnel_ForwardBurnin2
Hostname 130.199.20.1
User atishelma
LocalForward 4000 130.199.47.116:4000
```
</details>

Here, multiple hosts are defined (all are the cleanroom gateway machine) where each one has a LocalForward to either a specific PC in the cleanroom. The LocalForward will later be important for `SSH with graphical interface`.

## SSH without graphical interface

***Before starting, make sure you are connected to the BNL network. This must be done either with an ethernet cable, or by switching on the BNL VPN.***

To ssh into a machine in the testing cleanroom *without* a graphical interface, one needs to first ssh into the gateway machine using their gateway machine account. After sshing in the gateway machine, one can ssh into a machine in the cleanroom. See the following diagram which shows this in diagram form, and an example:

<img width="1000" src="/Images/Remote_Connection/ssh_noGUI.png" alt="SSH no GUI">

So from your PC, you would run (using `<gatewayMachineUsername>` defined from `Obtaining a gateway machine account`):

```
ssh <gatewayMachineUsername>@130.199.20.1 # ssh into the gateway machine
ssh stavetesting@130.199.47.108 # ssh into the stave testing PC
```

here are all of the ssh commands for connecting to testing room PCs after you've sshed into the gateway machine:

```
ssh stavetesting@130.199.47.108 # Stave testing PC
ssh qcbox@130.199.47.124 # Box 1
ssh qcbox@130.199.47.110 # Box 2
ssh qcbox2@130.199.47.114 # Box 3
```

## SSH with graphical interface

***Before starting, make sure you are connected to the BNL network. This must be done either with an ethernet cable, or by switching on the BNL VPN.***

To connect to a machine with a graphical interface, we need to use a `LocalForward` and `NoMachine`, as shown below:

<div style="text-align: center;">
  <img width="1000" src="/Images/Remote_Connection/ssh_withGUI.png" alt="SSH with GUI">
</div>

As an example, to connect to the Box 2 PC with a graphical interface, you first need to ssh into the gateway machine ***with a LocalForward*** to Box 2. You can do this by running on your laptop or PC: `ssh cleanroom_gateway_tunnel_ForwardBox2`. From this point, to connect to a machine and control the screen, we use [NoMachine](https://www.nomachine.com/). First install NoMachine on your PC, then Add a new connection:

<div style="text-align: center;">
<img width="1000" src="/Images/Remote_Connection/NoMachine_home.png" alt="NoMachine home">
</div>

Before connecting to any remote machines, you must first ensure that you are **not running a server on your local PC**. Ensure this by going to: Settings, Server, Status. If a server is running, stop it with "Shut down the server", and make sure your machine does not start a server upon startup - this is because when you later try to connect to a remote machine using the gateway machine as `localhost`, if you have a server running on your machine, this server's IP will be considered the `localhost` and you will just end up trying to connect to your own machine. This is an example of a machine with is NOT running a local server, and you want something like this:

<div style="text-align: center;">
<img width="1000" src="/Images/Remote_Connection/LocalServerNotRunning_Example.png" alt="NoMachine local server not running">
</div>

To continue setting up a connection to box 2, fill in the following parameters in the `Address` tab:

<div style="text-align: center;">
<img width="1000" src="/Images/Remote_Connection/NoMachine_box2_setup.png" alt="NoMachine box2 setup">
</div>

**Note - there is no need to update the parameters in the other tabs. Those should be left as their defaults.** 

After setting the machine address, click "connect", or go back to connections and double click your new box2 connection. After clicking yes or ok to any questions that may come up, you should be lead to the login screen which looks like this:

<div style="text-align: center;">
<img width="1000" src="/Images/Remote_Connection/NoMachine_box2_login.png" alt="NoMachine box2 login">
</div>

The username is `qcbox`. If you don't know the password, ask Abe. After logging in with the proper credentials and accepting / skipping all of the informational boxes shared by NoMachine, you should end up with a window viewing the qcbox desktop, as just another window on your local PC:

<div style="text-align: center;">
<img width="1000" src="/Images/Remote_Connection/NoMachine_window.png" alt="NoMachine window">
</div>

You should be able to click inside the window and control the PC as if you were there. Now you can control module box 2 from the comfort of your own office or living room! Have fun!

To connect to Box 3, the procedure is the same, except one should ssh from their machine with `ssh cleanroom_gateway_tunnel_ForwardBox3`, where they can then ssh just with a terminal via `ssh qcbox2@130.199.47.114`, or add a new NoMachine connection with similar configuration parameters to before:

<div style="text-align: center;">
<img width="1000" src="/Images/Remote_Connection/NoMachine_box3_setup.png" alt="NoMachine box3 setup">
</div>

For this machine, the username is `qcbox2`. 

# Debugging

<details><summary>Debugging details</summary>

## Permission denied (publickey)

If you try sshing and get `Permission denied (publickey)`, ...

```bash
sudo systemctl enable xrdp
sudo systemctl start xrdp
sudo systemctl stop firewalld
sudo firewall-cmd --add-port=3389/tcp --permanent
```

One can then ssh into a PC in the testing clean room, such as the qcbox PC with: `ssh qcbox@130.199.47.110`. Alternatively, one can view the PC browser by:
* Installing `Microsoft Remote Desktop`: ([Apple](https://apps.apple.com/us/app/microsoft-remote-desktop/id1295203466?mt=12))
* Create a new PC with parameters:
  * `PC name: 127.0.0.1`
  * `Friendly name: qcbox`

One should then be able to double click on this new PC within the application, and log in with the qcbox address (qcbox@130.199.47.110) and password (ask Abe if you need it). 

If everything worked, now you should be able to control the qcbox PC from the comfort of your office or home. 

## PC network settings

If things aren't working, it's possible the ethernet connections aren't set up properly. This is a working version for box 2 running CENTOS 8:

<img src="/Images/Remote_Connection/Network_Settings.png" alt="Network settings">
<img src="/Images/Remote_Connection/WallSettings1.png" alt="Wall settings 1">
<img src="/Images/Remote_Connection/WallSettings2.png" alt="Wall settings 2">
<img src="/Images/Remote_Connection/WallSettings3.png" alt="Wall settings 3">

Stave testing PC network settings:

<img src="/Images/Remote_Connection/StavetestingPC_Networkconfig_1.png" alt="Stave testing PC network configuration">

## Extra information

One can ssh into the PC for module box 2 with: `ssh qcbox@130.199.47.110`, if you just want to connect with a terminal and do not require the display of the screen provided by NoMachine.

Old `.ssh/config` for previous stave DCS setup:

```
Host stave_dcs
Hostname 130.199.20.1
User <yourUsername>
LocalForward 3389 130.199.47.108:3389
```

Previous commands to enter after that setup:

```bash
ssh stave_dcs 
your password
ssh stavetesting@130.199.47.108
password: XXX
ssh pi@10.2.252.213
password: 0000
```
</details>