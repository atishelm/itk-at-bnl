# Sensor IVs

Before gluing electronics onto a sensor and declaring it ready for module testing, we must ensure that the sensor does not go into [breakdown](https://en.wikipedia.org/wiki/Avalanche_breakdown), a phenomenon in which a sensor's current shoots up very high. If this happens below the maximum operating voltage, it will not be usable as a piece of our particle detector (Fails Quality Control) because it will not have a low, stable noise. 

To check sensors at different stages for breakdown, we apply a series of voltage values and note down the measured current values - this is called an IV. Below are the instructions for taking an IV at the BNL setup:

## Step-by-step Instructions

1. Navigate to the GUI in Firefox and turn on the chiller (it's in the **Advanced** tab). This requires one terminal tab with the pi running. If the coldjig web gui is not working, follow the below instructions.

<details>


# Starting the coldjig s/w web gui 

The purpose of these instructions are to explain how to start the coldbox s/w GUI from scratch. This is used to monitor and control the various hardware components of the setup. <br>
<br>
1. If not already done, open a firefox window and a konsole terminal window by (double) clicking the icons on the desktop.<br>
<br>
2. On the konsole terminal, type the command `ssh_pi` and press enter. You will then be asked for the password. If you do not know the password, ask Abe or Stefania. <br>
<br>
3. If the ssh into the pi was successful, you should see the terminal prompt start with `pi@raspberrypi:~ $`. If this is the case, you can switch to the software working directory by typing and entering the command `gui`. After this, type and enter these commands in order:

```
pipenv shell
source setenv.sh
./run_BNL.sh
```

If this was successful, you should begin to see some information printout in the window and you can move to step 4.<br>
<br>
If there was already an attempted instance to start the coldjig s/w running before you began, at this stage you may receive the error:

```
OSError: [Errno 98] Address already in use
```

This is because there is a job already using the IP address provided in the configuration file to start an instance of the GUI. You can kill that already existing process in one way by running `ps -a` to list all running processes, and killing the python3 process which is likely the culprit by identifying its "PID" (process ID) from the `ps -a` printout, and running `kill -9 <PID>` to kill the process. After this, try `./run.sh` again to see if you can start the web gui. <br>
<br>
A possibly related problem occurs when the web gui starts but the "OK" message does not print in the terminal and commands in the gui are not executed. To fix this, try clearing the jobs. Run 'jobs' and then 'kill %1' in the pi.<br>
<br>
4. Open a firefox tab and click on the bookmark in the bookmarks bar of your desired webgui: `Coldjig-GUI-QC` or `Coldjig-GUI-Reception`. If this worked properly, you should see a window load the GUI and a Grafana dashboard. <br>
<br>
5. Start the GUI by clicking "Start" in the Control Panel tab. If this worked, you should see additional printout in the konsole where you ran `./run.sh`. If all is working well, the grafana panel should start to update. One can always set the time of the grafana values to be more recent to confirm - e.g. switching absolute time range from "Last 1 hour" to "Last 5 minutes" with the dropdown menu.

</details>


2. Check that you're in `~/Desktop/IV/itsdaq-sw` with the linux command `pwd`. 
3. Note the wafer numbers and their chuck locations in the coldbox.
4. Note the chucks used so you know what channels to use in the next step.
5. Navigate to `DAT/config/power_supplies.json` and edit the 'hv_supplies' field to have the desired number of channels. You can "comment out" a channel by closing the 'hv_supplies' bracket and starting a "comment" field. For example, to do an IV on only the first two chucks:
```
"hv_supplies": [
                {
            "psName": "169.254.227.68:10001",
            "modName": "Module 0",
            "output": 1, 
            "deviceType" : "ISEG",
            "channel" : 0
            },
            {
            "psName": "169.254.227.68:10001",
            "modName": "Module 1",
            "output": 1, 
            "deviceType" : "ISEG",
            "channel" : 1
            }],
"comment": [
                {
            "psName": "169.254.227.68:10001",
            "modName": "Module 2",
            "output": 1, 
            "deviceType" : "ISEG",
            "channel" : 2
        },
                {
            "psName": "169.254.227.68:10001",
            "modName": "Module 3",
            "output": 1, 
            "deviceType" : "ISEG",
            "channel" : 3
        }

        ]
```
To make this easier, there are four different config files saved to the DAT/config folder: power_supplies_1.json, power_supplies_2.json, power_supplies_3.json, and power_supplies_4.json. Depending on how many sensors are in the coldbox, you can copy one of these to the power_supplies.json file, either by using a text editor or linux commands (cp power_supplies_3.json power_supplies.json if you have 3 modules).

6. Navigate to the main folder `~/Desktop/IV/itsdaq-sw/`.
7. Run the program by typing the alias `rid`, or do it the long way by typing `source RUNITSDAQ.sh`. 
8. After the popups come up, go back to the terimnal, press `Enter` and then press the `up arrow` button a few times until you get to the command `HVSupplies[n]->SetLimit(700)` where "n" is the channel of the HV supply that you are using. Run the command by pressing `Enter`. For example, if you only had modules in the first two chucks, you would run `HVSupplies[0]->SetLimit(700)` and `HVSupplies[1]->SetLimit(700)`. This sets that maximum voltage that the HV power supply is allowed to go to. We set this to 700 because when taking an IV, we test our sensors up to 700 volts. (NOTE: previously we set the limit to -700, but the software now takes a positive voltage here)
9. Click on the DCS tab in the "Burst+Data" window and click on either the `Sensor IV (700V)` or `Module (bare) IV scan (700V)` button, depending on the type of test you are doing:
  * Sensor (i.e. minimally populated PCB): `Sensor IV`
  * Sensor with HV Tab attached: `Module (bare) IV scan (700V)`

10. In the resulting popup, enter the appropriate details of the sensor, among others:
  * Sensor setting:
    * Short strip sensor / module (i.e. four columns of strips): `ATLAS18SS`
    * Long strip sensor / module (i.e. two columns of strips): `ATLAS18LS`
  * Temperature 20 degrees C (double check the chuck temperatures on the coldjig GUI), Humidity 0 (double check relative humidity reading on the coldjig GUI).
  * Enter the serial numbers for each of the sensors tested, from left to right in the coldbox (If you leave them as "missing", the output files will NOT be saved!)
  * Enter the wafer numbers for each of the sensors tested, from left to right in the coldbox.
11. Wait for test test to complete, but note the currents in the HV supply, checking whether they are of the order of 10^-7A, around 0.1 microAmperes. 
12. After the test is complete, you should find the `.dat` results at `~/Desktop/IV/itsdaq-sw/DAT/results`.

## Additional notes

When you are entering the serial number into the `wafer_chuck_locations.json`, make sure that:

1. For bare sensors, you use the sensor SN (there is no other option, as there is not yet an associated module SN)
2. For HV-tabbed sensors with IV NOT taken through the HV tab, you use the sensor SN.
3. For HV-tabbed assemblies with an IV taken through the HV tab, there are now referred to as modules, so you should use the module SN.
4. A picture of the sensor IV GUI is below. The current limit is automatically set. iSlope, iLimitAMAC, and Drain Resistors don't do anything for this test.

<img src="/Images/SensorIVGUI.png" alt="IV Scan GUI">

## Potential problems

1. If the pi runs out of space, a temporary solution is clearing the /var/log/ folder to create more space. A more robust solution is described on [this page](https://itk-at-bnl.docs.cern.ch/Known_Issues/No_Space_Left/).
