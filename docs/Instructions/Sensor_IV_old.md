# Sensor IVs

Before gluing electronics onto a sensor and declaring it ready for module testing, we must ensure that the sensor does not go into [breakdown](https://en.wikipedia.org/wiki/Avalanche_breakdown), a phenomenon in which a sensor's current shoots up very high. If this happens below the maximum operating voltage, it will not be usable as a piece of our particle detector (Fails Quality Control) because it will not have a low, stable noise. 

To check sensors at different stages for breakdown, we apply a series of voltage values and note down the measured current values - this is called an IV. Below are the instructions for taking an IV at the BNL setup:

## Step-by-step Instructions

1. Navigate to the GUI in Firefox and turn on the chiller (it's in the **Advanced** tab).
2. Check that you're in `~/Desktop/IV/itsdaq-sw` with the linux command `pwd`. 
3. Note the wafer numbers and their chuck locations in the coldbox.
4. Ensure the sensors are placed starting from the left-most side of the box, and placed in succession to the right with no gaps in between.
5. Navigate to `DAT/config/power_supplies.json` and edit the `numChannels` value to the number of sensors being tested. Keep this value between quotation marks.
6. Navigate to `DAT/config/wafer_chuck_locations.json` and note the wafer numbers and their order. It should go from left to right.
7. Adjust the value of `num_chucks` to the number of sensors being tested. Feel free to edit this file by adding or renaming entries under the "chucks" category, but do not alter the formatting/syntax.
8. Navigate to the main folder `~/Desktop/IV/itsdaq-sw/`.
9. Run the program by typing `source RUNITSDAQ.sh`. 
10. After the popups come up, go back to the terimnal, press `Enter` and then press the `up arrow` button a few times until you get to the command `HVSupplies[0]->SetLimit(-700)`, and then press `Enter`. This sets that maximum voltage that the HV power supply is allowed to go to. We set this to -700 because when taking an IV, we test our sensors up to -700 volts.
11. Click on the DCS tab in the "Burst+Data" window and click on either the `Sensor IV` or `Module IV` button, depending on the type of test you are doing:
  * Sensor (i.e. minimally populated PCB): `Sensor IV`
  * Sensor with HV Tab attached: `Sensor IV`
  * Module, but measuring current through the HV Tab: `Module IV`
12. In the resulting popup, enter the appropriate details of the sensor, among others:
  * Sensor setting:
    * Short strip sensor / module (i.e. four columns of strips): `ATLAS18SS`
    * Long strip sensor / module (i.e. two columns of strips): `ATLAS18LS`
  * Temperature 20 degrees C (double check the chuck temperatures on the coldjig GUI), Humidity 0 (double check relative humidity reading on the coldjig GUI).
  * To work around a current bug: set `iLimit` to `40 microAmps`
13. Wait for test test to complete, but note the currents in the HV supply, checking whether they are of the order of 10^-7A, around 0.1 microAmperes. 
14. After the test is complete, you should find the `.dat` results at `~/Desktop/IV/itsdaq-sw/DAT/results`.

## Additional notes

When you are entering the serial number into the `wafer_chuck_locations.json`, make sure that:

1. For bare sensors, you use the sensor SN (there is no other option, as there is not yet an associated module SN)
2. For HV-tabbed sensors with IV NOT taken through the HV tab, you use the sensor SN.
3. For HV-tabbed assemblies with an IV taken through the HV tab, there are now referred to as modules, so you should use the module SN.

## Potential problems

1. If the pi runs out of space, a temporary solution is clearing the /var/log/ folder to create more space. A more robust solution is described on [this page](https://itk-at-bnl.docs.cern.ch/Known_Issues/No_Space_Left/).
