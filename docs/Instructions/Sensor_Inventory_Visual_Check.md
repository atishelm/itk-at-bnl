# Sensor Inventory (Visual Check)

## Getting setup
**The entire process can take around 30+ minutes. If you do not have time to do the entire visual check DO NOT PROCEED!**


1. Turn on the microscope light
2.  Turn on the camera
   <details>
   <summary>Camera Setup</summary>
      If the camera does not turn on, you might have to change the battery. There is an additional charged battery hanging on the wall to the left of the box.
   </details>
3. Turn on the box light located on the right side of the camera box called "dimmer". Put on max setting by turning the knob.
4. Locate the following material on the benchtop:
   - ethly alcohol
   - Screwdriver
   - Wind Puffer 
   - suction cups 
   - Sharpie
   - 4 screws with red tops
   - Silver screws 
   - square white stickers 
   - paper cloths
   - Vacuum pump pen
   - purple gloves (located on bench outside of clean room) 
   -  tweezers. 
   <details>
      <summary>Image of materials</summary>

   </details>

5. Get a RAL Transport Frame (located on shelf beside microscope).
   <details>
      <summary>Image of RAL Transport Frame</summary>

   </details>

8. Get a FR4 module test frame (located on shelf beside microscope). 
   <details>
      <summary>Image of FR4 module test frame</summary>

   </details>

7. Get the black box that contains the sensors located in the humidty box. **Very Important: Check all sides of the box to look for any important details**
   - **Important details: Read first**
      - Place the box on the table with the label facing towards you
      - Open the box by peeling back the orange tape on the sides and one on the front.  
      - You will notice there are 3 sections. The section furtherest away is section 1 followed by section 2 and section 3.    
      - You must clear the section of sensors in order starting with section 1. Inside each section you will see multiple green envelopes containing sensors. You want to always grab the sensor closest to you. 
         - If you look at the Serial No. for each sensor (starting with the one closest to you) in a section you will notice that the **W###** will increases. We want to keep this order.
            <details>
               <summary>Documentation of sensor arrangement</summary>

             </details>
      - Get one sensor out and put the rest back in the humidity box.
   

8. The entire setup look like the following image below. Your arrangement does not have to match this setup.
   <details>
      <summary>Documentation of sensor arrangement</summary>

   </details>

1. **Important Note: DO NOT TOUCH ANY GOLD PARTS WITH YOUR HANDS! Grab all parts by the green portion using gloves!**
2. Remove the FR4 module test frame (green pcb board) from the box. Place the board on the white cloth where you are able to read the writing on the board (able to read the writing NOT upside down).
   - **DO NOT TOUCH ANY GOLD PARTS**

   - **NOTE: The pins on the testing frame can get stuck in the packaging. Be sure to carefully remove the board without damage**   
      <details>
         <summary>Image of pin stuck in wrapper</summary>

      </details>

3. Check to make sure the board is version 2.2
   - **Do not use version 2.1** 
      <details>
         <summary>Correct board vs. Incorrect Board</summary>

      </details>
5. Write the entire VPX identification number (located on the sensor envelope) on the green pcb board. 
   <details>
         <summary>Additional Information</summary>

      </details>


6. Place a white sticker (about an inch down) on the RAL transport frame. Note the orientation of the frame in the additional information section.
   <details>
         <summary>Additional Information</summary>

      </details>

7. Label the sticker with W and the following numbers (W###) which can be found on the sensor's green envelope
   - **Important Note: Once the sensor is removed from the package we will not be able to properly identify it if everything is not properly labeled**
      <details>
         <summary>Additional Information</summary>

      </details>


8. Unscrew the screws and flip the case over and add the 4 suction cups using the tweezers. **Do not touch the suction cups with your hands!**
   <details>
         <summary>Example</summary>

      </details>

9. **DO NOT TOUCH THE SENSOR WITH YOUR HANDS**
10. Open the sensor packaging  and keep the sensor in between the paper sandwhich 
      - The light side is the aluminum 

      - The dark side is the sensor. **DO NOT TOUCH THE SENSOR.**
         <details>
         <summary>Image of sensor sandwhich</summary>

      </details>

11. Dark side up (sensor side) gently and slowly lift the top paper off. It may stick sometimes just be gentle
14. Using the vacuum pen, place the sensor onto the FR4 testing frame in the correct orientation. (Press the button on the pen to release). 
      <details>
         <summary>Additional Information</summary>
            - Wipe vacuum pen with alcohol if needed
      </details>
12. Place the FR4 testing frame under the microscope.
13. On the bottom left side of the microscope you will see the letter F. It is important that the sensor's letter F orientation matches what is shown on the microscope. The sensor has an F in each corner for a total of 4. Move the sensor into the correct position.

## Visually insepct the sensor under the microscope
1. Write everything down on a notepad initially. Write down any marks/issues and where the marks/issues are located to the best of your ability. **Always try using the wind puffer tool to try blowing any marks away with the tool before indicating in the notepad. Dust will be a common occurance.**
1. Completely check around the boarder of the sensor (top, right side, bottom, left side) for any scracthes, debris, ..etc.
2. Check the center bounding lines (top to bottom) for any scratches debris, ... etc.
3. Check left side and right side for any scratches, debris, ... etc.
4. Using the puffer tool to puff any marks or debris (if the mark does not go away indicate the mark)
<details>
   <summary>Inspecting the board</summary>

</details>

## SS&CS note-book 
1. Using the PC, navigate to the Notepad file "**sensor_SS&LS_PPB_visualinspection**". It should already be open
1. Copy the last line and paste it below.
   - **Change the VPX#**
   - **Change the USB#**
   - **Change the DATE**
   <details>
      <summary>Additional Information</summary>

   </details>

3. Looking at the previous entires for refernces, add in any issues you found (written on your notepad) while inspecting the sensor and its positioning to the best of your abilities. 
   - **DO NOT add "#" in front of the VPX#** unless uploaded to the  BNL uploader program**

## Using the camera
1. The camera program is called **EOS 5DS R.** It will automatically open when you turn the camera on.
2. Press the remote shooting button
      <details>
         <summary>Additional Information</summary>

      </details>
3. Press live shoot. A new screen will pop up allowing you to visually see the testing frame (so you can visually see the plate). 
   <details>
         <summary>Additional Information</summary>

      </details>
4. Place the testing modules with the sensor into the camera box.

3. Line up the center of the sensor with the center line on the camera (as straight as possible).
   <details>
         <summary>Additional Information</summary>

   </details>


4. Press the black button in the top right corner on the **EOS 5DS R** to take the photo. The photo will pop on on the right monitor.
   - Two photos will be generated. 
5. Update the JPG image with the VPX number from the SS&Cs notebook document

## IMPORTANT: ENSURE NUMBERS ARE MATCHING
**IMPORTANT: CROSS REFERENCES ALL NUMBERS:** NUMBER WRITTEN ON THE FR4 TESTING FRAME, THE STICKER ON THE WHITE CASING, THE NUMBER ON THE GREEN ENVELOPE AND THE NUMBER ON SS&CS DOCUMENTATION

1. Once numbers are all confirmed and matching
2. Wipe the inside of the casing with the ethyl alcohol to remove any leftover particles
3. Dobule check that all 4 suction cups are in place
4. Place the FR4 testing frame (with sensor) in white casing
5. Add the spacer and top place cover 
6. Add in the 4 red screens
7. Add in the 4 silver screws
8. Tighten and place the module into the humidity box
9. **Remember to place the black box (with the humidity checker) contained of sensors back into the humdidity box if you are done**
10. Remember to place all trash in the trash can 


## Uploading to the database
Each JPG image taken of the sensor needs be uploaded to the BNL Uploader database

1. The BNL Uploader database is located here: https://bnl-uploader.app.cern.ch/
   - To learn more about the BNL Uploader, please click on BNL Uploader tab on the ITk at BNL website.

2. Enter your two access codes
3. Click on "Test"
4. Click on "Sensor"
5. Click on  "Visual Inspection"
6. Drag the JPG image of the sensor into the dashed box
   - Ensure the VPX#.. is correct on the JPG image
7. Press the upload button
8. The BNL uploader will give you a message that says the image was successfully added. 
9. Put the "#" symbol beside the VPX number in the ""**C:\User\Downloads\sensor_SS&LS_PPB_visualinspection**" notepad file.
10. You have now completed the visual inspection of one sensor. 


## General Problems
1. The BNL uploader can take multiple JPG images at one time. However sometimes you will experience issues with this feature. If you do have issues just submit one image and see if that fixes the issue.
2. "Internal Service Error" The server encountered an internal error and was unable to complete your request. Either the server is overloaded or there is an error in the application.
 - You can't fix this error! You have to check it randomly to see if the bnl uploader is working again. **Be sure that you did not put a "#" beside the sensor's VPX number in the "**sensor_SS&LS_PPB_visualinspection**" notepad file. 
3. ERROR! See logs below... No acceptable file provided 
   - a.) You did not upload a JPG image
   - b.) You are not in the visual inspection upload area. I got this error when I mistakenly clicked on "BNL dropbox" and not "Visual Inspection". Please follow the "Uploading to the database" steps.










