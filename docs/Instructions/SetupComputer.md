# Set up computer

To set up a computer from scratch, start by installing centos8 image on a USB stick, start bios w/ USB. 

To start bios on intel corei9 machines we have at BNL is `f2` at the start.

Make sure partition type is correct otherwise the Centos8 installation won't find disk to partition. 

When partitioning space, can delete all to reclaim for the centos8 installation.

Ask ITD to register the computer so that you can connect to ethernet using the wall. Send an email to `itdhelp@bnl.gov` with the MAC address and room number. Example:

```
Hello,

We would like to register a new computer with MAC address:

AA:AA:AA:AA:AA:AA

in Building 0510, Room 1-260.

Thank you,
<your name>
```

install root 
https://root.cern/install/

e.g. on centos8:

```bash
$ yum install epel-release
$ yum install root
```

make sure ITSDAQ is set up (if you need it). Will need to set include path for c++. 

Follow ITSDAQ build stuff:

https://atlas-strips-itsdaq.web.cern.ch/build_options.html

To connect Nexys to computer:

https://gitlab.cern.ch/atishelm/itk-at-bnl/-/blob/master/Machine-setups/Ethernet_config.png

## Network Configuration 
Here are the configuration for Box1, Box2, Box3 and stave Network:


https://gitlab.cern.ch/atishelm/itk-at-bnl/-/tree/master/docs/Images/Remote_Connection/Network_Configuration


## More information

To give a PCs user access to serial devices so that you don't always have to `chmod` for power supplies, run:

```
# For Fedora
sudo usermod -aG dialout USERNAME


# For Debian
sudo adduser USERNAME dialout


# For Arch
sudo usermod -a -G uucp USERNAME
```

More information: https://www.makeuseof.com/connect-to-serial-consoles-on-linux/
