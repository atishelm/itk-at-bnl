# Stave Testing

The local support structure of the ITk strip modules is called a Stave. One stave hosts 28 total modules, 14 on each side. BNL is going to assemble and test half of the ITk strips barrel, summing to 196 staves. The other half of the ITk strips barrel will be assembled and tested at RAL. This page contains instructions and procedures for how to test staves at BNL.

In order to remotely connect to the stave testing PC, following the instructions in the [Remote connections instruction page](https://itk-at-bnl.docs.cern.ch/Instructions/Remote_Connection/), following the case of `cleanroom_gateway_tunnel_ForwardStaveTestingPC`. 

**NOTE:** Much of this documentation is here for posterity and "just in case". In standard operation, mainly look towards **5. Configuring and Starting ITSDAQ Software**, **6. Testing** and **7. Plotting and Uploading**

## Useful Links

### Links
* [Environmental Grafana](http://localhost:3000/d/avsUXyFSz/environment?orgId=1&from=now-5m&to=now&refresh=5s)
* [DCS Grafana](http://localhost:3000/d/joQfxtNIk/dcs?orgId=1&from=now-5m&to=now&refresh=5s)
* [InfluxDB](http://localhost:8086/orgs/73a10cfb788b8879)
* [Master ISEG](https://169.254.5.113/en/control#&nav.sidebarnavi.en/control/0-0) 
* [Slave ISEG](http://169.254.5.114/en/control#&nav.sidebarnavi.en/control/0-0)
* [Abe's test storage](https://atishelm.web.cern.ch/atishelm/ITk/Stave_Testing/)
* [BNL_ModuleStatus spreadsheet](https://docs.google.com/spreadsheets/d/1PgVL5i0sD9h57ORrsTRfMN7Rfhz91kjiC8jE2fEcbeQ/edit#gid=2146007184)
* [Production Database](https://itkpd-test.unicorncollege.cz/)
* [BNL Uploader](https://bnl-uploader.app.cern.ch/)
* [ITSDAQ](https://gitlab.cern.ch/mbasso/itsdaq-sw)
* [ITSDAQ tutorial](https://indico.cern.ch/event/1223749/#17-itsdaq-tutorial-session)

### Previous Testing Logs
* [Stave 12](https://docs.google.com/document/d/1HvNOQszLahiEBaFp-2_CAZcj7r_GsFgobFiGVA_ymE0/edit?usp=sharing)
* [Stave 14](https://docs.google.com/document/d/1sbp1S_L3dFLezr-86OF5jLDbkZkRC-JVMKiCGw22lpE/edit?usp=sharing)
* [Stave 16](https://docs.google.com/document/d/1M5r69EC6pUd77ARRZjFm2QDSf9bbSNsA3DB26_RCgcw/edit?usp=sharing)


## 1. Set up the connectivity

### EOS cables

Note the orientation of the EOS cables:

<img width="500" src="/Images/Stave_Testing/EOS_Fiber_Cable_Orientation.jpg" alt="Orientation of EOS fiber cables">


### Fibers mapping

The definition of RX and TX is **Stave-Centric**:

- in **RX lines**, light (signals) goes from the DAQ board to the Stave; 
- in **TX lines**, light (signals) goes from the Stave to the DAQ board;

<img width="500" src="/Images/Stave_Testing/mezzanine-fibers.png" alt="Labelling of fibers on the DAQ board">

The DAQ board communicates with the Stave via a fiber box. The fiber box has room for 24 fibers, divided in 12 columns of 2 fibers each. Columns are numbered from 0 to 11 (left to right) and fibers within each column are labelled either A (top) or B (bottom).

<img width="500" src="/Images/Stave_Testing/empty-fiberbox.png" alt="Labelling of fiber box">

#### Stave 6

|           | Tan       | Yellow    | Blue      | Green     |
| ------    | ------    | ------    | ------    | ------    |
| RX        | A11       | B9        | B2        | A2        |
| TX        | A9        | A8        | B11       | B3        |

#### Stave 7

|           | Tan       | Yellow    | Blue      | Green     |
| ------    | ------    | ------    | ------    | ------    |
| RX        | B8        | B9        | A3        | B3        |
| TX        | A9        | A8        | B2        | A2        |

#### Stave 8

Appears to be same as Stave 6, at least for master side.

### HV Lines

!!! ADD SELF CONFIGURING SCRIPT

Correct configuration of the ISEG PSU:

| Variable                      | value          | unit   |
| ------                        | ------         | ------ |
| Control.channelEventMask      | 0              |        |
| Control.voltageBounds         | 0.0000000E+00  |   V    |
| Control.currentBounds         | 0.0000000E+00  |   A    |
| Control.currentRampspeedDown  | 1.200          |   mA/s |
| Control.currentRampspeedUp    | 1.200          |   mA/s |
| Control.currentSet            | 0.2000         |   mA   |
| Control.voltageRampspeedDown  | 10.0           |   V/s  |
| Control.voltageRampspeedUp    | 1.00           |   V/s  |
| Control.voltageSet            | 0.000          |   V    |

### LV Lines

## 2. Set up the DCS system

### Start CPS - DCS Software

Ssh into the raspberry pi:

```bash
ssh_pi
```
ask local stave people for the password.

Then open to CPS:
```bash
gui
```

This will open the CPS GUI:

<img width="500" src="/Images/Stave_Testing/CPS_GUI.png" alt="CPS GUI">

### Start the Grafana interface

Grafana should be running under localhost, port 3000. Click the respective links to open the two Grafana instances and monitor temperature, humidity, current, etc. Open the following two links, if not already open (also available in the bookmarks tool):

[Environment Grafana](http://localhost:3000/d/avsUXyFSz/environment?orgId=1&refresh=5s) \
[DCS grafana (currents and voltages)](http://localhost:3000/d/joQfxtNIk/dcs?orgId=1&var-Datasource=InfluxDB&var-Bucket=itsdaq&var-Measurement=DCS_AMAC&from=now-15m&to=now&refresh=5s)

After starting CPS, you should see the environment grafana begin to populate. It should look something like this:

<img width="1000" src="/Images/Stave_Testing/Environment_Grafana_Example.png" alt="Environment grafana example">

We will not see the DCS grafana populate until later (when we start the ITSDCS loop).

## 3. Set up the hardware

### Chiller

Set temperature of chiller in the Set Temperature box of the CPS GUI, then press Start:

<img width="500" src="/Images/Stave_Testing/CPS_GUI_SetTemp.png" alt="Set temp in CPS GUI">

<img width="500" src="/Images/Stave_Testing/CPS_GUI_StartChiller.png" alt="Start chiller in CPS GUI">

### Booster pump

The titanium piping in the stave core, through which coolant fill flow, has a small cross section and therefore requires a booster pump in order to provide enough pressure for the coolant to flow through the stave. Once the chiller is on, and **only if the chiller is on**, turn on the booster pump by pressing the Start button on the CPS GUI:

<img width="500" src="/Images/Stave_Testing/CPS_GUI_StartBoosterPump.png" alt="Start booster pump in CPS GUI">

OR by connecting to the PiDCS system and running the script start_pump.py. Note, this may not be recognized by the interlock. **Only use this if you know what you're doing!**:

```
cd /home/pi/itk_workdir/dcs
python3 start_pump.py
```

You should expect a pressure in the inlet of about 120 PSI, as seen on the pressure gauge:

<img width="500" src="/Images/Stave_Testing/PressureGauge.png" alt="Pressure gauge">

After testing, you can stop the pump with the corresponding button in the GUI:

<img width="500" src="/Images/Stave_Testing/CPS_GUI_StopBoosterPump.png" alt="Stop pump in CPS GUI">

OR by connecting to the PiDCS system and running the script stop_pump.py:

```
cd /home/pi/itk_workdir/dcs
python3 stop_pump.py
```

### LV Power

Turn on the Low Voltage line for either master, slave, or both by pressing the corresponding "output" buttons:

<img width="500" src="/Images/Stave_Testing/LV_ON.png" alt="LV ON">

If the output does not turn on, the LV could be in Remote mode, only responding to ITSDAQ/remote commands. If that's the case, press the Local button in order to gain local control:

<img width="500" src="/Images/Stave_Testing/LV_Local.png" alt="LV local control">

The current draw should be ~0.86 - 0.87 A per side (LV PSU channel) when the modules are not configured. 

## 4. Flash Genesys Firmware

On the Stave testing PC, if not connected, set up an ethernet connection to the Genesys board:

| Setting     | Value           |
| ------      | ------          |
| IPv4 Method | manual          |
| Address     | 192.168.222.146 |
| Netmask     | 255.255.255.0   |
| Gateway     |                 |

Find connected Genesys board:

```bash
djtgcfg enum
```

you should see an output similar or identical to:

```bash
Found 1 device(s)

Device: Genesys2
    Product Name:   Digilent Genesys 2
    User Name:      Genesys2
    Serial Number:  200300B18E16
```

make sure to note down the serial number.

Flash connected Genesys board using the serial number:

```bash
djtgcfg prog -d SN:200300B18E16 -i 0 -f ../../fw/genesys2_itsdaq_vc662_LPGBT_S4.bit
```

or:
```bash
FPGA_program
```

This may take a few minutes. The message on the Genesys board should change after this step. If this works properly, in the terminal you should see:

```bash
Programming device. Do not touch your board. This may take a few minutes...
Programming succeeded.
```

## 5. Configuring and Starting ITSDAQ Software

In a terminal, you can move to the `itsdaq-sw` folder using the alias `itsdaq`

### Configure st_system_config.dat

The `st_system_config.dat` is used to configure the stave's setup. In general, this is **set and forget.** A full description of the structure of the config file can be found below.

<details><summary>Description of `st_system_config`</summary>

*(The following is copied from an `st_system_config` on the hybrid computer)*

This file defines the mapping for connection to a module.
Lines that don't start "Module", "DAQ" or "Hybrid" are ignored.

After the Module string (in the same line) there are a series of fields.
Depending on the number of numerical fields, these are
interpreted as follows:

```
3 numbers:

id         ID of module in map
ccr        Segment/CCR
RX stream  The stream to receive data on
FileName   Base of filename to find configuration (see below)
DevType    Type of device

8 numbers:

id         ID of module in map
pr         This module is present (usually 1)
ac         Active in scans
DAQ id     ID of DAQ card to use for transmission and reception of data
RX id 0    The stream to receive data on
RX id 1    (-1 to ignore for Star)
RX del 0   Stream delay setting for input data
RX del 1   (ignored for Star, suggest -1)
FileName   Base of filename to find configuration (see below)
DevType    Type of device

18 numbers old style, but used for setting com (aka CCR):

id         ID of module in map
pr         This module is present (usually 1)
ac         Active in scans
LV id      LV number (used for VISA)
LV ch      LV channel (used for VISA)
HV id      HV number (used for VISA)
HV ch      HV channel (used for VISA)
(8-10)     Ignored
ccr_id     Segment/CCR (0 for all, 100 to 115 according to FMC)
(12-13)    Ignored
DAQ id     ID of card to use for data reception
RX id 0    The stream to receive data on
RX id 1    (-1 to ignore for Star)
RX del 0   Stream delay setting for input data
RX del 1   (ignored for Star, suggest -1)
FileName   Base of filename to find configuration (see below)
DevType    Type of device
```

The FileName field indicates the file to be read from. This proceeds in
the following order:

```
    sctvar/config/$filename.det
    sctvar/config/default$id.det
    sctvar/config/default.det
```
 
</details>

You can find the `st_system_config.dat` file in the `itsdaq-sw/DAT/config/` folder. In standard usage, we only ever comment out modules for ITSDAQ to ignore. This is done with a `#` at the front of the line

An example config file used to configure both sides of Stave 16 is below:

<details><summary>Example st_system_config.dat</summary>

*Note- the line breaks separate the CCR (clock control reject) lines*
```
DAQ udp 192.168.222.16 60002,60002
DAQ udp 192.168.222.16 60003,60003

# master
#default_Barrel

#Module    0   1  1   -1 -1 -1 -1  0 0 1 103 1 0   0  0  -1   50  50  JaneDoe0 Barrel   
#Module    1   1  1   -1 -1 -1 -1  0 0 1 103 1 0   0  2  -1   50  50  JaneDoe1 Barrel
#Module    2   1  1   -1 -1 -1 -1  0 0 1 103 1 0   0  4  -1   50  50  JaneDoe2 Barrel     
#Module    3   1  1   -1 -1 -1 -1  0 0 1 103 1 0   0  6  -1   50  50  JaneDoe3 Barrel     

Module    4   1  1   -1 -1 -1 -1  0 0 1 101 1 0   0  8  -1   50  50  JaneDoe4 Barrel 
Module    5   1  1   -1 -1 -1 -1  0 0 1 101 1 0   0  24 -1  50  50   JaneDoe5 Barrel
Module    6   1  1   -1 -1 -1 -1  0 0 1 101 1 0   0  10 -1  50  50   JaneDoe6 Barrel     
Module    7   1  1   -1 -1 -1 -1  0 0 1 101 1 0   0  12 -1  50  50   JaneDoe7 Barrel     

Module    8   1  1   -1 -1 -1 -1  0 0 1 102 1 0   0  14 -1  50  50   JaneDoe8 Barrel    
Module    9   1  1   -1 -1 -1 -1  0 0 1 102 1 0   0  16 -1  50  50   JaneDoe9 Barrel 
Module    10  1  1   -1 -1 -1 -1  0 0 1 102 1 0   0  18 -1  50  50   JaneDoe10 Barrel    
Module    11  1  1   -1 -1 -1 -1  0 0 1 102 1 0   0  20 -1  50  50   JaneDoe11 Barrel    
Module    12  1  1   -1 -1 -1 -1  0 0 1 102 1 0   0  22 -1  50  50   JaneDoe12 Barrel  

Module    13  1  1   -1 -1 -1 -1  0 0 1 100 1 0   0  26 -1  50  50   JaneDoe13 Barrel  
 
#slave 

Module    14   1  1   -1 -1 -1 -1  0 0 1 110 1 0   0  86  -1   50  50  JaneDoe14 Barrel 
Module    15   1  1   -1 -1 -1 -1  0 0 1 110 1 0   0  90  -1   50  50  JaneDoe15 Barrel
Module    16   1  1   -1 -1 -1 -1  0 0 1 110 1 0   0  84  -1   50  50  JaneDoe16 Barrel
Module    17   1  1   -1 -1 -1 -1  0 0 1 110 1 0   0  82  -1   50  50  JaneDoe17 Barrel

Module    18   1  1   -1 -1 -1 -1  0 0 1 109 1 0   0  80  -1   50  50  JaneDoe18 Barrel
Module    19   1  1   -1 -1 -1 -1  0 0 1 109 1 0   0  78 -1  50  50  JaneDoe19 Barrel
Module    20   1  1   -1 -1 -1 -1  0 0 1 109 1 0   0  76 -1  50  50  JaneDoe20 Barrel
Module    21   1  1   -1 -1 -1 -1  0 0 1 109 1 0   0  74 -1  50  50  JaneDoe21 Barrel

Module    22  1  1   -1 -1 -1 -1  0 0 1 108 1 0   0  72 -1  50  50  JaneDoe22 Barrel
Module    23  1  1   -1 -1 -1 -1  0 0 1 108 1 0   0  68 -1  50  50  JaneDoe23 Barrel
Module    24  1  1   -1 -1 -1 -1  0 0 1 108 1 0   0  66 -1  50  50  JaneDoe24 Barrel
Module    25  1  1   -1 -1 -1 -1  0 0 1 108 1 0   0  70 -1  50  50  JaneDoe25 Barrel
Module    26  1  1   -1 -1 -1 -1  0 0 1 108 1 0   0  64 -1  50  50  JaneDoe26 Barrel

Module    27  1  1   -1 -1 -1 -1  0 0 1 111 1 0   0  88 -1  50  50  JaneDoe27 Barrel
```
</details>

### Configure power_supplies.json

The `power_supplies.json` is used to configure the power supplies in use. Like the `st_system_config`, this tends to be **set and forget.**

You can find the `power_supplies.json` file in the `itsdaq-sw/DAT/config/` folder. It lists the LV power supplies and the serial connection they use, as well as the HV power supplies and their IP addresses. You can find more about the HV supplies in the "Setting up HV Power Supplies" page.

We tend to keep a `power_supplies_all.json` in the `config/` folder as a backup. Channels can be removed from the main `power_supplies.json` file as desired.

An example `power_supplies.json` to access all LV and HV segments is shown below.

<details><summary>Example power_supplies.json</summary>
```
{
        "lv_supplies": [
                    {
                    "psName" : "/dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0",
		    "modName": "Module 0-13",
		    "channel" : 1
	 	    },
                    {
                    "psName" : "/dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0",
		    "modName": "Module 14-27",
		    "channel" : 2
	 	    }
	],

	"hv_supplies": [
		{
		    "psName": "169.254.5.113:10001",
		    "deviceType" : 3,
		    "modName": "Module 0-3",
		    "output": 1,
		    "channel" : 0
		},
		{
		    "psName": "169.254.5.113:10001",
		    "deviceType" : 3,
		    "modName": "Module 4-7",
		    "output": 1,
		    "channel" : 1
		},
		{
		    "psName": "169.254.5.113:10001",
		    "deviceType" : 3,
		    "modName": "Module 8-10",
		    "output": 1,
		    "channel" : 2
		},
		{
		    "psName": "169.254.5.113:10001",
		    "deviceType" : 3,
		    "modName": "Module 11-13",
		    "output": 1,
		    "channel" : 3
		},
	
		{
		    "psName": "169.254.5.114:10001",
		    "deviceType" : 3,
		    "modName": "Module 14-17",
		    "output": 1,
		    "channel" : 0
		},
		{
		    "psName": "169.254.5.114:10001",
		    "deviceType" : 3,
		    "modName": "Module 18-21",
		    "output": 1,
		    "channel" : 1
		},
		{
		    "psName": "169.254.5.114:10001",
		    "deviceType" : 3,
		    "modName": "Module 22-24",
		    "output": 1,
		    "channel" : 2
		},
		{
		    "psName": "169.254.5.114:10001",
		    "deviceType" : 3,
		    "modName": "Module 25-27",
		    "output": 1,
		    "channel" : 3
		}
	
	]
}
```
</details>

### Start ITSDAQ

Move to the `itsdaq-sw/` directory by running the command `itsdaq` (this is an alias)

For good housekeeping, run the alias `FPGA_program` to reprogram the FPGA board.

Start a new instance of ITSDAQ by running `rid` (this is an alias for `source RUNITSDAQ.sh`). This will open an ITSDAQ session and configure the AMACs.

Next, we will turn on the low voltage (11V) to the modules. **Make sure the chiller is on before you do this.** 

The LV can be turned on with `DCS → Module LV On` in the ITSDAQ GUI. This will configure the AMACStars, ABCs and HCCs.

### Start Monitoring (ITSDCS)

The communication between ITSDAQ and the InfluxDB database is done via the ITSDCS program. 

In a new terminal window (or the terminal tab labeled `ITSDCS`) run `ram` (this is an alias for `source INFLUX_AMAC.sh`) 

Check monitoring is working properly by looking to the DCS Grafana page. It should populate similarly to the below example:

<img width="1000" src="/Images/Stave_Testing/Stave_DCS_1.png">
<img width="1000" src="/Images/Stave_Testing/Stave_DCS_2.png">

## 6. Testing

### Checklists

**General Checks**

*"Check yourself before you wreck yourself"*

* Before turning off HV...
    * Is HV ramped down to 0?
* Before turning off LV...
    * Is HV turned off?
* Before turning off chiller?
    * Is LV turned off?
    * Is pump turned off?s


**When changing temperatures:**

* [x] LV **on**
* [x] Hybrids **off** (`SC_HybridOff()`)
* [x] HV **off**
* Dry air...
    * [x] **20** if going cold
    * [x] **Maxed** if going warm


**Start of day checklist:**

* [x] Lower dry air flow to 20
* [x] Turn on chiller
* [x] Turn on booster pump (`python3 start_pump.py` in 'PUMP' terminal tab)
* [x] Start ITSDAQ (`rid`)
* [x] Module LV on 
* [x] Hybrids off (`SC_HybridOff()`)
* [x] Start ITSDCS (`ram`)
    * If left ITSDAQ/ITSDCS running, just turn on Module LV
* [x] Check that all grafana plots are being populated


**End of day checklist:**

* [x] Ramp down HV, HV off
* [x] LV off
* [x] Booster pump off (`python3 stop_pump.py` in 'PUMP' terminal tab)
* [x] Chiller off
* [x] Raise dry air flow to max


### General notes

The general testing cycle goes something like this:

> RT IV → RT FullTest → Cold IV → Cold FullTest → Repeat!


At a given temperature, the more specific testing cycle goes like this:

0. Depending on if testing is happening at the start of the day, or in the middle of cycling LV will be in a different stage
    * If LV is fully off, run `DCS → Module LV On` in the ITSDAQ gui. This will turn on hybrids as awell
    * If conducting standard cycling (testing every temperature), LV should already be on for monitoring, and hybrids should be off (`SC_HybridOff()`). To prepare for testing, in ITSDAQ terminal run `SC_HybridOn()`
1. In ITSDAQ gui, run `Diagnostics → FuseIDReadback` and check all ABCs are found
2. **IV Scan:** In the ITSDAQ terminal, run `SC_HybridOff()`, then in the ITSDAQ gui `DCS → Module (AMAC) IV scan (550V)`
    * This will take ~30 minutes
3. If vEnd was not set to -350V, after IV use HV PS web GUI to ramp down to -350V
4. In ITSDAQ terminal, run `SC_HybridOn()`, then in the ITSDAQ gui run `Diagnostics → FuseIDReadback` and check all ABCs are found
5. **Full Test:** In ITSDAQ gui, run `Test → FullTest`
    * This will take ~20 minutes
6. After `FullTest`, use web GUI to ramp down to 0V
7. Turn HV off in HV PS web GUI, then in the ITSDAQ terminal run `SC_HybridOff()`
8. Upload tests (This can also be done during any dead time, e.g. while the full test is running or while thermal cycling)


**Chiller → Stave Temps:**

| Name   | Stave Avg. | Chiller S.P. |
| ----   | ---------- | ------------ |
| RT     | 17.5°C     | 12°C         |
| Cold   | -35°C      | -54°C        |
| Colder | -45°C      | -65°C        |


**HV Channels:**

* Channel 0: Modules 11-13 or 25-27 (180uA)
* Channel 1: Modules 4-7 or 18-21 (230uA)
* Channel 2: Modules 8-10 or 22-24 (180uA)
* Channel 3: Modules 0-3 or 14-17 (230uA)

### Startup/Example Test Walkthrough

Below is a step-by-step narration of starting from zero, and running tests at room temperature and cold temperature:

1. Terminal Setup
    * Open a terminal with 6 tabs
        * **CPS**: Run `ssh_pi` to connect to the raspberry pi. The password is the usual
        * **PUMP**: Run `ssh_pi` to connect to the raspberry pi. The password is the usual
        * **ITSDAQ**: Run `itsdaq` to move to the correct folder
        * **ITSDCS**: Run `itsdaq` to move to the correct folder
        * **SSH**: Run `ssh [user]@lxplus.cern.ch` to connect to lxplus
        * **ETC**: Run `itsdaq` to move to the correct folder.
2. Start up the chiller
    * In the `CPS` terminal tab, run `gui` to start the chiller control and monitoring gui
        * Once open, start the chiller at 12°C (or other RT value)
    * In the `PUMP` terminal tab, change your directory to `~/itk_workdir/dcs`
        * Run `python3 start_pump.py` to start the booster pump
    * Set dry air from the wall to 20
3. Start up ITSDAQ
    * In the `ITSDAQ` terminal tab, run `program_FPGA` to program the FPGA board
    * In the `ITSDAQ` terminal tab, run `rid` to start ITSDAQ
        * *This terminal tab is now an ITSDAQ interface, not `bash`*
4. Start powering up
    * In the ITSDAQ GUI, run `DCS → Module LV On`
        * This will calibrate AMACs etc.
    * Sanity check, in the ITSDAQ gui run `Diagnostics → FuseIDReadback`
        * All ABCs should be found
5. Start monitoring
    * In the `ITSDCS` terminal tab, run `ram` to start monitoring
        * Check grafana to ensure all monitoring is reading properly
6. Run IV
    * In the `ITSDAQ` terminal tab, run `SC_HybridOff()` to turn hybrids off
    * In the ITSDAQ GUI, run `DCS → Module (AMAC) IV scan (550V)`
        * `vEnd` to 500
        * Set 'Stage' to `ON_CORE`
        * Set 'Temperature' and 'Humidity' to appropriate values from Grafana (likely 20 and 0)
        * Put in a useful comment. Something like 'Stave16 RT28'
        * Click OK to start the test (may need to click twice)
    * *The IV will take ~30 minutes*
8. Check IV
    * In `itsdaq-sw/DAT/ps/`, open the .pdf's for the IV scan
        * Check there are no spikes and record any unusual shapes
    * Document the run number and scan number of the IV scan
9. Prepare for FullTest
    * After IV test, use the HV PS web GUI to ramp down HV to 350V
    * Once at 350V, in the `ITSDAQ` terminal tab run `SC_HybridOn()` to turn hybrids back on.
    * Sanity check, in the ITSDAQ gui run `Diagnostics → FuseIDReadback`
10. Run full test
    * In the ITSDAQ GUI, run `Test → FullTest`
    * *The FullTest will take ~20 minutes*
11. Check FullTest
    * In `itsdaq-sw/DAT/ps/`, open the .pdf's for the response curves
        * You'll want the 10 point gain, which is the later of the two sets
        * Check the noise plots on pages 1 and 3 and record any large sections of dead channels or unusual shapes
    * Document the run number and scan number of the 10PG response curve
12. Preparing to change temperature
    * In the HV PS web GUI, ramp down HV to 0V
    * Once at 0V, in the HV PS web GUI, turn HV off
    * In the `ITSDAQ` terminal tab, run `SC_HybridOff()` to turn hybrids off
13. Going cold
    * Using the CPS GUI, set the temperature to the appropriate value
        * See **Chiller → Stave Temps**
        * e.g. -65°C for -45°C stave
    * Wait until the Stave Avg. Temp. reads -35°C or -45°C
    * *Going to -45 will take ~30 minutes*
14. Repeat steps "Run IV" through "Preparing to change temperature"
15. Going warm
    * Set dry air from the wall to the maximum
    * Using the CPS GUI, set the temperature to the appropriate value
        * See **Chiller → Stave Temps**
        * Likely 12°C
    * Wait until the Stave Avg. Temp. reads 17.5
    * *Warming up will take ~25 minutes*
16. Shutting down
    * Turn off LV
        * In the ITSDAQ GUI, run `DCS → Module LV Off`
    * Turn off the pump
        * In the `PUMP` terminal tab, run `python3 stop_pump.py`
    * Turn off the chiller
        * In the CPS GUI, stop the chiller
    * If you didn't already, set dry air from the wall to maximum
    * You can leave ITSDAQ and ITSDCS running so long as HV and LV are off

Bonus step (can be done while waiting for tests or cycling): plot and upload tests according to the "Plotting and Uploading"
    

### AMAC IV Scan

To start an IV scan, in the BurstData GUI, click on DCS and then Module (AMAC) IV Scan (550V):

Most settings can be left as the defaults.

* `vEnd`: 
    * If you will follow the IV scan with a FullTest, set `vEnd` to -350V
    * If you only want the IV scan, set `vEnd` to 0V
    * Sometimes ITSDAQ will timeout due to slow ramping speeds. If this is happening, set `vEnd` to -500V and use the web GUI to ramp down further. 
* Set 'Stage' to `ON_CORE`
* Set 'Temperature' and 'Humidity' to appropriate values from Grafana
* Put in a useful comment. Something like 'Stave16 RT28'

Once ready, click on OK twice to begin the scan

<details><summary>Starting IV Images</summary>

<img width="1000" src="/Images/Stave_Testing/ITSDAQ_IV_Combined_IV.png">  

<img width="500" src="/Images/Stave_Testing/ITSDAQ_GUI_AMACIV.png">  

</details>

You should see the lpGBT and AMACs calibrate (this may take a few minutes), and eventually see the HV channels increasing in voltage. 

During the IV scan, you can track progress in the ITSDAQ readout or in the DCS Grafana. See below for an example Grafana plot of an IV scan up to -550V:

<details><summary>Grafana IV Images</summary>

<img width="1000" src="/Images/Stave_Testing/IV_DCS.png">

</details>

If any modules experience early breakdown by hitting the compliance current on the HV power supply, ITSDAQ will end the IV scan and ramp down the HV.


#### Disabling HVmux

To open the GaNFet of a channel to avoid applying HV to that sensor, run either of the two commands below:

```
AMAC_common_setGANFET(10,0)

AMACStar_writeReg(amacNumber,40,0x0)
```

where `amacNumber` corresponds to the AMAC number. Then to restore the default setting and close the GanFet **leaving DC/DC on**, run:

```
AMACStar_writeReg(amacNumber,40,0x07077700)
```

You can also close the GanFet with DC/DC off with:

```
AMACStar_writeReg(amacNumber,40,0x07070000)
```

### FullTest

To run an ITSDAQ full test, perform the following steps:

1. Ensure LV is on
2. Ensure hybrds are on (`SC_HybridOn()` in ITSDAQ terminal)
3. Set HV to desired voltage for biasing (typically -350V)
4. In the ITSDAQ GUI, select `Test` → `FullTest`

### Shutting Down

Once testing is done, the full shutdown procedure is as follows:

1. Ramp down and shut off HV
    * This can be done in the web GUI
2. Turn off LV
    * In the ITSDAQ GUI, run `DCS → Module LV Off`
3. Turn off the booster pump
    * In the `PUMP` terminal tab, run `python3 stop_pump.py`
4. Turn off the chiller
    * In the CPS GUI, stop the chiller
5. If you didn't already, set dry air from the wall to maximum
6. You can leave ITSDAQ and ITSDCS running so long as HV and LV are off
    * To shut down ITSDAQ, either run `.q` in the `ITSDAQ` terminal tab or run `File → Exit` in the ITSDAQ GUI
    * To shut down ITSDCS, in the `ITSDCS` terminal tab hit `Ctrl+Z` to suspend the process and run `kill %` to kill it. 

## 7. Plotting and Uploading

### Plotting tests

IV plots are automatically generated by ITSDAQ. To generate a plot of combined IVs, run

```
itsdaq
python3 peterSlideScripts/staveIVPlotTitle.py --input "DAT/results/" --output "[output folder]" --runNumber [run#]_[scan#] --sort-sn --title "[test name]: IV"
```

Noise plots are made from within ITSDAQ. First, load the necessary macros by running (you only need to do this once per instance of ITSDAQ)

```
loadmacro("stan_util")
loadmacro("ToolBox")
loadmacro("staveENC_LUT")
```

You can then use the `DrawStave()` command within ITSDAQ to draw noise plots. These tests are saved to the `DAT/log/` folder

```
DrawStave([run#],[scan#]) # Draw 1 test
DrawStave([run#1],[scan#1],[run#2],[scan#2]) # Compare tests
```


### Uploading to Abe's site

Abe has set up a site to store plots of tests, located [here](https://atishelm.web.cern.ch/atishelm/ITk/Stave_Testing/).

On CERN's servers, the site is located at `/eos/user/a/atishelm/www/ITk/Stave_Testing/`. The general structure of the site is as follows:

```
Stave_Testing
    Stave#
        RT1
            IV
            10PG
        Cold1
            IV
            10PG
        RT2
        ...

```

A folder must have the `index.php` file in order to appear on the site. To automate creation of this testing folders, I recommend adding the following to your `.bashrc` in `lxplus`:

```bash
# Move to abes site
alias abe="cd /eos/user/a/atishelm/www/ITk/Stave_Testing/"

# Setup stave test folder
function makestf {
        if [ ! -n "$1" ]; then
                echo "Enter a directory name"
        elif [ -d $1 ]; then
                echo "\`$1' already exists"
        else
                mkdir $1 && cd $1 && mkdir IV 10PG && cp ../index.php . && cp ../convert.py . && cd 
IV/ && cp ../../index.php . && cp ../../convert.py . && cd ../10PG && cp ../../index.php . && cp ../
../convert.py . && cd ..
        fi      
}
```

Use the SCP method to upload files to the site. 

For IV, run (do something similar with the combined plot if you made one)

```
scp ~/Desktop/itsdaq-sw/DAT/ps/*[run#]_[scan#].pdf [lxplus_username]@lxplus.cern.ch:/eos/user/a/atishelm/www/ITk/Stave_Testing/[Stave#]/[test]/IV
```

For noise plots, run (this phrasing only uploads input noise and gain)

```
scp ~/Desktop/itsdaq-sw/DAT/log/[run#]-[scan#]*IN* [lxplus_username]@lxplus.cern.ch:/eos/user/a/atishelm/www/ITk/Stave_Testing/[Stave#]/[test]/10PG 
```

Within lxplus, in the `IV/` and `10PG/` folders, run `python3 convert.py` to convert the `.pdf` files into images (for easier viewing) 

### Uploading tests to the database

*WIP, to be updated*

### Summary Slides

A basic script was made by Peter J. to automagically generate summary slides for all tests for a given stave. It is stored in a gitlab repo [here](https://gitlab.cern.ch/pejacobs/stave-slide-script).

To use this, you must make a `.csv` file of names, run#'s, and scan#'s. An example file for stave16 is given below:

<details><summary>Example .csv</summary>

```
RT1-IV,1164,3
RT10-PG,1167,18
Cold1-IV,1167,31
Cold1-10PG,1167,38
RT2-IV,1167,50
RT2-10PG,1167,57
Cold2-IV,1167,71
Cold2-10PG,1167,78
RT3-IV,1167,90
RT3-10PG,1167,97
Cold3-IV,1169,3
Cold3-10PG,1169,10
RT4-IV,1174,2
RT4-10PG,1174,9
RT-After-5-LV-Off-IV,1177,2
RT-After-5-LV-Off-10PG,1177,9
RT17-IV,1179,2
RT17-10PG,1179,9
Cold17-IV,1179,21
Cold17-10PG,1179,28
RT28-IV,1181,3
RT28-10PG,1181,10
RT29-IV,1300,2
RT29-10PG,1300,9
```

</details>

Then simply use the `staveSlides.py` script with an input folder, output folder, title, and CSV to generate the `.tex` file. As an example, for stave 16 we run:

```
itsdaq
python3 ~/Desktop/itsdaq-sw/peterSlideScripts/staveSlides.py --input ~/Desktop/itsdaq-sw/DAT --output ~/Desktop/itsdaq-sw/peterSlideScripts/Stave16/Slides --title stave16 --csv ~/Desktop/itsdaq-sw/peterSlideScripts/Stave16/stave16.csv```
```

This can be compiled into a PDF by running (within the folder containing the `.tex` file):

```
pdflatex [texFileName].tex
```


## TROUBLESHOOTING

* If *something* isn't working, the general "fix attempts" are as follows:
    * Restart ITSDAQ
    * Restart ITSDAQ and reprogram FPGA
    * Physically power cycle the power supplies

* When trying to start ITSDAQ, you get the error "Failed to read from DAQ board to get initial status, closing connection".

<details><summary>Solution</summary>

- This problem most often occurs because there are too many instances of ITSDAQ running or the DAQ board needs to be powercycled. In most cases, simply power cycling the DAQ board or quitting unwanted ITSDAQ instances is enough to fix it.
- If this is not the case and you continue to get the error, restarting the PC has fixed it in the past.

</details>

* When finishing an IV, ITSDAQ crashes while plotting the IVs. Something like:

```
Generating stack trace...
0x00007f96ee849bf7 in THashList::Remove(TObject*) + 0x57 from /usr/local/root_6.24/root_install/lib/libCore.so
0x00007f96d4f36042 in TRootContextMenu::~TRootContextMenu() + 0x42 from /usr/local/root_6.24/root_install/lib/libGui.so
...
0x00007f96ede3feb0 in <unknown> from /lib64/libc.so.6
0x00007f96ede3ff60 in __libc_start_main at :? from /lib64/libc.so.6
0x0000000000401155 in _start + 0x25 from /usr/local/root_6.24/root_install/bin/root.exe
```

<details><summary>Solution</summary>

Basically, fully power cycle. Unfortunately the data is lost, so you'll need to redo the IV

1. Ramp down and shut down HV
2. Turn off LV
3. Stop ITSDAQ, stop ITSDCS
4. Physically power cycle HV and LV supplies
5. Reprogram FPGA
6. Start ITSDAQ
7. LV on
8. Start ITSDCS
9. Rerun the IV

Note this fix often encounters the bug below, see that for a solution.

</details>

* When trying to start ITSDAQ, it fails to start when loading the LV supplies. Something like:

```
Loading LV supplies (-1)
Check json file: /home/stavetesting/Desktop/itsdaq-sw//DAT/config/power_supplies.json
Loading supplies from JSON file
Create TTi/TInfluxLV from json:
 /dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0 for Module 0-13
open new port for /dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0
TSerialProxy: standard serial device detected
TSerialProxy: Error 13 from open: Permission denied
TTi::TTi (viOpen) for UNKNOWN at resource /dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0 failed with code 0xffffffff (TSerialProxy: IO Error)
```

<details><summary>Solution</summary>

This is a permissions issue with accessing the remote connection. Simply give yourself permission in the ITSDAQ and ITSDCS terminal tabs with

```
sudo usermod -aG dialout $USER
newgrp dialout
```

</details>

* When trying to start ITSDAQ, root crashes before ramping. Something like:

```
TIVScan::getHitIt called for invalid channel
AMAC_IVScan: channel 0 hit compliance at TIVScan::getLastX called for invalid channel
-1V TIVScan::getLastX called for invalid channel
-1uA
```

<details><summary>Solution</summary>

I don't know what causes this! It can be scary, because the crash leaves HV and LV on, but no ITSDAQ. To fix it, do the following:

1. Ramp down (if necessary) and turn off HV.
2. Restart ITSDAQ
3. Turn off LV, then turn LV back on
4. Turn off hybrids (`SC_HybridOff()`)
5. In the HV web GUI, reset current compliances to correct values (180uA and 230uA)
6. Manually turn on HV, ramp to 10V, and ramp back down
7. Turn off HV
8. Restart IV scan

</details>

* It seems like the `Ctrl` key is being held down, and unplugging keybaord doesn't help

<details><summary>Solution</summary>

1. Open Settings
    * Click in the top right, then select settings or
    * Press the Windows™ key and type “settings”
2. Select “Keyboard” from the list on the left-hand side
3. Press the “+” in the list of Input Sources
4. Add an input source, eg “English (United Kingdom)” -> “English (UK)”
5. Switch to the new input source and back to English (US)
6. he problem should be solved
7. Delete the UK source (switching without having just added the new source doesn’t seem to fix the problem)

</details>



## SPECIAL CONFIGURATION CASE

<details>
Before Peter's updates to lpGBTAutoSetup, we had to run the following commands after lpGBTAutoSetup:

```bash
AMACStar_lpGBT_calScan(true)
AMACStar_internalCalibrations()
```

These commands configure the AMAC. However, they are now both run by lpGBTAutoSetup and AMAC_IVScan. If you ever need to recalibrate the AMAC, run these commands.
</details>

## DEPRECATED PROCEDURE
<details><summary>Old way to run ITSDAQ tests (2023)</summary>

Currently, two distinct versions of ITSDAQ are used to configure the stave and run stave tests. This will be fixed in the future.

### Base ITSDAQ (Branch: Master, hash: 1f17ba5ce15edace3569ec90fc59ea6aa43234d0, date: July 12, 2022)

Base ITSDAQ is used to configure the lpGBTs. Set up the base version of ITSDAQ by modifying the .bashrc file at line:

`export SCTDAQ_ROOT=/home/receptionstation/Desktop/PPB_STAVE/itsdaq-sw`

Go to the Base ITSDAQ folder in `/home/receptionstation/Desktop/PPB_STAVE/itsdaq-sw` and run:

```bash 
bash
source RUNITSDAQ.sh
```

From within ITSDAQ, run:

Set up Genesys clock (if needed)
```bash
Matt_I2C_Si57x_Setup(14,0x55,156.25,320)
```

and configure the lpGBTs:

```bash
lpGBTAutoSetup(1,1,1,1,1)
```

The current draw on the LV line should increase to ~4.20 A.

### Test ITSDAQ (Branch: RAL_PARALLEL_HV_RAMP, hash: b6dd355f3406901f8298d771d99a36938ab4c362, date: July 5, 2023)

Test ITSDAQ is used to run IV tests on the stave. Set up the test version of ITSDAQ by modifying the .bashrc file at line:

`export SCTDAQ_ROOT=/home/receptionstation/Desktop/PPB_STAVE/itsdaq_sw_RAL_PARALLEL_HV_RAMP/itsdaq-sw`

Go to the Test ITSDAQ folder in `/home/receptionstation/Desktop/PPB_STAVE/itsdaq_sw_RAL_PARALLEL_HV_RAMP/itsdaq-sw` and run:

```bash
bash
source RUNITSDAQ.sh
```

From within ITSDAQ, run:

```bash
AMACStar_lpGBT_calScan(true)
AMACStar_internalCalibrations()
AutoConfig()
```
</details>


## Links
- [e-Link mapping Master Document](https://docs.google.com/spreadsheets/d/1-JpAoXPVoCOkTll_eeTxb8jB1nlesoTSFm5dG3PJ0lk/edit#gid=0)
- [EOS card Mapping](https://docs.google.com/document/d/1TLCZPOSKnFa9owv92wJIueK_41RfEpLuRgOtXh6C2Ts/edit#heading=h.kszdqke4uy8z)
- [Stave testing related code and files](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/tree/master/Stave-testing?ref_type=heads)
)
