# Thermal cycling

<details>
<summary>Important link for merging and DB upload:</summary>

<a href="https://docs.google.com/document/d/1RZ0dymizI-S9qdP90wGjS5G5gCWJ2mWg0oaagqv3s9k/edit">[Link]</a>

</details>

During the lifetime of the ITk, the detector will be cooled and warmed many times. In order to ensure modules are able to electronically and physically survive these temperature changes, each assembled module must be thermal cycled 10 times, with an electrical test performed at each low and high temperature point. The thermal cycling sequence is as shown below:

<img src="/Images/Thermal_Cycling/TC_Sequence_09April2024.png" alt="TC sequence">

shown in an alternative fashion like so:

<img src="/Images/Thermal_Cycling/TC_Sequence_09April2024_alternateview.png" alt="TC sequence">

## Setup

The BNL coldjig consists of an insulated box with 4 chucks cooled by an external chiller. See below:

<img src="/Images/Thermal_Cycling/Coldjig.png" alt="Entire coldjig setup for TC">

The PC should have a terminal open with 4 tabs: one for the coldjigdev, one for the coldjig, one for the AMAC/PS monitoring loop (ITSDCS) and one for the DAQ loop (ITSDAQ). You will also need to have the coldjig GUI and grafana open in your browser. A good PC setup is pictured below:

<img src="/Images/Thermal_Cycling/TC_PC_setup.png" alt="PC setup for TC">

## Procedure 

As a user, you will ideally have the software set up in the proper way and the GUI already generated. See details below for ensuring the correct software is used.

<details>
<summary>Set up production software</summary>
To ensure you're using the operational branches of the software, run the following commands (these only work on box 3).<br>
<br>
In the Pi:

```
gui
python3 SetupOperationsMode.py
```

On the PC (running ITSDAQ):

```
itsdaq
python3 SetupOperationsMode.py
```
</details>

1) Make sure coldjig GUI is running - requires one terminal on the raspberry pi running. In production mode, should already have this open by default. If not, follow these instructions to produce the GUI from scratch:

<details>
<summary>Starting the coldjig s/w web GUI</summary>

1. If not already done, open a firefox window and a konsole terminal window by (double) clicking the icons on the desktop.<br>
2. On the konsole terminal, type the command `ssh_pi` and press enter. You will then be asked for the password. If you do not know the password, ask Abe, Emily, or Stefania.<br>
3. If the ssh into the pi was successful, you should see the terminal prompt start with `pi@raspberrypi:~ $`. If this is the case, you can switch to the software working directory by typing and entering the command `gui`. After this, type and enter these commands in order:

```
pipenv shell
source run_BNL.sh
```

If this was successful, you should begin to see some information printout in the window and you can move to step 4.<br>
<br>
If there was already an attempted instance to start the coldjig s/w running before you began, at this stage you may receive the error:

```
OSError: [Errno 98] Address already in use
```

This is because there is a job already using the IP address provided in the configuration file to start an instance of the GUI. You can kill that already existing process in one way by running `jobs` to list all killed processes, and killing the python3 process which is likely the culprit. E.g. kill %1, kill %2, ... After this, try `source run_BNL.sh` again to see if you can start the web gui. <br>

4. Open a firefox tab and click on the bookmark `Coldjig-GUI`. If this worked properly, you should see a window load the GUI and a Grafana dashboard. <br>
5. Start the GUI by clicking "Start" in the Control Panel tab. If this worked, you should see additional printout in the konsole where you ran `run_BNL.sh`. If all is working well, the grafana panel should start to update. One can always set the time of the grafana values to be more recent to confirm - e.g. switching absolute time range from "Last 1 hour" to "Last 5 minutes" with the dropdown menu.<br>

</details>

2) Open box, connect up to 4 modules. This includes screwing them into the chucks, and connecting them to the LV/HV, data and AMAC connectors. See an example with four connected modules below:

<img src="/Images/Thermal_Cycling/Coldbox_modules.png" alt="Coldbox modules">

3) Screw grounding screws through the test frame into the chuck (see images below).

<details>

The grounding screws and an appropriate screwdriver should be by the sensor visual inspection setup:

<img src="/Images/GroundingScrewsLocation.jpeg" alt="TC sequence">

The screws can be screwed in the gap on the white spacer, directly into the test frame and chuck:

<img src="/Images/GroundingScrewPlacement.jpeg" alt="TC sequence">

</details>

4) Close box, and using the Advanced tab of the GUI, turn chiller ON. The chiller should automatically be set to its default temperature of +20C. If you have chiller alarm issues, see below.

<details>
<summary>Fix chiller refrigeration alarm</summary>

Sometimes the chiller alarm goes off due to an issue starting up the refrigeration (refrigeration button light off), after a minute or two from starting the chiller. To fix this, perform the following:

1) Press the "remote" button so the green light turns off, allowing you to control the chiller locally.<br>
2) Press the refrigeration button if the light is not turned on.<br>
3) Press the alarm clear button.<br>
4) Press the "remote" button again so we can control the chiller remotely (from the Pi) again.<br>
5) Turn chiller OFF and ON using the GUI, ensure alarm does not reappear<br>

</details>

5) In one of the ITSDAQ tabs, open the `DAT/config/st_system_config.dat` configuration file with your favorite text editor. You should define a `Module` line for each hybrid. See an example file and more information in the details below.

<details>

Notably, the 5th numerical column (s0) corresponds to the module's chuck, where the first chuck is 0 and subsequent chuck's values increase in increments of 8 for long strip modules. You can see an example configuration below:

```
DAQ udp 192.168.222.16 60002,60002
DAQ udp 192.168.222.16 60003,60003

         DETECTOR     MuSTARD              Module
          id  pr  ac   id  s0  s1  d0  d1  Filename                    Device Type
         -------------------------------------------------------------------------
# Four LS modules:
Module    0  1  1   0   0   -1  50  50   JaneDoe0 Barrel
Module    1  1  1   0   8   -1  50  50   JaneDoe1 Barrel 	
Module    2  1  1   0   16  -1  50  50   JaneDoe2 Barrel
```

This configuration file is set up to test three modules, with temporary IDs "JaneDoe0", "JaneDoe1", and "JaneDoe2" from left to right in the coldbox (as their `s0` columns are numbered 0, 8, 16). The string after "JaneDoeN" corresponds to the hybrid configuration files ITSDAQ is going to look for in the `DAT/config` folder. One should ensure, in this example, `DAT/config/default_Barrel.det`. If ITSDAQ cannot find them, it will simply look for 'default.det'.

</details>


6) Ensure all power supplies are defined in `DAT/config/power_supplies.json` - this is required for tracking LV/HV current/voltage in Influx, and controlling the power supplies. See an example file and more information in the details below.

<details>

There are example files in the `DAT/config` folder for each possible number of modules. For example, if you have 3 modules to test as in the above example, you should run:

```
cp DAT/config/power_supplies_three.json DAT/config/power_supplies.json
```

to make power_supplies.json have the correct number of LV and HV channels defined. One can see how `DAT/config/power_supplies_three.json` looks below:


```
{
	"lv_supplies": [
		{
			"psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VBYPX-if00-port0",
			"modName": "Module 0",
			"channel" : 1 
		},
		{
			"psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VBYPX-if00-port0",
			"modName": "Module 1",
			"channel" : 2
		},
		{
			"psName" : "/dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC6GG-if00-port0",
			"modName": "Module 2",
			"channel" : 1
		}
	],
	
	"hv_supplies": [
		{
			"psName": "169.254.21.40:10001",
			"modName": "Module 0",
			"output": 1,
			"deviceType" : "ISEG",
			"channel" : 0
		},
		{
			"psName": "169.254.21.40:10001",
			"modName": "Module 1",
			"output": 1,
			"deviceType" : "ISEG",
			"channel" : 1
		},
		{
			"psName": "169.254.21.40:10001",
			"modName": "Module 2",
			"output": 1,
			"deviceType" : "ISEG",
			"channel" : 2
		}
	]
}
```

</details>


7) Turn on the INSTEK LV power supplies (one required for every two modules), on button located on bottom left of each. Press `OUTPUT` on the INSTEK power supplies in order to supply 11V to the modules. For long strip modules, you should see current values of around 50 mA per channel.

8) If these are not the last modules you tested in this coldbox, start ITSDAQ by running the alias 'rid'. Then run AutoConfig(false,false,false) to configure the ASICs and pull the best parameters for each ABC from the database. You will have to enter your database access codes. Learn more about Autonfig [here](https://itk-at-bnl.docs.cern.ch/Instructions/AutoConfig/). If all the chips are reading and the LV current is around 280 mA, you can quit ITSDAQ.

9) In the `ITSDCS` terminal tab, run the command `ram` (this is an alias for `source INFLUX_AMAC.sh`). This is the AMAC/DCS monitoring loop, which will report AMAC and power supply values. The terminal should show something like:

```
TIME_LAST_COMMAND_RECEIVED 20230524144598
TIME_LAST_COMMAND_SENT     20230524144601
Waiting For new command to execute (11 since received command)
```

Which indicates this instance is now waiting for commands from the coldjig.

10) In the `ITSDAQ` terminal tab, run the command `rdm` (this is an alias for `source INFLUX_DAQ.sh`). This is the DAQ loop. When this completes initialization, you should see the low voltage power supply currents jump to `~280 mA` in each channel as the ASICs are configured. The terminal should show something like:

```
TIME_LAST_COMMAND_RECEIVED 20230524144598
TIME_LAST_COMMAND_SENT     20230524144601
Waiting For new command to execute (11 since received command)
```

Which indicates this instance is now waiting for commands from the coldjig.

11) In the `Advanced` tab of the coldjig GUI, set `Cold temperature` to -40C, `Hot temperature` to +20C, and `Warm-up temperature` to +20C. The ITk is expected to operate at a very cold temperature in order to reduce electronics noise, which is why we want to test modules cold in general, and modules will have to be warmed and cooled multiple times between these temperatures during operation. Going beyond 20C for a warm temperature can cause module bending and cracking.

12) In the `Control Panel` tab of the coldjig GUI, check off each of the chucks which has a module in it, on the left-hand side of the GUI.

13) Press the green `Start TC` button. If this works properly, you should see the GUI terminal begin printing out information about the thermal cycle. One can now track the thermal cycling in the two Grafana dashboards which should be open as tabs in the Firefox browser: `ColdJig` for monitoring temperatures and coldjig/ITSDAQ communication, and `DCS` for tracking the power supplies and module quantities.

See example ColdJig grafana dashboard quantities below - useful for monitoring environmental data:

<img src="/Images/Thermal_Cycling/Grafana_ColdJig.png" alt="Coldjig on Grafana">

TC communication below - this is in particular useful for ensuring the commands between the coldjig, and ITSDCS/ITSDAQ loops are continuing as expected:

<img src="/Images/Thermal_Cycling/Grafana_TC_Communication.png" alt="TC communication">

DCS information below - including LV/HV voltage and current vs. time along with chiller temperature, as well as AMAC current and shunting:

<img src="/Images/Thermal_Cycling/Grafana_DCS.png" alt="Grafana DCS">

<img src="/Images/Thermal_Cycling/Grafana_DCS_2.png" alt="Grafana DCS 2">

After the room temperature tests are complete, you should see the chiller begin to cool to -40C.

14) People are walking in an out of the clean room all day. As they may not all be able to identify that thermal cycling is ongoing, you should tape the "Thermal Cycling" sign on the coldbox handle :D

<!-- Add link to remote connection -->

15) After ensuring things get started properly, including the room temperature IV beginning, you can go to your office and monitor every hour or so via NoMachine. You should check the grafana plots for any strange behavior, and ensure the outputs from the IV and ITSDAQ FullTests (located in `DAT/ps`) look as expected. If there is an issue, you should go to the cleanroom to investigate, and if you should abort the thermal cycle (To stop the test early, kill the ITSDCS and ITSDAQ instances in the terminals (`control + c`, `control + z` a few times), and then click the `Stop TC` button on the Coldjig GUI.), power things down (HV first then LV) and click `Stop TC` in the GUI to stop the TC and ensure the safety of the modules.

16) Assuming the TC runs properly, make sure the HV and LV are powered down and the chiller is off before you leave for the day. Ensure you do not open the box unless the chucks are at room temperature, otherwise moisture from the clean room will enter the box and may freeze on the modules to form frost and eventually water.

17) Upload the results to CERNBox. If you not know how to do so, see the below:

<details>

To stay organized during Production, every production module tested at BNL should have a folder on [Abe's website](https://atishelm.web.cern.ch/atishelm/ITk/Production/). These folders are of the structure [LocalId]_[SerialNumber] so that they are easily searchable. A script to upload all results for a test run of 1 or more modules is in the itk-at-bnl git repository, which is pulled in the same directory as itsdaq-sw on every testing computer. The script is located at /Module-testing/Configs/CernboxUploadProduction.py.<br>
<br>
First, create a folder with the [LocalId]_[SerialNumber] format in Abe's CERNBox Production folder. If you do not have permission to do this, ask Abe to add you lxplus account. Then create a TC subfolder for the module.<br>
<br>
To run the script, navigate to the itk-at-bnl/Module-testing/Configs/ folder and open the CerboxUploadProduction.py file. You should change the last few lines so that you upload under a "TC" folder instead of directly under the module's folder. This keeps things more organized since there are many TC results and we often have room temp results as well that we would like to be easily viewed. Now run the line:<br>
<br>
python3 CernboxUploadProduction.py --username eduden --time '17 Nov 2023 15:00'<br>
<br>
where you change "eduden" to your own lxplus username and the time to the earliest time you want to upload files from. As the script runs, you will first have to enter your database access codes so the script can find the module corresponding to the hybrid SN on electrical test files. Then you will need to enter your lxplus password once for every module you are uploading tests for. If this does not work, ask Emily!

</details>


18) Merge the TC results:

<details>

At the end of the TC, we need to merge the results and upload it to the database.

One should first copy the json files to a dedicated folder than try:
```
python ~/Desktop/itsdaq-sw/contrib/itsdaq-merger/control_panel.py -i ./BNL-39/*NO_PPA.json -o ./merged/ -r 2 -t NO
python ~/Desktop/itsdaq-sw/contrib/itsdaq-merger/control_panel.py -i ./BNL-39/*RESPONSE_CURVE_PPA.json -o ./merged/ -r 2 -t RESPONSE_CURVE
python ~/Desktop/itsdaq-sw/contrib/itsdaq-merger/control_panel.py -i ./BNL-39/*TRIM_PPA.json -o ./merged/ -r 2 -t PEDESTAL_TRIM
python ~/Desktop/itsdaq-sw/contrib/itsdaq-merger/control_panel.py -i ./BNL-39/*_STROBE_DELAY_PPA.json -o ./merged/ -r 2 -t  STROBE_DELAY
```
where `-r 2`   means the ColdJig `RUN_NUMBER` is 2 (Please note it is different from `ITSDAQ` `RUN_NUMBER`).
Now the merge script works for Noise Occupancy, Open Channel Search (WIP), Pedestal Trim,  Strobe Delay, Response Curve.

</details>


Have fun thermal cycling!

Please send feedback on experience and how to improve these instructions to `abraham.tishelman.charny@cern.ch`

Note - more TC instructions, used at Birmingham, can be found [here](https://bilpa.docs.cern.ch/itkstrips/coldjig/thermalcycling/).

<details>
<summary>Troubleshooting</summary>

1. After pressing "Start TC", terminal prints:

```
TTi::Mon (V? viRead) for PST-3202 at resource /dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0 failed with code 0xffffffff (TSerialProxy: IO Error)
```

Solution: Fully turn off HV with switch in the back and turn it back. You may have to turn off LV as well to make sure calibration is all done correctly, bringing you back to step 6.


2. After trying to run 'ram', itsdaq eventually prints:
    Create TTi from json:
    /dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0 for BNL Module 1 LV
    return existing port for /dev/serial/by-id/usb-FTDI_Chipi-X_FT2VC3J3-if00-port0

    *** Break *** segmentation violation

 Either for one of the LV or HV supplies.

    Solution: If the power supply is not the first one, you may need to comment out the extraneous supplies in powersupplies.json.

3. After trying to run 'ram', itsdaq has a problem with loading module configuration:

    st_configure_modules: load configuration for MODULE 0
    st_read_module: failed to open file /home/qcbox/Desktop/itsdaq-sw/DAT/config/SCIPP-PBB_LS-027.det

and so on. 

    Solution: If the configuration file is indeed there, try literally copying and pasting the name into st_system_config.dat, there may be a small character difference that's hard to see.

4. After trying to run 'ram', itsdaq has a problem loading a HV (or LV) supply:

    TSerialProxy: Error 13 from open: Permission denied
    TkHV::Init Could not open resource /dev/serial/by-id/yaddyaddayadda

    Solution: You may need to reset the ports to your power supplies using sudo chmod a+rwx. You can look in history for the correct command to run, or try running the OpenPS.sh file.

5. After staring ITSDAQ, you get a 

    "Failed to read from DAQ board to get initial status, closing connection"

error, preceded by a message about the address already being in use (something like "udp_open:bind failed: Address already in use").

    Solution: You have a process in the background keeping the board busy. Run "ps -a" and look for the process. It might be a root instance. Then do "kill -9 [number1] [number 2] ..." where the numbers are associated with the processes you want to kill.  

6. When trying to start the coldbox GUI by doing "ssh_pi", you get the message:

    "ssh: connect to host 169.254.163.91 port 22: No route to host:

    Solution: It could be that the pi IP address changed, and this will probably not work. However, the problem may also be that the pi is somehow not reading. To check this, change the monitor to show the pi by pressing the leftmost button at the bottom right of the monitor twice. Then press the button next to this until you get to "input Control". Select via the leftmost button. Change the input control from DisplayPort to HDMI (the PC to the pi). Then remove the keyboard/mouse USB from the PC and plug it into the pi. If you can't see anything on the monitor when you click around with the mouse, the pi is not responding. You can turn off the pi and turn it back on by unplugging and replugging the white USBC cord on the leftmost port of the pi. You should now be able to click around and see the pi respond.

    Remember to plug the mouse/keyboard back into the PC and change the input control back!

If you have setup an ssh into the cleanroom gateway machine, you should be able to monitor the grafana panels from your office. You should then monitor the grafana dashboards - an example of the main one is shown below (for a case of 7 thermal cycles, with cold shunting, where a module went into breakdown after about 4 cycles):

<img src="/Images/Thermal_Cycling/22May2023_7_TCs_withShunting.png " alt="Coldjig dashboard">

And the `DCS` dashboard, where one can see the current increasing for the module in Chuck 2, both from the HV PS unit and from the AMAC (this is why it's crucial to set up power supply monitoring before starting to thermal cycle):

<img src="/Images/Thermal_Cycling/22May2023_7_TCs_withShunting_VoltageAndCurrent.png" alt="DCS dashboard">

</details>

Command to send influx command to ITSDAQ via terminal:

```
./bin/influx_command --sender COLDJIG --receiver ITSDAQ --setup BNL --command HV_ON_NOIV
```

# Example of "perfect" cycle

See below images from a set of 10 thermal cycles applied to 4 production modules, considered "perfect" as all of the desired steps ran, and none of the modules exhibited issues. One can use this as a reference of a "good" TC while monitoring their new TCs:

## Temperature vs time

<img src="/Images/Thermal_Cycling/Coldbox_temperatures_vs_time.png" alt="Coldbox temperatures vs. time">

## LV quantities vs. time

<img src="/Images/Thermal_Cycling/LV_voltage_vs_time.png" alt="LV voltage vs. time">
<img src="/Images/Thermal_Cycling/LV_current_vs_time.png" alt="LV current vs. time">

## HV quantities vs. time

<img src="/Images/Thermal_Cycling/HV_voltage_vs_time.png" alt="HV voltage vs. time">
<img src="/Images/Thermal_Cycling/HV_current_vs_time.png" alt="HV current vs. time">

## Module quantities vs. time

<img src="/Images/Thermal_Cycling/AMAC_current_vs_time.png" alt="AMAC current vs. time">
<img src="/Images/Thermal_Cycling/shunting_vs_time.png" alt="Shunting vs. time">
