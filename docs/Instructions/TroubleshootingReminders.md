# Troubleshooting Reminders

Sometimes we make stupid mistakes. Sometimes we forget a simple step in a process we do every day. Sometimes we forgets to press "on". This is a list of things to ask yourself before asking someone else.

1. Are you working in the correct branch?

2. Did you press "Start" on the coldjig GUI before trying to turn the chiller on?

3. Are you SURE you edited the config files (powersupplies.json and st_system_config.json) correctly?

## Trouble with Grafana

4. Are the LV and HV supplies on?

5. Did you start the AMAC monitoring and DAQ loops?
