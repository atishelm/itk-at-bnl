# Upload electrical test results to the database

The electrical tests outputted by ITSDAQ need a few modifications before the database will accept them. To make this modifications, I like to put all the jsons I want to upload in their own folder, and then run a script to change the filename and add the hybrid SN to each to each file. Navigate to

'/home/qcbox/Desktop/itk-at-bnl/Module-testing/Configs'

Run the script "MakeFilesDBready.py by doing:

'python3 MakeFilesDBready.py --inDir /home/qcbox2/Desktop/itsdaq-sw/DAT/upload/'

where you substitute the inDir you put the jsons you want to upload. You will need to enter your access codes. Check where the script puts the new, correctly formatted files and navigate to that directory in your file viewer. Then open your browser and go to the bnl uploader:

https://bnl-uploader.app.cern.ch/login?next=%2F

Log in with your access codes and then navigate to Tests>Modules>Electrical Tests. Drag the jsons you want to upload into the box, click upload, and hope it works!

Contact Emily if you have problems.
