# Influx permissions errors

**When trying to start the coldjig software on the pi, if you receive an error like this**

```
Message: 'Starting the coldjiglib failed!:'
Arguments: (ApiException('unauthorized access'),)
```

Somehow, you are not able to access influx to push data from the pi. In at least one instance, this has been solvable by creating a new influx token, implying that perhaps the old token expired or stopped working. Try these steps:

1. Go to localhost:8086 in the browser to view the influx UI

2. Select option "API Tokens"

3. Clone one of the tokens and copy it

4. Paste the token into configs /home/pi/Desktop/ColdboxWorkspace/coldjig_workspace/US_Barrel/configs/bnl_influx.ini AND into grafana under Home>Connections>Data sources>InfluxDB>InfluxDBDetails>Token

5. Save and rerun the run_BNL.sh file