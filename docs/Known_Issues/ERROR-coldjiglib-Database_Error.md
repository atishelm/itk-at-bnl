# ERROR - coldjiglib - Database Error
```

ERROR - coldjiglib - Database Error: (500)
Reason: Internal Server Error
HTTP response headers: HTTPHeaderDict({'Content-Type': 'application/json; charset=utf-8', 'X-Influxdb-Build': 'OSS', 'X-Influxdb-Version': 'v2.7.4', 'X-Platform-Error-Code': 'internal error', 'Date': 'Tue, 30 Jul 2024 15:15:06 GMT', 'Content-Length': '215'})
HTTP response body: {"code":"internal error","message":"unexpected error writing points to database: engine: error writing WAL entry: write /var/lib/influxdb/engine/wal/2fbe4362b2891340/autogen/111/_00001.wal: no space left on device"}
```
If you see the above error, first make sure the Pi and computer does have storage. 

Once confirmed, just restart the influx on the PC (not the Pi). PC is where the influx is running. Pi is only using a influx client ot send data to influxdb.

To restart use the following command.
```
su

sudo service influxdb restart
```