# ITSDAQ failed to read from DAQ board

**If you receive an error from ITSDAQ like this:**

```
# Initialising HSIO... 
st_hsio_init: Using daq_id 0 from 2 
Init HSIO interface Split ports: 60002 and 60002 
Creating UDP socket to 192.168.222.16 ports: 60002 to 60002 
Address: 
IP address: 192.168.222.16 
Have udp socket handle: 6 udp_open: bind 
failed: Address already in use Can't create socket due to error: HsioException: 
Failed to bind udp socket Type: udp params: '192.168.222.16' '60002,60002' 
Failed to open interface to HSIO Failed to read from DAQ board to get initial status, closing connection HSIO not initialised
```
It means that the program is attempting to bind a UDP socket to a specific IP address and port (192.168.222.16:60002), but it fails because the address is already in use. Here are steps to resolve this:

1. _Identify the process using the port_: First, you need to identify which process is currently using the port 60002 on IP address 192.168.222.16. You can use the _lsof_ command to find out which process is using the port. For example:

```
sudo lsof -i :60002
```
2. _Stop the conflicting process_: Once you identify the process using the port, you can either stop that process if it's not necessary or configure your program to bind to a different port. 

```
sudo kill <PID>
```
If the process doesn't stop after sending a regular termination signal, you can try a stronger signal by using:
```
sudo kill -9 <PID>
```

**If you receive an error from ITSDAQ like this:**

```
Read of status block failed!
Failed to read from DAQ board to get initial status, closing connection
```

It means there is an issue reading from your DAQ board (for example, a NEXYS). Sometimes, a fix for this is to power cycle the DAQ board with the physical switch on the board. For a given NEXYS, it can be seen by the red light here:

![NEXYS_Button](Documents/Images/NEXYS_Button.jpg)
<br />
<br />
<br />
For reference, here is what a full NEXYS board may look like: 
<br />
<br />
<br />
![NEXYS_Full](Documents/Images/NEXYS_Full.jpg)
<br />
<br />
<br />
And when connected to an FMC-0514-DP:
<br />
<br />
<br />
![NEXYS_with_FMC-0514-DP](Documents/Images/NEXYS_with_FMC-0514-DP.jpg)
