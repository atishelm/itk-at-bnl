# LV does not load in ITSDAQ

**If you receive an error from ITSDAQ like this**

```
Loading LV supplies (-1)
Extend var directory with /
Check json file: /home/stavetesting/Desktop/itsdaq-sw//DAT/config/power_supplies.json
Loading supplies from JSON file
Create TTi from json:
 /dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0 for Module 0-13
open new port for /dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0
TTi::TTi (*IDN? viRead) for UNKNOWN at resource /dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0 failed with code 0xfffffffe (TSerialProxy: Timeout Error)
```


It is because ITSDAQ cannot load the LV power supply. There are a few reasons this can happen, some of them very simple.

1. The LV supply you are trying to load is turned off. Turn it on!

2. You have added too many LV supplies to your power_supplies.json file. Make sure you actually want to load all of the LV channels in power_supplies.json!

3. You need to again give permissions to the PC to look at the relevant LV supply port. This is a simple fix. Just run

```
sudo chmod a+rwx /dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0
```

Replacing the path with the relevant port given in your error message.