# Missing INFLUX data

Is your data not displayed in grafana? Does this make you sad? In at least one case, we were able to find it and write it to the influx bucket being viewed in grafana. Here's how:

1. Find the missing data. it might be somewhere like '~/.influxdbv2/engine/data/fb57c73e71b21dca in root'.

2. Export it using the inspect influx command, which is already explained under the "Migrating influx data" header in the [AlmaLinux9 computer instruction page](https://itk-at-bnl.docs.cern.ch/Instructions/AlmaLinux/). For example:

```
su

influxd inspect export-lp --bucket-id fb57c73e71b21dca --engine-path ~/.influxdbv2/engine --output-path export.lp
```

3. Write it to the current bucket with 

```
influx write --bucket [current bucket] --file export.lp --org BNL
```