# No gas flow

Sometimes when running the coldjig S/W, there may be an issue due to no reading from the gas_flow. This has been fixed by simply turning the LV INSTEK supplies, HV PS on and off and restarting the coldbox gui. 
