# Debugging and known issues

The purpose of this directory is define the workflow for debugging and to keep track of known issues and log their solutions, in case the issue comes up again, so that the user can solve it. 

This is the protocol for debugging issues with electrical testing at BNL:

<div style="text-align: center;">
<img width="750" src="/Images/Known_Issues/Debugging_workflow.png" alt="Debugging workflow">
</div>

## Known issues

The list of known issues can be found by clicking the drag down menu on the left under `Known issues`. To update an existing issue or create a new issue, one needs to have `Maintainer` access on the [GitLab repository](https://gitlab.cern.ch/atishelm/itk-at-bnl) containing the files to create this page. If you do not see your name [here](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/project_members), you will not be able to update issues or create new ones. If you require access, please email Abe (`abraham.tishelman.charny@cern.ch`) saying that you need Maintainer access for the `itk-at-bnl` repository in order to update or create a new issue.

### Update an existing issue

After ensuring you have access rights as described above, one can update an existing issue by going to its page and clicking the edit icon on the top right, for example:

<div style="text-align: center;">
<img width="750" src="/Images/Known_Issues/Update_KnownIssue_1.png" alt="Update known issue">
</div>

This will bring you to the markdown file containing the page's content. One can choose to edit that file directly on GitLab via the `Edit` -> `Edit single file` options:

<div style="text-align: center;">
<img width="750" src="/Images/Known_Issues/Update_KnownIssue_2.png" alt="Update known issue">
</div>

After editing the file, one simply needs to commit to update the repository:

<div style="text-align: center;">
<img width="750" src="/Images/Known_Issues/Update_KnownIssue_3.png" alt="Update known issue">
</div>

This will start the pipeline for the GitLab mkdocs pages continuous intergration - a very short check which is performed to update the website itself. If it works correctly, you should see a checkmark after a few minutes alongside your commit:

<div style="text-align: center;">
<img width="750" src="/Images/Known_Issues/Update_KnownIssue_4.png" alt="Update known issue">
</div>

After seeing the check mark which indicates the CI is complete, you can check the page you updated and refresh it a few times to see if it has been updated. Sometimes this takes a few extra minutes, or you may need to go to the [homepage](https://itk-at-bnl.docs.cern.ch/) of the website and then navigate to the page you've updated.

### Creating a new issue

After ensuring you have access rights as described above, one can create a new issue by first creating a new markdown file in the folder: `docs/Known_Issues/`, for example `docs/Known_Issues/New_Issue.md` (but please use a meaningful name), then one has to update the `mkdocs.yml` file: [[link]](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/blob/master/mkdocs.yml?ref_type=heads), adding the path to your new markdown file in the `Known issues` portion of the navigation.