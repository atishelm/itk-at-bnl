When modules are assembled at BNL, after they are wirebonded, we need to perform the following tests:

1. Take a high-res picture of the module with the box and upload it to the BNL dropbox via the uploader. The process here is the same as when doing sensor visual inspection.

2.  Follow [these instructions](https://itk-at-bnl.docs.cern.ch/Instructions/IV_Curve/) to take an IV via the AMAC (unless the module is not bonded, in which case take an IV with the power supply) at 20 C and -40C. Note that you can currently use Peter's branch to do this simultaneously and the page will be updated once his MR is in master.

3. Follow [these instructions](https://itk-at-bnl.docs.cern.ch/Instructions/Reception_Test/) to perform an electrical test at 20 C.

4. Follow [these instructions](https://itk-at-bnl.docs.cern.ch/Instructions/Thermal_Cycling/) to thermal cycle the module. You should inspect the module before and after thermal cycling.

Remember to upload each test to the database! It is not necessary to upload cold AMAC IVs.
