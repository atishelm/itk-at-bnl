# Plans

The purpose of this page is to keep track of plans.

## Stave 12

This is the plan for Stave 12:

[[Download word document]](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/raw/master/Documents/Plans/stave_12_assembly_and_test_plan_web.docx?ref_type=heads)

[[Link to Google Doc]](https://docs.google.com/document/d/1lDm5vwiyKzcO_-zhfuyIMvq7_UHf6Tke/edit?usp=sharing&ouid=107517669516518144450&rtpof=true&sd=true)

<iframe src="https://docs.google.com/document/d/e/2PACX-1vSmmVb-QD4-OOz0M6D_vDAWxDAf19kRlQGGjKO7U3ahmXdPc6LPWCPMkGlZUSFFcg/pub?embedded=true" style="border: 1" width="1050" height="1400" frameborder="1" scrolling="yes"></iframe>
