# Spreadsheet emails

Do you enjoy constantly checking spreadsheets for updates? Wouldn't it be more efficient if the spreadsheet tells you when it was updated? This has been implemented at BNL for a few tasks including: Emailing technicians when a hybrid or module is ready to be wirebonded, emailing module testers when modules are ready to be tested. See instructions below:

1. In a google spreadsheet, on the top go to "Extentions", "Apps Script". 
2. Create a new project. 
3. Inside the project, create two files, using these as examples: [Code.gs](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/blob/71cddc21a1746cef257fb5ee981a2b5a05d6706e/Spreadsheet_Emails/Code.gs), [EmailMessages.gs](https://gitlab.cern.ch/atishelm/itk-at-bnl/-/blob/71cddc21a1746cef257fb5ee981a2b5a05d6706e/Spreadsheet_Emails/EmailMessages.gs). 
4. Run yourself from within the apps script IDE with the `Run` button. This might ask for permissions, which you must grant. 
5. Create a new trigger to enable your code to run, ensuring to again grant permissions. See below an example trigger configuration:

<div style="text-align: center;">
<img width="750" src="/Images/example_trigger.png" alt="example trigger">
</div>

If set up properly, `Code.gs` runs every time a cell is edited in the spreadsheet it's linked to, and checks if one of the cases of interest has occured, and sends an email with the appropriate subject, addresses, and body as defined in `EmailMessages.gs`. 
